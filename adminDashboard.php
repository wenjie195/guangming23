<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $liveDetails = getLiveShare($conn);
// $subDetails = getSubShare($conn);

// $liveDetails = getLiveShare($conn," WHERE status != 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE status != 'Available' AND type = '1' ");

$liveDetails = getLiveShare($conn," WHERE status != 'Delete' ");
$subDetails = getSubShare($conn," WHERE status != 'Delete' ");

// $allUser = getUser($conn," WHERE user_type = '1' ");
$allLive = getUser($conn," WHERE user_type = '1' ");
// $allLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminDashboard.php" />
<meta property="og:title" content="Admin Dashboard | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Admin Dashboard | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminDashboard.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Dashboard</h2>
    

	<div class="clear"></div>
    <div class="scroll-div margin-top30">

        <!--<h3 class="green-text h1-title">Broadcasting Settings</h3>-->        
        <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Current Status</th>
                        <th>Teaser Video</th>
                        <th>All Video </th>
                        <!-- <th>Company Details</th> -->
                        <th>Project Logo</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allLive)
                    {
                        for($cnt = 0;$cnt < count($allLive) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allLive[$cnt]->getUsername();?></td>
                                <td><?php echo $allLive[$cnt]->getEmail();?></td>
                                <td><?php echo $allLive[$cnt]->getBroadcastLive();?></td>

                                <td>
                                    <!-- <form action="editBroadcastDetails.php" method="POST" class="hover1"> -->
                                    <form action="adminEditUserTeaserVideo.php" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="data_uid" value="<?php echo $allLive[$cnt]->getUid();?>">
                                            Edit
                                        </button>
                                    </form> 
                                </td>

                                <td>
                                    <!-- <form action="adminViewBroadcastDetails.php" method="POST" class="hover1"> -->
                                    <form action="adminViewBroadcastDetails.php?id=<?php echo $allLive[$cnt]->getUid();?>" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="user_uid" value="<?php echo $allLive[$cnt]->getUid();?>">
                                            View 
                                        </button>
                                    </form> 
                                </td>

                                <!-- <td>
                                    <form action="adminEditUser.php" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="user_uid" value="<?php //echo $allLive[$cnt]->getUid();?>">
                                            Edit
                                        </button>
                                    </form> 
                                </td> -->

                                <td>
                                    <!-- <form action="editUserLogo.php" method="POST" class="hover1"> -->
                                    <!-- <form action="adminViewUserLogo.php" method="POST" class="hover1"> -->
                                    <form action="adminViewUserLogo.php?id=<?php echo $allLive[$cnt]->getUid();?>" method="POST" class="hover1">
                                        <button class="clean action-button" type="submit" name="user_uid" value="<?php echo $allLive[$cnt]->getUid();?>">
                                            View
                                        </button>
                                    </form> 
                                </td>

                                <td>
                                    <?php
                                        $liveStatus = $allLive[$cnt]->getBroadcastLive();
                                        if($liveStatus != 'Available')
                                        {
                                        ?>
                                            <form method="POST" action="utilities/startBroadcastFunction.php" class="hover1">
                                                <button class="clean action-button" type="submit" name="data_uid" value="<?php echo $allLive[$cnt]->getUid();?>">
                                                    Show
                                                </button>
                                            </form>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <form method="POST" action="utilities/stopBroadcastFunction.php" class="hover1">
                                                <button class="clean action-button" type="submit" name="data_uid" value="<?php echo $allLive[$cnt]->getUid();?>">
                                                    Hide
                                                </button>
                                            </form>
                                        <?php
                                        }

                                    ?>
                                </td>

                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>    

</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>