<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/addNewUser.php" />
<meta property="og:title" content="Add New User | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Add New User | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/addNewUser.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

	<h2 class="h1-title">Add New User</h2> 
	<div class="clear"></div>
        <form action="utilities/addNewUserFunction.php" method="POST">

            <div class="dual-input">
                <p class="input-top-text">Name</p>
                <input class="aidex-input clean" type="text" placeholder="Name" id="register_fullname" name="register_fullname" required>        
            </div> 

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Email</p>
                <input class="aidex-input clean"  type="email" placeholder="Email" id="register_email_user" name="register_email_user" required>        
            </div> 

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Contact</p>
                <input class="aidex-input clean"  type="text" placeholder="Contact" id="register_phone" name="register_phone" required>  
            </div>  

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Account Type</p>
                <select class="aidex-input clean" type="text" id="register_account_type" name="register_account_type" required>
                    <option value="" name=" ">PLEASE SELECT ACCOUNT TYPE</option>
                    <option value="0" name="0">Admin</option>
                    <option value="1" name="1">User</option>
                </select> 
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="register_password" name="register_password" required>
                     <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">  				</div>
            </div>
            
            <div class="dual-input second-dual-input">
                <p class="input-top-text">Retype Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>  
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            	</div>
            </div>

            <div class="clear"></div>

            <button class="clean-button clean login-btn pink-button" name="submit">Create Account</button>
        </form>
       
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>