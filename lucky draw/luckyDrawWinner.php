<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Winner.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

// $registrationDetails = getWinner($conn);
$registrationDetails = getWinner($conn, "ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/luckyDrawWinner.php" />
<meta property="og:title" content="Lucky Draw Winner | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Lucky Draw Winner   | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/luckyDrawWinner.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 overflow"><img src="img/desktop-banner.jpg" class="width100 desktop-banner"><img src="img/mobile-banner.jpg" class="width100 mobile-banner"></div>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="index-dual-title text-center"><b>Congratulations 恭贺以下幸运儿</b></h2>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">
  
        <table class="table-css">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Prize</th>
                        <th>Time</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($registrationDetails)
                    {
                        for($cnt = 0;$cnt < count($registrationDetails) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $registrationDetails[$cnt]->getName();?></td>
                                <td><?php echo $registrationDetails[$cnt]->getContact();?></td>
                                <!-- <td><?php //echo $registrationDetails[$cnt]->getPrize();?></td> -->

                                <td>
                                <?php
                                    $prize = $registrationDetails[$cnt]->getPrize();
                                    if($prize == 'VIVO 手机')
                                    {
                                    ?>

                                        <mark><?php echo $registrationDetails[$cnt]->getPrize();?></mark>
                                    
                                    <?php
                                    }
                                    else
                                    {
                                    ?>

                                        <?php echo $registrationDetails[$cnt]->getPrize();?>

                                    <?php
                                    }
                                ?>
                                </td>

                                <td><?php echo $registrationDetails[$cnt]->getDatetime();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>                                 
                </tbody>
        </table>

    </div>    

</div>

<style>
mark {
  background-color: yellow;
  color: black;
}
</style>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>