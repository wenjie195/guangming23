<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$userUid = $_SESSION['user_uid'];
$picLinkUid = $_SESSION['pic_link'];

$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	// $imageName = $userUid.time() . '.png';
	$imageName = time() . '.png';

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		if($imageName)
		{
		array_push($tableName,"image_two");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
		}

		array_push($tableValue,$picLinkUid,$userUid);
		$stringType .=  "ss";
		$uploadPetsImageOne = updateDynamicData($conn,"image"," WHERE uid = ? AND user_uid = ? ",$tableName,$tableValue,$stringType);
		// unset($_SESSION['user_uid']);
		// unset($_SESSION['pic_link']);
		if($uploadPetsImageOne)
		{		
		?>	
			<div>
				<img src=uploads/<?php echo $imageName  ?> />
			</div>	
			<h4><?php echo "Project Logo Added !!"; ?></h4>

			<form action="utilities/addProjectLinkTwoFunction.php" method="POST">
				<div class="width100 overflow text-center">     

					<div class="dual-input">
						<p class="input-top-text">Project Reference Link</p>
						<input class="aidex-input clean" type="text" placeholder="Project Reference Link" id="register_link" name="register_link" required>        
					</div>

					<div class="clear">  </div>

					<button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
				</div>

				<div class="clear">  </div>
			</form>

			<!-- <a href="adminDashboard.php">
				<div class="width100 overflow text-center">     
					<button class="clean-button clean login-btn pink-button">Complete</button>
				</div>
				<div class="clear">  </div>
			</a> -->

			<div class="clear">  </div>
		<?php
		}
		else
		{
			echo "fail";
		}
	}
}
?>