<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://gmvec.com/adminEditUserTeaserVideo.php" />-->
<meta property="og:title" content="Home Video | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Home Video  | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://gmvec.com/adminEditUserTeaserVideo.php" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">


<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <!-- <h2 class="h1-title">Home Video</h2> -->
    <!-- <h2 class="h1-title">User Main Video</h2> -->
    <h2 class="h1-title">User Teaser Video</h2>
        <div class="clear"></div>
      

        <form method="POST" action="utilities/editUserBroadcastFunction.php" enctype="multipart/form-data">

            <?php
            if(isset($_POST['data_uid']))
            {
                $conn = connDB();
                // $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['data_uid']),"i");
                $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['data_uid']),"s");
            ?>

                <div class="dual-input">
                    <p class="input-top-text">Platform</p>
                    <select class="aidex-input clean" type="text" name="update_platform" id="update_platform" required>
                        <option value="">Please Select A Platform</option>
                        <?php
                        if($userDetails[0]->getPlatform() == '')
                        {
                        ?>
                            <option selected>Please Select a Platform</option>
                            <?php
                            for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($platformDetails) ; $cnt++){
                                if ($userDetails[0]->getPlatform() == $platformDetails[$cnt]->getPlatformType())
                                {
                                ?>
                                    <option selected value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                        <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                        <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select> 

                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Link</p>
                    <!-- <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getLink();?>" name="update_link" id="update_link" required>        -->
                    <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getLink();?>" name="update_link" id="update_link">     
                </div>

                <div class="clear"></div>
                
                <!-- <div class="width100">
                    <p class="input-top-text">If You using Zoon as Platform, please attact with an image</p>
                    <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" /></p>
                </div> -->

                <div class="width100">
                    <p class="input-top-text">Temporarily Background Image</p>
                    <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" /></p>
                </div>

                <input type="hidden" value="<?php echo $userDetails[0]->getUid();?>" name="user_uid" id="user_uid" required> 

            <?php
            }
            ?>
    	
            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>