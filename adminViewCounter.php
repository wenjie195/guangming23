<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Counter.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$presetValue = getCounter($conn);
// $counterValue = $presetValue[0]->getAmount();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminViewCounter.php" />
<meta property="og:title" content="Admin (Counter Value) | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Admin (Counter Value)) | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminViewCounter.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Counter Value</h2>
        <div class="clear"></div>
    <!-- <form method="POST" action="utilities/editGoogleMeetFunction.php"> -->
    <form method="POST" action="utilities/editCounterFunction.php">

        <div class="dual-input">
            <p class="input-top-text">Current Counter Value</p>
            <input class="aidex-input clean" type="text" value="<?php echo $presetValue[0]->getAmount();?>" name="counter_value" id="counter_value" required>       
        </div>
        
        <input type="hidden" value="<?php echo $presetValue[0]->getID();?>" name="counter_id" id="counter_id" readonly> 

        <div class="clear"></div>  

        <div class="width100 overflow text-center">     
            <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
        </div>

    </form>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>