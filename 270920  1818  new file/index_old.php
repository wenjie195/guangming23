<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $countryList = getCountries($conn);

// $total_visitors=mysqli_num_rows($result);
// if($total_visitors < 1)
// // if($total_visitors != "")
// // if($total_visitors)
// {
//     $query=" INSERT INTO pageview(userip) VALUES ('$visitor_ip') ";
//     $result = mysqli_query($conn,$query);
// }

// $query="SELECT * FROM pageview";
// $result = mysqli_query($conn,$query);

// // if(!$result)
// // {
// //     die("error".$query);
// // }

// $total_visitors=mysqli_num_rows($result);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/index.php" />
<link rel="canonical" href="https://gmvec.com/index.php" />
<meta property="og:title" content="光明線上產業展 Guang Ming Properties E-Fair" />
<title>光明線上產業展 Guang Ming Virtual Property Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="vh100div2 overflow">
	<img src="img/exhibition.jpg" class="exhibition-img width100 desktop-banner1">
	<img src="img/exhibitiona.jpg" class="exhibition-img width100 mobile-banner1">
	<div class="two-button-div">
		<a href="registration2527.php"><div class="pink-button exhibition-btn">Register for Lucky Draw</div></a><br>
		<a href="index2.php"><div class="pink-button exhibition-btn exhibition-btn2">Go to Exhibition Hall</div></a>
	</div>
</div>
<style>
	.footer-div{
	display: none;}	
</style>
<?php include 'js.php'; ?>
</body>
</html>