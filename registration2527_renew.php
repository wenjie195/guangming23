<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/States.php';
// require_once dirname(__FILE__) . '/classes/Subshare.php';
// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allUser = getUser($conn," WHERE user_type = '1' ");
// $platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$allState = getStates($conn);
$allCountries = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/registration2527.php" />
<meta property="og:title" content="Lucky Draw Registration | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Lucky Draw Registration | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/registration2527.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 overflow"><img src="img/desktop-banner.jpg" class="width100 desktop-banner"><img src="img/mobile-banner.jpg" class="width100 mobile-banner"></div>
<!-- <div class="width100 same-padding overflow top-bg"> -->
<div class="width100 same-padding overflow top-bg min-height-footer-only">
	<!--<div class="width100 overflow text-center">
		<img src="img/luckydraw.gif" class="middle-lucky" alt="Lucky Draw" title="Lucky Draw">
	</div>-->
	<h1 class="title-h1 text-center landing-title-h1 black-text">馬上註冊贏取獎品<br>Register now to win prizes</h1>	
    <form action="utilities/registerLuckyDrawFunction.php" method="POST"> 
                <input class="three-div-width1 index-input clean" type="text" placeholder="Name 名字" id="register_name" name="register_name" required>
           

          
                <input class="three-div-width2 index-input clean" type="number" placeholder="Contact 联络号码" id="register_contact" name="register_contact" required>
          
                <input class="three-div-width3 index-input clean" type="email" placeholder="Email 电邮" id="register_email" name="register_email"  required>
				<div class="clear"></div>
				<!--<p class="middle-p-with-line">以下填写1个即可/Fill in 1 only</p>
				<div class="clear"></div>-->
				<div class="width100 overflow height-space"></div>
				<div class="clear"></div>
				<div class="dual-input">
                    <p class="input-top-text"><b>Your Location</b></p>
                    <select class="width100 index-input clean" id="countryselector" required>
                        <!-- <option>Select a State</option> -->
						<option>Select Your Location</option>
						<option value="malaysia1">Malaysia</option>
						<option value="oversea1">Oversea</option>
                    </select>		
				</div>
		
                <div class="dual-input second-dual-input country-div" id="malaysia1" style="display: none;">
                    <p class="input-top-text"><b>Malaysia (State)</b></p>
                    <select class="width100 index-input clean" id="register_state" name="register_state">
                    <!-- <select class="width100 index-input clean" id="register_state" name="register_state" required> -->
                        <!-- <option>Select a State</option> -->
						<option></option>
                        <?php
                        for ($cnt=0; $cnt <count($allState) ; $cnt++)
                        {
                        ?>
                            <option value="<?php echo $allState[$cnt]->getStateName(); ?>">
                                <?php echo $allState[$cnt]->getStateName();?>
                            </option>
                        <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="dual-input second-dual-input country-div" id="oversea1" style="display: none;">
                    <p class="input-top-text"><b>Oversea</b></p>
                    <select class="width100 index-input clean" id="register_location" name="register_location">
                        <!-- <option>Select a Location</option> -->
                        <option></option>
                        <?php
                        for ($cntAA=0; $cntAA <count($allCountries) ; $cntAA++)
                        {
                        ?>
                            <option value="<?php echo $allCountries[$cntAA]->getEnName(); ?>">
                                <?php echo $allCountries[$cntAA]->getEnName();?>
                            </option>
                        <?php
                        }
                        ?>
                    </select>  
                </div>
                
                <div class="clear"></div>
				<div class="width100 text-center">
					<button class="clean-button clean register-button pink-button" name="submit">Register 注册</button>
					
				</div>

    </form>

</div>

<div class="clear"></div>
<!-- <a href="luckyDrawWinner.php" id="lucky-draw"><img src="img/luckydraw.gif" class="absolute-chat opacity-hover big-size" title="Lucky Draw" alt="Lucky Draw"></a> -->


<a href="luckyDrawWinner.php" id="lucky-draw"><img src="img/ldresult.gif" class="absolute-chat opacity-hover big-size" title="Lucky Draw" alt="Lucky Draw"></a>
<?php include 'js.php'; ?>

</body>
</html>