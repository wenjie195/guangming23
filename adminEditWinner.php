<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
// require_once dirname(__FILE__) . '/classes/Platform.php';
// require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Winner.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allUser = getUser($conn," WHERE user_type = '1' ");
// $platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminAddWinner.php" />
<meta property="og:title" content="Add Lucky Draw Winner | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Add Lucky Draw Winner | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminAddWinner.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Lucky Draw Winner</h2> 

    <div class="clear"></div>

    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getWinner($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <form action="utilities/editWinnerFunction.php" method="POST"> 

            <div class="dual-input">
                <p class="input-top-text">Name</p>
                <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getName();?>" placeholder="Name" name="update_name" id="update_name" required>       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Contact</p>
                <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getContact();?>" placeholder="Contact" name="update_contact" id="update_contact" required>       
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Prize</p>
                <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getPrize();?>" placeholder="Prize" name="update_prize" id="update_prize" required>    
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Date</p>
                <input class="aidex-input clean" type="text" value="<?php echo $userDetails[0]->getDatetime();?>" placeholder="Date" name="update_date" id="update_date" >       
            </div>

            <div class="clear"></div>

            <input class="aidex-input clean" type="hidden" value="<?php echo $_POST['user_uid'];?>" name="winner_uid" id="winner_uid" readonly> 

            <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>