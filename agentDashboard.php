<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $subDetails = getSubShare($conn," WHERE status != 'Delete' ");

$shareDetails = getSharing($conn, "WHERE user_uid =? AND status != 'Delete'",array("user_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/agentDashboard.php" />
<meta property="og:title" content="Agent Dashboard | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Agent Dashboard | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/agentDashboard.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'agentHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Agent Dashboard</h2>
    
	<div class="clear"></div>

    <div class="scroll-div margin-top30">

    <div class="scroll-div margin-top30">                    
                    <table class="table-css">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Booth/Department Name (Line 1)</th>
                                    <th>Booth/Department Name (Line 2)</th>
                                    <!-- <th>Host</th> -->
                                    <th>Name</th>
                                    <th>Zoom Meeting Link</th>
                                    <th>Password</th>
                                    <th>Whatsapp</th>
                                    <th>Phone</th>
       
                                    <th>Action</th>
                                    <th>Edit Zoom Details</th>
              
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($shareDetails)
                            {
                                for($cntBB = 0;$cntBB < count($shareDetails) ;$cntBB++)
                                {
                                ?>    
                                    <tr>
                                        <td><?php echo ($cntBB+1)?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getTitle();?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getTitleTwo();?></td>
                                        <!-- <td><?php //echo $shareDetails[$cntBB]->getHost();?></td> -->
                                        <td><?php echo $shareDetails[$cntBB]->getRemark();?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getLink();?></td>

                                        <td><?php echo $shareDetails[$cntBB]->getOffline();?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getWhatsapp();?></td>
                                        <td><?php echo $shareDetails[$cntBB]->getPhone();?></td>


                                        <?php
                                            $status = $shareDetails[$cntBB]->getStatus();
                                            if($status == 'Available')
                                            {
                                            ?>
                                                <td>
                                                    <!-- <form method="POST" action="utilities/stopSharingFunction.php" class="hover1"> -->
                                                    <form method="POST" action="utilities/agentStopSharingFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                            Stop
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            elseif($status == 'Stop' || $status == 'Pending')
                                            {
                                            ?>
                                                <td>
                                                    <!-- <form method="POST" action="utilities/startSharingFunction.php" class="hover1"> -->
                                                    <form method="POST" action="utilities/agentStartSharingFunction.php" class="hover1">
                                                        <button class="clean action-button" type="submit" name="subdata_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                            Start
                                                        </button>
                                                    </form>
                                                </td>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                                <td>    </td>
                                            <?php
                                            }
                                        ?>

                                        <td>
                                            <form action="agentEditLink.php" method="POST" class="hover1">
                                                <button class="clean action-button" type="submit" name="subdata_uid" value="<?php echo $shareDetails[$cntBB]->getUid();?>">
                                                    Edit
                                                </button>
                                            </form> 
                                        </td>
                                        
                                    </tr>
                                <?php
                                }
                            }
                            ?>  
                            </tbody>
                    </table>
                </div>

    </div>    

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>