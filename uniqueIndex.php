<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
    <?php
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
    $link = "https";
    else
    $link = "http";

    // Here append the common URL characters.
    $link .= "://";

    // Append the host(domain name, ip) to the URL.
    $link .= $_SERVER['HTTP_HOST'];

    // Append the requested resource location to the URL
    $link .= $_SERVER['REQUEST_URI'];


    if(isset($_GET['id']))
    {
        $referUidLink = $_GET['id'];
    }
    else
    {
        $referUidLink = "";
    }
    ?>

<?php
$conn = connDB();
$liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
// $title = $liveDetails[0]->getUsername(); 
if($liveDetails)
{
for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
{
?>

<?php 
    $devName = $liveDetails[$cnt]->getUsername();
    if($devName == 'Ewein Properties')
    {
        $devDisplayName = 'Ewein City Of Dreams';
    }
    else
    {
        $devDisplayName = $devName;
    }
?>

<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/uniqueIndex.php" />
<link rel="canonical" href="https://gmvec.com/uniqueIndex.php" />
<meta property="og:title" content="<?php echo $devDisplayName;?> | 光明線上產業展 Guang Ming Properties E-Fair" />
<title><?php echo $devDisplayName;?> | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="<?php echo $devDisplayName;?>, 光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">

<?php
}
?>
<?php
}
?>

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow min-height">

    <?php
    $conn = connDB();
    if($referUidLink == '632372877da43ab6f7d8af2a73f2e0b3')
    {
        // echo "This is Mah Sing";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <img src="img/GM_AD_OP.jpg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    elseif($referUidLink == 'e4bbb370d994a76f41c90a0b0d749ad8')
    {
        // echo "This is COD";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <img src="img/COD-Pop-up.jpg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    elseif($referUidLink == 'b3691c35cea1dfed6274227f67744e80')
    {
        // echo "This is Hunza";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <img src="img/Hunza_pop.jpg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    elseif($referUidLink == 'e4f4785e35b9623455e78794eac15511')
    {
        // echo "This is TW";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <img src="img/tw_pop.jpeg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    elseif($referUidLink == '4881214b8b8b37a7c1c5f26da2b3f64f')
    {
        // echo "This is Berjaya";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <img src="img/berjaya-new-pop.jpg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    elseif($referUidLink == 'c22aa05c66ccf407b9172c26c9907358')
    {
        // echo "This is Aspen";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <!-- <img src="img/aspenpop2309.jpg" class="width100"> -->
                  <img src="img/aspenpop2409.jpg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    elseif($referUidLink == '9619819c11170ebf2f4785ca71e9d4bf')
    {
        // echo "This is Taman Jadi";
        ?>
			<div id="mahsing-modal" class="modal-css mahsing-modal-css">
			<div class="modal-content-css login-modal-content mahsing-modal-content">
				<span class="close-css close-mahsing">&times;</span>
				<div class="clear"></div>
		  
		   <div class="big-white-div">
				
				  <img src="img/tmnjadipop2309.jpg" class="width100">

					<button class="clean-button clean login-btn pink-button close-mahsing" name="login">Close</button>
				</form>
              
		 </div>


			</div>

		</div>
           
        <?php
    }
    else
    {}
    ?>

    <div class="width100 overflow margin-top30 first-div-margin">    
        
        <div class="width100 top-video-div overflow">
        <div class="left-video-div">
            <?php
            $conn = connDB();
            $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
            // $title = $liveDetails[0]->getUsername(); 
            $tempImgId = $referUidLink;
            if($liveDetails)
            {
                for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
                {
                ?>   
                    <?php 
                        $platfrom =  $liveDetails[$cnt]->getPlatform();
                        if($platfrom == 'Youtube')
                        {
                        ?>
                            
                                <iframe class="youtube-top-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                        <?php
                        }
                        elseif($platfrom == 'Facebook')
                        {
                        ?>
                            
                            <iframe  class="youtube-top-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

                        <?php
                        }
                        // else
                        // {   }
                        // else
                        elseif($platfrom == 'Temporarily')
                        {
                        ?> 

                            <div class="youtube-top-iframe" id="<?php echo "styleaa".$tempImgId.$cnt;?>"></div>
                        
                            <style>
                                /* .staff-1{ */
                                #<?php echo "styleaa".$tempImgId.$cnt;?>
                                {
                                    background-image:url("uploads/<?php echo $liveDetails[$cnt]->getLink();?>");
                                    background-size:cover;
                                    background-position:top;
                                }
                            </style>

                        <?php
                        }
                    ?>
                <?php
                }
                ?>
            <?php
            }
            ?>
			</div>
            <div class="right-project-div">

            <p class="logo-link-p">Click the logo for more information</p>

            <?php
            if(isset($referUidLink))
            $conn = connDB();
            // $projectLogo = getImage($conn,"WHERE user_uid = ? ", array("user_uid") ,array($referUidLink),"s");
            $projectLogo = getImage($conn," WHERE user_uid = ? AND type = 1 ", array("user_uid") ,array($referUidLink),"s");
            if($projectLogo)
            {
                for($cntAA = 0;$cntAA < count($projectLogo) ;$cntAA++)
                {
                ?>
                    
                        <a href="<?php echo $projectLogo[$cntAA]->getLinkOne();?>" target="_blank"><img src="uploads/<?php echo $projectLogo[$cntAA]->getImageOne();?>" class="project-logo opacity-hover"></a>
            
                        <?php 
                            $imgTwo = $projectLogo[$cntAA]->getImageTwo();
                            if($imgTwo != "")
                            {
                            ?>
                                <a href="<?php echo $projectLogo[$cntAA]->getLinkTwo();?>" target="_blank"><img src="uploads/<?php echo $projectLogo[$cntAA]->getImageTwo();?>" class="project-logo opacity-hover "></a>
                            <?php
                            }
                            else
                            {}
                        ?>

                    
                <?php
                }
                ?>
            <?php
            }
            ?>
				</div>
        </div>
    </div>

    <div class="clear"></div>
      <?php
    $conn = connDB();
    if($referUidLink == '4881214b8b8b37a7c1c5f26da2b3f64f')
    {
        // Berjaya
    ?>
        <p class="logo-link-p3">我們的專業線上銷售專員，為您提供產業諮詢服務，歡迎點擊通過WhatsApp 線上交流<br>Click on our WhatsApp for more informations and enquiries, our sales specialists are ready to consult you.</p>
    <?php
    }
    elseif($referUidLink == 'e4bbb370d994a76f41c90a0b0d749ad8')
    {
        // COD
    ?>
        <p class="logo-link-p3">我們的專業線上銷售專員，為您提供產業諮詢服務，歡迎點擊通過WhatsApp 線上交流<br>Click on our WhatsApp for more informations and enquiries, our sales specialists are ready to consult you.</p>
    <?php
    }
    else
    {
    ?>
        <p class="logo-link-p3">我們的專業線上銷售專員，為您提供產業諮詢服務，歡迎點擊通過Zoom/WhatsApp 線上交流。<br>
        Click on our Zoom/WhatsApp for more informations and enquiries, our sales specialists are ready to consult you.</p>
    <?php
    }
    ?>
	<div class="clear"></div>
    <div class="width100 overflow">
    	<div class="left-video-container-div overflow">
        <?php
        $conn = connDB();
        $subDetails = getSubShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        if($subDetails)
        {
            for($cntAA = 0;$cntAA < count($subDetails) ;$cntAA++)
            {
            ?>

                <?php 
                    $platfrom =  $subDetails[$cntAA]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                    
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/<?php echo $subDetails[$cntAA]->getLink();?>?&playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                                <?php
                                $conn = connDB();
                                if($referUidLink == '632372877da43ab6f7d8af2a73f2e0b3')
                                {
                                    // echo "This is Mah Sing";
                                    echo $subDetails[$cntAA]->getTitle();
                                }
                                else
                                {   }
                                ?>

                        </div>
                   
                    <?php
                    }
                    elseif($platfrom == 'Facebook')
                    {
                    ?>
                    
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $subDetails[$cntAA]->getLink();?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                        </div>
                    
                    <?php
                    }
                    // else
                    // {}

                    // else
                    elseif($platfrom == 'Temporarily')
                    {
                    ?>



                            <div class="four-div overflow">

                                <a href="<?php echo $subDetails[$cntAA]->getLink();?>" target="_blank">

                                <div class="four-div-iframe" id="<?php echo "styleyst".$subDetails.$cntAA;?>">
                                

                                
                                </div>
    
                                <style>
                                    /* .staff-1{ */
                                    #<?php echo "styleyst".$subDetails.$cntAA;?>
                                    {
                                        background-image:url("uploads/<?php echo $subDetails[$cntAA]->getFileTwo();?>");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>

                                <?php 
                                    $extrafile = $subDetails[$cntAA]->getFile();
                                    $extraRemarkFiles = $subDetails[$cntAA]->getRemark();
                                    // if($extrafile != "")
                                    if($extrafile != "" && $extraRemarkFiles != "")
                                    {
                                    ?>
                                        <p class="gold-text four-div-p"><b><a href="<?php echo $subDetails[$cntAA]->getRemark();?>" class="blue-to-orange" target="_blank">360°</a> | <a href="uploadsFiles/<?php echo $subDetails[$cntAA]->getFile();?>" class="blue-to-orange" target="_blank">Live Tour</a></b></p>
                                    <?php
                                    }
                                    elseif($extraRemarkFiles != "")
                                    {
                                    ?>

                                        <p class="gold-text four-div-p text-center"><a href="<?php echo $subDetails[$cntAA]->getRemark();?>" class="blue-to-orange" target="_blank">360°</a></b></p>
                                    <?php
                                    }
                                    elseif($extrafile != "")
                                    {
                                    ?>
                                        <p class="gold-text four-div-p"><b><a href="uploadsFiles/<?php echo $subDetails[$cntAA]->getFile();?>" class="blue-to-orange" target="_blank">Live Tour</a></b></p>

                                    <?php
                                    }
                                    else
                                    {   }
                                ?>
                            </a>     
                            </div>

                    <?php
                    }
                ?>

            <?php
            }
            ?>
        <?php
        }
        ?>
 		</div>
    
	<div class="left-video-container-div overflow">
		
        <?php
        $conn = connDB();
        // $sharingDetails = getSharing($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        $sharingDetails = getSharing($conn,"WHERE user_uid = ? AND status != 'Delete' ", array("user_uid") ,array($referUidLink),"s");

        if($sharingDetails)
        {
            for($cntBB = 0;$cntBB < count($sharingDetails) ;$cntBB++)
            {
                $agentStatus = $sharingDetails[$cntBB]->getStatus();
                $checkTitle = $sharingDetails[$cntBB]->getTitle();
                if($agentStatus == 'Available')
                {
                ?>
                
                    <div class="staff-div-css overflow">   
                        <div class="four-div-iframe" id="<?php echo "style".$sharingDetails[$cntBB]->getId();?>"></div>
                        <p class="gold-text four-div-p text-overflow">
                        <b><?php echo $sharingDetails[$cntBB]->getRemark();?></b>
                        <br>
                        <?php echo $sharingDetails[$cntBB]->getTitle();?> &nbsp;
                        <br>
                        <?php echo $sharingDetails[$cntBB]->getTitleTwo();?> &nbsp;
                        </p>
                        <a href="https://api.whatsapp.com/send?phone=6<?php echo $sharingDetails[$cntBB]->getWhatsapp();?>" target="_blank">
                            <img src="img/whatsapp.png" class="contact-icon opacity-hover">
                        </a>

                        <a href="tel:+<?php echo $sharingDetails[$cntBB]->getPhone();?>" ><img src="img/call.png" class="contact-icon opacity-hover"></a>

                        <?php
                        if($referUidLink == '4881214b8b8b37a7c1c5f26da2b3f64f')
                        {}
                        elseif($referUidLink == 'e4bbb370d994a76f41c90a0b0d749ad8')
                        {}
                        else
                        {
                        ?>
                            <a href="<?php echo $sharingDetails[$cntBB]->getLink();?>" target="_blank">
                                <img src="img/zoom.png" class="contact-icon opacity-hover">
                                <!-- <img src="img/googlemeet.png" class="contact-icon opacity-hover"> -->
                            </a>
                        <?php
                        }
                        ?>                               
                    </div>  
               
                    <style>
                        /* .staff-1{ */
                        #<?php echo "style".$sharingDetails[$cntBB]->getId();?>
                        {
                            background-image:url("userProfilePic/<?php echo $sharingDetails[$cntBB]->getFile();?>");
                            background-size:cover;
                            background-position:top;
                        }
                    </style>
                
                <?php
                }
                elseif($agentStatus == 'Stop')
                // elseif($agentStatus == 'Stop' && $agentStatus != 'Robot Agent')
                {
                ?>
                
                    <div class="staff-div-css overflow">   
                        <a href="https://api.whatsapp.com/send?phone=6<?php echo $sharingDetails[$cntBB]->getPlatform();?>" target="_blank">
                            <div class="four-div-iframe blur" id="<?php echo "style".$sharingDetails[$cntBB]->getId();?>"></div>
                            <p class="gold-text four-div-p text-overflow">
                            <b><?php echo $sharingDetails[$cntBB]->getRemark();?></b>
                            <br>
                            <?php echo $sharingDetails[$cntBB]->getTitle();?> &nbsp;
                            <br>
                            <?php echo $sharingDetails[$cntBB]->getTitleTwo();?> &nbsp;
                            </p>
                            <!-- <a href="https://api.whatsapp.com/send?phone=6<?php //echo $sharingDetails[$cntBB]->getWhatsapp();?>" target="_blank">
                                <img src="img/whatsapp.png" class="contact-icon opacity-hover">
                            </a> -->

                            <!-- <a href="tel:+<?php //echo $sharingDetails[$cntBB]->getPhone();?>" ><img src="img/call.png" class="contact-icon opacity-hover"></a> -->

                            <?php
                            if($referUidLink == '4881214b8b8b37a7c1c5f26da2b3f64f')
                            {}
                            elseif($referUidLink == 'e4bbb370d994a76f41c90a0b0d749ad8')
                            {}
                            else
                            {
                            ?>
                                <!-- <a href="<?php //echo $sharingDetails[$cntBB]->getPlatform();?>" target="_blank"> -->
                                <a href="https://api.whatsapp.com/send?phone=6<?php echo $sharingDetails[$cntBB]->getPlatform();?>" target="_blank">
                                    <img src="img/whatsapp.png" class="contact-icon opacity-hover">
                                    <!-- <img src="img/googlemeet.png" class="contact-icon opacity-hover"> -->
                                </a>
                            <?php
                            }
                            ?>    
                        </a>                          
                    </div>  
               
                    <style>
                        /* .staff-1{ */
                        #<?php echo "style".$sharingDetails[$cntBB]->getId();?>
                        {
                            background-image:url("userProfilePic/<?php echo $sharingDetails[$cntBB]->getFile();?>");
                            background-size:cover;
                            background-position:top;
                        }
                    </style>
                
                <?php
                }
                else
                {}
            }
            ?>
        <?php
        }
        ?>

    </div>
</div>
</div>
<!-- <?php // include 'chat.php'; ?> -->
<?php include 'js.php'; ?>

</body>
</html>