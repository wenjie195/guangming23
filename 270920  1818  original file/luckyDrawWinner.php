<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Winner.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

// $registrationDetails = getWinner($conn);
// $registrationDetails = getWinner($conn, "ORDER BY date_created DESC");

// $winner3pm = getWinner($conn, "WHERE state = '3pm' AND datetime = '25/09/2020' ORDER BY date_created DESC");
// $winner5pm = getWinner($conn, "WHERE state = '5pm' AND datetime = '25/09/2020' ORDER BY date_created DESC");
// $winner7pm = getWinner($conn, "WHERE state = '7pm' AND datetime = '25/09/2020' ORDER BY date_created DESC");
// $winner3pmA = getWinner($conn, "WHERE state = '3pm' AND datetime = '26/09/2020' ORDER BY date_created DESC");
// $winner5pmA = getWinner($conn, "WHERE state = '5pm' AND datetime = '26/09/2020' ORDER BY date_created DESC");
// $winner7pmA = getWinner($conn, "WHERE state = '7pm' AND datetime = '26/09/2020' ORDER BY date_created DESC");

$winner3pm = getWinner($conn, "WHERE state = '3pm' AND datetime = '25/09/2020' ORDER BY prize DESC");
$winner5pm = getWinner($conn, "WHERE state = '5pm' AND datetime = '25/09/2020' ORDER BY prize DESC");
$winner7pm = getWinner($conn, "WHERE state = '7pm' AND datetime = '25/09/2020' ORDER BY prize DESC");
$winner3pmA = getWinner($conn, "WHERE state = '3pm' AND datetime = '26/09/2020' ORDER BY prize DESC");
$winner5pmA = getWinner($conn, "WHERE state = '5pm' AND datetime = '26/09/2020' ORDER BY prize DESC");
$winner7pmA = getWinner($conn, "WHERE state = '7pm' AND datetime = '26/09/2020' ORDER BY prize DESC");

$winner3pmB = getWinner($conn, "WHERE state = '3pm' AND datetime = '27/09/2020' ORDER BY prize DESC");
$winner5pmB = getWinner($conn, "WHERE state = '5pm' AND datetime = '27/09/2020' ORDER BY prize DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/luckyDrawWinner.php" />
<meta property="og:title" content="Lucky Draw Winner | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Lucky Draw Winner   | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/luckyDrawWinner.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 overflow"><img src="img/desktop-banner.jpg" class="width100 desktop-banner"><img src="img/mobile-banner.jpg" class="width100 mobile-banner"></div>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="index-dual-title text-center"><b>Congratulations 恭贺以下幸运儿</b></h2>
    
	<div class="clear"></div>
	<p class="luckydraw-p text-center">Pick the timeframe of the lucky draw result<br>请选择要查询的抽奖结果时间段</p>
	<p class="text-center"><img src="img/down.png" class="down-png"></p>


    <div class="tab ow-tab" id="ow-tab">
        <button class="tablinks active" onclick="openCity('3pm')">25-09-2020 (3pm)</button>
        <button class="tablinks" onclick="openCity('5pm')">25-09-2020 (5pm)</button>
        <button class="tablinks" onclick="openCity('7pm')">25-09-2020 (7pm)</button>
        <button class="tablinks" onclick="openCity('3pm2')">26-09-2020 (3pm)</button>
        <button class="tablinks" onclick="openCity('5pm2')">26-09-2020 (5pm)</button>
        <button class="tablinks" onclick="openCity('7pm2')">26-09-2020 (7pm)</button>
        <button class="tablinks" onclick="openCity('3pm3')">27-09-2020 (3pm)</button>
        <button class="tablinks" onclick="openCity('5pm3')">27-09-2020 (5pm)</button>
    </div>

    <div class="clear"></div>

    <!-- <div class="scroll-div margin-top30"> -->

    <div id="3pm" class="w3-container city scroll-div">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner3pm)
                {
                    for($cnt = 0;$cnt < count($winner3pm) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner3pm[$cnt]->getName();?></td>
                            <td><?php echo $winner3pm[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner3pm[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner3pm[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner3pm[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner3pm[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="5pm" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner5pm)
                {
                    for($cnt = 0;$cnt < count($winner5pm) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner5pm[$cnt]->getName();?></td>
                            <td><?php echo $winner5pm[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner5pm[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner5pm[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner5pm[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner5pm[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="7pm" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner7pm)
                {
                    for($cnt = 0;$cnt < count($winner7pm) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner7pm[$cnt]->getName();?></td>
                            <td><?php echo $winner7pm[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner7pm[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner7pm[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner7pm[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner7pm[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="3pm2" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner3pmA)
                {
                    for($cnt = 0;$cnt < count($winner3pmA) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner3pmA[$cnt]->getName();?></td>
                            <td><?php echo $winner3pmA[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner3pmA[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner3pmA[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner3pmA[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner3pmA[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="5pm2" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner5pmA)
                {
                    for($cnt = 0;$cnt < count($winner5pmA) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner5pmA[$cnt]->getName();?></td>
                            <td><?php echo $winner5pmA[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner5pmA[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner5pmA[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner5pmA[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner5pmA[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="7pm2" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner7pmA)
                {
                    for($cnt = 0;$cnt < count($winner7pmA) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner7pmA[$cnt]->getName();?></td>
                            <td><?php echo $winner7pmA[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner7pmA[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner7pmA[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner7pmA[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner7pmA[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="3pm3" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner3pmB)
                {
                    for($cnt = 0;$cnt < count($winner3pmB) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner3pmB[$cnt]->getName();?></td>
                            <td><?php echo $winner3pmB[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner3pmB[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner3pmB[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner3pmB[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner3pmB[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    <div id="5pm3" class="w3-container city scroll-div" style="display:none">
        <table class="table-css">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Prize</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($winner5pmB)
                {
                    for($cnt = 0;$cnt < count($winner5pmB) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $winner5pmB[$cnt]->getName();?></td>
                            <td><?php echo $winner5pmB[$cnt]->getContact();?></td>
                            <td>
                            <?php
                                $prize = $winner5pmB[$cnt]->getPrize();
                                if($prize == 'VIVO 手机')
                                {
                                ?>
                                    <mark><?php echo $winner5pmB[$cnt]->getPrize();?></mark>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <?php echo $winner5pmB[$cnt]->getPrize();?>
                                <?php
                                }
                            ?>
                            </td>
                            <td><?php echo $winner5pmB[$cnt]->getDatetime();?></td>
                        </tr>
                    <?php
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>

    </div>




<script>
function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
	
  }
  document.getElementById(cityName).style.display = "block";
}
</script>
<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("ow-tab");
var btns = header.getElementsByClassName("tablinks");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  if (current.length > 0) { 
    current[0].className = current[0].className.replace(" active", "");
  }
  this.className += " active";
  });
}
</script>
	
<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>