<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$liveDetails = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ORDER BY id ASC");

$adminShare = getUser($conn," WHERE user_type = '0' ");
$adminData = $adminShare[0];
$adminPlatform = $adminData->getPlatform();
$adminLink = $adminData->getLink();
$adminAutoplay = $adminData->getAutoplay();

$mainAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '2' ");
$allAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '1' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/index2.php" />
<link rel="canonical" href="https://gmvec.com/index2.php" />
<meta property="og:title" content="光明線上產業展 Guang Ming Properties E-Fair" />
<title>光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">

<?php include 'css.php'; ?>
</head>

<body>

<!-- <div class="width100 gold-line"></div> -->
<div class="width100 overflow"><img src="img/desktop-banner.jpg" class="width100 desktop-banner"><img src="img/mobile-banner.jpg" class="width100 mobile-banner"></div>
    <marquee class="announcement-marquee opacity-hover" behavior="scroll" direction="left">
        <a href="" target="_blank">
            <?php
            if($mainAnnoucement)
            {
                for($cntAA = 0;$cntAA < count($mainAnnoucement) ;$cntAA++)
                {
                ?>
                    <?php echo $mainAnnoucement[$cntAA]->getContent();?>
                <?php
                }
            }
            ?>
        </a>
    </marquee>
    <div class="width100 same-padding overflow min-height marquee-distance">
    	<!-- <div class="width100 overflow text-center">
    		<img src="img/gmvec-logo.jpg" class="gmvec-logo" alt="光明線上產業展 Guang Ming Properties E-Fair" title="光明線上產業展 Guang Ming Properties E-Fair">
		</div>
        <h1 class="title-h1 text-center landing-title-h1 black-text">光明線上產業展<br>Guang Ming Properties E-Fair</h1> -->
        <!-- <h1 class="title-h1 text-center landing-title-h1 black-text"><?php //echo $mainTitle->getName();?></h1> -->
		<div class="left-video-only-div">

            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

		</div>
   <div class="right-announcement-big-div">
         <h2 class="announcement-title">Announcement</h2>
         <!-- <h2 class="announcement-title">Schedule</h2> -->
         
   		<!-- Start repeat php code --->
        <?php
        if($allAnnoucement)
        {
            for($cnt = 0;$cnt < count($allAnnoucement) ;$cnt++)
            {
            ?>
                <a href="" target="_blank">
                    <div class="announcement-per-div opacity-hover">
                        <p class="small-date"><?php echo date("d-m-Y",strtotime($allAnnoucement[$cnt]->getDateCreated()));?></p>
                        <p class="announcement-content">
                            <?php echo $allAnnoucement[$cnt]->getContent();?>
                        </p>
                    
                    </div>
                </a>
            <?php
            }
        }
        ?>
   		<!-- End of php code -->

   </div> 

		<div class="clear"></div> 
        <div class="item width100">
            <div class="clearfix width100 video-big-div">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <!-- <li>
                        <iframe class="item-iframe" src="https://www.youtube.com/embed/H63Oq8YYlgM?" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="live-iframe" allowfullscreen></iframe>
						<p class="five-div-p1">Mr Leon Lee 李烔良</p>
						<p class="five-div-p2">First time homebuyers’ guide</p>
                    </li>
                    <li>
                        <iframe class="item-iframe" src="https://www.youtube.com/embed/H63Oq8YYlgM?" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="live-iframe" allowfullscreen></iframe>
						<p class="five-div-p1">Dato Toh 拿督杜进良</p>
						<p class="five-div-p2">“HOC and other incentives; the time is NOW!”</p>	
                    </li>
                    <li>
                        <iframe  class="item-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=923178258168840"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
						<p class="five-div-p1">Mr Ong Yu Shin 王优幸</p>
						<p class="five-div-p2">Basics of Residential, Service Suits & Commercial Tenancies</p>
                    </li> -->
                    <li>
						<div  class="item-iframe" id="speaker-bg1"></div>
						<p class="five-div-p1">Mr Leon Lee 李烔良</p>
						<p class="five-div-p2">First time homebuyers’ guide</p>
						        <style>
                                    #speaker-bg1
                                    {
                                        background-image:url("img/s-leon-lee2.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>
                    <li>
						<div  class="item-iframe" id="speaker-bg2"></div>
						<p class="five-div-p1">Dato Toh 拿督杜进良</p>
						<p class="five-div-p2">HOC and other incentives; the time is NOW!</p>
						        <style>
                                    #speaker-bg2
                                    {
                                        background-image:url("img/s-dato-toh.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>
                    
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg3"></div>
						<p class="five-div-p1">Mr Ong Yu Shin 王优幸</p>
						<p class="five-div-p2">Basics of Residential, Service Suits & Commercial Tenancies</p>
						        <style>
                                    #speaker-bg3
                                    {
                                        background-image:url("img/s-ong-yu-shin.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->

                    <li>
						<div  class="item-iframe" id="speaker-bg13"></div>
						<p class="five-div-p1">Jessie Lee 大師</p>
						<p class="five-div-p2">馬來西亞中華玄學命理諮詢 Soleil Trinity Resource創辦人 正確的方位將讓您終身受益無窮</p>
						        <style>
                                    #speaker-bg13
                                    {
                                        background-image:url("img/s-jessie.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg4"></div>
						<p class="five-div-p1">Miss Adelyn Low 刘家妤</p>
						<p class="five-div-p2">Matters in relation to Sale and Purchase Agreement, a brief guide</p>
						        <style>
                                    #speaker-bg4
                                    {
                                        background-image:url("img/s-adelyn.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg8"></div>
						<p class="five-div-p1">Sr Micheal Geh 倪川鹏</p>
						<p class="five-div-p2">The State of the Property Market</p>
						        <style>
                                    #speaker-bg8
                                    {
                                        background-image:url("img/s-michael-geh.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg12"></div>
						<p class="five-div-p1">Dr Tong 唐英翔博士</p>
						<p class="five-div-p2">馬來西亞首位地質病理學家 地理能量的奧秘</p>
						        <style>
                                    #speaker-bg12
                                    {
                                        background-image:url("img/s-drtong.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg10"></div>
						<p class="five-div-p1">Mr Hadden Lim 林翰宏先生</p>
						<p class="five-div-p2">楷立商業顧問公司Kalyx Consultants</p>
						        <style>
                                    #speaker-bg10
                                    {
                                        background-image:url("img/s-hadden-lim.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg6"></div>
						<p class="five-div-p1">Mr Stephen Kam 甘伟志</p>
						<p class="five-div-p2">Public Auction</p>
						        <style>
                                    #speaker-bg6
                                    {
                                        background-image:url("img/s-stephen-kam3.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg3"></div>
						<p class="five-div-p1">Mr Ong Yu Shin 王优幸</p>
						<p class="five-div-p2">Basics of Residential, Service Suits & Commercial Tenancies</p>
						        <style>
                                    #speaker-bg3
                                    {
                                        background-image:url("img/s-ong-yu-shin.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg11"></div>
						<p class="five-div-p1">Master Lee Boon Hoe 李文和大師</p>
						<p class="five-div-p2">馬新著名道家學術資深講師 中國術數的分類</p>
						        <style>
                                    #speaker-bg11
                                    {
                                        background-image:url("img/s-master-lee.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg9"></div>
						<p class="five-div-p1">Chuah Say Win 蔡世赢</p>
						<p class="five-div-p2">Home Renovation Do’s and Dont’s</p>
						        <style>
                                    #speaker-bg9
                                    {
                                        background-image:url("img/s-chuah-say-win2.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>

                    <li>
						<div  class="item-iframe" id="speaker-bg5"></div>
						<p class="five-div-p1">Mr Stephen Soon 孙子巄</p>
						<p class="five-div-p2">Absolutely Successful Property Auction By Owner (Please take note, NOT bank auction). Can it really Work?</p>
						        <style>
                                    #speaker-bg5
                                    {
                                        background-image:url("img/s-stephen-soon.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>
                    
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg6"></div>
						<p class="five-div-p1">Mr Stephen Kam 甘伟志</p>
						<p class="five-div-p2">Public Auction</p>
						        <style>
                                    #speaker-bg6
                                    {
                                        background-image:url("img/s-stephen-kam3.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->
                    <li>
						<div  class="item-iframe" id="speaker-bg7"></div>
						<p class="five-div-p1">Sr Tan Chean Hwa 陈健桦</p>
						<p class="five-div-p2">新常态下的房地产业</p>
						        <style>
                                    #speaker-bg7
                                    {
                                        background-image:url("img/s-tan-chean-hwa.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li>
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg8"></div>
						<p class="five-div-p1">Sr Micheal Geh 倪川鹏</p>
						<p class="five-div-p2">The State of the Property Market</p>
						        <style>
                                    #speaker-bg8
                                    {
                                        background-image:url("img/s-michael-geh.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->

                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg9"></div>
						<p class="five-div-p1">Chuah Say Win 蔡世赢</p>
						<p class="five-div-p2">Home Renovation Do’s and Dont’s</p>
						        <style>
                                    #speaker-bg9
                                    {
                                        background-image:url("img/s-chuah-say-win2.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg10"></div>
						<p class="five-div-p1">Mr Hadden Lim 林翰宏先生</p>
						<p class="five-div-p2">楷立商業顧問公司Kalyx Consultants</p>
						        <style>
                                    #speaker-bg10
                                    {
                                        background-image:url("img/s-hadden-lim.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg11"></div>
						<p class="five-div-p1">Master Lee Boon Hoe 李文和大師</p>
						<p class="five-div-p2">馬新著名道家學術資深講師 中國術數的分類</p>
						        <style>
                                    #speaker-bg11
                                    {
                                        background-image:url("img/s-master-lee.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg12"></div>
						<p class="five-div-p1">Dr Tong 唐英翔博士</p>
						<p class="five-div-p2">馬來西亞首位地質病理學家 地理能量的奧秘</p>
						        <style>
                                    #speaker-bg12
                                    {
                                        background-image:url("img/s-drtong.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->
                    <!-- <li>
						<div  class="item-iframe" id="speaker-bg13"></div>
						<p class="five-div-p1">Jessie Lee 大師</p>
						<p class="five-div-p2">馬來西亞中華玄學命理諮詢 Soleil Trinity Resource創辦人 正確的方位將讓您終身受益無窮</p>
						        <style>
                                    #speaker-bg13
                                    {
                                        background-image:url("img/s-jessie.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>
                    </li> -->

                </ul>
            </div>
        </div>		
		
		<div class="clear"></div>
		<!--
        <div class="overflow five-speaker-div">
			<div class="five-div">
				<div class="five-div-inner" id="speaker-bg"></div>
				<p class="five-div-p1">Mr Leon Lee 李烔良</p>
				<p class="five-div-p2">First time homebuyers’ guide</p>
			</div>
			<div class="five-div">
				<div class="five-div-inner" id="speaker-bg"></div>
				<p class="five-div-p1">Dato Toh 拿督杜进良</p>
				<p class="five-div-p2">“HOC and other incentives; the time is NOW!”</p>			
			</div>
			<div class="five-div">
				<div class="five-div-inner" >
                                    <iframe class="five-div-inner" src="https://www.youtube.com/embed/H63Oq8YYlgM?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>					
				</div>
				<p class="five-div-p1">Mr Ong Yu Shin 王优幸</p>
				<p class="five-div-p2">Basics of Residential, Service Suits & Commercial Tenancies</p>
			</div>
			<div class="five-div">
				<div class="five-div-inner" id="speaker-bg"></div>
				<p class="five-div-p1">Miss Adelyn Low 刘家妤</p>
				<p class="five-div-p2">Matters in relation to Sale and Purchase Agreement, a brief guide</p>			
			</div>	
			<div class="five-div">
				<div class="five-div-inner">
				                                    <iframe  class="five-div-inner" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=923178258168840"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
				
				</div>
				<p class="five-div-p1">Mr Stephen Soon 孙子巄</p>
				<p class="five-div-p2">Absolutely Successful Property Auction By Owner (Please take note, NOT bank auction). Can it really Work?<br>
绝对成功的业主所委任产业拍卖（请注意，非银行拍卖），真的行得通吗？</p>			
			</div>			
			<div class="five-div">
				<div class="five-div-inner">
				                                    <iframe  class="five-div-inner" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=923178258168840"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>				
				
				
				</div>
				<p class="five-div-p1">Mr Stephen Kam 甘伟志</p>
				<p class="five-div-p2">Public Auction</p>			
			</div>	
			<div class="five-div">
				<div class="five-div-inner">
				                                    <iframe  class="five-div-inner" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=923178258168840"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>				
				
				</div>
				<p class="five-div-p1">Sr Tan Chean Hwa 陈健桦</p>
				<p class="five-div-p2">新常态下的房地产业</p>			
			</div>				
			<div class="five-div">
				<div class="five-div-inner" id="speaker-bg"></div>
				<p class="five-div-p1">Sr Micheal Geh 倪川鹏</p>
				<p class="five-div-p2">“The State of the Property Market"<br> "目前的房地产市场趋势"</p>			
			</div>					
			<div class="five-div">
				<div class="five-div-inner">
                                    <iframe class="five-div-inner" src="https://www.youtube.com/embed/H63Oq8YYlgM?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>					
				
				</div>
				<p class="five-div-p1">Chuah Say Win 蔡世赢</p>
				<p class="five-div-p2">Home Renovation Do’s and Dont’s</p>			
			</div>			
			
			
                                <style>
                                    /* .staff-1{ */
                                    #speaker-bg
                                    {
                                        background-image:url("img/leon-lee2.jpg");
                                        background-size:cover;
                                        background-position:top;
                                    }
                                </style>			
			
		</div>-->
        <div class="two-section-container overflow">
        <?php
        $conn = connDB();
        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>

                <?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                    <iframe class="two-section-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>
                    <?php
                    }

                    elseif($platfrom == 'Zoom')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                <div class="two-section-iframe background-css" id="z<?php echo $liveDetails[$cnt]->getUid();?>" value="<?php echo $liveDetails[$cnt]->getUid();?>">
                            	</div>
                                <div class="clear"></div>
                                <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
							</div>
                        </a>
                        
						<style>
                        	#z<?php echo $liveDetails[$cnt]->getUid();?>{
								background-image:url(userProfilePic/<?php echo $liveDetails[$cnt]->getBroadcastShare();?>);}
                        </style>
                    <?php
                    }

                    elseif($platfrom == 'Facebook')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                                    <iframe  class="two-section-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                           			<div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>

                    <?php
                    }

                    elseif($platfrom == 'Temporarily')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                            <!-- <div class="staff-div-css overflow opacity-hover"> -->
                                    <div class="two-section-iframe" id="<?php echo "style".$liveDetails[$cnt]->getUid();?>"></div>

                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            <!-- </div>   -->
                        
                        <style>
                            /* .staff-1{ */
                            #<?php echo "style".$liveDetails[$cnt]->getUid();?>
                            {
                                background-image:url("uploads/<?php echo $liveDetails[$cnt]->getTitle();?>");
                                background-size:cover;
                                background-position:top;
                            }
                        </style>
                                    
                            </div>
                        </a>

                    <?php
                    }

                    else
                    {   }
                ?>

            <?php
            }
            ?>

        <?php
        }

        else
        {
        ?>
            NO BROADCASTING AT THE MOMENT, WE WILL RETURN SOON !!
        <?php
        }

        ?>
    </div>


    
    
    </div>

    <div class="clear"></div>
    
</div>
<div class="width100 overflow"><img src="img/special-thanks.png" class="width100"></div>
<p id="hide" class="absolute-hide opacity-hover" title="Close">X</p>
<!-- <a href="registration2527.php" target="_blank" id="lucky-draw"><img src="img/luckydraw.gif" class="absolute-chat opacity-hover" title="Lucky Draw" alt="Lucky Draw"></a> -->
<a href="registration2527.php" id="lucky-draw"><img src="img/luckydraw.gif" class="absolute-chat opacity-hover" title="Lucky Draw" alt="Lucky Draw"></a>
<?php include 'js.php'; ?>

</body>
</html>