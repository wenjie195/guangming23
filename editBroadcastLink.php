<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
// require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");
$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/editBroadcastLink.php" />
<meta property="og:title" content="Home Main Video | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Home Main Video  | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/editBroadcastLink.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <!-- <h2 class="h1-title">Home Main Video <img src="img/note.png" class="opacity-hover icon-png open-homevideo" alt="What is it?" title="What is it?"></h2>  -->
    <h2 class="h1-title">Admin Main Video <img src="img/note.png" class="opacity-hover icon-png open-homevideo" alt="What is it?" title="What is it?"></h2> 
        <div class="clear"></div>

        <form method="POST" action="utilities/editAdminBroadcastFunction.php">

                <!-- <div class="dual-input"> -->
                <div class="width100">
                    <p class="input-top-text">Platform</p>
                    <select class="aidex-input clean" type="text" name="update_platform" id="update_platform" required>
                        <option value="">Please Select A Platform</option>
                        <?php
                        if($userData->getPlatform() == '')
                        {
                        ?>
                            <option selected>Please Select a Platform</option>
                            <?php
                            for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($platformDetails) ; $cnt++){
                                if ($userData->getPlatform() == $platformDetails[$cnt]->getPlatformType())
                                {
                                ?>
                                    <option selected value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                        <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                        <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select> 
                </div>

                <div class="clear"></div>  

            <div class="width100">
                <p class="input-top-text">Link</p>
                <input class="aidex-input clean" type="text" value="<?php echo $userData->getLink();?>" name="update_link" id="update_link" required>       
            </div>
    	
            <div class="clear"></div>  

           <!-- <div class="dual-input">
                <p class="input-top-p admin-top-p">Autoplay</p>
                <select class="aidex-input clean" type="text" name="autoplay_function" id="autoplay_function">
                    <?php
                        if($userData->getAutoplay() == '')
                        {
                        ?>
                            <option value="No"  name='No'>No</option>
                            <option value="Yes"  name='Yes'>Yes</option>
                            <option selected value=""  name=''>Please Select An Option</option>
                        <?php
                        }
                        else if($userData->getAutoplay()  == 'Yes')
                        {
                        ?>
                            <option value="No"  name='No'>No</option>
                            <option selected value="Yes"  name='Yes'>Yes</option>
                        <?php
                        }
                        else if($userData->getAutoplay()  == 'No')
                        {
                        ?>
                            <option selected value="No"  name='No'>No</option>
                            <option value="Yes"  name='Yes'>Yes</option>
                    <?php
                    }
                    ?>
                </select> 

            </div> -->

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>