<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
// require_once dirname(__FILE__) . '/classes/Platform.php';
// require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';


require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allUser = getUser($conn," WHERE user_type = '1' ");
// $platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminAddWinner.php" />
<meta property="og:title" content="Add Lucky Draw Winner | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Add Lucky Draw Winner | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminAddWinner.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <form action="utilities/registerWinnerFunction.php" method="POST"> 

    <h2 class="h1-title">Add Lucky Draw Winner</h2> 

    <div class="clear"></div>

    <div class="dual-input">
        <p class="input-top-text">Name</p>
        <input class="aidex-input clean" type="text" placeholder="Name" name="register_name" id="register_name" required>       
    </div>

    <div class="dual-input second-dual-input">
        <p class="input-top-text">Contact</p>
        <input class="aidex-input clean" type="text" placeholder="Contact" name="register_contact" id="register_contact" required>       
    </div>

    <div class="clear"></div>

    <div class="dual-input">
        <p class="input-top-text">Prize</p>
        <input class="aidex-input clean" type="text" placeholder="Prize" name="register_prize" id="register_contact" required>    
    </div>

    <div class="dual-input second-dual-input">
        <p class="input-top-text">Date</p>
        <input class="aidex-input clean" type="text" placeholder="Date" name="register_date" id="register_date" >       
    </div>

    <div class="clear"></div>

    <div class="dual-input">
        <p class="input-top-text">Session (3pm / 5pm / 7pm)</p>
        <input class="aidex-input clean" type="text" placeholder="Session (3pm / 5pm / 7pm)" name="register_session" id="register_session" required>    
    </div>

    <div class="clear"></div>

    <div class="width100 overflow text-center">     
        <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
    </div>

    </form>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>