<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Userdata.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $sql_query = "SELECT `id`,`username`,`email`,`phone_no` FROM `userdata`";

$sql_query = "SELECT `id`,`name`,`email`,`contact`,`state`,`location` FROM `lucky_draw`";

$resultset = mysqli_query($conn, $sql_query) or die("database error:". mysqli_error($conn));
$developer_records = array();
while( $rows = mysqli_fetch_assoc($resultset) ) 
{
	$developer_records[] = $rows;
}
if(isset($_POST["export_data"])) 
{
	$filename = "gmvec data ". date('d M Y') . ".xls";
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=\"$filename\"");
	$show_coloumn = true;
	if(!empty($developer_records))
	{
	  foreach($developer_records as $record) 
	  {
		if(!$show_coloumn)
		{
		  // display field/column names in first row
		  echo implode("\t", array_keys($record)) . "\n";
		  $show_coloumn = true;
		}
		echo implode("\t", array_values($record)) . "\n";
	  }
	}
	exit;
}
?>