<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/register.php" />
<link rel="canonical" href="https://gmvec.com/register.php" />
<meta property="og:title" content="Register | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Register | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
    <div class="cover-gap content min-height2 ow-content-gap">
        <div class="big-white-div same-padding">
		<h1 class="landing-h1 margin-left-0"><?php echo _HEADER_SIGN_UP ?></h1>   
        
        <form action="utilities/newUserRegisterFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text"><?php echo _MAINJS_INDEX_USERNAME ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="register_username" name="register_username" required>
            </div>

            <div class="login-input-div">
            <p class="input-top-text"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
            <input class="aidex-input clean" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="register_password" name="register_password" required>
            </div>

            <div class="login-input-div">
            <p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?></p>
            <input class="aidex-input clean" type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required>
            </div>

            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
                <input class="aidex-input clean" type="<?php echo _JS_EMAIL ?>" placeholder="Type Your Email" id="register_email" name="register_email" required>
            </div>

            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_CONTACT_NO ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _JS_CONTACT_NO ?>" id="register_phone" name="register_phone" required>
            </div>

            <div class="clear"></div>

            <button class="clean-button clean login-btn pink-button" name="register"><?php echo _HEADER_SIGN_UP ?></button>

            </div>
        </form>

    </div>
</div>
<?php include 'footer.php'; ?>

</body>
</html>