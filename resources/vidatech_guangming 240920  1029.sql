-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2020 at 04:27 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_guangming`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `uid`, `title`, `content`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '71dbfdf1997603d9610a3f9f90c45a1b', NULL, '早起的鸟儿有虫吃！抢先注册，名牌手机、星巴克和Family Mart 现金券的得主，可能就是你！\r\nAnnouncement: We will have a live talk in 25th Sep 2020. Stay Tuned!  ', 'Available', 2, '2020-07-21 03:46:13', '2020-09-22 11:29:42'),
(2, '1362413c4842c918c87c09917a7a6227', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 1', 'Available', 1, '2020-07-21 03:46:39', '2020-07-21 03:46:39'),
(3, '5d4ad3b1b412f1aa01f73a5d4fb57c6f', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 2\r\n', 'Available', 1, '2020-07-21 03:46:46', '2020-07-21 03:46:46'),
(4, '6eb492ce0f4fe93827ee5d9c7ae882b6', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 3', 'Available', 1, '2020-07-21 03:46:51', '2020-07-21 03:46:51'),
(5, 'd14db52f8c57b997cef7c032bfdd323a', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 4\r\n', 'Available', 1, '2020-07-21 03:53:07', '2020-07-21 03:53:07'),
(6, '0cc59292c2308b7d2d41ef9cdcfe0d2e', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 5', 'Available', 1, '2020-07-21 03:53:14', '2020-07-21 03:53:14'),
(7, '6d30e124799c7d79293f91f5a60e393d', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 6', 'Available', 1, '2020-07-21 03:53:20', '2020-07-21 03:53:20'),
(8, '016d2bf60f79ce76cffa816301bc1ca3', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 7\r\n', 'Available', 1, '2020-07-21 03:53:34', '2020-07-21 03:53:34'),
(9, '422644553d984015b2ebd8d1f828e4ab', NULL, 'We will have a live talk tomorrow. Stay Tuned. Part 8', 'Available', 1, '2020-07-21 03:53:40', '2020-07-21 03:53:40');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL,
  `ch_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ch_name`, `en_name`, `date_created`, `date_updated`) VALUES
(1, '阿布哈茲', 'Abkhazia', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(2, '阿富汗', 'Afghanistan', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(3, '奥兰', 'Åland', '2020-02-28 02:51:32', '2020-02-28 02:53:16'),
(4, '阿尔巴尼亚', 'Albania', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(5, '阿尔及利亚', 'Algeria', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(6, '美属萨摩亚', 'American Samoa', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(7, '安道尔', 'Andorra', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(8, '安哥拉', 'Angola', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(9, '安圭拉', 'Anguilla', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(10, '安地卡及巴布達', 'Antigua and Barbuda', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(11, '阿根廷', 'Argentina', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(12, '亞美尼亞', 'Armenia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(13, '阿尔扎赫', 'Artsakh', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(14, '阿鲁巴', 'Aruba', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(15, '澳大利亚', 'Australia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(16, '奥地利', 'Austria', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(17, '阿塞拜疆', 'Azerbaijan', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(18, '巴哈马', 'Bahamas', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(19, '巴林', 'Bahrain', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(20, '孟加拉国 孟加拉国', 'Bangladesh', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(21, '巴巴多斯', 'Barbados', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(22, '白俄羅斯', 'Belarus', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(23, '比利時', 'Belgium', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(24, '伯利兹', 'Belize', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(25, '贝宁', 'Benin', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(26, '百慕大', 'Bermuda', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(27, '不丹', 'Bhutan', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(28, '玻利维亚', 'Bolivia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(29, '波斯尼亚和黑塞哥维那', 'Bosnia and Herzegovina', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(30, '博茨瓦纳', 'Botswana', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(31, '巴西', 'Brazil', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(32, '文莱', 'Brunei', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(33, '保加利亚', 'Bulgaria', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(34, '布吉納法索', 'Burkina Faso', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(35, '布隆迪', 'Burundi', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(36, '柬埔寨', 'Cambodia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(37, '喀麦隆', 'Cameroon', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(38, '加拿大', 'Canada', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(39, '佛得角', 'Cape Verde', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(40, '开曼群岛', 'Cayman Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(41, '中非共和国', 'Central African Republic', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(42, '乍得', 'Chad', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(43, '智利', 'Chile', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(44, '中国', 'China', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(45, '圣诞岛', 'Christmas Island', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(46, '科科斯（基林）', 'Cocos (Keeling) Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(47, '哥伦比亚', 'Colombia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(48, '科摩罗', 'Comoros', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(49, '刚果（布）', 'Congo (Brazzaville)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(50, '刚果（金）', 'Congo (Kinshasa)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(51, '庫克群島', 'Cook Islands', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(52, '哥斯达黎加', 'Costa Rica', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(53, '科特迪瓦', 'Côte d\'Ivoire\r\n', '2020-02-28 02:51:37', '2020-02-28 03:38:22'),
(54, '克罗地亚', 'Croatia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(55, '古巴', 'Cuba', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(56, '库拉索', 'Curaçao\r\n', '2020-02-28 02:51:37', '2020-02-28 03:37:19'),
(57, '賽普勒斯', 'Cyprus', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(58, '捷克', 'Czech Republic', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(59, '丹麥', 'Denmark', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(60, '吉布提', 'Djibouti', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(61, '多米尼克', 'Dominica', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(62, '多米尼加', 'Dominican Republic', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(63, '顿涅茨克', 'Donetsk', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(64, '厄瓜多尔', 'Ecuador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(65, '埃及', 'Egypt', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(66, '薩爾瓦多', 'El Salvador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(67, '赤道几内亚', 'Equatorial Guinea', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(68, '厄立特里亚', 'Eritrea', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(69, '爱沙尼亚', 'Estonia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(70, '斯威士兰', 'Eswatini', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(71, '衣索比亞', 'Ethiopia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(72, '福克蘭群島', 'Falkland Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(73, '法罗群岛', 'Faroe Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(74, '斐济', 'Fiji', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(75, '芬兰', 'Finland', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(76, '法國', 'France', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(77, '法屬玻里尼西亞', 'French Polynesia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(78, '加彭', 'Gabon', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(79, '冈比亚', 'Gambia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(80, '格鲁吉亚', 'Georgia', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(81, '德國', 'Germany', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(82, '加纳', 'Ghana', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(83, '直布罗陀', 'Gibraltar', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(84, '希臘', 'Greece', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(85, '格陵兰', 'Greenland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(86, '格瑞那達', 'Grenada', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(87, '關島', 'Guam', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(88, '危地马拉', 'Guatemala', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(89, '根西', 'Guernsey', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(90, '几内亚', 'Guinea', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(91, '几内亚比绍', 'Guinea-Bissau', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(92, '圭亚那', 'Guyana', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(93, '海地', 'Haiti', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(94, '洪都拉斯', 'Honduras', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(95, '香港', 'Hong Kong', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(96, '匈牙利', 'Hungary', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(97, '冰島', 'Iceland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(98, '印度', 'India', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(99, '印尼', 'Indonesia', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(100, '伊朗', 'Iran', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(101, '伊拉克', 'Iraq', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(102, '爱尔兰', 'Ireland', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(103, '马恩岛', 'Isle of Man', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(104, '以色列', 'Israel', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(105, '意大利', 'Italy', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(106, '牙买加', 'Jamaica', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(107, '日本', 'Japan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(108, '澤西', 'Jersey', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(109, '约旦', 'Jordan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(110, '哈萨克斯坦', 'Kazakhstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(111, '肯尼亚', 'Kenya', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(112, '基里巴斯', 'Kiribati', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(113, '科索沃', 'Kosovo', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(114, '科威特', 'Kuwait', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(115, '吉尔吉斯斯坦', 'Kyrgyzstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(116, '老挝', 'Laos', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(117, '拉脫維亞', 'Latvia', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(118, '黎巴嫩', 'Lebanon', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(119, '賴索托', 'Lesotho', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(120, '利比里亚', 'Liberia', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(121, '利比亞', 'Libya', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(122, '列支敦斯登', 'Liechtenstein', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(123, '立陶宛', 'Lithuania', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(124, '卢甘斯克', 'Luhansk', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(125, '卢森堡', 'Luxembourg', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(126, '澳門', 'Macau', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(127, '马达加斯加', 'Madagascar', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(128, '马拉维', 'Malawi', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(129, '马来西亚', 'Malaysia', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(130, '馬爾地夫', 'Maldives', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(131, '马里', 'Mali', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(132, '馬爾他', 'Malta', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(133, '马绍尔群岛', 'Marshall Islands', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(134, '毛里塔尼亚', 'Mauritania', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(135, '模里西斯', 'Mauritius', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(136, '墨西哥', 'Mexico', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(137, '密克羅尼西亞聯邦', 'Micronesia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(138, '摩尔多瓦', 'Moldova', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(139, '摩納哥', 'Monaco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(140, '蒙古國', 'Mongolia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(141, '蒙特內哥羅', 'Montenegro', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(142, '蒙特塞拉特', 'Montserrat', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(143, '摩洛哥', 'Morocco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(144, '莫桑比克', 'Mozambique', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(145, '緬甸', 'Myanmar', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(146, '纳米比亚', 'Namibia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(147, '瑙鲁', 'Nauru', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(148, '尼泊尔', 'Nepal', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(149, '荷蘭', 'Netherlands', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(150, '新喀里多尼亞', 'New Caledonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(151, '新西蘭', 'New Zealand', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(152, '尼加拉瓜', 'Nicaragua', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(153, '尼日尔', 'Niger', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(154, '奈及利亞', 'Nigeria', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(155, '纽埃', 'Niue', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(156, '朝鲜', 'North Korea', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(157, '北馬其頓', 'North Macedonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(158, '北塞浦路斯', 'Northern Cyprus', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(159, '北馬里亞納群島', 'Northern Mariana Islands', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(160, '挪威', 'Norway', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(161, '阿曼', 'Oman', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(162, '巴基斯坦', 'Pakistan', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(163, '帛琉', 'Palau', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(164, '巴勒斯坦', 'Palestine', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(165, '巴拿马', 'Panama', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(166, '巴布亚新几内亚', 'Papua New Guinea', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(167, '巴拉圭', 'Paraguay', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(168, '秘魯', 'Peru', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(169, '菲律賓', 'Philippines', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(170, '皮特凯恩群岛', 'Pitcairn Islands', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(171, '波蘭', 'Poland', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(172, '葡萄牙', 'Portugal', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(173, '德涅斯特河沿岸', 'Pridnestrovie', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(174, '波多黎各', 'Puerto Rico', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(175, '卡塔尔', 'Qatar', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(176, '羅馬尼亞', 'Romania', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(177, '俄羅斯', 'Russia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(178, '卢旺达', 'Rwanda', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(179, '圣巴泰勒米', 'Saint Barthelemy', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(180, '圣基茨和尼维斯', 'Saint Christopher and Nevis', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(181, '圣赫勒拿、阿森松和特里斯坦-达库尼亚', 'Saint Helena, Ascension and Tristan da Cunha', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(182, '圣卢西亚', 'Saint Lucia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(183, '圣皮埃尔和密克隆', 'Saint Pierre and Miquelon', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(184, '圣文森特和格林纳丁斯', 'Saint Vincent and the Grenadines', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(185, '萨摩亚', 'Samoa', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(186, '圣马力诺', 'San Marino', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(187, '聖多美和普林西比', 'São Tomé and Príncipe\r\n', '2020-02-28 02:51:52', '2020-02-28 03:39:40'),
(188, '沙烏地阿拉伯', 'Saudi Arabia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(189, '塞内加尔', 'Senegal', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(190, '塞爾維亞', 'Serbia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(191, '塞舌尔', 'Seychelles', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(192, '塞拉利昂', 'Sierra Leone', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(193, '新加坡', 'Singapore', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(194, '荷屬聖馬丁', 'Sint Maarten', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(195, '斯洛伐克', 'Slovakia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(196, '斯洛維尼亞', 'Slovenia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(197, '所罗门群岛', 'Solomon Islands', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(198, '索馬利亞', 'Somalia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(199, '索馬利蘭', 'Somaliland', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(200, '南非', 'South Africa', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(201, '韩国', 'South Korea', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(202, '南奥塞梯', 'South Ossetia', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(203, '南蘇丹', 'South Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(204, '西班牙', 'Spain', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(205, '斯里蘭卡', 'Sri Lanka', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(206, '苏丹', 'Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(207, '苏里南', 'Suriname', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(208, '斯瓦尔巴', 'Svalbard', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(209, '瑞典', 'Sweden', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(210, '瑞士', 'Switzerland', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(211, '叙利亚', 'Syria', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(212, '中華民國', 'Taiwan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(213, '塔吉克斯坦', 'Tajikistan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(214, '坦桑尼亚', 'Tanzania', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(215, '泰國', 'Thailand', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(216, '梵蒂冈', 'The Holy See（Vatican City）', '2020-02-28 02:51:55', '2020-02-28 03:40:13'),
(217, '东帝汶', 'Timor-Leste', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(218, '多哥', 'Togo', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(219, '托克勞', 'Tokelau', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(220, '汤加', 'Tonga', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(221, '千里達及托巴哥', 'Trinidad and Tobago', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(222, '突尼西亞', 'Tunisia', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(223, '土耳其', 'Turkey', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(224, '土库曼斯坦', 'Turkmenistan', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(225, '特克斯和凯科斯群岛', 'Turks and Caicos Islands', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(226, '图瓦卢', 'Tuvalu', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(227, '乌干达', 'Uganda', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(228, '烏克蘭', 'Ukraine', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(229, '阿联酋', 'United Arab Emirates', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(230, '英國', 'United Kingdom', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(231, '美國', 'United States', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(232, '乌拉圭', 'Uruguay', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(233, '乌兹别克斯坦', 'Uzbekistan', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(234, '瓦努阿圖', 'Vanuatu', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(235, '委內瑞拉', 'Venezuela', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(236, '越南', 'Vietnam', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(237, '英屬維爾京群島', 'Virgin Islands, British', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(238, '瓦利斯和富圖納', 'Wallis and Futuna', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(239, '西撒哈拉', 'Western Sahara', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(240, '葉門', 'Yemen', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(241, '尚比亞', 'Zambia', '2020-02-28 02:51:58', '2020-02-28 02:51:58'),
(242, '辛巴威', 'Zimbabwe', '2020-02-28 02:51:58', '2020-02-28 02:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `link_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `link_two` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `uid`, `user_uid`, `image_one`, `link_one`, `image_two`, `link_two`, `type`, `date_created`, `date_updated`) VALUES
(1, '9a2257c9de18c44cabd3bb8a9ed46516', '923a1263cf3818339855fcf8f37cbef2', '1595304402.png', 'https://www.google.com/', NULL, NULL, 1, '2020-07-21 04:06:42', '2020-07-21 04:06:57'),
(2, '33db1d4c1546e4131d54e06a308bfb3f', '923a1263cf3818339855fcf8f37cbef2', '1595304434.png', 'http://www.ixftv.com/', NULL, NULL, 1, '2020-07-21 04:07:14', '2020-07-21 04:07:16'),
(3, '5ce0fe981215364c1fdc2bc5c33aff7a', '923a1263cf3818339855fcf8f37cbef2', '1595304446.png', 'https://www.amway.my/', NULL, NULL, 1, '2020-07-21 04:07:26', '2020-07-21 04:07:36'),
(4, 'b1730bcc9c1e0b9b8105099d06b9a0c6', '923a1263cf3818339855fcf8f37cbef2', '1595304474.png', 'https://guangming.com.my/', NULL, NULL, 1, '2020-07-21 04:07:54', '2020-07-21 04:07:56'),
(5, 'ca478b2068ada5c5b6a89d1016319bcd', 'e4bbb370d994a76f41c90a0b0d749ad8', '1599808355.png', 'https://www.zeon.com.my/projects/info?id=proje6fffdde-dde6-48c3-8c85-b1e99b8cee50', NULL, NULL, 1, '2020-09-11 07:12:35', '2020-09-11 07:12:52'),
(6, 'f5062934c85d967b52e729cfe44741ea', '632372877da43ab6f7d8af2a73f2e0b3', '1600768245.png', 'https://mahsing.com.my', NULL, NULL, 1, '2020-09-14 10:57:32', '2020-09-23 10:40:02'),
(7, '80ef99d5e5e3f9dd6e96d73f8a887c46', '632372877da43ab6f7d8af2a73f2e0b3', '1600768273.png', 'https://mahsing.com.my', NULL, NULL, 1, '2020-09-14 10:58:29', '2020-09-23 10:39:57'),
(8, '1f93284da254c33906c1006cbe0ed421', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600081595.png', 'https://www.facebook.com/CityofDreamsPG/', NULL, NULL, 1, '2020-09-14 11:06:35', '2020-09-14 11:08:32'),
(9, '50f20149e1c8876ecb293ca80c175985', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600081726.png', 'https://www.eweinberhad.com/', NULL, NULL, 3, '2020-09-14 11:08:46', '2020-09-22 07:50:17'),
(10, '6b8951aa79f82189b06e490c4cff3659', 'b3691c35cea1dfed6274227f67744e80', '1600167631.png', 'https://www.facebook.com/picc.penang', NULL, NULL, 1, '2020-09-15 11:00:31', '2020-09-22 05:12:58'),
(11, '29ecb44718448c86c95df317384f4f4d', 'b3691c35cea1dfed6274227f67744e80', '1600847967.png', 'https://www.facebook.com/picc.penang', NULL, NULL, 1, '2020-09-15 11:03:47', '2020-09-23 08:00:04'),
(12, 'b91d08210dd2dc3572e9ea68197f0908', 'b3691c35cea1dfed6274227f67744e80', '1600848018.png', 'http://www.picc-penang.com/index.php', NULL, NULL, 1, '2020-09-15 11:04:24', '2020-09-23 08:01:17'),
(13, 'd15e210a2c480bf75e1af0cd63cee38a', 'b3691c35cea1dfed6274227f67744e80', '1600848114.png', 'http://www.picc-penang.com/index.php', NULL, NULL, 1, '2020-09-15 11:05:11', '2020-09-23 08:02:11'),
(14, '7b6b0d81e48346e63e21b1f0e8f66934', 'b3691c35cea1dfed6274227f67744e80', '1600167962.png', 'https://www.alila.com.my/', NULL, NULL, 1, '2020-09-15 11:06:02', '2020-09-15 11:06:05'),
(15, '3a4346f20fa5f93ed1d94df7fd77b9e4', '3942b0430fbe8374db84b3aa7f1d9831', '1600168544.png', 'https://jesseltonvillas.com/', NULL, NULL, 3, '2020-09-15 11:15:44', '2020-09-23 10:18:06'),
(16, 'acdbd6120b26c969c901f9d3e7a259ba', '3942b0430fbe8374db84b3aa7f1d9831', '1600169030.png', 'https://www.jadigroup.com/', NULL, NULL, 3, '2020-09-15 11:23:50', '2020-09-15 11:26:52'),
(17, '36f597d8b791f672afca431642dff375', '9619819c11170ebf2f4785ca71e9d4bf', '1600169237.png', 'https://www.jadigroup.com/', NULL, NULL, 1, '2020-09-15 11:27:17', '2020-09-15 11:27:19'),
(18, '82f66b8ac479bbcf828ce3eacf56f679', '9619819c11170ebf2f4785ca71e9d4bf', '1600169274.png', 'https://www.tambunroyalecity.com/royale-infinity/', NULL, NULL, 1, '2020-09-15 11:27:54', '2020-09-15 11:27:57'),
(19, '98cdc1427c01fb5430346978af0e9ab9', '9619819c11170ebf2f4785ca71e9d4bf', '1600169290.png', 'https://www.tambunroyalecity.com/', NULL, NULL, 3, '2020-09-15 11:28:10', '2020-09-23 07:18:53'),
(20, '25bc6ea4a204f3b500becc75309577be', '9619819c11170ebf2f4785ca71e9d4bf', '1600169361.png', 'https://www.tambunroyalecity.com/royale-heights/', NULL, NULL, 1, '2020-09-15 11:29:21', '2020-09-15 11:32:15'),
(21, '4b80c018ffbd9db04031b5a71b830672', '9619819c11170ebf2f4785ca71e9d4bf', '1600169491.png', 'https://www.jadigroup.com/portfolio-item/bm-highland/', NULL, NULL, 1, '2020-09-15 11:31:31', '2020-09-15 11:31:44'),
(22, 'd9a3680075f5e154bbe71a077a5c75bc', 'e4f4785e35b9623455e78794eac15511', '1600169906.png', 'http://www.tahwah.com/TWProjectD.asp?id=25', NULL, NULL, 3, '2020-09-15 11:38:26', '2020-09-23 08:19:25'),
(23, 'ef6ee09fb3aabfd8580525705e700433', 'e4f4785e35b9623455e78794eac15511', '1600170061.png', 'http://www.tahwah.com/TWProjectD.asp?id=18', NULL, NULL, 3, '2020-09-15 11:41:01', '2020-09-23 08:19:28'),
(24, '028ad2a36d20ad52eabf6f39055d30b3', 'e4f4785e35b9623455e78794eac15511', '1600744386.png', 'https://www.facebook.com/pages/category/Real-Estate-Developer/Orange-Residence-2438039779574418/', NULL, NULL, 3, '2020-09-15 11:45:21', '2020-09-23 08:19:28'),
(25, '18720759739d64cac739a784644dc4fe', 'e4f4785e35b9623455e78794eac15511', '1600170415.png', 'http://www.tahwah.com/', NULL, NULL, 3, '2020-09-15 11:46:55', '2020-09-23 08:19:29'),
(26, '6344fe3f4f99f78b75aab60ce13e3a43', 'e4f4785e35b9623455e78794eac15511', '1600341519.png', 'http://www.tahwah.com/TWProjectD.asp?id=20', NULL, NULL, 3, '2020-09-17 11:18:39', '2020-09-23 08:19:30'),
(27, 'c0ecbf2fc41458893eaea136f435f699', 'c22aa05c66ccf407b9172c26c9907358', '1600673035.png', 'https://aspen.com.my/', NULL, NULL, 1, '2020-09-17 11:21:34', '2020-09-21 07:23:55'),
(28, '749b630b62cebc2bd38d73cd1a051f42', 'c22aa05c66ccf407b9172c26c9907358', '1600846992.png', 'http://verturesort.com/', NULL, NULL, 1, '2020-09-21 06:44:38', '2020-09-23 07:43:53'),
(29, '9e36034332d9ccc8f0c6a1d57b451eb4', 'c22aa05c66ccf407b9172c26c9907358', '1600670803.png', 'https://vogue-lifestyle-residence.com/', NULL, NULL, 3, '2020-09-21 06:46:43', '2020-09-21 07:17:24'),
(30, '5da439a617eb1394028dbcc3130f15e3', 'c22aa05c66ccf407b9172c26c9907358', '1600671153.png', 'https://viluxe.com.my/', NULL, NULL, 3, '2020-09-21 06:52:33', '2020-09-21 07:17:09'),
(31, 'cd5dc0d0650b35ef8a071d58219deae6', 'c22aa05c66ccf407b9172c26c9907358', '1600847007.png', 'https://vivo-avc.com/', NULL, NULL, 1, '2020-09-21 06:53:46', '2020-09-23 07:44:02'),
(32, 'c854fe34e8371ff230df056a34d08b61', 'c22aa05c66ccf407b9172c26c9907358', '1600671637.png', 'https://tri-pinnacle.com/', NULL, NULL, 3, '2020-09-21 07:00:37', '2020-09-21 07:17:11'),
(33, 'cbe17e18a51246f28c87da460651f9e2', 'c22aa05c66ccf407b9172c26c9907358', '1600671707.png', 'http://hhpark.com.my/', NULL, NULL, 3, '2020-09-21 07:01:47', '2020-09-21 07:17:12'),
(34, '904af3bd09af29904cc0482b79f28ae3', 'c22aa05c66ccf407b9172c26c9907358', '1600672754.png', 'https://www.beacon-suites.com/', NULL, NULL, 1, '2020-09-21 07:19:14', '2020-09-21 07:19:36'),
(35, '582c4b17dc571b11c91e7aca2050b3f1', 'b3691c35cea1dfed6274227f67744e80', '1600848158.png', 'http://www.mekarsari.com.my/en/home/', NULL, NULL, 1, '2020-09-22 09:36:56', '2020-09-23 08:02:53'),
(36, 'ddc129bebd3f7ebe996edb68f7b960d9', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600775946.png', 'https://www.facebook.com/CityofDreamsPG/', NULL, NULL, 3, '2020-09-22 11:59:06', '2020-09-23 08:40:35'),
(37, 'd22e71476ad04c41ca0127f8d4e4e74b', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600850382.png', 'https://www.facebook.com/CityofDreamsPG/', NULL, NULL, 1, '2020-09-22 11:59:43', '2020-09-23 08:41:31'),
(38, '8180456e7827a12a3aaa60929974ae8d', 'e4f4785e35b9623455e78794eac15511', '1600845025.png', NULL, NULL, NULL, 3, '2020-09-23 07:10:25', '2020-09-23 08:19:31'),
(39, '68b72a0e3548b036696305592c23ed74', '632372877da43ab6f7d8af2a73f2e0b3', '1600846138.png', 'https://www.facebook.com/pgFR2/?ti=as', NULL, NULL, 1, '2020-09-23 07:28:58', '2020-09-23 07:29:08'),
(40, '57f492940e32724ea7529c31a5c18c7b', '632372877da43ab6f7d8af2a73f2e0b3', '1600846156.png', 'https://instagram.com/mahsinggroup?igshid=wx5b1q41gl9o', NULL, NULL, 1, '2020-09-23 07:29:16', '2020-09-23 07:29:23'),
(41, '900a69f6a1f6bc1276ccc675b532e104', 'b3691c35cea1dfed6274227f67744e80', '1600848248.png', 'https://www.treeo-hunza.com/', NULL, NULL, 1, '2020-09-23 08:04:08', '2020-09-23 08:04:21'),
(42, '5488b08c82c7992cfe488ae17277cd03', 'e4f4785e35b9623455e78794eac15511', '1600864679.png', 'http://www.tahwah.com/', NULL, NULL, 1, '2020-09-23 08:20:04', '2020-09-23 12:37:59'),
(43, 'f213dca55ee8ab20ef6470107139f171', 'e4f4785e35b9623455e78794eac15511', '1600849501.png', 'http://www.tahwah.com/TWProjectD.asp?id=18', NULL, NULL, 3, '2020-09-23 08:25:01', '2020-09-23 08:26:46'),
(44, '46a5c09072746f35deb7a5d0b0fdf9b8', 'e4f4785e35b9623455e78794eac15511', '1600864248.png', 'http://www.tahwah.com/TWProjectD.asp?id=18', NULL, NULL, 1, '2020-09-23 08:27:15', '2020-09-23 12:30:48'),
(45, 'e42819879cc52de34ba8532b368a4d89', 'e4f4785e35b9623455e78794eac15511', '1600864277.png', 'https://www.facebook.com/pages/category/Real-Estate-Developer/Orange-Residence-2438039779574418/', NULL, NULL, 1, '2020-09-23 08:28:00', '2020-09-23 12:31:17'),
(46, '433a3f50a1defc32b6a0a6aceb74b2f3', 'e4f4785e35b9623455e78794eac15511', '1600864307.png', 'http://www.tahwah.com/TWProjectD.asp?id=20', NULL, NULL, 1, '2020-09-23 08:29:34', '2020-09-23 12:31:47'),
(47, '38170c0cd9ef634dc62ccad811cb6721', 'e4f4785e35b9623455e78794eac15511', '1600849843.png', 'http://www.tahwah.com/TWProjectD.asp?id=25', NULL, NULL, 3, '2020-09-23 08:30:43', '2020-09-23 08:31:20'),
(48, 'c40fe0101af93d2c84ca0283fe15eaff', 'e4f4785e35b9623455e78794eac15511', '1600864476.png', 'http://www.tahwah.com/TWProjectD.asp?id=25', NULL, NULL, 1, '2020-09-23 08:31:37', '2020-09-23 12:34:36'),
(49, '0399fbc0eb97dc7ea5d17a94452519f2', 'e4f4785e35b9623455e78794eac15511', '1600864780.png', 'https://www.facebook.com/pages/category/Real-Estate-Developer/Tah-Wah-Group-%E5%A4%A7%E8%8F%AF%E9%9B%86%E5%9C%98-1968814076733804/', NULL, NULL, 1, '2020-09-23 08:33:48', '2020-09-23 12:39:40'),
(50, 'dabe17744c5514ca01a302c0a10083bc', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600850518.png', 'https://instagram.com/cityofdreamspenang', NULL, NULL, 3, '2020-09-23 08:41:58', '2020-09-23 08:42:26'),
(51, '0ca1ad8fcbfd3a75b9c9aca7e683d489', 'e4bbb370d994a76f41c90a0b0d749ad8', '1600850598.png', 'https://instagram.com/cityofdreamspenang', NULL, NULL, 1, '2020-09-23 08:43:18', '2020-09-23 08:43:26'),
(52, '7397f2ade095edda0836414f4f8de8fd', '3942b0430fbe8374db84b3aa7f1d9831', '1600856329.png', NULL, NULL, NULL, 3, '2020-09-23 10:18:49', '2020-09-23 10:19:06'),
(53, 'c7711390cb0735ab61c7024f1dba37f7', '3942b0430fbe8374db84b3aa7f1d9831', '1600856331.png', 'https://jesseltonvillas.com/', NULL, NULL, 1, '2020-09-23 10:18:51', '2020-09-23 10:18:59'),
(54, '4730b3fe7906fd2869e255010aa17299', '9619819c11170ebf2f4785ca71e9d4bf', '1600856412.png', 'https://www.facebook.com/JadiGroup/', NULL, NULL, 1, '2020-09-23 10:20:12', '2020-09-23 10:20:17');

-- --------------------------------------------------------

--
-- Table structure for table `live_share`
--

CREATE TABLE `live_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `live_share`
--

INSERT INTO `live_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '4d1bbbed461a9ec43995d606504a52eb', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'City of Dream Penang', 'Ewein Properties', 'Youtube', 'LFzduCN2QWk', 'Available', 1, '2020-09-11 07:11:48', '2020-09-11 07:11:48'),
(2, '1faa1f4f2ef38a04072647e32b0d3910', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Ferringhi Residence 2 Show Gallery Tour', 'Mah Sing Group', 'Youtube', '3lXggG62M1I', 'Available', 1, '2020-09-14 09:39:33', '2020-09-14 09:39:33'),
(3, '8a7400bf2233688947afffb31f81b85c', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Hunza', 'Hunza', 'Youtube', 'FLSRWnJVnJ8', 'Available', 1, '2020-09-18 10:22:43', '2020-09-18 10:22:43'),
(4, '30ff0d14bfef69e18a95536dd311075b', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '', '', 'Temporarily', '', 'Delete', 3, '2020-09-21 03:46:16', '2020-09-21 03:46:16'),
(5, 'cecb3265fecfe28a5c895cf9bce45778', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Royal Height', '', 'Temporarily', '', 'Delete', 3, '2020-09-21 03:53:43', '2020-09-21 03:53:43'),
(6, 'c9d098df060138f3780a986ac532cf2e', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '', '', 'Temporarily', 'Club house-day-e2.jpg', 'Delete', 3, '2020-09-21 04:59:10', '2020-09-21 04:59:10'),
(7, '2ef2eb03c070cae43dc15da15f9b9a02', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen Img 1', 'Aspen Group', 'Temporarily', 'Royale Heights - DSTH OPTION 1.jpg', 'Delete', 3, '2020-09-21 05:01:53', '2020-09-21 05:01:53'),
(8, '91df29724cebd017839d8e4164357891', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', '', '', 'Temporarily', 'rsz_vivo_webimage-1.jpg', 'Delete', 3, '2020-09-21 05:19:19', '2020-09-21 05:19:19'),
(10, 'b2251b0c621e18cbd4b5bbe041cc358f', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Tmn Jadi  G1', 'Tmn Jadi  G1', 'Temporarily', 'Leon Lee.jpeg', 'Delete', 3, '2020-09-21 05:35:25', '2020-09-21 05:35:25'),
(11, 'cd5355a83db717762d4884a2e74c3848', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '', '', 'Temporarily', '2str ter-e12.jpg', 'Delete', 3, '2020-09-21 06:12:47', '2020-09-21 06:12:47'),
(12, '6ddc50d01f61aa55f2648ddb50d12fc0', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Youtube', '6fVywrlozOY', 'Available', 1, '2020-09-21 06:15:03', '2020-09-21 06:15:03'),
(13, '2efcea7d7b44a63dc1aed1774a4f95f3', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Temporarily', 'rsz_vivo_webimage-1.jpg', 'Delete', 3, '2020-09-21 06:20:26', '2020-09-21 06:20:26'),
(14, 'b2ba4f34d18117bff89ea8ea3831d4d8', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'slider-1.jpg', 'Delete', 3, '2020-09-21 06:21:45', '2020-09-21 06:21:45'),
(15, '29e5e534f0c11997fbea5c2e91b374d6', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard 2-100.jpg', 'Delete', 3, '2020-09-21 06:34:10', '2020-09-21 06:34:10'),
(16, '19b4006ee7f81afc42dc0f1cdf34f0d7', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard-2-100s.jpg', 'Delete', 3, '2020-09-21 06:36:34', '2020-09-21 06:36:34'),
(17, '26d018171c5225edb7114698136742ea', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Temporarily', 'Artboard-2-100s2.jpg', 'Delete', 3, '2020-09-21 06:38:44', '2020-09-21 06:38:44'),
(18, 'f3b42a8beeb8ccccc61242a4435dc61d', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Temporarily', 'rsz_vivo_webimage-1.jpg', 'Delete', 3, '2020-09-21 07:38:04', '2020-09-21 07:38:04'),
(19, '3ac79e9b72e697993146d473ac2a57ed', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Temporarily', 'rsz_vivo_webimage-1a.jpg', 'Delete', 3, '2020-09-21 07:39:23', '2020-09-21 07:39:23'),
(20, '1728369f96d6fabe7b76368462242b67', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', 'Royale-Infinity22.jpg', 'Delete', 3, '2020-09-21 07:56:35', '2020-09-21 07:56:35'),
(21, '1dfff7e7cceedd6372fba22f7b919aa0', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Youtube', 'lnfn6U1ihgI', 'Available', 1, '2020-09-21 07:58:04', '2020-09-21 07:58:04'),
(22, 'b8cdbd533146f3c617ef251c03a35d49', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Berjaya Land Berhad', 'Youtube', 'GxgVlvtFG08', 'Available', 1, '2020-09-21 08:04:48', '2020-09-21 08:04:48'),
(23, 'e2029197d6afdfec1370d45d5c641d94', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen', 'Aspen', 'Youtube', 'E-RdJolDAx8', 'Available', 1, '2020-09-21 09:42:56', '2020-09-21 09:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `lucky_draw`
--

CREATE TABLE `lucky_draw` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lucky_draw`
--

INSERT INTO `lucky_draw` (`id`, `uid`, `name`, `email`, `contact`, `state`, `location`, `user_type`, `date_created`, `date_updated`) VALUES
(1, 'f63e88a40653696573bee46aa31f840b', 'oliver', 'oliver@gmail.com', '+6012-7788997', 'Negeri Sembilan', '', 1, '2020-09-24 02:25:08', '2020-09-24 02:25:08'),
(2, '08df847c987f9422919fd76ffe348303', 'barry', 'barry@gmail.com', '6012-7744114', '', 'Ethiopia', 1, '2020-09-24 02:27:23', '2020-09-24 02:27:23');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `message_uid` varchar(255) DEFAULT NULL,
  `receive_message` varchar(255) DEFAULT NULL,
  `reply_message` varchar(255) DEFAULT NULL,
  `reply_one` varchar(255) DEFAULT NULL,
  `reply_two` varchar(255) DEFAULT NULL,
  `reply_three` varchar(255) DEFAULT NULL,
  `user_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, READ = user read sms',
  `admin_status` varchar(255) DEFAULT NULL COMMENT 'GET = user get sms, REPLY = admin reply sms',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `platform`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Youtube', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'Facebook', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'Zoom', 'Stop', 2, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'UStream', 'Stop', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'Temporarily', 'Available', 1, '2020-09-21 03:16:03', '2020-09-21 03:16:03');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sharing`
--

CREATE TABLE `sharing` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_two` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `offline` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sharing`
--

INSERT INTO `sharing` (`id`, `uid`, `user_uid`, `username`, `title`, `title_two`, `host`, `platform`, `link`, `remark`, `file`, `whatsapp`, `phone`, `offline`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '0568ec8f367619c2b88de85bddbbefbb', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Booth 1', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Staff 1', '1600078274staff1.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 10:11:14', '2020-09-14 10:11:14'),
(2, '3f2cbf179332fa20966a0566aea76c34', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Booth 2', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Staff 2', '1600078300staff2.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 10:11:40', '2020-09-14 10:11:40'),
(3, '0c53d3ea681f4ac8201f835ed1e3a1af', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Master of Project', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Vincent Koay 郭洺神', '1600080566vincent.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 10:49:26', '2020-09-14 10:49:26'),
(4, '0b175cc1fd8a2f644ac2306b728f9d45', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Business Development', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Carol Cheah 谢晓双', '1600080699carol.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 10:51:39', '2020-09-14 10:51:39'),
(5, '419b6ef81d8f400e6b170765e47c0c20', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Master of Project', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Vincent Koay 郭洺神', '1600081216vincent.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:00:16', '2020-09-14 11:00:16'),
(6, '3ef00204e5e1cf470ecba8e2d73146d4', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Business Development', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Carol Cheah 谢晓双', '1600081249carol.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:00:49', '2020-09-14 11:00:49'),
(7, '2f562529389d7bfd0ecf2a7b0e435c55', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Assistant Manager Sales and Marketing', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Allycia Ng', '16007518511600081528allycia.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:05:28', '2020-09-14 11:05:28'),
(8, '126b5854cc6d607ed0b07cde27798e7e', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售助理经理  Assistant Manager Sales and Marketing', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '吕汶霓  Cheryl Loo', '16007518851600081955cheryl.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:12:35', '2020-09-14 11:12:35'),
(9, '7a9a62150b7693e75c454ea6c4342509', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '(853998) 市場销售高級经理  Senior Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/95851925255?pwd=VHhkWTRPamlWYzZWcEx2V0tZdmtVUT09', '周芷芸  Chew Zhi Ying', '16007519011600082118chewzhiying.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:15:18', '2020-09-14 11:15:18'),
(10, 'a3161c89ff2a07393e7e6a4bf4642079', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Assistant Manager Sales and Marketing', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Jesphin Goh', '1600082265jesphin.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:17:45', '2020-09-14 11:17:45'),
(12, '53834cd15e9906705104b57ff38ae86f', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '(701949) 市場销售高級经理  Senior Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/94498234464?pwd=b3luR3VUR2VDK21tTVhLTHk1amxPdz09', '张嘉薏  Joey Teoh Kar Yee', '1600082459joeyteoh.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:20:59', '2020-09-14 11:20:59'),
(14, '7e61c053fa255c62b01880a076d687f7', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'Assistant Manager Sales and Marketing', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Jesphin Goh', '1600082827jesphin.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:27:07', '2020-09-14 11:27:07'),
(15, '3c5ab31fffa64c587717a27c25569ad2', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '(649498) 市場销售助理经理  Assistant Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/99471383604?pwd=OC8vSUFNMXhRQitaR2lQY01XSk9Cdz09', '林晓琪  Lim Shiau Qi', '16007519231600082852limshia.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-14 11:27:32', '2020-09-14 11:27:32'),
(16, 'deb4f58ee57742ee3b7327bfab93f3d6', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Property Advisor 产业销售顾问', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Shawn 方志松', '1600425805Shawn-Hong,-Mah-Sing-Property-Advisor-.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-18 10:43:25', '2020-09-18 10:43:25'),
(17, 'b52e2f809ab3c7f1eeb5f7ffa3784fac', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Mah Sing Property Advisor 产业销售顾问', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Alan 谢其伦', '1600425844Alan-Cheah,-Mah-Sing-Property-Advisor-.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-18 10:44:04', '2020-09-18 10:44:04'),
(18, '1574ba13ae6343776d47a04db70a23c6', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Senior Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Katherine Cheang', '1600483641Katherine-Cheang3.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-19 02:47:22', '2020-09-19 02:47:22'),
(19, 'ea27c1d94b1f82faa0c6d620ee8963ad', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Senior Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Ang Khwang Harn', '1600483680Ang-Khwang-Harn2.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-19 02:48:00', '2020-09-19 02:48:00'),
(20, '013f71749e7f935467b3cfa28afec2dd', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Ng Wei Ping', '1600483706Ng-Wei-Ping2.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-19 02:48:26', '2020-09-19 02:48:26'),
(21, '8de7fbbf2c73a380bb474358a3d01e4e', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Kellie Leen', '1600483734Kellie-Leen2.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-19 02:48:54', '2020-09-19 02:48:54'),
(22, '5a8ae874afa312426dec10ddfd1bd6a2', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Dickson Lau', '1600483760Dickson-Lau2.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-19 02:49:20', '2020-09-19 02:49:20'),
(23, 'a4c6caa0cbbd8864650e0ba1d3a07a16', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Director Business Development 业务发展总监', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Vincent Koay 郭洺神', '16006518471600080566vincent.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-21 01:30:47', '2020-09-21 01:30:47'),
(24, '40eb35da467e4591978943b31dea33da', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Director Business Development 业务发展总监', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Carol Cheah 谢晓双', '16006518901600080699carol.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-21 01:31:30', '2020-09-21 01:31:30'),
(25, '08362a4311821d22c140fe193937f312', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', '-', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'PICC', '1600657846picc-tower.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-21 03:10:46', '2020-09-21 03:10:46'),
(26, '37ec9c1bf6d4ccd5c08e6e7c0bf1a607', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', '-', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Muze Grand Entrance', '160065795001 Muze Grand Entrance.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-21 03:12:30', '2020-09-21 03:12:30'),
(28, '5ab04340ff78478ed9d0942db7d0e9f6', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', '-', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'PICC Medical Center', '160066262403 PICC Medical Center.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:30:24', '2020-09-21 04:30:24'),
(29, 'ac3d664ed9a0d2630afa427048d7f5c8', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', '-', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'BBQ Area', '1600664850staff1.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-21 05:07:30', '2020-09-21 05:07:30'),
(30, 'de33d8ab95734c6203469d1f9f5712d2', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', '-', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Swimming Pool', '1600664881staff2.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-21 05:08:01', '2020-09-21 05:08:01'),
(31, 'd79478855bb7be8a10cfd277f8105b44', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Sales Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665000booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:10:00', '2020-09-21 05:10:00'),
(32, '8119abd7b8d1d94e1ee82b5456c12d5b', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Marketing Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665052booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:10:52', '2020-09-21 05:10:52'),
(33, 'cdee25f87b3d0fb16d1f31f059488836', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Business Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665083booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:11:23', '2020-09-21 05:11:23'),
(34, 'b1a25f825b1d9caee42a245d37db20e9', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665161booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:12:41', '2020-09-21 05:12:41'),
(35, '0efad2c2b73bb7c86a13ee135c3c8010', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Marketing Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665182booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:13:02', '2020-09-21 05:13:02'),
(36, 'e171bda4bdced4a12598171d7fc2f200', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Business Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665200booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:13:20', '2020-09-21 05:13:20'),
(37, '09e931e3e441ec2779aa050495309581', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665261booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:14:21', '2020-09-21 05:14:21'),
(38, '251d0bf65d674447bc1e4ffa626758cc', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Marketing Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665285booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:14:45', '2020-09-21 05:14:45'),
(39, '5963b4bb12c09dd7e464e3fadcac32c1', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Business Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665305booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:15:05', '2020-09-21 05:15:05'),
(40, 'fc5d990345525a7796bf4ed675a9d385', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Sales Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665393booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:16:33', '2020-09-21 05:16:33'),
(41, '2b959fa14904991abcf892adf5e315d3', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Marketing Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665415booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:16:55', '2020-09-21 05:16:55'),
(42, '4d90c5e18e0c9c07c06a5770a531c8a4', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Business Dept', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Virtual Booth', '1600665436booth.png', NULL, NULL, NULL, 'Stop', 2, '2020-09-21 05:17:16', '2020-09-21 05:17:16'),
(43, '334d8b6bf58c76bd4d81b8d5b36f812a', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Senior Sales Manager', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Daniel Ng 黄添俊', '1600673642daniel.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-21 07:34:02', '2020-09-21 07:34:02'),
(44, '13c420d16694fba842f3f5274c6b3a2c', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Sales Consultant', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Chuah Thean Cheat 蔡展捷', '1600673742chua.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-21 07:35:42', '2020-09-21 07:35:42'),
(45, '64cac3ea89f95afd67115c9661379bfc', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Area Sales Manager', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Doris Ericia 吴佳怡', '1600673775doris.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-21 07:36:15', '2020-09-21 07:36:15'),
(46, '7a07cbe87b248b797ba274c8fa5d3ad8', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Sales Consultant', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Ashley Wee 黄靖琇', '1600673798ashley.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-21 07:36:38', '2020-09-21 07:36:38'),
(47, 'ab056a73ef919ab2dac670e6bce9e202', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Senior Sales Consultant', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'Enson Lim 林成发', '1600673826enson.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-21 07:37:06', '2020-09-21 07:37:06'),
(48, 'b2f2c9c0ba703f1590033d6416a73b66', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', NULL, '', '', 'https://api.whatsapp.com/send?phone=60182333927', 'JEN', '1600745267Jen.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-22 03:27:47', '2020-09-22 03:27:47'),
(49, '23035d68eb654cd2a097f1bb9b65aff6', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', NULL, '', '', 'https://api.whatsapp.com/send?phone=60183668223', 'JO ANN', '1600745300Jo Ann.jpg', NULL, NULL, NULL, 'Delete', 3, '2020-09-22 03:28:20', '2020-09-22 03:28:20'),
(50, '236d09da68bdb85600036e93407dd81d', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', NULL, '', '', 'https://api.whatsapp.com/send?phone=60183701991', 'EUNICE', '1600745320Eunice.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:28:40', '2020-09-22 03:28:40'),
(51, '915b4ea0f019fd1bc9e61fe9b06aff34', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Senior Sales &amp; Marketing Executive', NULL, '', '', 'https://api.whatsapp.com/send?phone=60194184663', 'Ooi Lai Teik', '16007513741a.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:46:14', '2020-09-22 03:46:14'),
(52, '7710a4bf513db62755ccefd650b3437e', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Senior Sales &amp; Marketing Executive', NULL, '', '', 'https://api.whatsapp.com/send?phone=60134284663', 'Tan Kher Suan', '16007513912a.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:47:00', '2020-09-22 03:47:00'),
(53, '88511df01e2258a383a490cba32e7595', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Sales &amp; Marketing Executive', NULL, '', '', 'https://zoom.us/j/91490396021?pwd=bWF6V3A2WmZZbjZEUFE3SWhhYmVZdz09', 'Vicky Tan  (193371)', '16007513983a.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:47:33', '2020-09-22 03:47:33'),
(54, 'f032ea38f89ace571b89de3c7e409a50', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林瑞端 Joselyn Lim', '1600752552josel.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:52:29', '2020-09-22 03:52:29'),
(55, 'd9ef4e7022a2cf19cfc8ca2ad273b6e9', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '曾垂聪 Allan Chan', '1600752569allan.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:53:22', '2020-09-22 03:53:22'),
(56, 'b8a321d971d28856f4d466288788c44c', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '沈彩颜 Vivian Sim', '1600752591ee.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:53:57', '2020-09-22 03:53:57'),
(57, 'fd6f6095a3291ad1e5141958233d3117', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '谢瑞媚  Swee Mi', '1600752605--.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:54:27', '2020-09-22 03:54:27'),
(58, '056904639b71d5cd98f5156077c92435', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '吴志勤 Sunny Goh', '1600752625sunny.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:55:07', '2020-09-22 03:55:07'),
(59, '313e7a9bb2b6190874de0ef7076a1eee', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林炜阳 Lim Wei Yang', '1600752637lim-weiyang.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:55:39', '2020-09-22 03:55:39'),
(60, 'c70a6798cdcecb42e82fbfbc7c6712bf', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '林荣祖 Matthew Lim', '1600752649math.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:56:11', '2020-09-22 03:56:11'),
(61, '7bb2a0664ada86e3c4b74db4b8bed306', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '周小彦 Vivian Chow', '1600752661vivian-chow.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:57:57', '2020-09-22 03:57:57'),
(62, '8c541d7aaf88b5baa220a7dead148844', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'Sales Executive', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', '王征熙 Ivan Ong', '1600752674-------------.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 03:58:21', '2020-09-22 03:58:21'),
(63, 'f5371a61510a2b254e678daa1d215a65', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', NULL, '', '', 'https://api.whatsapp.com/send?phone=60183668223', 'JO ANN', '1600759145Jo Ann.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 07:19:05', '2020-09-22 07:19:05'),
(64, '27be7306c088fb2278fd0d4dc31d711e', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Sales Specialist 销售专员', NULL, '', '', 'https://api.whatsapp.com/send?phone=60182333927', 'JEN', '1600759190Jen.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 07:19:50', '2020-09-22 07:19:50'),
(65, '702d19453c5111a3869e97f4b26914a8', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Property Negotiator 房地产中介协调员', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'ANJO Tan 陈美娴', '1600777744mahsing.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 09:34:17', '2020-09-22 09:34:17'),
(66, 'c620dbb308473adb7cc1b5d2125b8e9b', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Manager Property Negotiation房地产协商经理', NULL, '', '', 'https://us04web.zoom.us/j/78702977780?pwd=cTh4NDg0bUpQL05TN1BQWjJ5RFI4Zz09', 'GARY Thor 涂敬新', '1600777764mahsing2.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-22 09:34:40', '2020-09-22 09:34:40'),
(67, 'a37787064b8518b90ea9cc5a7c7ed119', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售高級经理', 'Senior Manager Sales and Marketing', '', '', 'https://zoom.us/j/91594568524?pwd=dThuNnJFckxuUkh6cDlGQnhTeDcvUT09', '周芷芸 Chew Zhi Ying', '1600836993Chew Zhi Ying - Senior Manager Sales and Marketing.jpg', '0164663107', '0164663107', '111222zoom', 'Available', 1, '2020-09-23 04:56:33', '2020-09-23 04:56:33'),
(68, '8ac2fb0515b7291880cb456c2d380118', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售高級经理 Senior Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/98786085369?pwd=RjBkMEZrY3NlVlZiYzk0UkFydTB4QT09', '江思韵 Shereen Kung', '1600847054Shereen Kung - Senior Manager Sales  _ Marketing.jpg', NULL, NULL, NULL, 'Stop', 2, '2020-09-23 07:44:14', '2020-09-23 07:44:14'),
(69, 'b30cbfeaf2bf3df53230fcccfe53af11', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售高級经理 Senior Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/96386039517?pwd=VnBIRi9IU2t6ck1mdkJseG5KM0F6UT09', '张嘉薏 Joey Teoh Kar Yee', '1600848466Joey Teoh - Senior Manager Sales  And Marketing.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-23 08:07:46', '2020-09-23 08:07:46'),
(70, '631f0f60f332cb9f6f8254d153d74a10', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售助理经理 Assistant Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/95591800012?pwd=aWJoM05CUUJCRmxsRWQ5MkJRSXg3QT09', '吕汶霓 Cheryl Loo', '1600851519Cheryl Loo - Assistant Manager  Sales and Marketing.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-23 08:18:34', '2020-09-23 08:18:34'),
(71, '669aced358b61a0a73ea0275d33a9db1', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售助理经理 Assistant Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/92640336314?pwd=Q0FERlVMMnZOOTc4UlB0R2xpUUI0dz09', '林晓琪 Lim Shiau Qi', '1600849635Lim Shiau Qi - Assistant Manager  Sales and Marketing.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-23 08:27:15', '2020-09-23 08:27:15'),
(72, '61ea0dee0c7e6263ed6a4e40c15ef84c', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', '市場销售助理经理 Assistant Manager Sales and Marketing', NULL, '', '', 'https://zoom.us/j/96630547936?pwd=dnBzMUVscDBpbFR0UzlTRFNqOVBKUT09', '陈紫薇 Iris Tan', '1600849906Iris Tan - Assistant Manager Sales  and Marketing.jpg', NULL, NULL, NULL, 'Available', 1, '2020-09-23 08:31:46', '2020-09-23 08:31:46'),
(73, '741b0a9ddd8e0975ded85e7a0c0a3b48', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'AAA Ch', 'BBB En', '', '', 'https://zoom.us/j/91594568524?pwd=dThuNnJFckxuUkh6cDlGQnhTeDcvUT09', 'CCC Name', '160087500386721714_489468691990267_5129452383318835200_n.jpg', '01159118132', '01159118132', '111222', 'Available', 1, '2020-09-23 15:30:03', '2020-09-23 15:30:03');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `state_name`, `date_created`, `date_updated`) VALUES
(1, 'Johor', '2020-05-22 06:42:19', '2020-05-29 01:44:12'),
(2, 'Kedah', '2020-05-22 06:42:48', '2020-05-29 01:44:08'),
(3, 'Kelantan', '2020-05-22 06:42:48', '2020-05-29 01:44:03'),
(4, 'Negeri Sembilan', '2020-05-22 06:43:46', '2020-05-29 01:43:59'),
(5, 'Pahang', '2020-05-22 06:43:46', '2020-05-29 01:43:55'),
(6, 'Perak', '2020-05-22 06:44:16', '2020-05-29 01:43:52'),
(7, 'Perlis', '2020-05-22 06:44:16', '2020-05-29 01:43:50'),
(8, 'Selangor', '2020-05-22 06:44:37', '2020-05-29 01:43:45'),
(9, 'Terengannu', '2020-05-22 06:44:37', '2020-05-29 01:43:40'),
(10, 'Malacca', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(11, 'Penang', '2020-05-22 06:44:48', '2020-05-22 06:44:48'),
(12, 'Sabah ', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(13, 'Sarawak', '2020-05-22 06:44:58', '2020-05-22 06:44:58'),
(14, 'Kuala Lumpur', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(15, 'Labuan', '2020-05-22 06:48:57', '2020-05-22 06:48:57'),
(16, 'Putrajaya', '2020-05-22 06:49:05', '2020-05-22 06:49:05');

-- --------------------------------------------------------

--
-- Table structure for table `sub_share`
--

CREATE TABLE `sub_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `title_two` varchar(255) DEFAULT NULL,
  `host_two` varchar(255) DEFAULT NULL,
  `platform_two` varchar(255) DEFAULT NULL,
  `link_two` text DEFAULT NULL,
  `remark_two` text DEFAULT NULL,
  `file_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_share`
--

INSERT INTO `sub_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `remark`, `file`, `title_two`, `host_two`, `platform_two`, `link_two`, `remark_two`, `file_two`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '9b79e78e32e0214e860cdb89b5293e36', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'City of Dream Penang', 'Ewein Properties', 'Youtube', 'LFzduCN2QWk', 'https://my.matterport.com/show/?m=WZfy6SNApdB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-11 07:15:14', '2020-09-11 07:15:14'),
(2, '0da47871acb2521b35016a8472ab963d', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Ferringhi Residence 2 with VO HD 1080p', 'Mah Sing Group', 'Youtube', 'EkMVnIr7HUA', 'https://ferringhiresidence2.com/360.php', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-09-14 09:41:23', '2020-09-14 09:41:23'),
(3, '11a07fb014a095b60663ebd555d6345c', '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'Ferringhi Residence 2 with facilities', 'Mah Sing Group', 'Youtube', 'Hu4gvtoi-eg', 'https://ferringhiresidence2.com/360.php', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Stop', 2, '2020-09-14 09:42:05', '2020-09-14 09:42:05'),
(4, '55b0555488c08356ab77b6683f045207', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'PICC', 'Hunza', 'Youtube', 'FLSRWnJVnJ8', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Stop', 2, '2020-09-18 10:23:19', '2020-09-18 10:23:19'),
(5, 'a298cae922e091b303349faaeb7d1226', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'PICC', 'Hunza', 'Youtube', 'GjypSzfkE3c', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Available', 1, '2020-09-18 10:24:05', '2020-09-18 10:24:05'),
(6, 'c175acbb4f8d05020807032d48bfe578', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Orange BM Night', '', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:02:38', '2020-09-21 04:02:38'),
(7, '57881803a3e044ba3f9d26926f8e040f', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Royale Heights', '', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:03:18', '2020-09-21 04:03:18'),
(8, '7a802af889b8ac9fd97456854af3991d', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', '1', 'Tah Wah', 'Temporarily', '', '', '16006617532str-bung-CD1 small.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Delete', 3, '2020-09-21 04:15:53', '2020-09-21 04:15:53'),
(13, '216dac7771cf383e2900afc916627b33', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah Test 1', 'Tah Wah Test 1', 'Temporarily', 'Tah Wah Test Link 1', '360', '', NULL, NULL, NULL, NULL, NULL, '1600671661Stephen Kam.JPG', 'Delete', 3, '2020-09-21 07:01:01', '2020-09-21 07:01:01'),
(14, 'e16766a6eb6143d98db2319496441c97', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'HH Park Residence', 'Aspen Group', 'Youtube', 's9rKZB3Vj_g', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Stop', 2, '2020-09-21 07:04:09', '2020-09-21 07:04:09'),
(15, 'd4cd5fd8442e5fa574bf6473b6db029d', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Tri Pinnacle', 'Aspen Group', 'Temporarily', '', 'https://my.matterport.com/show/?m=PXbwhSJzzq5', '', NULL, NULL, NULL, NULL, NULL, '1600671968tri.jpg', 'Stop', 2, '2020-09-21 07:06:08', '2020-09-21 07:06:08'),
(16, '406947a43f074a30d4a581dca6090645', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD1', 'Ewein Properties1', 'Youtube', '5vjSJVbY930', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067218214007_COD_Lobby V5_20190822.jpg', 'Available', 1, '2020-09-21 07:09:42', '2020-09-21 07:09:42'),
(17, '23124ddcbe6b9625108912f15cdc6e3e', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Youtube', 'f3BiYc3SUBQ', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067223214007_COD_Lobby V1A_20191218.jpg', 'Available', 1, '2020-09-21 07:10:32', '2020-09-21 07:10:32'),
(18, 'c455a0d549f35a5e283f486ee7e688ec', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Temporarily', '', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067225414007_COD_Lobby V4a_20190822.jpg', 'Stop', 2, '2020-09-21 07:10:54', '2020-09-21 07:10:54'),
(19, '2ca32bf90b2ff7ebc5f6fd3eb7f5bf41', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Vertu Resort', 'Aspen Group', 'Temporarily', '_Jv5-43YWI0', '', '', NULL, NULL, NULL, NULL, NULL, '1600681413vertu.jpg', 'Delete', 3, '2020-09-21 07:15:20', '2020-09-21 07:15:20'),
(20, '7bbea90c024d1f15083b2b385026952c', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Temporarily', '', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160067417914007_COD Ballroom V1_20200219.jpg', 'Stop', 2, '2020-09-21 07:42:59', '2020-09-21 07:42:59'),
(21, 'a31a908cc0b9224c8226a4ef9370cadf', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Delete', 3, '2020-09-21 07:48:40', '2020-09-21 07:48:40'),
(22, '7b017c119e97082cb69fc52b3181bc67', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', 'https://kuula.co/share/collection/7PY6x?fs=1&amp;amp;vr=0&amp;amp;zoom=1&amp;amp;sd=1&amp;amp;initload=0&amp;amp;thumbs=1&amp;amp;chromeless=1&amp;amp;logo=-1', '', '', NULL, NULL, NULL, NULL, NULL, '1600674554Club house-day-e2.jpg', 'Delete', 3, '2020-09-21 07:49:14', '2020-09-21 07:49:14'),
(23, 'e3334992cf706a4a5dc2bd05596cfb3f', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', 'https://kuula.co/share/collection/7PY6x?fs=1&amp;amp;vr=0&amp;amp;zoom=1&amp;amp;sd=1&amp;amp;initload=0&amp;amp;thumbs=1&amp;amp;chromeless=1&amp;amp;logo=-1', '', '', NULL, NULL, NULL, NULL, NULL, '16006747582str-bung-CD1 small.jpg', 'Delete', 3, '2020-09-21 07:52:38', '2020-09-21 07:52:38'),
(24, '02d5bbe905b50ba227d2eb08c36380fa', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', 'https://kuula.co/share/collection/7PY6x?fs=1&amp;amp;vr=0&amp;amp;zoom=1&amp;amp;sd=1&amp;amp;initload=0&amp;amp;thumbs=1&amp;amp;chromeless=1&amp;amp;logo=-1', 'https://kuula.co/share/collection/7PY6x?fs=1&amp;vr=0&amp;zoom=1&amp;sd=1&amp;initload=0&amp;thumbs=1&amp;chromeless=1&amp;logo=-1', '', NULL, NULL, NULL, NULL, NULL, '1600674843Pool-morning-e13.jpg', 'Available', 1, '2020-09-21 07:54:03', '2020-09-21 07:54:03'),
(25, '55751cd87a7c79ebb7c8587fa11d6b14', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Tah Wah', 'Tah Wah', 'Temporarily', 'https://kuula.co/share/collection/7PY6x?fs=1&amp;amp;vr=0&amp;amp;zoom=1&amp;amp;sd=1&amp;amp;initload=0&amp;amp;thumbs=1&amp;amp;chromeless=1&amp;amp;logo=-1', '', '', NULL, NULL, NULL, NULL, NULL, '16006748733str Semi-D.jpg', 'Delete', 3, '2020-09-21 07:54:33', '2020-09-21 07:54:33'),
(26, '5046eecc1b6551cca4c6c1675be43a41', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Youtube', 'F0_pV1qN37Y', 'http://gmvec.com/index2.php', '', NULL, NULL, NULL, NULL, NULL, '1600675123staff1.jpg', 'Available', 1, '2020-09-21 07:58:43', '2020-09-21 07:58:43'),
(27, 'a0cc29ebd124e62e7a6cbf6ebbaa9886', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Youtube', 'IP6wRZ4O-n0', '', '', NULL, NULL, NULL, NULL, NULL, '1600851006Taman Jadi c .jpg', 'Available', 1, '2020-09-21 07:58:57', '2020-09-21 07:58:57'),
(28, '82612d0025b4e04edad9c425114dc769', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Youtube', 'MR0SS8WCWhs', '', '', NULL, NULL, NULL, NULL, NULL, '1600675152staff3.jpg', 'Available', 1, '2020-09-21 07:59:12', '2020-09-21 07:59:12'),
(29, '8d3a07069b1293fdb484fe14898f4b6d', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'PICC', 'Hunza', 'Facebook', '923178258168840', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-22 04:27:12', '2020-09-22 04:27:12'),
(30, '2f1c6ed19fe5d502c64f953f910cd340', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Vertu Resort', 'Aspen Group', 'Youtube', '_Jv5-43YWI0', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Stop', 2, '2020-09-23 07:48:38', '2020-09-23 07:48:38'),
(31, '34e758021d931073c0b508eb34486ec7', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'The Muze', 'Hunza Properties Berhad', 'Youtube', 'vtYn_jJ6xFE', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, '160085006213 Duplex - Balcony 2nd Bridge View.jpg', 'Available', 1, '2020-09-23 08:34:22', '2020-09-23 08:34:22'),
(32, 'dfcfb65b51c303127a70919050dc9148', '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'Taman Jadi Group', 'Taman Jadi Group', 'Temporarily', '', '', '', NULL, NULL, NULL, NULL, NULL, '1600852638taman jadi win myvi.jpg', 'Available', 1, '2020-09-23 09:17:18', '2020-09-23 09:17:18'),
(33, '97771e54a42ba2777c3481e9eead0d12', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya full n short video', 'Berjaya Land Berhad', 'Youtube', '7x_2ydJ-E94', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 14:21:27', '2020-09-23 14:21:27'),
(34, '8cf886d63e7812ade288515a47235016', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD', 'Ewein Properties', 'Temporarily', 'https://my.matterport.com/show/?m=WZfy6SNApdB', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '160085550814007_COD Ballroom V1_20200219.jpg', 'Available', 1, '2020-09-23 09:56:37', '2020-09-23 09:56:37'),
(35, '77a52abda963e96f0ef3cf8ded177fc9', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Aspen Vision City', 'Aspen Group', 'Youtube', 'E-RdJolDAx8', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 09:57:55', '2020-09-23 09:57:55'),
(36, 'beb66b376cc1b00a229ac14547e4073c', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Vertu Resort (wrong)', 'Aspen Group', 'Youtube', '_Jv5-43YWI0', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 09:58:29', '2020-09-23 09:58:29'),
(37, 'c76cb14cb4adb46f444b48f0760c5582', 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'Vertu Resort', 'Aspen Group', 'Youtube', '_Jv5-43YWI0', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 10:00:04', '2020-09-23 10:00:04'),
(38, 'fe8569c63872825985880b58ac8b3cd4', 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'The Muze', 'Hunza Properties Berhad', 'Temporarily', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', 'http://www.picc-penang.com/The%20Muze%20ShowUnit%20VR/Type%20B/TypeB.html', '', NULL, NULL, NULL, NULL, NULL, '160085586713 Duplex - Balcony 2nd Bridge View.jpg', 'Available', 1, '2020-09-23 10:11:07', '2020-09-23 10:11:07'),
(39, 'f6fdad24960ad4a4260a239f9f4d5f8c', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Location Map', 'Berjaya Land Berhad', 'Temporarily', '', '', '1600856473FA_KG Location Map_OL-01.jpg', NULL, NULL, NULL, NULL, NULL, '', 'Delete', 3, '2020-09-23 10:21:13', '2020-09-23 10:21:13'),
(40, '0bb0b7af737768b14c3cfcf625719038', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Berjaya Group Pop Out', 'Berjaya Land Berhad', 'Temporarily', 'https://jesseltonvillas.com/', '', '', NULL, NULL, NULL, NULL, NULL, '1600852658berjaya-new-pop.jpg', 'Available', 1, '2020-09-23 09:17:38', '2020-09-23 09:17:38'),
(41, 'a202f2a6178222af05ebd6c844391ec5', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Location Map', 'Berjaya Land Berhad', 'Temporarily', 'https://i.postimg.cc/LXtYWCWs/FA-KG-Location-Map-OL-01.jpg', '', '', NULL, NULL, NULL, NULL, NULL, '1600861271FA_KG Location Map_OL-01.jpg', 'Available', 1, '2020-09-23 10:22:28', '2020-09-23 10:22:28'),
(42, '662181b4b383d43da4bd2033e7a45b3b', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Orange Park', 'Tah Wah Group', 'Youtube', 'https://youtu.be/d1eP2rvGyac', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Delete', 3, '2020-09-23 13:39:12', '2020-09-23 13:39:12'),
(43, '14f08ac8e9ca0fee9b671b2a29a9c1ea', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Orange Park', 'Tah Wah Group', 'Youtube', 'd1eP2rvGyac', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 13:42:12', '2020-09-23 13:42:12'),
(44, 'e15a2ea2c87cdb1ca717d8e1697eb95c', 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'Orange BM', 'Tah Wah Group', 'Youtube', 'm3Ggi2z1d-Y', '', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 13:42:46', '2020-09-23 13:42:46'),
(45, '4aac76c380522fa029ce1ffa0f017933', 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'COD Model House', 'Ewein Properties', 'Youtube', '3D-DBu5T9M4', 'https://my.matterport.com/show/?m=WZfy6SNApdB', '', NULL, NULL, NULL, NULL, NULL, '', 'Available', 1, '2020-09-23 13:57:38', '2020-09-23 13:57:38'),
(46, 'da36959931b1300469eb8ed5a3bc673a', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Lake Brighten', 'Berjaya Land Berhad', 'Youtube', '7x_2ydJ-E94', '', '', NULL, NULL, NULL, NULL, NULL, '1600869838Lake Brighten.jpg', 'Delete', 3, '2020-09-23 14:02:57', '2020-09-23 14:02:57'),
(47, 'af1da15bbb752bc6e9b4c713a1c2efa0', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Aerial View', 'Berjaya Land Berhad', 'Temporarily', 'https://i.postimg.cc/VsSL5Hg0/Aerial-View-latest.jpg', '', '', NULL, NULL, NULL, NULL, NULL, '1600868621Aerial View.jpg', 'Available', 1, '2020-09-23 13:31:42', '2020-09-23 13:31:42'),
(48, '2b34b0b7c7e9e09eeb4df40d9a2dfaf1', '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'Aerial Super Impost', 'Berjaya Land Berhad', 'Temporarily', 'https://i.postimg.cc/MZf4XX3x/Aerial-Super-Impost-V3.jpg', '', '', NULL, NULL, NULL, NULL, NULL, '1600870106Aerial-Super-Impost-V3.jpg', 'Available', 1, '2020-09-23 14:08:26', '2020-09-23 14:08:26');

-- --------------------------------------------------------

--
-- Table structure for table `title`
--

CREATE TABLE `title` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `title`
--

INSERT INTO `title` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'WELCOME TO LIVE-STREAMING PLATFORM', 'Available', 1, '2020-07-08 01:18:45', '2020-07-08 01:18:45');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `broadcast_live` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `title` varchar(255) DEFAULT NULL,
  `broadcast_share` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `autoplay` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `nationality`, `broadcast_live`, `title`, `broadcast_share`, `platform`, `link`, `autoplay`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', '6012-3456789', NULL, NULL, NULL, NULL, NULL, 'Youtube', 'H63Oq8YYlgM', NULL, 1, 0, '2020-03-18 06:28:23', '2020-09-21 02:29:53'),
(2, 'e4bbb370d994a76f41c90a0b0d749ad8', 'Ewein Properties', 'info@zeon.com.my', 'badf5f9aa63f176b7bbb791435a48a7d97b65eb3fd0c2877e425eb2968728878', '1143f7c0663b667b433fe968c5eb4375ceda5bfb', '042910036', NULL, NULL, 'Available', NULL, '1600424768', 'Youtube', 'LFzduCN2QWk', NULL, 1, 1, '2020-08-12 01:29:22', '2020-09-21 06:35:50'),
(3, 'c22aa05c66ccf407b9172c26c9907358', 'Aspen Group', 'enquiry@aspengroup.com', '5b5ba99347814a60a60e7087187e9b3fd1fde9bda8018e713fabd7795bf19f5c', '55a89e50829562b991d1e2a2a95549fc5716250e', '042275000', NULL, NULL, 'Available', '1600847185', NULL, 'Youtube', 'E-RdJolDAx8', NULL, 1, 1, '2020-09-14 07:22:52', '2020-09-23 07:46:26'),
(4, '632372877da43ab6f7d8af2a73f2e0b3', 'Mah Sing Group', 'crm@mahsing.com.my', 'e261844ed0a4556c3c69fca0881a7b2850ca9c5a9c04335dd731419f33b0d0e5', 'b3578f4f0a135e95aa9bf41b7af638414812e099', '046599989', NULL, NULL, 'Available', NULL, '1600076339', 'Youtube', '3lXggG62M1I', NULL, 1, 1, '2020-09-14 07:08:08', '2020-09-21 06:36:02'),
(5, 'b3691c35cea1dfed6274227f67744e80', 'Hunza Properties Berhad', 'hunza@hunzagroup.com', 'c3efd0f9a47ab616c56438531ae2cbf7ad43c0dd186b7e302cb63ed43792262d', '8d8abbf246e1e084b64854f4d98d5961f8236ca3', '042290888', NULL, NULL, 'Available', '1600748902', '1600656689', 'Youtube', 'FLSRWnJVnJ8', NULL, 1, 1, '2020-09-14 07:15:54', '2020-09-22 04:28:22'),
(6, 'e4f4785e35b9623455e78794eac15511', 'Tah Wah Group', 'enquiry@tahwah.com', '476bcf8f55b54467702d0415295e89c0dddfec0d57fc6cadb078b17f654e2b10', 'cd2addc406af5e903b1fda16d3961e5fd3a4581d', '043140638', NULL, NULL, 'Available', '1600870428', NULL, 'Youtube', '6fVywrlozOY', NULL, 1, 1, '2020-09-14 07:10:11', '2020-09-23 14:13:48'),
(7, '9619819c11170ebf2f4785ca71e9d4bf', 'Taman Jadi Group', 'marketing@jadigroup.com', '721e6b5c5db8793137ac0ec8d306db2e3287a1789fd9ebdb35d307a5472e1065', 'b3021f4096bf3aea8534cbe4302d93aa9a20af99', '045937168', NULL, NULL, 'Available', '1600850941', NULL, 'Youtube', 'lnfn6U1ihgI', NULL, 1, 1, '2020-09-14 07:14:45', '2020-09-23 08:49:01'),
(8, '3942b0430fbe8374db84b3aa7f1d9831', 'Berjaya Land Berhad', 'enquiry@berjayaland.com', '96105833b51eb1a7f4c7d9c62bce4369670ed06feb4c876e0f39e51e237171a8', '9205f7c15f48e57e27696cbd149cae7fdf219399', '0321491999', NULL, NULL, 'Available', '1600870667', NULL, 'Youtube', 'GxgVlvtFG08', NULL, 1, 1, '2020-09-14 07:13:38', '2020-09-23 14:17:47');

-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userdata`
--

INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(1, 'fdc700332b8cdf88e179d60fe34d9ba8', 'Lim Theam Huat', 'limtheamhuat2012@gmail.com', 'f059d809ce946ece99b1525157ffe4d28e2643146bb1733d772ada4dd42ca921', '3a99e14af2b1112c3c9c3d27edc9fe65eab9df17', '0162319397', NULL, 1, '2020-09-07 02:41:18', '2020-09-07 02:41:18'),
(2, 'aa990e0054a9091ba74b2bc00b547c7d', 'Simon Lim', 'limlikean@gmail.com', 'f01bc863c2f05b851cd34e006779f4d513e78500bd5d51f8fa0e99567bddb9ff', 'c68fb9d59e7beee87e12966ea88959e39234fca7', '0194459405', NULL, 1, '2020-09-07 02:41:47', '2020-09-07 02:41:47'),
(3, '343412e6a1d5570d6f49d8c64f247a79', 'David', 'davidyewab@gmail.com', 'bf050a0ac92e494da2f2d4ebba7f1aa191f15b9a9c59536cf3c6b1a38e2b9697', '155c36a59422be9ee6c08f909f1cc2599b02ad4b', '0164232203', NULL, 1, '2020-09-07 02:42:15', '2020-09-07 02:42:15'),
(4, '69229a4ddbad02a7cc3ccbd0e092129d', 'Chuzen Lee', 'leechuzen@yahoo.com', 'b856b153a7f0132aa1b1ce513ff733ad0f8b35f7362c02792c412b45e1cfba85', '2992d25615c7170e914b84c7b34e32df158d9cae', '0164744193', NULL, 1, '2020-09-07 02:42:30', '2020-09-07 02:42:30'),
(5, 'eb7fc103524b6801c3116bfd845e2887', 'Cheah Yi Wei', 'cheahywei@hotmail.com', '7819b95ac947036e92be5243c739d011cea11793d61d95e993477b5441939cd4', '6fe8a3c7d441c2aa075d9d6eb60d3ccaa791d213', '0125280869', NULL, 1, '2020-09-07 02:42:44', '2020-09-07 02:42:44'),
(6, 'ab13945019ad09c9fca6ab5adc488fbc', 'Cheah Chin Wah', 'majorcheah@gmail.com', 'da1b5486b3960e2a465519ee449c479a41a66bbbd8c21c49d0a58d648499b4dc', 'b2663843dda8d63d760490c107287aef14ce7542', '0124280869', NULL, 1, '2020-09-07 02:42:58', '2020-09-07 02:42:58'),
(7, 'cf0a947e487d19ffb5b45b1d4c57215c', 'Jimmy Ng Leong heng', 'Jimnglh8@gmail.com', '8212ef6a99bb55a6c1d9254280b6ba15c1900f7ea94a6851b0e23aeae5101d48', '7745648360f4f78df8d5a708241b942efed5aad2', '0125590999', NULL, 1, '2020-09-07 02:43:12', '2020-09-07 02:43:12'),
(8, 'bacdaa77dbf496cb1498491192035f12', 'TIU KOK KEI', 'tiukk@yahoo.com', '9007342cca89ca75171e30f0f5eba723a326c793a1e9077adba5f05157626fe9', '50a879acfb93ed1512a7c7acc6bdf3acd668bb4f', '0174167942', NULL, 1, '2020-09-07 02:43:32', '2020-09-07 02:43:32'),
(9, '0bf8570a61a9efaee2853a800f00d092', 'Leena Lee', 'lee@wellpoint.com.my', '9938433fddab3472e79519a4e95941ee069acfc095d538d097048512366ea73c', 'eaf0a8f6cf9c4ca91e096f79dae789ccb6b67a3f', '0124775428', NULL, 1, '2020-09-07 02:43:45', '2020-09-07 02:43:45'),
(10, '0ec87fc594ee329470996e6471ed2ce8', 'Kevin Yam', 'kevinyam168@gmail.com', 'adf5467921c9e3992bb06d0ea0ccad5f611867ddc551eaf9abc1bf858800a548', '166786196d475fe310115d849fbede3b70551146', '0165324691', NULL, 1, '2020-09-07 05:01:57', '2020-09-07 05:01:57'),
(11, '4353c6dbe204808ce26bb1887e9fe654', 'Jimmy nlh', 'jimnglh7@gmail.com', '05a24c005c4b72502e1aaa05ae38f04f70c36d829e10491fe085f01018632990', 'e7a78080aeceec9a242e3a86a6e8313129da44a3', '0125590999', NULL, 1, '2020-09-07 05:08:23', '2020-09-07 05:08:23'),
(12, 'd5e1373f3d295cb0868fadbb16cb90a2', 'Shirly', 'shirlykhoo95@gmail.com', '0fcf27fd8874394e24316f6a2fa4f8571623e4a504aed9b29718075a6c4f2d03', '522d4c683a5cfe936a63f27393465848c8095ae4', '0164745367', NULL, 1, '2020-09-07 22:52:38', '2020-09-07 22:52:38'),
(13, '65f85be79c2b393c28c495cd34fc9611', 'Sherry Tan', 'sherry.tanhl@gmail.com', '2855b15969f3fef85a72921e885316ac65942601cd2cb495876da29f8a71e791', 'fd7d2233810689456c11c9a1f666ab24f64f3a92', '01234567877', NULL, 1, '2020-09-08 00:53:16', '2020-09-08 00:53:16'),
(14, '043d93b17950177af59a7fc860c16b30', 'Carolyn Leong', 'Carolynleongmc@gmail.com', 'fea818fe46b6b181ad93ea07003bc58cbe7fe2550bebbb66216ddc2701303ecf', '68fd1f18587a362582ba403bd2370382c7d8b70a', '0124882226', NULL, 1, '2020-09-08 01:04:22', '2020-09-08 01:04:22'),
(15, 'e5be253ff00101b7bd90b5365eeb1a47', 'Sherry Tan', 'sherry.tanhl9@gmail.com', '6e80440f67ab916e03337a0b67151cf84e7012fdd418776ae9229a6d67f403a5', '01d5432662f5fcdeef7a93c212d095642b62604d', '01234567855', NULL, 1, '2020-09-08 01:10:47', '2020-09-08 01:10:47'),
(16, '6d533cb6349942cc72242e1ee971dfbf', 'Jul', 'Julia.ngcl@outlook.com', '298122a682bb782c64ef1fc3425c39216fdc2b3177d2e77bdf080f240421f28c', 'ca3c7dca6478ac76620d611c78c9335f76eb9df0', '+61432605890', NULL, 1, '2020-09-08 02:11:20', '2020-09-08 02:11:20'),
(17, '9dda4793c0040d93561187bf1bfb229e', 'GOH', 'ikgohspt@yahoo.com', '3f9e57ee2497a85cf8323d2a3a2c1621e225403d5a4278aecd528285c05ac994', '37f7292651dd4dffd60b435a4bf0ae93462e5a92', '0194817631', NULL, 1, '2020-09-09 01:15:24', '2020-09-09 01:15:24'),
(18, 'f019e76a7730c59508d65828a3a91977', 'Saw Cheng Siew', 'mitscs61@yahoo.com', 'adfb79a1ef78b027c65732cd146ca428236429c7843520d0c7ccbdae2c959c73', 'c6b8ed90577992c24a63739f586ae3d2255bdf16', '0164105808', NULL, 1, '2020-09-09 08:31:40', '2020-09-09 08:31:40'),
(19, '218d9e412818e03306144e7203ffd16a', 'Sherry Tan', 'sherry.tanhl12@gmail.com', 'b9607d30a4da994a716fc6c6fbff89047e5d5a00a14e2a0ef6fa9f204840619b', 'c492f156f9bbc1b35d4418f63a3c46ab64c14bbd', '0123456781111', NULL, 1, '2020-09-10 04:49:55', '2020-09-10 04:49:55'),
(20, 'fbea12995fa14326bf4ab4d0b2a85152', 'Cheryl Loo', 'cherylloo@zeon.com.my', '3a5fd8d6027ef3109b69a6267b63a7476d1418afd601d6bc87084ac9f9fc16da', 'd361ff72626eb1255444065c7802fb4706fda719', '01112165658', NULL, 1, '2020-09-10 07:08:49', '2020-09-10 07:08:49'),
(21, '94ea604545f008f01bf264b589cc570f', 'Melvin Tan Jie You', 'tanmelvin666@gmail.com', 'acfc47232f5f3196c407accbae1d8f1d4e08b143a196f5190f5ddb367da637ae', 'bc38fd0db9ace4a6415e0b32eac2f762faf3afd8', '0143020994', NULL, 1, '2020-09-10 07:10:45', '2020-09-10 07:10:45'),
(22, '55851f974839891ae21501115c4dc644', 'Goh Jia Pei', 'jesphingoh@zeon.com.my', 'f05829c8ea7f019acb804b6723b7c8df3d9af5e958b1e270bda6dbb08b82f639', '61ea52a4c7441e318ea310b6b23b2641f7f66890', '0103974255', NULL, 1, '2020-09-10 07:11:35', '2020-09-10 07:11:35'),
(23, '2b26eb08be534bbcc4230a56fd5b181a', 'Yap Wen Ming', 'yapwenming@zeon.com.my', '0475f6ff4d4131a5deb8fea78ff0903ed4e9156d76691f467d7a2ed38680c831', 'c3633f7d3f537b38c038f550f224319c2a1de9a8', '0164263797', NULL, 1, '2020-09-10 07:12:47', '2020-09-10 07:12:47'),
(24, '215065b5e54739da4ac5dd975366bf4c', 'Alicia Ng', 'q-alicia-q@outlook.com', '0686a37df841026321f8aea7d97b7afcff5b1d82b8b155e8247aafb5b04906fb', '1a0777367876aef7f3cf28fcdbadf1bf287f64fe', '0164440261', NULL, 1, '2020-09-10 07:13:03', '2020-09-10 07:13:03'),
(25, '33d652729cc58518bfd2739915841237', 'Shiau Qi', 'shiauqi_97@hotmail.com', 'a617c96f377df6130e5dc54fe226589e4f5fe41339cf8b66936eb4f9aee59e74', '47396260f53129dc3a2a1da1f77415180816d533', '0175451837', NULL, 1, '2020-09-10 07:13:03', '2020-09-10 07:13:03'),
(26, '43e8f32ea0ceb9993ca995ef70a32d6e', 'Joey teoh', 'joeyteoh@zeon.com.my', 'f0399587977f7bda6742e42e61d14d334925c4ef4ef21140b82ee823982d902e', 'd2bd92aed2a944a787bdde66ef922f10507a1b91', '0189477725', NULL, 1, '2020-09-10 07:14:26', '2020-09-10 07:14:26'),
(27, '9fdc625fef459a7a4a9257c005e10de6', 'SingNing Kee', 'singningk@gmail.com', '972823d5e9e33e78dcd1081c93fca9d95d0d6cb937802c624c54e8bc7445f8f2', 'f16f93455311474c197b3bd8a3dc0ed8da6edf59', '0126544227', NULL, 1, '2020-09-10 07:14:51', '2020-09-10 07:14:51'),
(28, '5a1b8eade36b97e2f4faf3c6b45b609a', 'Lim Jia Cong', 'jclim92@gmail.com', '27eafa65f0d2205024427ac231bdca3e2691a87df00c1b5af2c8a5ede561e713', 'f124a81e36e5bf0f86fd289b19bb9c2df5e6b69a', '0164824044', NULL, 1, '2020-09-10 07:22:54', '2020-09-10 07:22:54'),
(29, 'ec0dc9bca3ca4531159d735bb9f547ab', 'Koay Boon Chang', 'boonchang1017@gmail.com', 'a9a5ae34ac31804125aefad44e6647f254b323d8ac048357ef3fffde7b8a6c8c', '0ce92bab1a7bd4e02c104286516547d068e0b924', '0192673299', NULL, 1, '2020-09-10 07:26:44', '2020-09-10 07:26:44'),
(30, 'de9a54f41029ee114b1eb35da0852b70', 'Michael Fong', 'michaelfg81@gmail.com', 'c4fb33b732ed32b11bf112015466044e93391d2c2197c0f196cbf7b7297a862b', 'cf55e080a39377908bad5cfdbfd8063d8b9fa23b', '0177797511', NULL, 1, '2020-09-10 07:30:17', '2020-09-10 07:30:17'),
(31, '4ae517573b120dbf884221efdd1997bc', 'Bent Lee', 'bentlee3288@gmail.com', 'f1159818848452fbdc4604392def9c9995a79892bbab2c512e9d7c99a87d7250', '73ee719b65e832d31dcbdf9e6ab274fbdf695b86', '0169046071', NULL, 1, '2020-09-10 07:33:53', '2020-09-10 07:33:53'),
(32, '38c7f9093ba584b62d91bbb3fe6b04fc', 'Goh Hau Che', 'gohhc99@gmail.com', '72283920b0af71efd78c8c35f289daf552d797491ce258acaaf5ddd3129880af', '58d816a3669f050f94595559a8cf22103a474323', '0194643985', NULL, 1, '2020-09-10 07:43:22', '2020-09-10 07:43:22'),
(33, '2e05a73a06f41898b77ef8264bf71de3', 'Khor Tien Wei', 'fiolikhor1231@gmail.com', '13ff5dd25f7847ee698d8e47927c3a705ecd190aa7ef213fb7b11c2861813291', '61e01f370afa54ef1377898ca59e5371a57b295e', '01139287504', NULL, 1, '2020-09-10 07:45:51', '2020-09-10 07:45:51'),
(34, '351dd62b0bd4b9cb7a8151e12ee0c046', 'TEOH BOON LIANG', 'blteoh9338@gmail.com', 'f131b655da670d8388e0022cca30cf50667df6a156bd4c50ee720f88d27f3372', '6e6a859c7915ff7510077ac03aa60396f50ad1d3', '0164346628', NULL, 1, '2020-09-10 09:35:50', '2020-09-10 09:35:50'),
(35, 'd4a6ab37682033416615412813606a6e', 'ML WONG', 'mlwong57@gmail.com', '2103cd65e8b16c7a101523e5d26638fa2909bb0d23ae35f5c3c1f56d422e00cb', '26e0056958dc11323b2799acbd6a833a2eecc636', '0124100199', NULL, 1, '2020-09-10 10:57:55', '2020-09-10 10:57:55'),
(36, 'd0133900805bd38f3150865168683b78', 'Max Ding', 'Maxding@zeon.com.my', 'a48e80541d57eed4a36394f17f50a5de98148fa673cab8eb7219667d8b2ad6ad', 'fdb72d80fb9ee84772e854309267265772bf737a', '0129505443', NULL, 1, '2020-09-10 10:59:51', '2020-09-10 10:59:51'),
(37, '0a6a2a97d882c54c2b74ba0c3a3b1494', 'Jimmy Ng', 'jimnglh9@gmail.com', '1b17293b8a5a64b44490e0f55fb2d6db519da11d33005a1b25cfcc51733ade46', 'c0b4cb3866c3ec204b3b4140ac0712a1a83bd4d5', '0164186688', NULL, 1, '2020-09-10 11:06:20', '2020-09-10 11:06:20'),
(38, 'c3f95d2bb7fc3ebf578799656357f461', 'Jimmy Ng lh', '', '1d0359a8983bb0ffa359c9d1a09fdbacd2f024d01205cda0daf893526dcacbbf', '7925e5abe805ca451c55e19f21dfbfc77f808432', '0164156688', NULL, 1, '2020-09-10 11:06:42', '2020-09-10 11:06:42'),
(39, 'd0cc540aa863c96aab36ece5cfaca33c', 'Wendy Teoh', 'ccteoh@guangming.com.my', '85050d4e74def200e837a54b0eb3248e5c58502ef0bf5ca40005bf8bc6c8b2aa', '1990bb4499b3a5ac9a53e97293f743b4685cca28', '0164198226', NULL, 1, '2020-09-10 11:10:20', '2020-09-10 11:10:20'),
(40, 'b0c5f403fa167cfc848f300a13687d5b', 'yeow yeang hong', 'stevenyeow2@gmail.com', 'a07da796df15ed94ec64979f92d945e01a344dd70515d5748a96fa9a36903ab5', 'a0338a2e827169a7206c9ddebd3675eaa1bd6133', '0183218288', NULL, 1, '2020-09-10 11:20:20', '2020-09-10 11:20:20'),
(41, '049c3d3a4870e5f492746e5f06212690', 'Ang Hock Tat', 'Hocktat@gmail.com', '22f66e972373bd74b79eae78713b19fd07ceeeeabffaf259dec197faff14983e', '6c6f962f5a6ca40bdc8ef52e5b0f3221754a77a8', '0124776441', NULL, 1, '2020-09-10 11:28:28', '2020-09-10 11:28:28'),
(42, '7b1e755f6d024c912aa7ed62f02a2fa2', 'Tan', 'tanailiassoc@gmail.com', '03c2f8e6e83754d33ffc7c5d17a48f8608d2ba9abb37970c67f7b42fee65b3c5', '53bd9e5d7538890bb3ef081f82c81cca93b5a35c', '0176629608', NULL, 1, '2020-09-10 11:32:55', '2020-09-10 11:32:55'),
(43, 'e564a0683831d63eea99f3205e279871', 'Ngo chee yi', 'Yicompg@gmail.com', '8b98decbc6372e42bba0cf1c218eaee4972fc395e558fa47253a0e111fa2bf70', 'c7ac6590fa07d6d1693462066d335bfa84fc0e8a', '0174782238', NULL, 1, '2020-09-10 11:36:57', '2020-09-10 11:36:57'),
(44, '54e3caa91dfc51582f8876b7f940a259', 'Yen Chan', 'somuiyen@hotmail.com', 'a94b7558f1f75f2721b797eab178d0a5641efeecc57744d806ca93a5147a87d5', '8f553258d2f39eb8049cca58fab7f9f282a6b757', '0125812005', NULL, 1, '2020-09-10 11:52:14', '2020-09-10 11:52:14'),
(45, '7716b19c341af7a300ff390862897904', 'Lim Seng Choon', 'schoonlim@hotmail.com', 'e69e2cb57a0dbd40810b5ae9208ca2e65b722a8c7ca6050accb812d1004d22a3', '8385e794fbaf1dc5bba2629084e8249bf6c2beda', '0195771840', NULL, 1, '2020-09-10 11:55:39', '2020-09-10 11:55:39'),
(46, '4e445d8acf7576e143433bd9d4ffb645', 'TAN CHUN WEI', 'jordantan83@gmail.com', '2d5cf65f9ee99255940ae5bbe185432219445b13e9d2e69950dff17b268aa9f4', '6eb29293584ecd3fd8aaf4ae31690a27566a1816', '0124108289', NULL, 1, '2020-09-10 12:04:16', '2020-09-10 12:04:16'),
(47, 'e344da056fb189a5ce7b23fd556606ba', 'PHUAH Eng Hai', 'ehphuah9876@gmail.com', '96e01ed9296c44ebb46b3792809e3d6b0027ea520d0c31dcd322086fc56232e5', '74d9ed567706b0aa7aeee1ca574262501663346f', '0164198130', NULL, 1, '2020-09-10 12:10:55', '2020-09-10 12:10:55'),
(48, '8f069d87b8c08ca79f2b58870f20c6d6', 'Yap Pow Ping', 'yappowping@yahoo.com', '88859a4891ec04b852f4ce7b5a7fae43b1fb6a3aa396e74b48bfedd969a082c5', '8a9cb92f50b36f918ff1a2457292e5ed438a2d41', '0194417972', NULL, 1, '2020-09-10 12:15:13', '2020-09-10 12:15:13'),
(49, '6bb8d54d3b394b2cafb249e4c878fc50', 'TAN SEONG PEOW', 'tanjack126@gmail.com', '1f469491ea4340284ee1cc4f2b68409e4bf6c3663bf29f394c962c96a1acb671', '1d706a1b9bdc401e54254af75b3672a2a07c0076', '0125771314', NULL, 1, '2020-09-10 12:16:26', '2020-09-10 12:16:26'),
(50, '984b2dee96952407688896e3d37188aa', 'goh xin min', 'icequeen5454@gmail.com', 'e5bf67d8f4ab01e25d0a84f844974faf3ab985645702324bceef2291a3601e4c', '7218254a1321690df9b4ce3b86436632a0bbe4cc', '0164717314', NULL, 1, '2020-09-10 12:19:58', '2020-09-10 12:19:58'),
(51, 'b3c6e70ccdf8d462bf2889d2b166068f', 'Elaine Ooi', 'bayelaine@hotmail.com', 'ce4d293206f95f01c371c23fd7573bce64596c1c59db0505ac3755261647d3b6', '882205259429becc2d79bd8a5360d32d5aa2a40a', '60179210696', NULL, 1, '2020-09-10 12:21:06', '2020-09-10 12:21:06'),
(52, 'ffbeabee7eeff7c5475ece1a636be30c', 'chongcheeooi', 'cochong@guangming.com.my', 'c77b0316e6af926c77427aada1fb735825d9de05d7a90a3c9037261a03a50007', 'eb6308d04e0b933c4d3877b3b6f18de8453737ce', '0164196637', NULL, 1, '2020-09-10 12:28:10', '2020-09-10 12:28:10'),
(53, '068b9f3b5d0a027feab8c195d81bee9b', 'CHIN WAY LI', 'chinwayli70@gmail.com', '0d7e2711e7c11fbd95f0daf54d2c5044caebc4fadcb7874de6c0dc32cc7ce5fd', '7edf103a95fa91b18c66fba726ce6f1e4a402dc0', '0164198140', NULL, 1, '2020-09-10 12:31:54', '2020-09-10 12:31:54'),
(54, '5417a0c511d0aa56f6eaaa6bcd5d2da9', 'Tay', 'chtay731@gmail.com', '4097097f551197a411c610e21f4ab880d43379d17142c3226d595b38aca758db', '2db31120451918e8840e9bc9081284183ba5e20d', '0174235939', NULL, 1, '2020-09-10 12:40:41', '2020-09-10 12:40:41'),
(55, '3a59bf439eecb122ab03371dd0261e0c', 'Lim Mei Fern', 'mflim2000@yahoo.com', '66fb7f6615b2b3750bbc7749312db7df0579c13f8aa87e24d9af50a249128eb4', '008cb8f5290f38a1a663244d7245a7bebc1bdc59', '0124666524', NULL, 1, '2020-09-10 12:45:04', '2020-09-10 12:45:04'),
(56, '957b38302653526b429a7e24a5fc70d8', 'Lim Bong Ha', 'bonghalim@gmail.com', '51e1f2ee6df7a6ede2105eb3ed64831c27376b40b98e24e96a84ed842072df04', '3d57793e5d2cb0ae3ce16df1945952cfcbb7912c', '0175444018', NULL, 1, '2020-09-10 12:45:43', '2020-09-10 12:45:43'),
(57, 'd161694e2d8234ba39a7c1898900b619', 'John Lim', 'johnlimty@gmail.com', 'd6ac3262d5b437feadd6a18ed5e6de0be6bf5b261cc3d9edf3ac661fe06240cc', '3dfdba86c4ca6aec730955bb1c8566f2e19cf9bd', '0124288416', NULL, 1, '2020-09-10 12:58:23', '2020-09-10 12:58:23'),
(58, '57d66e45dc2260ed35877133d6677d2f', 'Yuen Wen Jieh', 'wjyuen@gmail.com', '9f466fa20c4838f8faaa65c11cb11c400300341669952d196a02efcc6c27b894', '9cde92b547b1f9d793b69f76be122f18f5d395c0', '0164141919', NULL, 1, '2020-09-10 13:03:23', '2020-09-10 13:03:23'),
(59, '4c590f913eb95dcaa3227f6d79163c13', 'AIMAX LEE', 'aimaxlee88@gmail.com', '2c0c70f3b7209f27a7689c4e0a4eb8f4b04a0c8eeec8421e865299c07eb405fb', 'e7c8d91cf7cb15c0ff6751719983f89f76f19cd0', '0175996877', NULL, 1, '2020-09-10 13:30:12', '2020-09-10 13:30:12'),
(60, '94abd1c39c21cc2804df92cd304667bb', 'Ms Goh', 'karengpshin@gmail.com', '5da5fce4f2ba0b5b79ec19ad7b13a7a6af8a3ff317a5e346565e5a2bac2bc078', '326dad55d87a9cbd35287217f5c40f7d5a8b306a', '0125276892', NULL, 1, '2020-09-10 13:37:47', '2020-09-10 13:37:47'),
(61, '63bbf8e030dc91921558730ba870241a', 'Sharon Teoh', 'sharonleang@yahoo.com.my', '765071d802c1b36c737dcaa2bba1827dcbe420c59b249efbf78f95ca0e73c731', '3c176b546e2e638f6f4bfaf4be1262789c2923ce', '0174731903', NULL, 1, '2020-09-10 15:15:27', '2020-09-10 15:15:27'),
(62, 'ff3efeafc97f7f62615c09965c4db1ea', 'CHOO YAW GAN', 'summergan8@gmail.com', '212598e51e8d0c7e0f92a06c1b9b9b96bd284c28b299cebf54b8287d30ac1a4c', 'bc2026d4eeab105d1f20984111f8b744fe9928ba', '0189540822', NULL, 1, '2020-09-10 16:19:16', '2020-09-10 16:19:16'),
(63, '950f56cc08294d49c8723008b3988fc5', 'Khoo', 'sinar5231@gmail.com.my', 'a4935c1f0405985fbd4826c411b7485ffbcd2a4d750aa19ddd4733546131eeea', '1910089dbacfacd3eb71747ec32fc6b505a96440', '0124535427', NULL, 1, '2020-09-10 16:35:10', '2020-09-10 16:35:10'),
(64, '506a7e40c1dc8c5a3b5274affb6b312e', 'Lim Soon Seng', 'jasenlim@affinex.com.my', '6fc6ded2efec691e54357b7b9442ea52cbb861aca642a608ae48d19a6d12e224', '2aff87c392f6372963365bfb38d0508b350903a5', '0124278858', NULL, 1, '2020-09-10 17:18:38', '2020-09-10 17:18:38'),
(65, '4a59d8579f5352c32e56190d765b1fae', 'Iris Tan', 'iristjw93@gmail.com', 'd0f9b538c7cbb00ec7f681b49eed44a8df71144afdfad3b2dd17852c87c53e4f', 'b69b66594ae47dca696847e13fbde8d7769f7bc3', '0143093427', NULL, 1, '2020-09-11 01:58:47', '2020-09-11 01:58:47'),
(66, '5475a0391d26adc407569df964c4a093', 'Shereen Kung', 'shereenkung@zeon.com.my', '62544ee93b6d9365335a1bbe46911002ae8c03f7aedfcc7c4e3fff1f05e8fb4c', 'e26c42c3b001315b1a3a9eee0ec08956caaee60f', '0164634065', NULL, 1, '2020-09-11 01:59:33', '2020-09-11 01:59:33'),
(67, '79d5d4f7ef7a1714dd3efc80241a7b90', 'Lau Chun Keat', 'twc1008@hotmail.com', '31b4d69fccc6bb4fcf0b2d326b2b13f71dbe2faa520f83952daa24f57797ea5c', '07c72a4dbe8185a8bfa938b0ed4c005263fd2664', '0164642079', NULL, 1, '2020-09-11 03:08:00', '2020-09-11 03:08:00'),
(68, 'e7e1a3805db1da508f16d8db1f9c9ebf', 'Ng Yeong Mun', 'ngyeongmun@gmail.com', 'b6b4207dd9f98b3c09a791191e83ff98bfddc756b743457d5eea7b6a5e41a044', '1f205a41db3e3476ae549763e698c5ac688e383c', '0126422877', NULL, 1, '2020-09-11 03:37:48', '2020-09-11 03:37:48'),
(69, '069a0f9d5d5ad58f6f7a21a6ea901eee', 'Loo', 'felicia.lyc@gmail.com', 'a7b4d679c51df522d9cf615e0bcbcf8ebc06d8731aae62657cadcb3dc0d0d5e3', '9c8ca63f78f89949069640ab5c00a2d256de3324', '0124819109', NULL, 1, '2020-09-11 07:00:59', '2020-09-11 07:00:59'),
(70, 'ee5a0e459c3a6b4f0d546c0950f03da3', 'lim lay chin', 'jimnglh3@gmail.com', 'bc7f2cb4043b1985cc93dc19cf3a7d8dfe0be01392e6dfce5092fe23b20fc2de', '073124c6bde20d4dcae131199d806ba19ffa8a78', '0174880809', NULL, 1, '2020-09-11 07:53:18', '2020-09-11 07:53:18'),
(71, '5d81494a4ca44801142d8eb120bf5c8e', 'Tan Kang Wei', 'Kangheyhey@hotmail.com', 'd172f013b24bd3d920e23b2b9df90219d28270331648b278c7e4bfd30fd57ab8', 'b228b6a1ef2ce4144a29622c37bd19f34fcba295', '60124129370', NULL, 1, '2020-09-11 08:04:45', '2020-09-11 08:04:45'),
(72, '88baa48ed89f714ab84b49cbfd7eeeda', 'Tan Kang Wei', 'edutech0830@gmail.com', '42ee198a6157f2e797b88fca5eb9837206235c42264eef47b58bf6ca67050ccd', 'e888f89054d3ed2da461e4b2744e11526cd45382', '01111279370', NULL, 1, '2020-09-11 08:05:12', '2020-09-11 08:05:12'),
(73, '9bfb85145012d8eb919e5457eb550b35', 'Kenny Chia', 'kennyjuti@gmail.com', '22607f3cbd71673466e3a05e085b1b0804a6cbeaf4c22ba7a5ee5bfe21faf0ca', '965feecfb8066afd69408ed1440eb1b9dc5a4204', '60192260801', NULL, 1, '2020-09-11 08:37:37', '2020-09-11 08:37:37'),
(74, 'd6874123f3cd64af542a13b55a80439d', '戴志豪', 'Hauwen2000@gmail.com', '0a994afbabba34e5160e045fce38aa3a7dd83c59e0c9b822c34ddd058ccd5958', '0eaf404fab4390c2753ba238ebc40345d31da9ea', '0124274562', NULL, 1, '2020-09-11 08:53:36', '2020-09-11 08:53:36'),
(75, '62c72466ff31bf42f5413e340e74aabc', 'Wang Hang Min', 'whmjohor@yahoo.com', '0481fe9b9469ba4ed16ef5d4e75257e862616a0a9f6c75ed464eaebeefe7d618', '5527ff88cd03c343d1af459b52910debfcec1b66', '0129003353', NULL, 1, '2020-09-11 09:57:42', '2020-09-11 09:57:42'),
(76, '4750bb7e6a75b4f1057d877bb5a999d3', 'Chuah zhi hao', 'Topbrandgary@gmail.com', '69500a2827e4f79e6f188fae476ae78d92cfa353efc072f6a1cfc8abd57951c9', '482e482b64dd39fb79eac7c6fe857d5ef62b8b5c', '01139548361', NULL, 1, '2020-09-11 10:16:33', '2020-09-11 10:16:33'),
(77, '6dc5aac0d010177b8d8c10aebe2fb377', 'Tan', 'pohteik@hotmail.com', '805baea1efe191e3bd1e27a0f723b03832d4109a80c94c9576cbbd203d21f690', 'f25a18ae668a5ac5762db42fcf3c2a8459828bab', '0176661662', NULL, 1, '2020-09-11 10:19:20', '2020-09-11 10:19:20'),
(78, '3eafb453509ffd5c6ebe896a88d9466e', 'EK', 'ekng2000@gmail.com', '6eb6afc8feaf7dc4deb80f193fe758f026cfa5f80f40a0bdea93982fd8719c50', 'c59c91b3c69616ba0f0d130a21ba85943a575bd6', '0164991470', NULL, 1, '2020-09-11 10:50:41', '2020-09-11 10:50:41'),
(79, '634ab5943ef0e4cc46d9600765958806', 'Edwin Ong', 'edwinoce@gmail.com', 'e41ab678db9d7916abb00c8bceb21001de98bd7f5ec69a588ce8f5d4f095fc1b', '2ce6e1ee8c680930fd5a3c4e1a6659d67f75056d', '0124233368', NULL, 1, '2020-09-11 10:57:26', '2020-09-11 10:57:26'),
(80, '8e04ee825f1d08f325fe9be3a9feb80e', 'Tan Teik Chuan', 'spe_tan@yahoo.com', '7e82f8e7e0946164c2efaed9236bba15266d614aca3c8fbdbe3a107d0bc1d7c6', '18d79c7658b3a3faa5a5e1a76386df0af311ab1e', '0125556666', NULL, 1, '2020-09-11 12:43:07', '2020-09-11 12:43:07'),
(81, '8e3c2137b19abcf93d6966571ea204dc', 'Royden', 'Royden931@gmail.com', '7f8a0354c18fba6e0cda67edd38a4d7bb431e58d0e2322da3fbdce6f1688a87c', 'ab54a8f851df2b13e7323d483fc4eaf17286fe6d', '0124258616', NULL, 1, '2020-09-11 12:45:56', '2020-09-11 12:45:56'),
(82, 'd295709cd45f76c993bd790aa1535a6a', 'Song', 'sharonsong2@gmail.com', '87eb4036a9e3cc269c69e949a6095145e63f3d2e7653e5f383c7afdf7d5dfbb5', 'e2da83bbcdc4173a7e36f6fb6e2dbabe7a4c907d', '0124312738', NULL, 1, '2020-09-11 13:18:06', '2020-09-11 13:18:06'),
(83, 'dff327e890c34d01d81501c724bd9f69', 'BB TAN', 'bbtan.yomax@gmail.com', 'f3cc88052e4e50823621aafbe98f7979918a6308a2cb7103b91cd8a860c65f31', '63a10c9e4153115c827671c075fb3db5ffea7328', '0124711517', NULL, 1, '2020-09-11 13:52:27', '2020-09-11 13:52:27'),
(84, 'b340638b3b281004102c3e5af217921d', 'Wong Wai Sing', 'Jasonwg1818@gmail.com', 'fbed0c531c04924f913f6f46d418515e38614fb828677854c84c63589ddb2325', 'f4c4ac65ece65e0d830d398e1ae6adc0fd8684db', '0169765692', NULL, 1, '2020-09-11 14:09:32', '2020-09-11 14:09:32'),
(85, 'cf6d037e1467edb42bc22f764256a54b', 'Ho', 'hbc1977@yahoo.com', 'a8cf484d72fe4481306b4a31c24a2884d226a2e8d50d7c7fb0773722fffd1aba', 'e6021c2e134d95120af25c034de1da0d9c3ac8af', '0183729072', NULL, 1, '2020-09-12 01:17:46', '2020-09-12 01:17:46'),
(86, 'a73951b3d3139bcb3feaecb28a28287b', 'Harris Loo', 'harris@acewideasia.com', '328b271ed87ee844958a5c4723c87f9f657a4d08545451771b842a29fd3304af', '5fcfb433b01c44881d9546e1b35254dc71204672', '0124756220', NULL, 1, '2020-09-12 03:33:26', '2020-09-12 03:33:26'),
(87, 'dc96efef451ed8335c2a59611bb366b1', 'Shaun', 'jagamjnb@gmail.com', 'c0be2db38ddff4a88f42a490966ee92f715bbc9a869721126b108fb363e7e1c0', '11cafc77dc4c3f7b94449e1372b1fe7032f7f8ad', '0195779523', NULL, 1, '2020-09-12 09:26:05', '2020-09-12 09:26:05'),
(88, 'e828b117f5844ae3519910749c8e352b', 'JC Loo', 'jcloo526@gmail.com', '45ba348f023a2b1f7cefc773f194bced218f6fdd92fd9b56a55956c1bd2d4ffb', '6d74359cad17025ca857c56bb644d6eb6fae1982', '0174061366', NULL, 1, '2020-09-12 09:27:58', '2020-09-12 09:27:58'),
(89, '869670806104798d93e9f89456ecf487', 'Ooi Eng Seng', 'oes64@hotmail.com', 'bf6921380c64d148c9afb08588de371d9ef5a5727674e10a916bcaad25ee36fd', '1d56dfd0803a7a387bc9123e31abe19bd6677320', '0164719783', NULL, 1, '2020-09-12 11:12:32', '2020-09-12 11:12:32'),
(90, '405947fa47eba82fb91893c0aa1ed7bf', 'Darren Choy', 'darrenckchoy@yahoo.com', '58e3bc91745515357d94b55dd66cfe2ed4464bde4e96ba5c16a95aef3045ca76', '8ca7e4faab6a774463e1c55fda21533360ca42ff', '0124771515', NULL, 1, '2020-09-12 13:10:34', '2020-09-12 13:10:34'),
(91, 'fb49df0616fd67bccd0905c87878c220', 'Chris Quah', 'chrisquah@hotmail.com', '5fc7442135c4b13814db07aee9d0e045debd4682bbc588b146d5d69a83ea418d', 'e6a0cd0ac78d9930f758af824c0121d830b0f4d0', '0164196609', NULL, 1, '2020-09-12 13:11:40', '2020-09-12 13:11:40'),
(92, '15a735d53eee2bd9cb40c35aaa57e8fc', 'SF Thum', 'sfthum@gmail.com', 'cd1f9aa841d181c16693180f2ecf6d37b3c242d05e3d8624fa1a72799489a0e5', 'efba02c4012405edc59e1107905142104da14098', '0164764959', NULL, 1, '2020-09-12 13:19:01', '2020-09-12 13:19:01'),
(93, 'af9dc2857b603a919374bc88815f08b1', 'Lacey teh', 'Lacey.teh@gmail.com', '6a53b2f195c0fbff2efabbc2741c93c6de67a51b07bb107c158726e11ee223ad', '62cef8281c8679d5614a88d32a599ed6ca62b4d4', '60124721246', NULL, 1, '2020-09-12 13:42:41', '2020-09-12 13:42:41'),
(94, 'c65a73c7ea8eb242ffd79f7d16805922', 'Nelson Teoh', 'nelsonteoh@gmail.com', '7b66c350ec05cdac04e3f7957d9ae0fc62dee2f2c5d2ec4dc970e2d5f9788ea7', '7c126e6d96c3065bf3fbbbdb2fb7d999a8a8ddf2', '0164220422', NULL, 1, '2020-09-12 14:28:30', '2020-09-12 14:28:30'),
(95, '0b8804c1ba5da3b32d6a95c477e00f0c', 'TG Ch’ng', 'taiyochng@gmail.com', '0e5f07b71a95c282cbc765494ac05db4acc734e574742f3c4f2e56f8aba2908f', 'b73118169d08da049517650594410ba129b97800', '0124876777', NULL, 1, '2020-09-12 15:26:12', '2020-09-12 15:26:12'),
(96, '4252d1270250f1d29079be05fa50ff65', 'Tee Hong Ping', 'HPTee@hlbb.hongleong.com.my', 'bd1cc5483356337af93ebac16eef42748723e187b44cd496089d759b5173e3fe', '409102ef1107f51d3ded917b94ed2ad9e896d588', '0164527545', NULL, 1, '2020-09-12 16:11:20', '2020-09-12 16:11:20'),
(97, 'a84f7f148180abfddbf079d983fad3c4', 'Chong Chee Kheong', 'sjckc2001@yahoo.com', '089660375062b704744e69b87ee10078e02079b6f87fde222f53d5a0e14abbcf', '4c6f0f51a3d4bc03121a4d7db46980521dec7eab', '0124938146', NULL, 1, '2020-09-12 23:03:32', '2020-09-12 23:03:32'),
(98, '4fc0c1df34ada4ceadbafde4ab7a2104', 'Tan Kim Leng', 'kltan208@gmail.com', '561e914649f9a8588a77acac4f46b4cca90624b25d6d15d971435d39a399c143', '15664a76a651d269f38e1c28ce19c98b36511da5', '0124237193', NULL, 1, '2020-09-12 23:24:07', '2020-09-12 23:24:07'),
(99, '9699a40a6a6a25aca17ebb8214aa640b', 'TH Teoh', 'Tteoh8 @gmail.com', 'cae9b5d936d2e8d806ba0fd691f678810bac2323e3c34f857166ccdca41d0218', '624c3068719ed5a950d2de90ff5d8befad9f9601', '0124183866', NULL, 1, '2020-09-12 23:30:07', '2020-09-12 23:30:07'),
(100, '5e682ce0987dbbebe97c4ac7a3f53da9', 'Low Seu Beng', '1234andykok@gmail.com', 'd65eebe74fc2d98bf34f70c43c5cf62afe5e0d37118e9fcdac4b47fe267c20bb', 'ea4249e1c1e08d046030d1d047b54401fba62beb', '0124105680', NULL, 1, '2020-09-13 01:49:04', '2020-09-13 01:49:04'),
(101, '1eb882d7f8ce0040a17d893565ab4d44', 'TANI', 'kyokogtn@yahoo.com', 'f75e2e8155a9f80bde125940b52a4b7c61776051eb1b227e6d2778bcf5fbf27c', '4e5614f744ea2f859df831cbeff5eb747e676548', '0194421119', NULL, 1, '2020-09-13 03:07:23', '2020-09-13 03:07:23'),
(102, '5407484987edf3c7662bba0a6eda4c7e', 'Pk Hoh', 'Pkhoh2013@gmail.com', '0b8fd4ef10a25a95d054a1a229b213521b1a1693ee1c6a100db01b9af19279fb', '0a6e41ec3a93036bbf5a7ce19058d3f1908dbe61', '0163460030', NULL, 1, '2020-09-13 03:27:37', '2020-09-13 03:27:37'),
(103, '580e29f410afee8fa518485676aac601', 'Sim kooi lean', 'hedenasim@yahoo.com.hk', '137c976579362b5800d162e17a229f5c1f258b043b218dd4982befdf4e698182', 'fdd7343effcac945b788bdbfef5c2527a9607f71', '0194120604', NULL, 1, '2020-09-13 04:05:43', '2020-09-13 04:05:43'),
(104, '4b0f93aa45c9dd52052649d1c045b0a8', 'LIM LEE SA', 'adeline7669@gmail.com', '07ce2bd051f104d6a2dd0bc80e944efc07233f75a72a6e11a35313dffa44a2ae', '442698859ac9aad578334498aa80e2a31d2aa23d', '01112442832', NULL, 1, '2020-09-13 04:50:08', '2020-09-13 04:50:08'),
(105, '51b8dbeb96aa559839fb0eedc1516159', 'Fong KF', 'Fkf98@yahoo.com', 'ce1e29fede986b1bfc110808e327d288881b61a04b3401bb1e143f00b292fd09', '9f9b0e47d85932005aa30bee17f09175ba30fd2a', '0124097330', NULL, 1, '2020-09-13 05:06:48', '2020-09-13 05:06:48'),
(106, 'fbf916b674a6e9058806a2fbfde0d99f', 'TAN SENG SOON', 'tsengsoon@gmail.com', '667c4ca287e2d749ad118386a10a89909eef67f967769d8b1421e1332c3be1ad', '36f7e22a88c576474aff1bb880c4cc055ff929df', '0134895426', NULL, 1, '2020-09-13 06:15:06', '2020-09-13 06:15:06'),
(107, 'bd178837b7e8158873a133e0129c4047', 'Soo Kee Leng', 'alecsoo@gmail.com', 'ff48bdcfe05b7bfb2a94e8d4747a1d92f898c564823d943e8af9f2f692b54ca5', 'd6cffd4fd2b8210caf1d3788a74b0f4041ad3421', '0124783282', NULL, 1, '2020-09-13 06:41:41', '2020-09-13 06:41:41'),
(108, 'bfd2a7cb8e325468827efa97c1bacc1d', 'Jether Koh', 'jether.koh@gmail.com', '54007eb6c9be2d27d8d592e205d9510d11010ca917df3491d07952a53e1a663a', '817b868917524cf835f01c82c5f051d7653c1eea', '0125616272', NULL, 1, '2020-09-13 08:32:45', '2020-09-13 08:32:45'),
(109, '448789e7550d8648458a23902e97d2d9', 'ang siew siew', 'ssangren20@gmail.com', '2d36a81ed6e02bde13877cca3842682f204cd9fda2bafdce113713d39995913e', '272b9f0419b7f00f534a58589e214f9fcbdf71c0', '0174182192', NULL, 1, '2020-09-13 09:06:36', '2020-09-13 09:06:36'),
(110, '591727a2607671c0a727d98d24dfe472', 'Chris', 'Chris_lgk@hotmail.com', '748a50ff2a58304591d75ae2ebc632316232edfd09a5955aff820678927d176d', '159815073b53dfe89cbfdb0b2dd45ab830cce958', '0124183873', NULL, 1, '2020-09-13 09:38:06', '2020-09-13 09:38:06'),
(111, 'dfc0090cb992c8aed2e3b5468d77f6ab', 'LK Tan', 'whitedovetlk@yahoo.com', 'ac0758880409e6537487527a42919b5bd1f344ac97b487b243b4646960b37409', '64e1983c931488ce31ff563fc8c74436d21e63f3', '0164964476', NULL, 1, '2020-09-13 10:10:31', '2020-09-13 10:10:31'),
(112, 'da74fd0ecdaed32f10ba0375d832a7bb', 'James Tan', 'jamestth@hotmail.com', 'e743b1074a4002daeaf0a28de39f243e37cb0f9e9c6e6ce9cc7043edc32105ce', '1f229d141404faf721c1b24930f2105ddc8f0d35', '0196621673', NULL, 1, '2020-09-13 10:14:45', '2020-09-13 10:14:45'),
(113, 'e2dd50d8826fd7feae5fee269d5be63b', 'Chai charng yuan', 'Mynonis@hotmail.com', 'd55b5280f20b4b185df31fabb28442935dff3bc3023589a791343462d83207d7', '29e14bcb20ef1a6a3817a67511c88866977e75a5', '0123731913', NULL, 1, '2020-09-13 10:18:00', '2020-09-13 10:18:00'),
(114, 'e27ff5768b46f8b3f87844cac75721bb', 'Recca Loh', 'wewiclown@hotmail.com', 'f6345277ead9fdc3d69781f069daba94bb7345107030aa15a68c5227c6fec9f4', '389dc2d3daac699fea9511de669c2f87e91d2fd1', '0125321045', NULL, 1, '2020-09-13 10:20:21', '2020-09-13 10:20:21'),
(115, '86e71b4eae202ee7e535de1de74fda9b', 'Ooi Yinn Pin', 'yinnpin@gmail.com', '3716236dab616b11bdd522bc35f94cad285c96e352e00b8ab4e12116a0fb60f2', 'bab692e2c7589485e7ec30413342cc5e2f40ac58', '0124711408', NULL, 1, '2020-09-13 10:21:21', '2020-09-13 10:21:21'),
(116, '5e84734abddf7f0bbabf640f452f693c', 'Ng Irene', 'ngirene123@gmail.com', '256766d4f4db782c6ce3232cbee4c7a08e0fad9b662c7d86fee86c7d93223683', 'a8d58908cfa443cfb0ee71ed5bbbf44ca91b640c', '0124679887', NULL, 1, '2020-09-13 11:27:19', '2020-09-13 11:27:19'),
(117, '56525a15f03810b79fabd05113b4ab05', 'Ng Chyun Wei', 'Jeffz_93@hotmail.com', '4be3e6b8dc771d8aa151b6667eb4a8bb8a64975fa18caede4d95ae90a6e06b4d', '7c10d41bf769baca7a02528f02173156937e25f4', '0174271899', NULL, 1, '2020-09-13 11:29:21', '2020-09-13 11:29:21'),
(118, '0e5e6bafbda3d0e7eeb6d2be0ba9613c', 'Tan Tong Yang', 'tongyangtan99@gmail.com', '1157d215f04f8139a4fbf1504d397b3918579e4fe0a4e06614f576d5d81eed3b', '9f9f57b3ed4aa4ae49726bcd86e3358152a943bf', '0126689123', NULL, 1, '2020-09-13 11:47:06', '2020-09-13 11:47:06'),
(119, 'f068f563f94bf9fcb932d22340a99b5f', 'Chong Khai Jong', 'khaijong@gmail.com', '17ace881a36f1e2777ace1e7df672e73d6b28e94edf5794db577f1baf2064e42', '6bb1c22e739f043fdc366eddcc7cbddb675f61c1', '0127588542', NULL, 1, '2020-09-13 11:48:58', '2020-09-13 11:48:58'),
(120, '04a830eebfbd6a895c5c6efa10e7200c', 'ONG SENG LYE', 'darren_osl@yahoo.com', '5acd4bb5cfe04a2fccad4561d9e1dd821a3cb6a64e0658d6189e696d4d529c6f', '0d45c555c6ea776f84d4a2872efe215801edfa30', '0125487669', NULL, 1, '2020-09-13 14:17:52', '2020-09-13 14:17:52'),
(121, '948c4a1a542491f1d5459b8680d4237e', 'Heng', 'fotomate2929@gmail.com', '5c19beccf3934697b2d067e04fde0f06a753ce70b24937e66fe6636bfb77498a', '54be45131d8f11b4c0608fbb783ad015968ac05b', '0125586333', NULL, 1, '2020-09-13 14:31:46', '2020-09-13 14:31:46'),
(122, 'cb224c2a10efe0b8d9f9a7547f6f5cde', 'Siew Yong', 'ooisiewyong@gmail.com', '157b89a222148f64296d642307f08865974d2a9115a3e34f96327b14bcb3988c', '7c019636e829cab32ccf729c5cadd26bd2f7c89d', '0164179393', NULL, 1, '2020-09-13 14:54:50', '2020-09-13 14:54:50'),
(123, 'a099fe6667295774ac6655bff7f31dde', 'CHONG WOON FAH', 'wfchong829@gmail.com', '67d316147d6c2ec247d6707728e8d7e1f7a20ae1103826b6c864fdb288c658b7', '7590231e7006ec1721ad1cf81d87cf513a9a871f', '0174889188', NULL, 1, '2020-09-13 15:09:52', '2020-09-13 15:09:52'),
(124, '56022a709750ac5f5667a8f886eeaed1', 'Lee Beng Tiek', 'tieklee0910@gmail.com', 'da148c24ec5521dc17d1defefe18b4c247b98c828c589e6924f138935d7e137e', '19ef81ea9d0f1dab2ce6845fa4cdbf9a0e2e3ceb', '0164261122', NULL, 1, '2020-09-14 00:56:01', '2020-09-14 00:56:01'),
(125, '6da7bba935249906800cde6cce3e68cf', 'Dato Lee', 'Teong-li.lee@amphenol-TCS.com', 'ebff394fd3ef6acd51f550f72bf9c68d090113bf9e137e1a455f25fa16c2a508', 'da71765868698397677ecdd24432e114924536b6', '0124778181', NULL, 1, '2020-09-14 02:19:06', '2020-09-14 02:19:06'),
(126, '4ca19e41765da6b49dbcd66ea11c1ee6', 'Koay Kah Guan', 'Koaykg@gmail.com', '50d2241c9c6dde691da1a74598a59954d76cf94a037c575193d045b4c316d143', 'e542526ccfd25febec41c6b249536db1c6adb5a2', '0124881047', NULL, 1, '2020-09-14 02:35:55', '2020-09-14 02:35:55'),
(127, '2fdd37305fb232562e6649f05de4beb3', 'Jayden Cheah', 'kmcheah94@hotmail.com', '1c11f8fcd7942346b7b22a2f5a92854d60fa8b41e0efa4ec9d78fbcb44cacd52', '5b3c2d4e1f58e69aa7a37810dab355ea632424f3', '0123440869', NULL, 1, '2020-09-14 02:40:17', '2020-09-14 02:40:17'),
(128, '46e09dd19c03ae3c58f2495c9ce956dd', 'Chong Hai Li', 'haili-94@live.com', 'ffe6689fd7ac151dbe15caf0de114332af5e560545dcffd78f4a423f2add8b48', '8b9e6fedb2440e406b41f595ad23d14bd31d51d2', '0164782886', NULL, 1, '2020-09-14 02:54:07', '2020-09-14 02:54:07'),
(129, '42236e7e6293aa52879084588e7a72d6', 'Simon Loo', 'Simon.loo@hotmail.com', 'efdab576a88c3cb045378f436e3621e9ff49c4a6069be710032cd2d1bf2ccc0f', '8c004c1f0a5bda53ab75d23a7c01dab8bc5fc114', '0124438632', NULL, 1, '2020-09-14 04:46:12', '2020-09-14 04:46:12'),
(130, '66783d356a8accb34d58df30c9e47291', 'ANG AH EAN', 'angahean@gmail.com', '0d08e97c2a8c6490d202031e6e44ca896ac24077becf9a5317386133c630980d', '1441e0ccd7ef05845fe422385bea257ab91dc2e1', '0124815510', NULL, 1, '2020-09-14 06:04:31', '2020-09-14 06:04:31'),
(131, '1a73b804f8fe09858320faac343e4759', 'Ng Kok Hwa', 'ng_kkhwa@yahoo.com', 'd8adeb84770aa1bcc21807e72da13f14b0026b3205f115ff51a3edced0ca8ebf', 'f52dca37aa3b947b440e4c4889e1cdee6c38faba', '0124438268', NULL, 1, '2020-09-14 11:56:58', '2020-09-14 11:56:58'),
(132, '6eb7e36edce7327b5c6d94a12d785ac5', 'Ong  Hock Leong', 'Eddie7700@hotmail.com', '02c317f53ea947551f4046d61a655304ff2cc0955c45dff227d1adb991aa302d', '4e595a45536f695e8c6ddab3a1424c4e93945346', '0124780881', NULL, 1, '2020-09-14 14:06:10', '2020-09-14 14:06:10'),
(133, '0ac03abf9306842a01ded8da0a202b12', 'Penny Ng', 'pence_ng1201@hotmail.com', 'b8f36a2ac63e433c8e5cc0ee19889abccc8a92a36f5305b6082187718fac529d', 'e3345f4b469a704606cf613cfa763b9ec26ef75a', '0124022066', NULL, 1, '2020-09-14 14:07:06', '2020-09-14 14:07:06'),
(134, '1f69e957c55ed5516f5fad17eb7b227e', 'Khoo yuet boon', 'Kayabun80@hotmail.com', '59dc5569dd573a8444fe8e1503cd615226d427caec7c6eb1244143627692d106', 'a8ccef4b08139f167380ddb5ed5868c6423cf358', '0194666613', NULL, 1, '2020-09-14 14:15:08', '2020-09-14 14:15:08'),
(135, 'ae1dc61c75446f7b1773099e883b4dce', 'Chuah chin hin', 'Chinhin @hotmail.co.uk', '5230e6cc2b2580adac1934abea41d88c56f315a8c3b87668aa57e085ccbc376b', '64d004e4683b8baf10ec8881f51243a3c2f0fd05', '0194472928', NULL, 1, '2020-09-14 14:16:17', '2020-09-14 14:16:17'),
(136, '61420a7e72690c5c5093931c56f3cb8e', 'Lee Wei loong', 'Jakson_8888@hotmail.com', 'c7a79876a659405289a3279de8c57e16ad195bf064ebb3662e495ca2eba1ba1b', '433c427b092a51077c1865a5505e5115396ca9ca', '0164801419', NULL, 1, '2020-09-14 14:59:12', '2020-09-14 14:59:12'),
(137, '46fbd83faa829a2bc3beaa616660fdce', 'Rayson', 'rayson_cts@yahoo.com', '7cfd6f95f6d8c3193f6cd34c87af1f5efb0daef2332e910c56aca9689be9c768', '40022320e8366aa317bc1d345684822a7e2674b5', '0225673166', NULL, 1, '2020-09-14 15:01:34', '2020-09-14 15:01:34'),
(138, 'bd9e680d4b65756a5c1cf1b9b1aa30b7', 'Philip Yew', 'Philipyew@hotmail.com', '35553da0e63efb3df9ea4c6e7b95c051b6f5e91dfd34cd8fdf5c830aa11f8605', '197a46c32322a034460dcd9b1c50cbc4300f551b', '0134776868', NULL, 1, '2020-09-14 15:28:56', '2020-09-14 15:28:56'),
(139, 'd884ff38fa9de3313d1fde720c79c2a9', 'Choong Yen Kong', 'cyk7354@gmail.com', '14b5636519aa2bc4835506689d6500ab8eebf33211a8a7545d45a7bf207114b1', '95503ab1804eae967ec16d2f5cbffc6814adad1e', '0175616988', NULL, 1, '2020-09-14 16:35:03', '2020-09-14 16:35:03'),
(140, 'e24f908df94c54fc319b4a679ba2ad7a', 'Ricky Lew', 'ricky_lew@yahoo.com', 'e6554f74228dab77edaaf289bea3225dfe924712dd7f736cef06b294143886be', '28d075d8c3e2699726a37562b5588802fe349911', '0165265823', NULL, 1, '2020-09-15 00:30:28', '2020-09-15 00:30:28'),
(141, '6be1ff5179d2e70348f5e379694b28a9', 'lim boon teik', 'jlim362@gmail.com', '17b8c98183f2dab6360ada3fcbec9c78e7e9df8a161ab8321f34b95525304560', '7e976bc2401835687e64d4f2fbb1f771a35576bf', '0164199932', NULL, 1, '2020-09-15 00:48:59', '2020-09-15 00:48:59'),
(142, '35ae1a3c3e057af5ca587d3390512259', 'Ng', 'Jimnglh.lhng@gmail.com', '6a34fd695785bc9d96bbe0988d29ff0a265678392da416793f851910f25bc93a', '7007acde3adf3023a886cd9256441e10b67213fe', '0108908909', NULL, 1, '2020-09-15 00:50:49', '2020-09-15 00:50:49'),
(143, '33cedf59e066c0dd304d6fab2da6e519', 'Koay Hian Beng', 'koayhianbeng@gmail.com', '88f3f8317cfc7d704246344751482daed3886143a58b2ff86731d743037f4b20', '7e83a1480c50f285f4bc4c52bcf204d28943b902', '0124666903', NULL, 1, '2020-09-15 03:36:30', '2020-09-15 03:36:30'),
(144, '9bae92ffa6fb2f2148aca170e0c08b13', 'Lee Eng Leong', 'klaoven@yahoo.com', '9f8f81093feb91fca297852775dfaf4955820be410dd394a443e9951dbe4fc7f', '22b9b1df434c592db8c3f0ecb087c833072fcb8d', '0194883576', NULL, 1, '2020-09-15 04:26:30', '2020-09-15 04:26:30'),
(145, '31a1e18bc08a87a9b329b5fc144f01d6', 'Ang Beng seang', 'Bengseang@gmail.com', '2aa46480d0e8d554124a8f67b9df52ea9dddaffdec36e69b0f8da7a4d0ad7b19', '73573b0faf5b63370b2c055a642fb00a2bd93653', '0164711778', NULL, 1, '2020-09-15 04:42:53', '2020-09-15 04:42:53'),
(146, '992e954e01eda485a76da453815b1d82', 'Tan Chee Keong', 'cheekeong_my@yahoo.com', '01507ad4e38a8b7a1689279642e4b0b5005750d47cf29badc3fcda768ee10213', '898c7eaede1bc2b8b021d37c1bd0587b8bde2a7c', '0164755376', NULL, 1, '2020-09-15 05:05:40', '2020-09-15 05:05:40'),
(147, '99bd540c7ee295d5b9981fb01dae1779', 'Tan Poh Hing', 'cystamp@gmail.com', '6edc64549ab0f091886dbdebe7c4f38f7d8258dd355598337a8c65c5e35ab8ac', '666b43776019524f30995851840a6f1a23aa7944', '0164034162', NULL, 1, '2020-09-15 07:32:34', '2020-09-15 07:32:34'),
(148, '385390f6c21e51b1fd8678255124eed6', 'Thum Lik Tatt', 'sonygrey97@gmail.com', 'a020b052cb7e923970c6a9be7aa9995eaa014c5b1e12e2f3cf0e851796f929aa', '036f549d599e6570a18424de24a47fb87fda4857', '01116561098', NULL, 1, '2020-09-15 08:48:05', '2020-09-15 08:48:05'),
(149, 'ff029ce240638019eaf8d9c400088ef4', 'Tan', 'lilianloke947@gmail.com', '5d7ea1c371a23485910da0b628d9b95cefadc454a2f673b1b407b03f239b75bc', '9589dbaa1bee7bd3680e372bc3034d8d21ad9bae', '0146035875', NULL, 1, '2020-09-15 12:30:42', '2020-09-15 12:30:42'),
(150, 'b203c097e594caca628ec2c9c9e54f9b', 'Ong', 'pohaik@hotmail.com', 'bf6b3927380715b2b03f14302d7e10ec1a052cd5eed693d9a787ae238ec681fa', '8e047f8cd9f5e025e7723f3e8aaf65381ec5901f', '0164228771', NULL, 1, '2020-09-15 13:12:51', '2020-09-15 13:12:51'),
(151, 'a77b8e6a0a3677a2f3e7e50b8265630d', '', '', 'be396252ac7afde7fef8d8e0488f325171807120c9a3b81c5e53e7adcdd8449a', '6fd9bd6ff6312aef174844b221cb9123ff270a18', '', NULL, 1, '2020-09-15 13:41:33', '2020-09-15 13:41:33'),
(152, 'c709791f5cf836aca4fdef9bd4dd8c88', 'WONG KHAI KHAY', 'Kkwong3049@yahoo.com', '7ba0cddcca14b2902a9340d7db4e0de6f6290fb3fb690d056053ad4bb5f66dfd', '6eb33cb2ce6cac4d5cdcbf1614879f3f592bd240', '0164192038', NULL, 1, '2020-09-15 14:04:37', '2020-09-15 14:04:37'),
(153, '52bdc993ac2c1f882a8ab47fcd974345', 'Loai Peng Hoong', 'loaiph64@gmail.com', 'd40f357918472d492380e8b518d7e5b7ab1f60ad7529fe83f4cf394ef2acaca6', '6595f048079e062f7cdaa55e5e107b8276d528ed', '0175015417', NULL, 1, '2020-09-15 14:45:48', '2020-09-15 14:45:48'),
(154, '979d72d01f99e9f05e4bbf74a2fde382', 'Sally Goh', '5sallygoh@gmail.com', '482d626c0356446e65175a8b3b9b8adeee44ba7d198abfe9b62d68f5480cfb36', 'bf98417eb2a0c5ac321fb46751780e950eabda33', '0194705799', NULL, 1, '2020-09-15 20:19:11', '2020-09-15 20:19:11'),
(155, '620a8ca0ef4254c3a708ebd6f62ba0b2', 'SAW KIAN WEI', '', '7d17b456ed51a2b4ef6024afa0952022c7747cf0bc41d8afeca53b9fc59a6ba4', '1484eeab4488f6c5bd98b2a19de3b62070f255d7', '0125678699', NULL, 1, '2020-09-15 23:46:46', '2020-09-15 23:46:46'),
(156, 'c8a92f8e05d657a6da59327362cb8cf5', 'SAW KIAN WEI', 'davidsaw83@gmail.com', '981ea01ce78ce8b856637169eb6dd5efa2bdf8c4fd3732d8ee670767f5a9f73f', 'f9a38c4a00ee4ce11d4b996ba9e504f62e9c40ed', '0186884211', NULL, 1, '2020-09-15 23:47:59', '2020-09-15 23:47:59'),
(157, '9590d174903c957273d7eea0f8a00038', 'LoonHooi', 'loonhooi@gmail.com', '7a173b12aacf47e192c4481cd7741d866f362d2c6da3f0e28985dd4bac1e39af', '7103e73c5d35211b76a7b00c3d00f811b64a0a7c', '0164752782', NULL, 1, '2020-09-16 01:14:57', '2020-09-16 01:14:57'),
(158, '46044af321f357e44cd8995c048b1cd6', 'Cp', 'Cplee.personal@gmail.com', '02c249c697b80fee4d35c798f9d1e6b5ee33c17e410e137a5d1abda332512f70', '1cedf51fd3335a971d28885035800854dcdfb454', '0125788668', NULL, 1, '2020-09-16 02:48:28', '2020-09-16 02:48:28'),
(159, '58afed1a30af37c467f203fd6ffd2c55', 'Pek Bee Hong', 'pekbh88@yahoo.com', '94edce8d33ec7f6a12ec63ef1c04dd1fc3198b2c368ac66102f9d41b620d4587', 'e1c081b7af0175b611a1ff9b291c0098eb9213ea', '0124618355', NULL, 1, '2020-09-16 04:13:08', '2020-09-16 04:13:08'),
(160, '793ad196c1954aa06c6a1667a54547fb', 'CHNG', 'clchng@guangming.com.my', 'c120ee7fe7b9985672f20092d6bff5e66ee4723857a375c74c7e05d1df4d1852', '78187dfcc09bebb526d28c13d92b5ccf087fa0b9', '0129628975', NULL, 1, '2020-09-16 05:43:05', '2020-09-16 05:43:05'),
(161, 'cd77264c9384dba0da771584ef1b58f1', 'CHEW CHONG KHENG', 'chwwllm@yahoo.com', '7fa492f0a78aa0ae37250aaaab21395cf9dc5975107c0e92b20e7b5b8802936a', '25767205308ed8e7e293c7a6cf5699f6baa606e9', '0124287268', NULL, 1, '2020-09-16 07:06:11', '2020-09-16 07:06:11'),
(162, '3f1d2bd06c596e2fd77892a618f0b557', 'Joshua Ong', 'ong.joshua@outlook.com', 'bd1698ce15593b3a2c93f05ed1ffec35e936fd3ad4f270fd0f903eb622319647', 'ded79f621597abffa383dbd78c537cc1376a36a0', '0124543014', NULL, 1, '2020-09-16 08:18:38', '2020-09-16 08:18:38'),
(163, 'f11a00ea202387c7638fd0c004e966ca', 'cm chong', 'benchalon@gmail.com', 'abac17e89dbd30d266ca5d1c3e37912245cfb547996afe5daac14ecee35b2842', '870355a66953b96a4710296c9421bd7d69fe1bad', '0149019960', NULL, 1, '2020-09-16 08:44:10', '2020-09-16 08:44:10'),
(164, 'c0156963ed5e8b53d18dad7c4472bcbf', 'CH&#039;NG', 'sweeean@yahoo.com', '9094afbccc015e73e1a2c9e2db8510deddb31c607e0fa94913106ac7d1fcb153', 'dd6abafec02735a9509163142a434dca87c284bb', '0124668722', NULL, 1, '2020-09-16 09:05:18', '2020-09-16 09:05:18'),
(165, '6f0f2ebd5a93d944e146f6551b2134d6', 'FCWONG', 'fcwongpg@gmail.com', '02e963dbca89de724ed4bc5595015a03f8d333fed4c4fa1042097deec3052a15', '3fe795643b79d4966d6459288b0362464a7e41e5', '0124545315', NULL, 1, '2020-09-16 11:45:20', '2020-09-16 11:45:20'),
(166, '30e7d148c2edbae44ed21bf12fb2d353', 'Alan lim', '', 'f3548ff26f6138a467081ac11211ad6f19c754f1ef3bf26dfe7bc623dd61d056', '8764e405200f0e0c63f170b6c52fa250139fa3da', '0194475585', NULL, 1, '2020-09-16 13:00:06', '2020-09-16 13:00:06'),
(167, 'c67e7e96016e96ab993e1922fbf79da0', 'Khor Wei chen', 'weichenkhor@gmail.com', '83fcc39b04a28833fbd03a670288cd47d9d88ae6f309e0c0b75469e7ba123858', '806b0ba1d46939b04cd8fe61bb150032b0453840', '0195772125', NULL, 1, '2020-09-16 13:01:43', '2020-09-16 13:01:43'),
(168, 'a2124344a89aaffab8f5b609b7731a9b', 'UNG GIM LING', 'jinh_ung@yahoo.com', 'df8537fd5fd6e0bb3a8d693d55ad4427c764487637f161409427a080e531d1f6', '7eaa240ba50fb1ec27aa4371173bb1b332077245', '0164223530', NULL, 1, '2020-09-16 15:55:53', '2020-09-16 15:55:53'),
(169, '60e1ae36a62375e1275825d3ef06d4d8', 'CHEW KIA LIANG', 'klchew6@gmail.com', '72dec013c62345dc0f33b201695a6e15cb3e33a261822e7f8f13c46ca991797d', '484c698475ca60e24fb751a2883561b328cdbdd1', '0165534126', NULL, 1, '2020-09-16 17:05:38', '2020-09-16 17:05:38'),
(170, 'fa73dadb2b9d96c990f1d32f7fa90f59', 'Eunice', 'hlingchng@gmail.com', '6dd84f7ebdb7664b0f20b4350b8b7777abf62a9368e2ba40ce63212851bf9152', '78a84f018cb113155cdd71e8354662759240d7c6', '0127172808', NULL, 1, '2020-09-16 17:06:40', '2020-09-16 17:06:40'),
(171, '737e62aefe5f17913b5518f3cfb7fb4f', 'Jeffry Liew', '', '2436e57d84086281957cefa3a4b7738887e93a20cac27c81e2687c59891dc418', 'fc33b6fba9618cc9c665588bd48c156032a60229', '0124516367', NULL, 1, '2020-09-16 21:07:59', '2020-09-16 21:07:59'),
(172, '25c6f38042c090873ce6b2bb3fd7f971', 'ooi mooi yong', 'maxteh@yahoo.com', '36333f2d09fdcbbc1bfd72298045155953179d158c4a90c6eb458f0dac33c298', '998968d9bb6d7c3a9dd5367d4f39a20ef9a67c26', '0102221043', NULL, 1, '2020-09-16 21:12:35', '2020-09-16 21:12:35'),
(173, '930166f2abbcfb3b4e874025ec7bd9e0', 'Choy Chun Keet', '', 'c1b5a217f670c6e9342a8904af596322bedb7c3f7c3a8ef054eb21e135b94b23', '9518c0bc7a118dfc382597bd199f648a2e2ed34c', '0125535126', NULL, 1, '2020-09-16 21:13:29', '2020-09-16 21:13:29'),
(174, 'f4debd8073ca758cdb290f96b8e40a13', 'Lim Jiew Chiew', 'jclim55@gmail.com', 'f5225eb14c05fb4e79e73a613dc9065848749b9db2c5ab9e2160e6781012bf85', 'b2f565f14cbc23276ab1cbe3a6a5665a9fdf033b', '0164206189', NULL, 1, '2020-09-16 21:18:46', '2020-09-16 21:18:46'),
(175, 'd78e2adaa3fcb1b4661a8e5612fa867c', 'Joe', 'obze91@gmail.com', '9ee3835eca09c198d51b42ab92cca894ca982e90c91dc4fc52cfd7efe7a78ce6', 'd5b9a0ab737f65704916d99b8a434cfbb73d4e23', '0199390215', NULL, 1, '2020-09-16 22:46:45', '2020-09-16 22:46:45'),
(176, '9a3c8f4bae3528718d09473325fd9033', 'Vincent Low', 'luweilong23@gmail.com', '5a4d6fdd0c57eef2efd20031bddee174307873e61a3c421a4bf025dc6f790485', '72d08d04edcd8ab4864c01d6b522853fa5415758', '0124200988', NULL, 1, '2020-09-16 23:24:31', '2020-09-16 23:24:31'),
(177, 'b1b5fc14584ffc3ddf05470492b51975', 'OOI POH SENG', '1986ooipohseng@gmail. Com', 'a5b4dbc170e1295160bf8e86f98a79d32c3f94a44921f78cfd1002e68b2daa67', '291fce42a382b985a4f796610ff8c8afea884fac', '0184604879', NULL, 1, '2020-09-16 23:31:07', '2020-09-16 23:31:07'),
(178, 'b0358a467baf176cf15d105f57f64d5d', 'Lai cheah swee', 'laicheahswee@yahoo.com', '3a3d9d063f1730c34c3d1e4424a26778a47d4913cb96cde893e207e8d508ea0c', '83715625d3fca9c3571d422d6a02a71d40a623c9', '0194000261', NULL, 1, '2020-09-17 00:36:57', '2020-09-17 00:36:57'),
(179, '4947d4a8601c4511097c849c18c97b58', 'Sim Khoon Hooi', 'simkh2@gmail.com', '392095394e4b2364472431e6cad4974d4f4d4d4314c1cb05bd8f462e355a4927', '4c84362de9f2ad66a75b455f839d347a7f9812ad', '0124618024', NULL, 1, '2020-09-17 00:50:50', '2020-09-17 00:50:50'),
(180, 'bdd3d775398a587a86fbc6e6cffbecc3', 'Lim Sheau Rou', 'sheaurou@gmail.com', '0617515605f051c8be213b891dfef7c32ef94b22b889a42e5b71474be2266452', '2f9f8a120b85e4a7f4c22e0acc5aad1df607fdfd', '0124583172', NULL, 1, '2020-09-17 01:10:59', '2020-09-17 01:10:59'),
(181, '061b71e540b9991c5c2a295d47985ff0', 'EL', 'ewelee1@gmail.com', '48db07eefe24509cb11cf29f3c77586d36672063996d241bf028cd71f4a4e549', '8eee839cfd7a878c1802002b418b5780858e3e19', '0165559210', NULL, 1, '2020-09-17 01:19:28', '2020-09-17 01:19:28'),
(182, 'a50f16e7a8deda11b1c159d08df77b63', 'NGAN THIEN HOE', 'nganth66@gmail.com', '711b3d7f2b728fb27935aa0bd1709a0c13c3013dd229135071944e606294dcdf', '0a67c5b9bbe2681754f93749734c796cbe459c88', '0194789998', NULL, 1, '2020-09-17 01:36:04', '2020-09-17 01:36:04'),
(183, '19be2f2d12b2c2ea3661f1a5bd203a02', 'Seah WC', 'srah_wc@yahoo.com', 'f90016a830908e88d9125572afc876560a52eb149b1e4d5596c2bb50577a3dff', 'ae576cafcf067b6fc0231bf6bf99ca1537a857f1', '0124302107', NULL, 1, '2020-09-17 01:36:43', '2020-09-17 01:36:43'),
(184, '2b2e8ed9fd40201450e87e309a1d0ef3', 'PW', 'pohweng@gmail.com', '4f45729da097b29fa9df3821552c25319754e89e3494959627e67fe15d300390', '816064acdc21f5b478ee0e3e875cb2984e48fc2b', '0125253585', NULL, 1, '2020-09-17 01:53:07', '2020-09-17 01:53:07'),
(185, '165a3f4c83c8b063b659977f88c8e3cf', '李慧卿', 'qinglee1122@gmail.com', 'f0605e0a90826132762c21c5794726763294fcda452ae766baeb8c68eeb3ace8', 'b16308902e44724d13bdc4f640ebf6f9869594f7', '0126597897', NULL, 1, '2020-09-17 02:55:33', '2020-09-17 02:55:33'),
(186, 'ba951dd5c02fab60743a1d2fc2d2b4b4', 'CHEAH YEE MEI', 'yeemei.cheah@wellpoint.com.my', '27ef976ddd5fdd28833fbcf4a721ce60bedccd1209294986610a44f94d38c934', 'ae71b169337fb13444574494644f3793301993d1', '0174158003', NULL, 1, '2020-09-17 03:57:19', '2020-09-17 03:57:19'),
(187, 'fa34f67eab5c69d48d2e14932a89fd59', 'JANICE', '', '15a122cd2f0d14f42c9e82d736b9944517584a4e01105ee54676a87f44e8f74b', '3b5634ade790693482f0f2569e15b01ee232806f', '0122820802', NULL, 1, '2020-09-17 03:58:15', '2020-09-17 03:58:15'),
(188, 'e6bb93bad284be754d3958788b15181d', 'TAN WEI LYNN', 'weilynn.tan@wellpoint.com.my', '8eb43f155a168acef51ac6eeb749987760a235d7e69f53ebb1adac561fdd5b44', '7f68659bced58d75785abad902ff02617e410013', '0125252839', NULL, 1, '2020-09-17 04:00:20', '2020-09-17 04:00:20'),
(189, '8b378ab673c11932b3974aece3083188', 'WAN LILI', 'lili.wan@wellpoint.com.my', 'd46a99150c33ba8611ae1e579fd2ef0d060c57e6d5f8a9cdf7cbeefb21ee045e', 'ef3a9046251d5e566ff9b5f57d377f7987f210f2', '0124651668', NULL, 1, '2020-09-17 04:01:01', '2020-09-17 04:01:01'),
(190, '06a638ccffd848c557d34eac596c80d7', 'CHEW KE XING', 'kexing.chew@wellpoint.com.my', '05731a942a36918b0287d3eb39184f41a10e90160c43040ac5f909a09a471361', 'dd33d7ad2211d92a7ce5b958d0fbe0ac9ae99b0b', '0184086998', NULL, 1, '2020-09-17 04:01:58', '2020-09-17 04:01:58'),
(191, '5b59f2d781d417cb38fe088479061792', 'NG MEI FONG', 'meifong.ng@wellpoint.com.my', '52684faef56e79ce4eea03b953d5ac148f8d49596ae75f2c9eeb446b2dc5e924', '8fbec80d8988e92b8e44c58c869f8c6c376070eb', '0189870511', NULL, 1, '2020-09-17 04:03:06', '2020-09-17 04:03:06'),
(192, 'd091d7b79f2917b7e9b4f121d5143514', 'SONG BOON HOU', 'boonhou.song@wellpoint.com.my', '8c3408e17bdc757236272651db6f41266b5fecb4758a707cf7d91709d38cf8e2', 'ee14490f4be68adc809445e8066a49b1e5514384', '0174303850', NULL, 1, '2020-09-17 04:03:44', '2020-09-17 04:03:44');
INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(193, '1470b3aa26bc3ee42ac84b2f37683078', 'WEE ANCO', 'anco.wee@wellpoint.com.my', 'ce8e8e07c445fdae7c331d4917493ab8e30ab09597c6512a34587373ca959fea', 'c5d13a809ac9f1c5278869b8834086be453f1e36', '0177681809', NULL, 1, '2020-09-17 04:05:04', '2020-09-17 04:05:04'),
(194, '3d4ca709e6ccb224b14696649379e2d7', 'KHING XI NIAN', 'xinian.khing@wellpoint.com.my', 'c7ee45ba882fb60f7f3440cf6ce3f2820deba368ddfa3c306543742d707d97f5', '66abb2c2d2139b461ca4e6ba6d315e5c4bd3923b', '0174371794', NULL, 1, '2020-09-17 04:05:39', '2020-09-17 04:05:39'),
(195, '24105ece3a41c148caa3e2866230fe40', 'TEOH KHANG WEI', 'khangwei.teoh@wellpoint.com.my', 'f6c6ff49e97efafadb3724ea662972838ec5767b7870af3d3026415fe9a7dc86', 'c0411affe5135384d91db09364c26bcd61b94d19', '0184663010', NULL, 1, '2020-09-17 04:06:20', '2020-09-17 04:06:20'),
(196, '562fcda27a34e53b853b5115d9e031c4', 'EOH YEE CHING', 'yeeching.eoh@wellpoint.com.', '8792558811aaf9632ddbec70563680a4bccfa27560cbf234ac2a25f2b27bae2a', '246fc1b655bf87323cff9b83548103a7ceeb95f8', '0164246364', NULL, 1, '2020-09-17 04:07:02', '2020-09-17 04:07:02'),
(197, '377b3157dd683e610ecab346f15bbd5a', 'LIM HAN NEE', 'hannee.lim@wellpoint.com.', 'd3e2d457ac571cd4c9d73e1a5dbbac1cacb07765351b25584ebf5fe2a7be27ae', 'a6b3652c9732517f3af6a299872fb1acffe17ae9', '0194216318', NULL, 1, '2020-09-17 04:07:46', '2020-09-17 04:07:46'),
(198, '70f0f90ec09cb1fb84fc9e013a7ea217', 'LEE CHIA XIN', 'chiaxin@wellpoint.com.my', 'fb982e6a773a3a7494919f92bc5e084deb357f9619fd07d324a14bda92d5a3a7', '5e0ffec4fcf2ec9dd757ef23613f64518d85985b', '0135347820', NULL, 1, '2020-09-17 04:08:25', '2020-09-17 04:08:25'),
(199, 'be8b40c82127c3244783ae29974bba43', 'Vince', 'Vskl1811@yahoo.com', '809fe3b05b2ec478d0466e2594c5d713d58af6343d4861b7172f0557e6e4330f', 'fdb4d04de9f5d67d86621fa35c21ff4fcd350415', '0126448868', NULL, 1, '2020-09-17 04:17:20', '2020-09-17 04:17:20'),
(200, 'ed9136911dc7a28df6d0a35ca6b26349', 'Edmund Chew', 'chewannboon@gmail.com', 'ecd2d5286935c10b5e868346e176ea04909338c06ce092267b8459d0c8e9e0cc', '8e5b8976641e33be9a8bf8bbac57a3ced854b953', '0165661707', NULL, 1, '2020-09-17 04:50:09', '2020-09-17 04:50:09'),
(201, '0e7977c17fd8acfb977ae56bdc7c5b71', 'CHEE TOO TUCK YUAN', 'alex.cheetoo@wellpoint.com.m', '124a87e036aa0078ae513533b8898fcba863bdc65e3d743159a68f783e802a2e', '53ea33dbc94765a6768acb2b7a41991843545ae1', '0143418790', NULL, 1, '2020-09-17 05:40:15', '2020-09-17 05:40:15'),
(202, '9b48a7e778097f97b69d31706271c416', 'PHANG SHI NI', 'shini.phang@wellpoint.com.my', '8ba201d54d96eb2d6762e03c84926b23add3c40c0d2ff761e8c91fdaa49b7b24', 'bc6724bde1e60c5d1f8f6d49d8db28d6d2746abb', '0164289282', NULL, 1, '2020-09-17 05:44:02', '2020-09-17 05:44:02'),
(203, 'd01ef46ceb190d8ddf53c47cb44aef8a', 'SEE HUA', 'seehua.wellpoint@gmail.com', '0a3af9150d1f4d40504e73bdfac0609cfd55be03b44a406b419f20c33fb557c4', '2a6033533f9d663075b6da3a41fd7eb08f8febfa', '0134888683', NULL, 1, '2020-09-17 05:45:01', '2020-09-17 05:45:01'),
(204, '06218559015d027bf126dd22bbc2b7a0', 'OH SU HUI', 'suhui.oh@wellpoint.com.my', 'cf1e433fa010ea99f83d64a05d52d55315a9ca163a2d6d45188c87d0d065f6d5', 'b8567c29f682ff54a20f4548a81fc85e023237c7', '0124989021', NULL, 1, '2020-09-17 05:45:45', '2020-09-17 05:45:45'),
(205, '86e2b293b3efc80fb81b3aa16226a800', 'TAN HONG PING', 'hongping.tan@wellpoint.com.my', 'fb5f5d4d02773611748554caf361356098f9c711cf66a7a6b1b94b4ca4109069', '7f406bb95c994c1215b254410c0543fb60e78290', '0184034796', NULL, 1, '2020-09-17 05:46:40', '2020-09-17 05:46:40'),
(206, '228b85e3036e99a5ab61a1a0f6ea352e', 'LIM CHEN YING', 'admin@wellpoint.com.my', '3caa3f8bf3023498cbf28bdc8b932dc5667151c6e7eb4f85dee874e5d72b4206', '6b2b223d494cf652a1101b198fe0d9807fe6fea6', '0165215302', NULL, 1, '2020-09-17 05:47:30', '2020-09-17 05:47:30'),
(207, '62847140ff70adff2d05130269148bad', 'LIM HOOI LENG', 'gst@wellpoint.com.my', '1bd8905f82d21177941a74e0fd31d6141743c5a2aacd552feab41e563a3e35e9', '3f86e2a4a58cc2069c64738f3272b50a89cb56d6', '0164140937', NULL, 1, '2020-09-17 05:48:16', '2020-09-17 05:48:16'),
(208, '2b1a4bcc91d70742b404e5a692dd9857', 'QUAH SWEE IM', 'ocean.quah@wpco.com.my', '602f111e5a4115a02f9d15dd046908378a4cd3952c330e095476a3162c96d278', '921e823e6ef09f4c1a5db3c8b5c59f13d01f1734', '0125564103', NULL, 1, '2020-09-17 05:49:39', '2020-09-17 05:49:39'),
(209, '17aab7c0d901a00a688a2de711b4a822', 'YEAP PEI LUAN', 'peiluan.yep@wpco.com.my', 'd49bbc914517c8ff5a6abb6b1745c067f41308f4ae7d186518ef36f3d3e70db3', 'b4ccd605893107af6bc05b280996332209f9d6cf', '0125754778', NULL, 1, '2020-09-17 05:50:16', '2020-09-17 05:50:16'),
(210, 'e03ffc696e570034351af33779902bd6', 'QI HUA', 'qihua.wellpoint@gmail.com', 'b00fa64325bc15f881dc70d1629552d00661040c4770839ebeac487362988de0', '2124327d52c78df16a8e801c97105f8ae13793a9', '01110634679', NULL, 1, '2020-09-17 05:51:02', '2020-09-17 05:51:02'),
(211, 'a9476841680f549944c7607d63bc9265', 'TIU CHIEN CHIEN', 'chienchien.tiu@wellpoint.com.my', '22ba7a77fbb9fd6039a3ecd4e8513f25932b4721d945d9f04d414c449c5dd649', '5cee74240cdae3ba697e346d37a41e4f6ba30e82', '0174150168', NULL, 1, '2020-09-17 05:59:01', '2020-09-17 05:59:01'),
(212, 'd259bde35b8d1f235c0cf3618fcf1e64', 'LIM LIANG CHENG', 'liangcheng.lim@wellpoint.com.my', 'ca927d4def51aa919c9a02d3120a9c2317540a32f2055c51dcfca7f84e14c71f', 'd7f630765403229b4e13c016cc5bfafe677757a6', '0164538010', NULL, 1, '2020-09-17 05:59:52', '2020-09-17 05:59:52'),
(213, '05c53b596a0a7eb6d09534899412cf06', 'CHONG PEI WAH', 'peiwah.chong@wellpoint.com.my', '489915015458568badb2ab9a10b944f8004bd1383ea4c298f836d0889a0b3bda', '8d0437647b8a10869a49e5cf88b2801723dbc83d', '0124308428', NULL, 1, '2020-09-17 06:00:43', '2020-09-17 06:00:43'),
(214, 'c59840c654edef366cc35224c5d54092', 'LOH KHAI SHIN', 'khaishin.loh@wellpoint.com.my', '0437d02b63796ed687d8424cb6ae723df7b699a9ec06f32e81f3d045d1df43f7', '4032973dfb88539562fb68000195189d562052fc', '0164374992', NULL, 1, '2020-09-17 06:01:36', '2020-09-17 06:01:36'),
(215, 'aff9b462eba5a519ae0a58c26f1d2d1e', 'CHEW WEN XUN', 'wenxun.chew@wellpoint.com.my', '831f26166e62fbfa37c7dafa41692b38b67735af31a3dec00debae085d1e0d46', '3374df22e5fa4bfc43dbfc140481865c75e77e2c', '0175109546', NULL, 1, '2020-09-17 06:02:46', '2020-09-17 06:02:46'),
(216, '40b5363bdd7a2cf43a39ee8c3809b345', 'KENNY', 'hoesoon.song@wpco.com.my', '7304e27d12e8ca9fd49af059b271770192f039c42c554df48162b442c3b36893', 'dc0f4374478f0efaa9a4e0eb29de03ac4b3c641a', '0164353979', NULL, 1, '2020-09-17 06:04:51', '2020-09-17 06:04:51'),
(217, 'aeff3e03c4ab808041455284a553b6a3', 'LYE SWEE THENG', 'sweetheng.lye@wpco.com.my', '7d6e2f4e749c47bebf5f7bf1432c862dd40f2057657bedeb2679c490eebf7571', '2ca6432c72eeb084a6618dfbdcea178c90825c2f', '0124260087', NULL, 1, '2020-09-17 06:05:53', '2020-09-17 06:05:53'),
(218, '88e157d808d60e6a609d7617ae83aa36', 'CHONG KAI LEE', 'bmsp@wellpoint.com.my', '1544c1bb0ae82271c98b901e6844e01974b4ebfaa958de758d188593329d82b2', '200e03fe5acd608198baf46628670363196a482b', '0125508408', NULL, 1, '2020-09-17 06:06:35', '2020-09-17 06:06:35'),
(219, '592a79eb00e2c44e27de35236209f1d8', 'SIM YEAN NING', 'yeaning.sim@wellpoint.com.my', '5bd058d7b25766cdc6da54f850120e398938f6575e1a861a6b7a95669eb1733a', 'd6a8cb9428c6f8e9feceadde631faffd6c7cd729', '0164513553', NULL, 1, '2020-09-17 06:08:55', '2020-09-17 06:08:55'),
(220, '0ccca67dfc6aa98bf4a7d4783557d666', 'TAN HUI LIN', 'huilin.tan@wellpoint.com.my', 'cd6368f33d6657ad76710d1e82629f24d0c6561ee23c16484af22b319cb799cc', '90a9ddaf849676dc83dfa095e0350f0d317bc07f', '0174649343', NULL, 1, '2020-09-17 06:09:47', '2020-09-17 06:09:47'),
(221, '17b72a5c60f165b44e5cfeb3a71fa25a', 'WOON SHU WEN', 'shuwen.woon@wpco.com.my', 'de1f0d2a64f350c17af87f78625f06b16dc25c578f65b8fc54e3567502dd1984', '7876a7a1593c014b60b46dd9aeb701a4d8b0d831', '0164743286', NULL, 1, '2020-09-17 06:10:35', '2020-09-17 06:10:35'),
(222, '65ea8116c196e487d8912cee80cf105c', 'QUAH BEE CHUEN', 'beechuen.quah@wellpoint.com.my', '6dc7be20d69cfada9a203ca1251bca83e36992e788c69407b667f3d60ac237d3', '69861ab4cf7995c2b1c6a5c768a4cd82f7601c13', '0125596950', NULL, 1, '2020-09-17 06:16:01', '2020-09-17 06:16:01'),
(223, 'd3d3970e9695696dc6d28c61693a9ad4', 'Indiran A/L Balakrsnan', '', 'b26e754ccdaf0c7ab9e42edb4ab9b7abadc7ae94b96760a45c05982654647b0f', '9612e9958369f0dd83068c7af97957ad2bd3e79d', '0125225428', NULL, 1, '2020-09-17 06:23:37', '2020-09-17 06:23:37'),
(224, '2b157c0411dc0c18cd7215a3a71f49ca', 'MAS HAFIZA BT RAZALAN', 'mashafiza.razalan@wellpoint.com.my', '02869bcfc1ac8d5c461a141a0bcbf5063fa1785d2db2e01d10e1434e1790a88f', '0d6c5f23135722e8a93eea658a594de7273a9a06', '0125528197', NULL, 1, '2020-09-17 06:24:25', '2020-09-17 06:24:25'),
(225, 'b81950e6841e85d94ae2ceeb9ac989e7', 'TAN CHEE FONG', 'patricia.tcf10@gmail.com', '7003e7f1269e260b04ad97181250be52317fcaf2fd6bda505bbe1b6e9d5d9e39', '9adaf6844eed92dfa2fbb4adefb8cb6222fc8fb2', '0124529857', NULL, 1, '2020-09-17 06:24:34', '2020-09-17 06:24:34'),
(226, 'c2934b112767efcb59f652abbd5f513d', 'NORAIN BINTI ABU HASAN', 'norain@wpco.com.my', '8d848ab53a5426d3e7be081a49652ace790893bfbc3ec47594ae16574dc1148c', '13812bf722cfffcedd9724f778a3ed8c201462a6', '0194616641', NULL, 1, '2020-09-17 06:25:30', '2020-09-17 06:25:30'),
(227, 'd0e5f1d8c0d7fae6c22b71c243c2dd9b', 'NORFAHANA BINTI YUSOP', 'norfahana@wpco.com.my', '2ab1674926a74b3401914dc86e0aaa5f4111ac59953c39433ab415fd85c07a6e', 'd2a95054c9cf29eb58bf64783a054f0cb6855ec4', '0194083558', NULL, 1, '2020-09-17 06:29:56', '2020-09-17 06:29:56'),
(228, '0e1a5886d1b99e5e4cefbc8e445b2cb9', 'GAYATHIRI A/P ELENGGOVEN', 'gayathiri@wellpoint.com.my', 'ac0317437a0247189d38e3a50b0e97439acec0e284bddb0902835eaa0813c864', '6078825cb9e3b6e6ac78ffd9d11e675774c2f1ca', '0143420762', NULL, 1, '2020-09-17 06:31:59', '2020-09-17 06:31:59'),
(229, '9e466a008db8cdd1b43a91c5a26066b8', 'ANUSHA A/P RAJARAM NAIDU', 'anusha.rajaramnaidu@wellpoint.com.my', '52650b50d56f69db87cc4449a3335862ed1b5b8064997d61ae789633f6dba25e', 'e05d5e2dd207d1c098996b9c4d9ff50795995ac8', '0169335894', NULL, 1, '2020-09-17 06:33:09', '2020-09-17 06:33:09'),
(230, '909fae0954b60bdab380c120e5f90940', 'Ooi weng khuang', 'owkhiang@gmail.com', 'a4ca624a58d946159c447c6c225919904617f8fd554ccfd4b94376b9da320551', '0dfad1b0ecd970d96efab4aa564e4e56dd54c960', '0124716168', NULL, 1, '2020-09-17 08:02:19', '2020-09-17 08:02:19'),
(231, '099c11149ef484e5debc871e6f3ae392', 'EUNICE KHOR', 'cns9912@gmail.com', '06cf485e9b89d4bbd36aabe0455250b1c4feab0b0d0abc1482b10ea62092e062', '90d363f53ff189e2f76b8534efb78b357cf5ab47', '0164561233', NULL, 1, '2020-09-17 08:22:11', '2020-09-17 08:22:11'),
(232, 'f685c498551341277c4bec86084527de', 'SL Lianh', 'sllianh598@gmail.com', '5db3c43746799bfe290402ea99aed85d602c9df7fad0a1c0d2dad0c2a2ead4c2', '9f29431ccf22a4044e2c58d1934103c9d2399c9c', '0174764243', NULL, 1, '2020-09-17 08:25:06', '2020-09-17 08:25:06'),
(233, '87a1d2fc176ecd4cea460c2b7c6c046e', 'ANG SAY TEIK', 'sayteikang@gmail.com', '67c41029c4dd1b4d1e5e942c5ede3f539e7f9ec8d2c8dfb203237b6c89dca67c', '9c7f180cc9fbfe96ec0f77e4d48aca430235a9af', '0124825009', NULL, 1, '2020-09-17 08:55:17', '2020-09-17 08:55:17'),
(234, '3c0e4a6f2f99f504c31a0685bc3f7d6d', 'Tiw chee keong', 'edmundtiw@yahoo.com', 'd05a4b343dcf4e5043cd16a40325257b7f34087c262597469a952d1b89890d73', 'b39ce5d045f0dc2933ee8480b7d358824497b32c', '0189051439', NULL, 1, '2020-09-17 09:01:31', '2020-09-17 09:01:31'),
(235, '4e8c1b9c393a290bffaf565f45364654', 'WENDY LIM', 'siewcheng82@yahoo.com', '9a3b0c1bed3145012a547965f40e7173b6a6dcdc612b160284e6f116e437b41f', '61c3f11023aa5bb79c62df71077f36b4f0ccdf31', '0105394349', NULL, 1, '2020-09-17 09:02:47', '2020-09-17 09:02:47'),
(236, '62b02a858d23b8912ba78f6ef5054580', 'Lim siew cheng', 'wendylims@gmail.com', '380a28fb2ce8878ae0992b6b0c4b841472902ad12916f4180bba6cc69e328a83', 'c3bee3342981ed4a17f6ec15749c29acb0397085', '0164313124', NULL, 1, '2020-09-17 09:05:25', '2020-09-17 09:05:25'),
(237, '223eb69e983050f567d1197341667402', 'Edmund tiw', 'edmundtiw@gmail.com', '505081dd2536cd8d3f2bb8ba3d591bb71398b5a9f35ea8cf4cc8ceddcfd0756d', 'ca967542fc0f4a80268bff48388250d1789421f8', '01123890114', NULL, 1, '2020-09-17 09:24:56', '2020-09-17 09:24:56'),
(238, '2423365a510afae88ad00ceb757e7632', 'LOH NAM SENG', 'lohnamseng@gmail.com', '8e7d8c3f11932ec9a9fab5721ae7212a0edd4a9ac290fc1b0a28aae09c41bd37', '20656729232839cc6fa8dad29af28339782b5dda', '0124879888', NULL, 1, '2020-09-17 11:41:47', '2020-09-17 11:41:47'),
(239, '3adae8cf94d7eb85ea8674836be2a579', 'Ang Tze Jing', '', '145ef1586195b8e176244588d063e90f60a5bb4f7f3d3494093498fc4f60547b', 'e61d91f1362bea491b90504986cd0b90e10b8829', '61', NULL, 1, '2020-09-17 11:43:34', '2020-09-17 11:43:34'),
(240, '0a0a2b5436bcf2faa4a073687ecc6f58', 'Ang Tze jing', 'Zing_ang@yahoo.com', 'd5c34da16a5ea7a531bc3a0e6f014b9f17191b3c8be425dfbf5cc2b04ead8473', 'd13c27192bb6cb295a30870db60893c3d590c0bd', '0125589899', NULL, 1, '2020-09-17 11:44:15', '2020-09-17 11:44:15'),
(241, '0b5eb1dc3484cb1b3b4db5fddc91a8a1', 'vincent ooi', '1163719183@qq.com', '25e2717abed70ef6e6f9a215a8a898b11db26b6a6852ea38c1e6967d0244a824', '1ac32512426ace43845a9c647b385ea4ebf509f8', '01112859552', NULL, 1, '2020-09-17 12:16:49', '2020-09-17 12:16:49'),
(242, '1be18dba8f6ce3c7370e8999d6412719', 'CHIA KAH LENG', 'ckleng3873@gmail.com', '986f8238a8e6c9fb81fe4a49ee434348a8188cb968ddfa210346c5b9502fc3a6', '59ed55f710993c4b7a63b8bfb7e908ffebf6597b', '0174818133', NULL, 1, '2020-09-17 13:41:32', '2020-09-17 13:41:32'),
(243, 'dad157d2ec9ae2fb1d484c58792b69d6', 'LIM FONG CHOON', 'limfc2004@yahoo.com', '3128b61d65377781ea25051fe3e48b7a6a47a9fe10ca3a9f878379a3324f6f65', '15f583db1611d826dadb169cac8c2bb01cd4edae', '0124848907', NULL, 1, '2020-09-17 14:13:48', '2020-09-17 14:13:48'),
(244, 'f7fc95149fc7f8e8cf490ab2e3b3505d', 'GOH TUAN TEE', 'gohtuantee@gmail.com', 'a561e18cb3d8fb7fd889e99228d9cab718b6364a5ce9c4310dc290091d4e34a1', '517a0f20522179841b30422e69cdc23e4785f8a4', '0194791503', NULL, 1, '2020-09-17 14:37:28', '2020-09-17 14:37:28'),
(245, 'ee2939384ced33a994d967e3e1001885', 'Choo Yen Mei', 'ymchoo91@gmail.com', 'b914d273e2719f2d66e7b0fc7de3636c1844f3363a7924be5006f817f6ff1c2d', 'bae7e649f83ae0b4734975e91c586282e0a23598', '0124577741', NULL, 1, '2020-09-17 14:57:15', '2020-09-17 14:57:15'),
(246, 'b073d68ac613eaed22c3784c92ef19bb', 'Teh Yong Hong', 'tehsyonghong@hotmail.com', '40b26a3d5057c40d5f20b2b7995eb707dd75032559694c4ec906f6ccd9709a9b', '160492ce70802622392fdbac31f253a7eb347178', '0174170418', NULL, 1, '2020-09-17 16:47:37', '2020-09-17 16:47:37'),
(247, 'f4c4bd9283f20b78de0680309ade7a5a', 'Tan Gaik Hoon', 'tpc63@hotmail.com', 'c6083b278a9eaa66cbd0a6339eae8765640daeb7425583f714b9d83066d55846', '8e678f0c6de47e183715c8273fb04afdbd0900ba', '0124659315', NULL, 1, '2020-09-17 16:48:59', '2020-09-17 16:48:59'),
(248, '2de8bdac88233e5f2c6215f101ddbe3f', 'Chooi', 'Cheo6855@gmail.com', '128437389baefa6b3d1bb8cb590ca7641f0c7eada1bd3d6005a9132dff2d92e2', '7d1a44c882d9f5a9855454927d2816738ba4fd56', '0124088863', NULL, 1, '2020-09-17 21:57:38', '2020-09-17 21:57:38'),
(249, '6b299ea1b056d9754fbb14d3cd2d9611', 'Jackyteo', 'Jackyteo66@gmail.com', '74a625541e83cdffbb8d7a56247a79832f7c23635650ec69883424b76a42eb5c', '2b965160c2655acc94fb11a77753b40abe04f7ca', '60168281313', NULL, 1, '2020-09-17 22:10:29', '2020-09-17 22:10:29'),
(250, 'a1b78cc30d7e50b282d2087831f5b728', 'Alicia Toe', 'aliciatoe@gmail.com', 'c20cffa5ec20c6d53179f6f86f366189c67b47efcd38e8e4ca5f2b1fb6731f19', 'f874992aeb05f69c601a9cd9033057264308b10b', '0165558565', NULL, 1, '2020-09-17 23:52:17', '2020-09-17 23:52:17'),
(251, '0a5cccffd5a114a2a4cdac0e77ed6c65', 'Melinda', 'hooikeng.c@gmail.com', 'fb2fde4a5b07192cec29f525dccc37b959a4aeaca5a6c56e89f3504c397c0613', 'd7cb9ff626bd5ab2f4a3ddf0dca2b096ee0d50d5', '0124188013', NULL, 1, '2020-09-17 23:53:11', '2020-09-17 23:53:11'),
(252, 'b756809c0cb2e08beae1acadbf7805e7', 'Vanessa Ch&#039;ng', 'vanessacck@yahoo.com', '5475b953baa325c2531b26f18f6b050b271d531b3f28c77d5f8d15d9dcb29588', '38d4459432de189e84352c67a1c58f7ca5021cf8', '0195506699', NULL, 1, '2020-09-18 00:40:23', '2020-09-18 00:40:23'),
(253, '30b429b3047eff571aece1993e68d65b', 'Kenneth lim', 'Kenneth1037@gmail.com', '3460f45f38f6021fe8ee39bcba9451e4f87950a3790ab8c5f3d0b2e7a7f7726b', '80c42d02dde03d0b1c496d52ebabe211d03ab9b3', '0167110766', NULL, 1, '2020-09-18 01:06:46', '2020-09-18 01:06:46'),
(254, '94fa594aa21207fc7c7bbaff8bb6204f', 'Teoh Chiew Hai', 'chiewhai@gmail.com', '012613783d098ed41b8ba930444bd39f057dc61dae3d5d454048448908dd6ebb', 'daf0ae0d3f25502151ef3d04fc697ced73be1d54', '0194267502', NULL, 1, '2020-09-18 01:08:41', '2020-09-18 01:08:41'),
(255, 'b1aad25c8c79c2608b5b8a1e72c4eeb1', 'Chan Poh Geok', 'pgchan724@gmail.com', 'c5c7f995a03cbcbefc1463b77730449ffc5c9f360d4906b7a755042fa34d31c9', '44aca1adfd3c28bd3f90926caa3b1589973c5f08', '0125699868', NULL, 1, '2020-09-18 01:10:47', '2020-09-18 01:10:47'),
(256, '9a9bfb0b440a348c79763319c61fbdc6', 'WONG SHIAU FEN', 'vivianvarro@gmail.com', '546b95e1cd2820b76bd0d9006a22aa616cf127886c422b950d0bf4af0a7fbf49', 'c7fa3b4c83252471c550f35decedda0219e20191', '0124298526', NULL, 1, '2020-09-18 01:52:06', '2020-09-18 01:52:06'),
(257, '06ac953c92ec13756fe57ce669729b89', 'Wendy soon', 'chanjh9402@gmail.com', '5e05f1d9d5062ee91d3fe61c11f10258269af2e7db4f9dff684bb435dd0e22e9', 'ac2ecc2ad68aede3ba7c78ed5af56aab0e423655', '0194705593', NULL, 1, '2020-09-18 02:43:47', '2020-09-18 02:43:47'),
(258, 'e9a7af2d43a56803494b372e147b0e93', '6', '', '498092e13fb297c8cdbf55254b36bef5462cdcb544c4abd66bf68f197fee14c3', 'dc4620ad58b3686e645a65644b6ef09dd9e9f82d', '73473', NULL, 1, '2020-09-18 03:06:42', '2020-09-18 03:06:42'),
(259, 'c33e51dd9a1ab3355c9432e5b9191dd3', 'LEOW JOO HAI', 'jhleow@yahoo.com', '37cf61cfc718a65e066bd88789c50675ff7a055fb9340f0d4dba5777842e63b1', 'f8030d5907890ab41bebd0eeb0ccc47960aec62b', '0124882620', NULL, 1, '2020-09-18 03:21:53', '2020-09-18 03:21:53'),
(260, 'cfef41d53f41d4ba5f7680f420d2cfde', 'Michael', 'likkit1990@gmail.com', 'ea74396fa0d25d40a0f4bdeff02ec118974c605d069861ebf781086e972dae99', '6299296d609ff57bcb76c6179e065df7b1d65249', '174776832', NULL, 1, '2020-09-18 03:25:44', '2020-09-18 03:25:44'),
(261, '0aaf49f9752215d01eb175a7f8b96974', 'Natalie Tan', 'Natalietaneeyee@gmail.com', '4d19105a091d6aa71d163e3390f4a94b45dc8122a4c213137e684c0e9479c75a', 'fe50e51419011e337e6c08491826e99edaccc001', '01110770678', NULL, 1, '2020-09-18 03:31:47', '2020-09-18 03:31:47'),
(262, 'e78244a44d69b85b4477b1d135346e1a', 'Valerie Tan', 'Valeriethy@gmail.com', 'a450a74bcd62c1d6acbbc0747d6add777796cea73c8708a7d91b360c2db9ea4a', '8a4bd42be796e21c85a606d0e80de6c3e6b7c89d', '0164821822', NULL, 1, '2020-09-18 03:32:43', '2020-09-18 03:32:43'),
(263, 'fea470bcb272642b30132ae2dfc76736', 'Vanessa Tan', 'Vanessathf927@gmail.com', 'd81af98c95b8b0f38552561b3f476ef552cad8dcc11ae527f249bb02792c4317', '0ccf718c3759ad336fb17c28ea0eb13e3903b7c4', '0199880927', NULL, 1, '2020-09-18 03:33:54', '2020-09-18 03:33:54'),
(264, '5e1c552c9848b84adf9ae64529bbd1f2', 'Felicia Tan', 'Felicia_tanhz@hotmail.com', '24a954d431c28091f9b188c152dad59d3912b325b2c556196050ae12bf19cdd0', 'b0cd08a7026c33b45944367a66510fc540c7552c', '0124069914', NULL, 1, '2020-09-18 03:34:38', '2020-09-18 03:34:38'),
(265, '4f02b6464e0ac4db3a2f2ab5e9ab5959', 'Tan', 'Tankokkuang1962@gmail.com', '619566e1dc83cf0f77d229f1f02d352df34365bcb3a146c81a291e45760bb4bb', 'f3f6af961da4629fd86dee8ee3257f80d99177d6', '0194131010', NULL, 1, '2020-09-18 03:35:28', '2020-09-18 03:35:28'),
(266, '9257a61676c6c436810a5848f1b84446', 'BLay Teh', 'Tehbl2266@gmail.com', '7b1b6088287949f92843617ff6dea33e110d20434bfd0269d50c5e20b7204b57', '3935c6bb69430a16346bdea684e81491f7cc80d1', '0164851006', NULL, 1, '2020-09-18 03:36:14', '2020-09-18 03:36:14'),
(267, '65d14cd30f9958ae6d9fe5721c41a444', 'Ho', 'bhho88@yahoo.com', '03da2fd9d4b2d55c9a10c0f25e31da4abfbe24172cd5e7e65eac7fb53dca7cea', '83735193c58faf2c264be50f09a00b3fb410485a', '0125551335', NULL, 1, '2020-09-18 03:45:01', '2020-09-18 03:45:01'),
(268, 'd24e72b552e4f72118b696256f047dd6', 'YEN', 'yentyt_1002@hotmail.com', '18d5a8b4e73fca328991324b5265293d5dd92488e5a4116e0fac9e28be831298', 'b429323e965cdacb992943f38e4c54dce79bbfb1', '0198148687', NULL, 1, '2020-09-18 03:45:35', '2020-09-18 03:45:35'),
(269, '1faf7839f84707c329ac8975d98fa8ec', 'Yeoh Kok Beng', 'wilsonsonic@yahoo.com', '4ceff2f49883de53e996c994344a7f28f4f2cbc20c10329af74d16b82b73f700', '70730fb429b501be47627b9ba9083e7e852c4ff7', '0164581981', NULL, 1, '2020-09-18 03:50:51', '2020-09-18 03:50:51'),
(270, 'f1ecba8193da96ccaf84a53b60f0f0b9', 'Huong', 'huongmeeling86@hotmail.com', 'acf08be70e495bb78479d4706e4bf78c83b11d007825071bd646aee195777d5a', 'c10be465a10e97262d76a3beaedf1e46c7c9790a', '01113146513', NULL, 1, '2020-09-18 04:13:37', '2020-09-18 04:13:37'),
(271, '3a25dc14b74a51aa081002047a379b88', 'ginnyliew', 'ginnyliew000@hotmail.com', '5af2177593b84874b13920d07ce92cc9315b1f5c6923065b2414ade144526c31', 'ff613d2c2e8e02f245b4f811b6adb1814ed8c233', '0124825433', NULL, 1, '2020-09-18 05:25:48', '2020-09-18 05:25:48'),
(272, '4e9a19727ebef06dbbbf27732d8776be', 'Ann', 'fiera25@email.com', '7cc1689730440fab189755ec589179117ee7f59cc301364707838d9784dcfb59', 'bfabef3225f21a0e961e14f8c6d398cb9029f9e3', '0102100675', NULL, 1, '2020-09-18 05:39:46', '2020-09-18 05:39:46'),
(273, '148c6d46a9ad2e4666769eb36d2e203d', 'OOI SIEW LAY', 'slooi2903@gmail.com', '2d2e38a44cfc6b1970aa60aa2bca521dbc462db293f95183519db29eb7a2ffba', '46f6b489bc95061eb9824327beb5d8f89b985dba', '0164740669', NULL, 1, '2020-09-18 05:42:02', '2020-09-18 05:42:02'),
(274, '80e66f0d9e9330b26355acb2441d3107', 'HSLok', 'eileenlokhs@gmail.com', '177bfd1f544de6e15f5fe61564ec1b1404f721e04c007c634d4bdb8ede3a9609', 'e77a233d8fe093f88f323d9bf33ec0ce157217a0', '0105630190', NULL, 1, '2020-09-18 06:54:11', '2020-09-18 06:54:11'),
(275, 'aed7e2dcb740d61aa9b7afe0076171fc', 'LIM SIEW KEE', 'siewkee168@gmail.com', 'ec1db2ec77b6d87da0d33d2b68cdb2c4963de6f37e06846b1a4fb63326cfa0c7', '6a5da906929c1f95ead1f6745b31f84072d5cb35', '0125888281', NULL, 1, '2020-09-18 07:32:02', '2020-09-18 07:32:02'),
(276, '090b825eaa4b352bb44a4c49d05b1406', 'YEOH SHIN HUI', 'shinhuiyeoh@hotmail.co.uk', '4afee751537bc99e1d171fb30832fbc4aa2776395025ad55396c13b3e607489a', 'e9b04590cfe1176c75edcbc2b7c3930b4823bf54', '0175984328', NULL, 1, '2020-09-18 07:55:52', '2020-09-18 07:55:52'),
(277, '89912fac064bcaf90da09d568260d73d', 'Siah Tick Tee', 'Siahticktee@gmail.com', 'f45b89fcbb22905c73f13a368ce0030d471c35b63a8ecf082faf90c65f2e46b3', '65b055339267a171a406123a6677b2c0cbbb591c', '0189824241', NULL, 1, '2020-09-18 08:14:09', '2020-09-18 08:14:09'),
(278, '0cfb722f54737c501f07f72f524f8ea7', 'Tracy Ang', 'tracy8921@gmail.com', 'b3433ea597067253329495ea62675e077a9b3f6bc9b5abe80c772b0b71035469', '61563fde35b198e7530891647bc49a95ed0de2d6', '0104648921', NULL, 1, '2020-09-18 08:32:50', '2020-09-18 08:32:50'),
(279, 'ce5520e4def1c606cbce20795bf28fc9', 'Irene', 'cheesl2002@gmail.com', 'a3a384df4860bd90da4f63c74d2701744d074b6d885de083e164d2c05ed785b2', 'c35d9ad7b1d33d4ba21b1ec1a207cf53120cabad', '0125379180', NULL, 1, '2020-09-18 08:36:18', '2020-09-18 08:36:18'),
(280, '83cd825d983551db7083e7dcc0ba22fe', 'Kuang pit chye', 'pckuang1980@gmail.com', '6d61eca1548eaed13077f6179c781c5d0d08218225350ad0923920ad83f997c6', '4577c2fef2fdc082a0677d2656a412bf95d27fb5', '0164401893', NULL, 1, '2020-09-18 08:51:26', '2020-09-18 08:51:26'),
(281, 'ee78edf8cbb8ca2e070400312b676e82', 'ONG CHIN SOON', '88soon88@gmail.com', '82de992d803882e75f21ec958ddc8d54de53b617ccd77c648bf980609cdadf89', '9cb1d119e82efc9ba1f75af2863e52c4c6f63e71', '0164161062', NULL, 1, '2020-09-18 09:19:55', '2020-09-18 09:19:55'),
(282, 'e60ba8c0e7e9cbb52282a51889c6f2a7', '陈冠羽', 'thbectwjwrgynat@gmail.com', '58fdbdf10757041e53d60a1c72882c28b9f2c94c77e33cfcb92cfa007d5653f4', 'd3e4bf3d291911ce3cee40eee0ff828648d0b12e', '0126439470', NULL, 1, '2020-09-18 09:52:16', '2020-09-18 09:52:16'),
(283, '7edfe54d76e35f8d43f173de0e02c299', 'Teng Kim Lai', 'tengkimlai@gmail.com', '22ff8da250188c188277b26779909384a072f7718db883e9367d35f5e7e3fb49', '42b725cd9318cbff568c2bdf7a32c5549e54b9e1', '0124021226', NULL, 1, '2020-09-18 11:31:12', '2020-09-18 11:31:12'),
(284, '319407b3b48fe85923aab807825c1ee7', 'Loh', 'kgloh_23@yahoo.co.uk', '5934a64ab09fa9a2b38b7554e19d6d38fe3a06494e3726ead7fcc993bb2c8819', '4a1f5fcdf52ede666d155cdcf9010b9a71dd0188', '0139108132', NULL, 1, '2020-09-18 12:12:33', '2020-09-18 12:12:33'),
(285, 'ac2d2f2a5360a7060538997d5ed42bb3', 'P.H.Lim', 'shlimyan@yahoo.com', '5d77059365ca7a6f7fc1e9c9f92d12eadb8e3bb75014f7ce1288cde0d600c912', '36a3b9f59816ec180fd443ccb3f7c0d601e1d056', '60182436788', NULL, 1, '2020-09-18 12:54:38', '2020-09-18 12:54:38'),
(286, '9fadfffcc0632be050dfa90b240dfa9f', 'Khong', 'khongwy@yahoo.com', '52c8506d7dde6198cbd0947c313e4950eee028bc5431dbb6cc0bbfc1d31b8bae', 'e877640cb741b084839660998d14507422478211', '0122307577', NULL, 1, '2020-09-18 14:02:28', '2020-09-18 14:02:28'),
(287, '6fcd189ed9cfc5c0a52b64a67070abcf', 'Leonard', 'leonard_tan83@yahoo.com', '9d5b336c16d11fa46774f7aa800add954bcfb4d2c237839c6b8e119564478f78', '444b2eae10f9f899a9f112a0c5ab57198f9408f2', '0124918691', NULL, 1, '2020-09-18 14:43:58', '2020-09-18 14:43:58'),
(288, 'db291a13bbe2443d0d5b74a531876d73', 'Leow Cheah Wei', 'cheahwei.leow@gmail.com', '6eefa9148307f27265546522d8e8da20f79295ad54653446b2859df4f4f90b0a', '9aeba72c9be80abf6774f958fededfbf0abbd046', '0127239729', NULL, 1, '2020-09-18 15:00:17', '2020-09-18 15:00:17'),
(289, '7e2b7b069781f8fea896467db5da9c85', 'Chooi Wenc', 'chooiwen26@gmail.com', '54d8c97704af22bc952d10d761a08c4ab14387ad60df2275ad94408b081629cb', '7fe505087b758c2c52dd0cfc1ec48d3b0cb2765a', '0134314932', NULL, 1, '2020-09-18 15:01:09', '2020-09-18 15:01:09'),
(290, '479ee361b2fcb512c4a3ac87abb6963b', 'Tan Chee Seng', 'cstan87@hotmail.com', '039948717538659cd6039eaceb8a6cc49267f2f09cff0be8ac299f50b370e245', '334f2b9f3921de670416e11d05844df95bafd9a0', '0124521486', NULL, 1, '2020-09-18 15:11:30', '2020-09-18 15:11:30'),
(291, 'd32ce43edfc1fab2c833bdffefb1e5f9', 'Chew Chee Teong', 'chewct72@gmail.com', '700279b8c740050afe513f0db55766fc50989c494cdd4003c4874e686b74c8fe', '85e91d45aa0d3272c663421dea569b3f9bf9128b', '0125179293', NULL, 1, '2020-09-18 15:13:46', '2020-09-18 15:13:46'),
(292, 'f3d8c01ee5f186ac96dbbe517dd10d9c', 'Ng Tong Heak', 'ngtongheak@gmail.com', '3df5aba3ce98aa56aada1203029732cb8663a84eaa541f810017f2f5d541fc1c', '518669cc89a54c30acf164cfcba3ca2762b6f7d0', '0125883855', NULL, 1, '2020-09-18 15:49:20', '2020-09-18 15:49:20'),
(293, '7e7235ee639d4830d978977e1d0c8829', 'SHIRLEY LIM', 'stan1886@yahoo.com', '962b08dd8538db44c9a561637248cb178bcb1919bfe7996f604f3b05a46b753c', 'db68bef2be3e91e463b1454bf3be47f041b6c79c', '0124865535', NULL, 1, '2020-09-18 16:13:02', '2020-09-18 16:13:02'),
(294, 'b893a39e5aae0a0b5dfc37c1c0423700', 'cmahui', 'zhou6308@gmail.com', 'ae49d0e8e1de103dc53b599942d3fd9fa8ac9ceb02a665f63fa5d14a28762fc9', 'd05e9cf729707f85a381b47383734a53005bc7dc', '0195220220', NULL, 1, '2020-09-18 17:06:31', '2020-09-18 17:06:31'),
(295, 'd86b4b36a7e6b04a26efa3e3adf90d7b', 'SIEW CHIN FLY', 'flysiew118@gmail.com', 'de9eaf98e964b1942a4fe49bea12bb8c27aef62a54b386af4ce6661e6a93df4a', 'b1f092678366dca1a1392702336dda547666b050', '0124896571', NULL, 1, '2020-09-18 23:04:32', '2020-09-18 23:04:32'),
(296, '14fa6c4d0d1808fe8da314c69f11a147', 'ong pook hun', 'ongpookhun@yahoo.com', '4852bfa80b8e2784134f9ccf7cc9a7d663880e629b0529bad3bd530223824b6c', '9097d93081d591f140d00a8637e97845d9c863c7', '0124561022', NULL, 1, '2020-09-18 23:09:08', '2020-09-18 23:09:08'),
(297, 'f715d43e8f78654a99f628a051c42c46', 'Ms Ng', 'ncc6639@gmail.com', '52255ce5229b74969beeeeb57dc0fff2def692dc28a555be58ca2fcf843a0333', '4d3248f5ff41138b21ee22981115c7f3277e9d15', '0124181871', NULL, 1, '2020-09-18 23:54:43', '2020-09-18 23:54:43'),
(298, 'f7ec5a9e9490c7b2bf0e0500049b4334', 'Brianna Ooi', 'boonpei_ooi@hotmail.com', '2cf19b63823d0819e8e267e48250a99b759b24f32b97fb77a45f223f87411eef', '6f1af655168f5c5c7f3e58340973fc85a42e9e0f', '0164191774', NULL, 1, '2020-09-19 00:02:23', '2020-09-19 00:02:23'),
(299, '086bf9701d2c34fac5bdc6042f4ed3da', 'Leslim', 'leslimhg@gmail.com', '112e49c5fcb2e8e84f7270cb2196298558928c5f53adde3745ef0e2d591f433c', '1e1fbf78748707b1853654e34400481e9b97ce88', '0124855045', NULL, 1, '2020-09-19 00:19:08', '2020-09-19 00:19:08'),
(300, '01843a573f2c59662e1ef25c224a5308', 'Mooi Tuck Hoy', 'admin@mth.com.my', 'c0ae830ceb198743b8ddb5b80f02e8fc3f9a5cfa1fa6d5b2279cdf7173dc1209', 'cc954b44c41c68d9ebe14e61ac4ef501dc7ddab9', '0174628888', NULL, 1, '2020-09-19 01:06:13', '2020-09-19 01:06:13'),
(301, 'efffdf0e6b0dd71b41fd816bbb891e4f', 'ronny yong', 'yongronny@gmail.com', '699160258dcd99b2856e586bb033ca67c47664de367a86a775f06ae27d7dc5fa', '4f76eea7a1498dd272c60006c73cac1f784455e4', '0124726830', NULL, 1, '2020-09-19 02:30:52', '2020-09-19 02:30:52'),
(302, '846402448dd91035e3e56e03c77234ec', 'loh  Poee Hong', 'lohbh64@gmail.com', 'b9c17ce7d17fa620bee6243407f1f647b6a6f5046855fef4a3b0ef33a93a158b', 'b6186f5c16acc300825ad18b72ca2e53d890a71c', '0194736806', NULL, 1, '2020-09-19 03:06:04', '2020-09-19 03:06:04'),
(303, '52f7105b886629c4cecc53470d441465', 'Kenneth Low', 'kenneth_2001@hotmail.com', '6d6c70640733fcefa7601c72bc087365b1d19153891b3359de802ded27d4eb6b', '20d6dc71f5cbefbecb3655a5f8c4eb634e2eaf30', '01112371269', NULL, 1, '2020-09-19 03:09:45', '2020-09-19 03:09:45'),
(304, '6f2c85753d89cd6cbd9fb91ab81b7fd9', 'Queh Chin Teh', 'tehquehchin@gmail.com', '58b4c8927090ce80ccb3f11ea2707601b3d7840ee5a81073b8fa0765992934b1', 'ff7a337fc5478247378551a166048d883e6b869c', '0124865299', NULL, 1, '2020-09-19 03:46:15', '2020-09-19 03:46:15'),
(305, 'ca8225ab68b7b54ac14484ac1181962f', 'Sim Kong Beng', 'kbgsim@outlook.com', '9430e883780160d874d749c7fd7a297261e972e2351ae44087f622bc01fc9cda', '6119efa960758ec0b3e788050ea40fa8892b4f17', '0194107325', NULL, 1, '2020-09-19 04:39:25', '2020-09-19 04:39:25'),
(306, 'a22ddd0a2ef521656a24d372ef6e64e6', 'Hui Jin', 'annatang0210@gmail.com', '1e99fe8d865659f6fbe88b8c09ad7409ed7c7406038f1fd5a18c04e9695be974', '0663879e1555852ac825ca69132a55e1e019d0fa', '01113089567', NULL, 1, '2020-09-19 05:20:04', '2020-09-19 05:20:04'),
(307, '7d6f54fa1a2561a79a5bde63c7df7f81', 'Ooi Hock Kooi', 'ohk6599@gmail.com', 'e4796a6eaddd0cfa57925938ad86cee966d3901787214ef1b1ea6b3afc820187', '5fe9b7eb697be4ed008201106a264470e9292364', '0125782793', NULL, 1, '2020-09-19 06:24:19', '2020-09-19 06:24:19'),
(308, '76e9f3b2d39d043424bfcec758ed83c1', 'Tan Kee Hooi', 'keehooi@gmail.com', '131ad509464f67e491c65e0501e22dd0de339834bad9894706a5ad56f07b4fc1', 'a67f4e0b28554c37dcb794ddb8aa694c365f2a78', '0164883398', NULL, 1, '2020-09-19 06:38:09', '2020-09-19 06:38:09'),
(309, 'fc113fc4fc2b64fec7ff9d44395d0410', 'Shirley Ang', 'softwarestorage666@gmail.com', '7355ffa3089c8cb742b48cc3a19ce14c2b77b62c5684153213f8a90477e52d7e', '310c09d4002b5ff46d5a836718f55434866a860d', '0164287099', NULL, 1, '2020-09-19 07:25:22', '2020-09-19 07:25:22'),
(310, '3377f928d3addce9d8d80679d757a7fb', 'ong kwang yee', 'kyong13@hotmail.com', '66a294841fddff05248718017c1f2eb9f795fde5bc8aaf6257ad2c12c133a498', 'dda7d9dc30b7cf21df753d66b8f16989f9d034c5', '0164555846', NULL, 1, '2020-09-19 08:18:48', '2020-09-19 08:18:48'),
(311, 'ed802736e768513b04b9a82ae75cbf9e', 'Daniel Wong', 'dscwong76@hotmail.com', 'fba699b1194256f591528686702471c255b6eb06499620fb8696abf701b32da0', '5aa1413882b8bd0f5cc85c94dce5e30de346c4ca', '0125566025', NULL, 1, '2020-09-19 08:22:12', '2020-09-19 08:22:12'),
(312, 'd042106c09fa822a57b2c7bc72d4266f', 'Tham Chiew Suan', 'cstham25@gmail.com', '23d7ae2fa7ab1034e65deb3e53c5f7cf79616ffc86b1aefcba2767003a5fe60d', 'fa0cc08787b975632f2455c34ba7ad9935303671', '0164513373', NULL, 1, '2020-09-19 08:31:01', '2020-09-19 08:31:01'),
(313, 'c7ba8298f6e6d04929b8ef0d47924f11', 'Tan Chew Aun', 'chewaun_tan@yahoo.com', '737e0f0acadb86db9bea7da52bdf47658a0e4c1a8cbd5f5d31a6337ab32f2840', '4189a1aa8896b7e51ad2629eeb11e5059fd0fc5a', '0174139583', NULL, 1, '2020-09-19 08:39:06', '2020-09-19 08:39:06'),
(314, '3d5a516e697ba3e99d3a7cd1e7c72e89', 'Augustine tiong', 'augus2777@hotmail.com', '3fc7e5ef0f1cb37745f9fd505fbf5e44ca5b3c2ac05d26a14a389b40692e0519', 'c5c8f36ad46ceee8ca71d8c620e0b912b039abb4', '0167002500', NULL, 1, '2020-09-19 08:58:48', '2020-09-19 08:58:48'),
(315, '86cd051c4d62d7f4c4f8f9a8f5187aac', 'Seow Pheng Chew', 'smallapple1807@gmail.com', 'acbf818244c9f66663d127709a1ad86c79b2878a2c96a143dad8d651151c392b', 'e9cdd652f3459e8c3d83246ecf4623520af06e8c', '0124881807', NULL, 1, '2020-09-19 08:59:10', '2020-09-19 08:59:10'),
(316, '3e5e6161b8245f19490555ad017d1b38', 'Koay lay tin', 'wwkoay@hotmail.com', '8be87b6773396c38f06421830898a8e5d75f57a94007d889d5c47f906f932a2f', 'ec11c9c0b8b161dff809e1e2199f95b8b53df818', '0164810889', NULL, 1, '2020-09-19 09:18:42', '2020-09-19 09:18:42'),
(317, '52f2bb95deb7014754f1561c719e157f', 'Tan chin waye', 'kennytancw22@gmail.com', '74aea7deba3f36271ee21433f55ffeac38aec78880fd7723c818797fce1a0275', '9ee3eb7af7918913664bfdec31a9014cb93ab3ca', '0164111727', NULL, 1, '2020-09-19 09:30:49', '2020-09-19 09:30:49'),
(318, '5762b48be0c7679f69cde8ad072cf8a5', 'Tan Alice', 'virgo23_08@hotmail.com', 'cc565718434f1e744138ecb28dfda363fc7eb8c7bb4d5fa3121940604a33b635', '45f1e782e24b7daef19580662cba86f345c175f0', '0139020152', NULL, 1, '2020-09-19 09:48:41', '2020-09-19 09:48:41'),
(319, 'c183a32525717be4a7f3bad6064659d0', 'darius', 'darius.tkh@gmail.com', '063565cf7213f88c2a19dc7a5ee23ff956627bf9354ed5aa6f8840a203f0c635', 'f82cb419b68ed64e7c4a6e152f6d53bba517cc0c', '0167216603', NULL, 1, '2020-09-19 12:17:56', '2020-09-19 12:17:56'),
(320, 'a994d7e1236b73974bee9346f8399ddd', 'Hneah Chin Thong', 'cthneah@yahoo.com', 'a4874ef553006e0851dec8ff946e1fc1e2978672306491f226c72f2bfa19f2e1', '4cfd161887f9c7ea52cf04f965789fd16c0f076b', '0124213318', NULL, 1, '2020-09-19 12:32:48', '2020-09-19 12:32:48'),
(321, '7fd05229d64c20a1daf162ce3a560ad2', '王明发', 'bob0124600828@yahoo.com', 'f2ef5581ccdcda9f61b7430d7f0c2a242d5b3434429bbc1acad95e5f61c36bcf', '93a63aed87b741193494a7e3b16d72f4b4cad804', '0124600828', NULL, 1, '2020-09-19 12:54:25', '2020-09-19 12:54:25'),
(322, '439a812c3b9cdbfc7ef3f50bef963be1', 'KOAY BING SEN', 'Vincentkoay33@gmail.com', 'a14e72d1ee8a37114ab2903df86020431e4aace140edfc1b88ef786955aa9d30', '6896e58fcdf20c0b5184f87f9036ba79126fb04f', '0182727255', NULL, 1, '2020-09-19 12:56:55', '2020-09-19 12:56:55'),
(323, '04888dbbde05e8f5ee23634add983151', 'Chiew May', 'chiewmey@gmail.com', 'd876d5055077dcd88019416410b8ed8d6a57c9a8faa7ac48755bf191acbc90ad', '16c8f388b88005a0e7f7e8abf19e82b72206a21b', '0164263634', NULL, 1, '2020-09-19 13:51:12', '2020-09-19 13:51:12'),
(324, 'c69367efb79675ad91c1ba81aebcb976', 'Hong Jay', 'Jack03082001@gmail.com', '25ac4309c70c7d1f2b779e588314c2379c4976fffb4ceabd6142005ddf095a23', 'da45c93bf4ebed23503b65ca63234bb6f6700fa4', '01136893938', NULL, 1, '2020-09-19 14:30:33', '2020-09-19 14:30:33'),
(325, 'c59358d010ccd2208fcb84385ece1d87', 'Loh Hong Kit', '', '40d2fe9db359baaee1edbfe58c162653006e15a2b8db03c10035ac430b1ec7c1', '00aa33ae979ac8f91b7967edc72d73180d44fca2', '01115', NULL, 1, '2020-09-19 14:34:24', '2020-09-19 14:34:24'),
(326, '07c2a66daa3af02b5207b079781a0147', 'Loh Hong Kit', 'Raymond981117@gmail.com', '9e9ae5ae6a538c00179ee196aa7ff98fbff102d91eef27c387fc765262f32dc7', 'ab2abf0ae3afc40a855fdb66a5f9c57f1d4c5efc', '01115065129', NULL, 1, '2020-09-19 14:35:07', '2020-09-19 14:35:07'),
(327, 'd8382a773d613212536cc7319bc27c31', 'Jackson Khoo', 'kevkte@gmail.com', '299d89d6d32f43b2ad59b31cae251ef13680777ac840316953fd6fadd5bbff63', '7de35a4c1cde95bad9f77be8dfbabde87d7b4468', '0135818333', NULL, 1, '2020-09-19 14:44:09', '2020-09-19 14:44:09'),
(328, '58a06b94e595d1e2798aee73103bda32', 'Richard Chng', 'chngrichard00@gmail.com', '34ba9e118ca58d014a4217bf428f02b8a3334288511ad04639b6fa8e814a903c', 'd0cef24ecb2f9544648b97381ab1171545132969', '0125082358', NULL, 1, '2020-09-19 14:47:00', '2020-09-19 14:47:00'),
(329, '4af50b8820aa17ff3073b65b0d83c5db', 'Santiyaa kala', 'Kala2320.tk@gmail.com', '4ef8052876122fe8b53f600c518b560bed1c4e76f0364bf5618801cbcbf6263c', 'bcdecd46b02afe0010add7f816016cea80aa2248', '01136177170', NULL, 1, '2020-09-19 14:47:57', '2020-09-19 14:47:57'),
(330, 'a14cce51eb69eae2de8463d0e7343acb', 'Pierre chuah', 'Pierrechuah69@gmail.com', '482e4b4621cfcd15547411714fbe9cd98589db16c25eec70e80589ca589607da', 'b0c2258997af406f7fc11b8da205beb8af081f28', '0134888277', NULL, 1, '2020-09-19 14:50:21', '2020-09-19 14:50:21'),
(331, 'e45dd3cc882367e7447ec85576d2182f', 'Brandon Koh', 'brandonkhc@gmail.com', 'ee5abcbeba31e5d2786e1b78616e84c7780cfe72bab3e6ff7a20a3e3a8accc7e', 'b4acec0ec2df5511411e4b2c3d7d361d3f993b19', '0164801805', NULL, 1, '2020-09-19 15:04:05', '2020-09-19 15:04:05'),
(332, '98a4b79f6f1128a183b60e836688e64d', 'Loh Hong Weng', 'Michael-1897@hotmail.com', '38ea7adcacc7937e8d91f9da5988c8ffeec0bcbd4c250df509ceae81ce79bd5f', '13dbd99379c593bcb40b0eb56f88bf2948bfd81c', '01135171897', NULL, 1, '2020-09-19 15:05:31', '2020-09-19 15:05:31'),
(333, 'd1699fdbcc0ffe9d3b9cbe92081df384', 'Hibari Chin', 'Hibarichin47@gmail.com', '5064fb4c47d8f2785d8216198f8589219faf5be0200cc258c827381bc12cac00', '3c0ee6957120f740e8435a4a65b854b844e113d2', '01116159122', NULL, 1, '2020-09-19 15:05:55', '2020-09-19 15:05:55'),
(334, '64e0e4a28c3a63222a8f701bf40401ce', 'SOON BEE HONG', 'ds_soon@hitmail.com', '4fd48590a56da479644bf41b9742df69007fcb3096c36297c30477ef0c7e172e', '984dd615eebfea62cdbf4082632ec8b14bb281cd', '0164088887', NULL, 1, '2020-09-19 15:34:25', '2020-09-19 15:34:25'),
(335, '95c269d3255ca9cca4a2368773ffa0ad', 'KYOKO LOW', 'Kyokosanjo88@hotmail.com', '8c37bcb933e45aef33f9e9f04e5d9a65fd88d34ead81786859b2fa1c8fbcaea4', '40743cc92632bf764bb7b367d368cd47930af20d', '0165080918', NULL, 1, '2020-09-19 15:36:57', '2020-09-19 15:36:57'),
(336, '26b1286d2d1dc242c6c2efc8feee50a5', 'Ewe Kean Soon', 'dennisson2003@yahoo.com', 'ff7c41daaca674d66818485cfbe8ae879548956c110a72b2e66f07704a4ee4df', 'a4bd746822c05f751bcdc5a834e66ccf4a57ddc8', '0125305847', NULL, 1, '2020-09-19 15:39:08', '2020-09-19 15:39:08'),
(337, '1eb73bb9c179826b41d1046eb61dc1e5', 'mr ong', 'melvynocc@yahoo.com', 'be4142203c60db4eacc7fd5311765250e1e88b3cfed3cb42888b0338e47e2e2b', 'bf1bccdd21a1b45de39e0d7a514774b5c0debf5d', '0124939459', NULL, 1, '2020-09-19 15:43:21', '2020-09-19 15:43:21'),
(338, '002d12e514733982fa045218b962c829', 'Calmin Lim Khai Bin', 'calmin@live.com', '8ce287a892ae4effe241dfb8c777fcbe923a023631e50c2ba54235a29c989b68', 'e38883184a7ec7bf4a2cef8d82f2a079c3c0c467', '0124050558', NULL, 1, '2020-09-19 16:17:31', '2020-09-19 16:17:31'),
(339, 'a11707fb01654da7d0bb838ccbb29043', 'Iris Lim', 'ilzh@hotmail.com', '835dc55fdbc83c521d4c9d636a68fe215faa864e80931c5aa1a13df6fd393743', 'd818ffd45c4e2d73e446fedff91c7353f4496af2', '0124223747', NULL, 1, '2020-09-19 16:28:33', '2020-09-19 16:28:33'),
(340, '36b8ca62a0d801993ef4488d82dcbe31', 'Genevie teoh', 'teohhuijuan@hotmail.com', '282d9b44e80d4ca49ba592e798db4b4e7e2073e4c8f1291e1253b929ab2bf397', 'a0d3cd5632f01d21d85c3a9db35afb32c1b52378', '0135297728', NULL, 1, '2020-09-19 16:30:28', '2020-09-19 16:30:28'),
(341, '5cbbd0290bc7235ba08040598d8cfc9f', 'Weni Chua', 'wenicwn@hotmail.com', '79f2280a6dcc70fca5f08d2591f781ea9fa4694a593c6d32c4b9937eb0aaf07c', '2768b1d4814812a1871ff629fbf81306b49ada5f', '0164421775', NULL, 1, '2020-09-19 16:31:06', '2020-09-19 16:31:06'),
(342, 'fa615b397a834cb078a9d762f91a90f6', 'Sarah Tee', 'teeyinchuan@gmail.com', '6b7e88f414b859b39f7d4af1c8265c70f32294ed7972b03042c5bd1833b79193', '9b6d3450c1bee86c1a68ee77aa62f168941cd3d6', '0124882538', NULL, 1, '2020-09-19 18:08:59', '2020-09-19 18:08:59'),
(343, '23ad71849b6c75c8876735116df5b0e3', 'Chong Kwang Yew', 'c.kwangyew@gmail.com', 'ad4b2cbbf44805b8b530a09827e0a71dfc8b613e93f155c2f0cde03c39c886a1', '46db6dab539b76ef9f9b8d5e7f93c0467b30b3e4', '0164764801', NULL, 1, '2020-09-19 23:12:11', '2020-09-19 23:12:11'),
(344, '069b562acf4851bee52cc51aaffec9b3', 'Luah Ah Chang', 'luahac@gmail.com', '90285edac3565273cbde49a4e079e99db2e053437a4a458048b4e79297fa729b', '0e659770712e06fe105292733ea1cc57eeb7ce48', '01128226866', NULL, 1, '2020-09-20 01:36:21', '2020-09-20 01:36:21'),
(345, '985a71863d01a2a844df196aba7d6948', 'Stephy Tan', 'tanstephy_0730@hotmail.com', '48abf07497f61d93cadf83094c89f285c2abe85dd3eb48c538be7d8abaa05c44', '8d56e638fdbe3eefa5d548c9880327392cb7d306', '0164764599', NULL, 1, '2020-09-20 02:21:26', '2020-09-20 02:21:26'),
(346, '919c203aa55adec95c69aed4d4e0d40d', 'Alwin', 'alwinlim@zeon.com.my', 'e833dfb741af6d75e1214129a4af59a4de9a70cb33b5482da3f2fc5e93c73a50', 'f8af8f74ba2aeee2e5b2fb6f58c6d26de556bd23', '0134885988', NULL, 1, '2020-09-20 02:24:13', '2020-09-20 02:24:13'),
(347, 'c814540789ceb36af1a3163647fc6b4d', 'Dan', 'ols81@yahoo.com', 'f897c9d7591179052bc2db02662251dfe1601dca2ea2ff71ad340b27af373ae0', '4fc7686b398cf4288e80ee975f8a6b6f8df01a28', '0124300494', NULL, 1, '2020-09-20 02:44:47', '2020-09-20 02:44:47'),
(348, 'd00ccfc67f9a21729eae2e84651e44d7', 'Kathryn Lee', 'Kathrynlee@zeon.com.my', '646feb5b2b0453bed1693813c95d0e87edf65dc28367d1aa80a05855d6ee39ea', 'f6b4907acf16b78c2fc07f149935d633d1eeda1c', '0164231423', NULL, 1, '2020-09-20 02:47:36', '2020-09-20 02:47:36'),
(349, '5a801bbb08bbd1161d14329b30d85e5e', 'Chong Bee Khim', 'chongbk62@gmail.com', '65691978b7be762750aae3948a4282b395bc22fb97dfb72421b22016e5fd203c', '28b1e5399df840a19257970f3bd6c360c41fed01', '0164827339', NULL, 1, '2020-09-20 03:27:51', '2020-09-20 03:27:51'),
(350, '599f70da81427e0df1cc2170170831ae', 'Pedly khoo aik loon', 'Pedlyloon@gmail.com', '85a110a89e072c086aab57316450fceb5e65e4e2bb310d97ec25ecdaa0f8fffc', '4ce56acd195ef3baf6b56be0bee0aacc36526a71', '0164977966', NULL, 1, '2020-09-20 04:14:08', '2020-09-20 04:14:08'),
(351, '22a6d6b3c99d286f438ba1857a3e45d9', 'YEOH KEAN KEONG', 'yeohkk0620@gmail.com', '09549389887ceecc88c8ed96037324f7dce69e02b3434d79110e081105f9e436', '02986a82b90f1be181b971064a69c4e459406a3d', '0124217964', NULL, 1, '2020-09-20 04:57:31', '2020-09-20 04:57:31'),
(352, 'd5b1487b52befa420c213a9dc1007ab8', 'Ong Hock Aun', 'penang.oha@gmail.com', 'e9a724ab8be0d55cf05f9b6188d9f52f7b5790f27c2ce61cdce1a7bc47ae04dd', '95374e39c36a18843b4df883c49c6e038f17f151', '0164078393', NULL, 1, '2020-09-20 05:01:37', '2020-09-20 05:01:37'),
(353, '76f0b128535e9369610c107807e36af6', '陈文周', 'bctan8@gmail.com', '20d0e72c791d9e712857f86ea7eeee576b99400d6de3028e08467dc8e601924c', '16aced112d2b6455f5e96a00ce32c0cec62fa28a', '0125523630', NULL, 1, '2020-09-20 06:20:04', '2020-09-20 06:20:04'),
(354, 'a0e486607777119faa7875bea129c582', 'Ooi Chin Chin', '', '07283d3b1bd6c05724e49fd186d818863c154158d47197f5b1fd4b93978ed7d0', '5387240bbe3dd8bf4b1d16f6320543fefa8a3bef', '0164528093', NULL, 1, '2020-09-20 07:22:37', '2020-09-20 07:22:37'),
(355, '43f13422a78aabe87c46147c153165b2', 'Teoh Pek Em', 'rosie264@gmail.com', '9ae49f82edc55092f7e4c2e67a42ff0b526108fee6ebe6cea552f718ce715bcc', 'd0307a194ac4f69db1cd07c40ac89384c5ca558e', '0124565289', NULL, 1, '2020-09-20 08:06:18', '2020-09-20 08:06:18'),
(356, '8a352b7a357b5fed1e86fb3376ab9b5f', 'Teoh Shin Lynn', 'shinlynnteoh@gmail.com', 'c5454244ecdb30dc8f46221fd8176ff72b4f43835fc0e60c58b77ef7ad68eb96', '086a95bb509b6d2203c350550d57ea91589c96dd', '0124989059', NULL, 1, '2020-09-20 08:19:24', '2020-09-20 08:19:24'),
(357, '7cadca336c1bdc1c74957d90c88b2da7', 'Ooi Lip Xun', 'ooilipxun0908@gmail.com', 'cb300c5d2c11951183ab7a5cf771b8d9ea096f4f6b4c2e569f40edc7bbe25d08', '93ccaeac396635947db8f3cb33b410d894e8e673', '0125680703', NULL, 1, '2020-09-20 08:57:25', '2020-09-20 08:57:25'),
(358, 'e450ca09112007f29080e569081810f8', 'Wong Wei Sheng', 'wongwei78996@gmail.com', '5c0ca768b5d740f0f344f9e4ada922354ccaa03eca16ccb7118bf695d9e3a589', '3bfd51654821c19ce84d19f1da2ca33468c86a98', '125802747', NULL, 1, '2020-09-20 09:39:36', '2020-09-20 09:39:36'),
(359, 'a2ebf16af4fcafdc09923b990c97fe8a', 'Sim Guek liang', '9_4_8jalan bagan jermal 10250 pg', 'c1ef247ec72e3bb8eb84bdf46dc1399d56e569132db8fbc7417a040194774642', '9cf803031d66a47e8185d21877ba0fc8965c73e4', '0164900018', NULL, 1, '2020-09-20 11:52:25', '2020-09-20 11:52:25'),
(360, '2b6616da04750159c4b1170bad60cdef', 'Kay Chan', 'kayyie@gmail.com', 'aa16bb13180b7478fa7f4af62bfb7cae87e3b4a71d503a5e6b8a732ce418ac50', 'e544272b22f72255c44bd1bc6121a33dc2c9d8b1', '01159453928', NULL, 1, '2020-09-20 12:05:09', '2020-09-20 12:05:09'),
(361, '366b136aa72ad0879ccb635a2e229aa3', 'Chuachookooi', 'Cckpcc118@yahoo.com', '3766406fb9164d230217f49b6dd8a79d39853b5e1b981532b7451f60286439b7', 'a10f38caa210b9598a751eece32fd48f03407815', '0124292121', NULL, 1, '2020-09-20 12:09:02', '2020-09-20 12:09:02'),
(362, 'a5faba1c8082afbd49f688ad70f2c173', 'Mr Chong', 'huthoo@yahoo.com', '70df71b510192d020db26d9c49776d082e5856aa43f3956ce16b8657aedddd27', '0a395e47200e87c2ce516ace2138275173af1b36', '0124811998', NULL, 1, '2020-09-20 12:38:15', '2020-09-20 12:38:15'),
(363, 'b3cf99492adc4ab7700d730750ce2d93', 'Elaine', 'elainechg@msn.com', '3ee721bd21637cf5433b5062ad906938597fb92e70e238caea6ce04bc0307e8a', '317eaea6d38e0763812760a328c2fb1670ec9478', '0164655753', NULL, 1, '2020-09-20 13:43:53', '2020-09-20 13:43:53'),
(364, '38fc7fc6bbc44fe3b523d0e6c989bd5f', 'Goh chin hack', 'bm-goh@hotmail.com', 'da47ce5803de489324ca7e9bec61a44663d9db01cde3a66ed4b661d058bde553', '54191b4177bbdf5472b14b39f46c67cde2dc4cf7', '0174222100', NULL, 1, '2020-09-20 13:46:40', '2020-09-20 13:46:40'),
(365, 'c36b111766f8d6a7bb9fe87a0b6a6ea3', 'Li cheng', 'Lichen_khaw@hotmail.com', '0680944bc6e23a2fe46544c38451ffc4bac41f1a9bee6647d02cdea7f98da496', 'c91d149de04a477f1885c3e296a4b479cb5f4bef', '0175088468', NULL, 1, '2020-09-20 14:22:40', '2020-09-20 14:22:40'),
(366, 'fa43be0cc471645ef8b2772425ef68ee', 'Loo cheng hong', 'annyloo_44@hotmail.com', '94e4e586e82586e832a6981b20354bc0c5950293234b71031e8c60ff951d9baf', '9b619113e17a8c62cd9aae6c14106809d04a7f37', '0164397296', NULL, 1, '2020-09-20 14:29:21', '2020-09-20 14:29:21'),
(367, '0c743251179c5fee6c946df304d31bf4', 'Low Li Yuet', 'Yuetlow@yahoo.com', 'a496d096eeb0905440f73c2cddf5c2b85ff65e7c4956910b980dc7dcdab0c7ab', 'c9097c1288d9cd547d29150b2f91ac9282534b4f', '0164212685', NULL, 1, '2020-09-20 14:40:49', '2020-09-20 14:40:49'),
(368, 'b1d0c3086565221af1d984fff0400ce8', 'Iven NG', 'iven.ng@klkoleo.com', 'e8c8f6257f86abc9d656b87f9fecc09b209f452bbccc5fff3fc3c0138523e962', 'bcb76df6d58241d0bc28f587a7c48e770dd1aed2', '0123045394', NULL, 1, '2020-09-20 15:40:36', '2020-09-20 15:40:36'),
(369, '8826a0c588324f0421956f9f6b7b7605', 'tanchienyang', 'jian_319@hotmail.com', 'a4ccad94ae0eafde63184d603a2e58234f44d78c66f8ddc5f681aecec4551454', '3db57a3ec2d9460fa3ee65368f36873638b07198', '01115448098', NULL, 1, '2020-09-20 16:35:13', '2020-09-20 16:35:13'),
(370, 'bf0c5778b1f2690f5aa38d295aa167c6', 'SIM CHING SERN', 'simphortay@gmail.com', '4791411e71ac5e25c982a1978f5616ea7dc44e28cd654d5e67ced491166bddae', 'fa19864cb4910a10b8243450da3ad461ded3425b', '0125046650', NULL, 1, '2020-09-21 01:18:13', '2020-09-21 01:18:13'),
(371, '03708ce055cecebbda87cd0581ebeaf2', 'Adam', 'jjtham_5459@yahoo.com', 'b064e041c5103db825910c249a74406394d3e721394f5126458dbaa8f88cab11', '3f7b1fb02b5c289a8323aa4d3498190492fb05af', '0126566309', NULL, 1, '2020-09-21 01:52:40', '2020-09-21 01:52:40'),
(372, '21f658b4e95b553e29c199b9672dabf3', 'Chiang', 'yf_chiang@hotmail.com', '5e98fb4fd004b8ada3c3f26d37d54725d71e9f4b5933ba40bb5b41cd1e30929d', '64b9ebb1772cfa9ff4414c4db5d646b9ec05ebc3', '0194708098', NULL, 1, '2020-09-21 02:10:54', '2020-09-21 02:10:54'),
(373, '5217ac6b65411097b822c6605be139a0', 'Steven Ong Seng Ann', 'stevenosa88@gmail.com', '5c8b91efbb8794f6fd293a13aa4912796d50fb37687a9a3244ee3976a7381f7f', 'ee1958e722d12e05e94907b1687e615a6c44f6f0', '0124068833', NULL, 1, '2020-09-21 02:52:22', '2020-09-21 02:52:22'),
(374, 'ccc3422d8e3aee5b13fe0e184a9c6171', 'Qian Ning', 'n1ng-@hotmail.com', '764a2e89c918898a14e41237ebe7790679961aed713c42d56e5bf240bf30f34e', '3dd44fe76268b95920f057fba4a3afd079bac453', '0175551637', NULL, 1, '2020-09-21 03:27:46', '2020-09-21 03:27:46'),
(375, 'b6ef7522bd58325e75a0b4ee923d32e1', 'Chong Xin Wei', 'x.w.chong@hotmail.my', 'b1a644cd37d3d7ee9671e48f818c2ea7e7b3a33fb79602023b85200eae48f33d', '36feffbd6f34660109bb588b1e38b22bb2b572fd', '0193312881', NULL, 1, '2020-09-21 03:35:40', '2020-09-21 03:35:40'),
(376, '123d2eba320894c6b9f4e510a8e9ac17', 'Eunice Tan', 'hltaneunice@gmail.com', '2c68c529ec104e01a4f01802256d4ea0e49b07707f1ac17ed12029d39bf92e9b', 'e9bc4a521b5e62331d0cbe72ab1758802958f3fd', '0124216504', NULL, 1, '2020-09-21 05:05:45', '2020-09-21 05:05:45'),
(377, 'f5d25fce40c10532dd307ba27220ae33', 'JEN', 'jenchoo.1221@gmail.com', '7daea7a8254e43ebd1ad02328285c18ab0cdea631dba1f82b823fd15392dfde5', '34545298806bf50631058714ca75d78afcfaef5d', '0164319426', NULL, 1, '2020-09-21 05:11:58', '2020-09-21 05:11:58'),
(378, 'e38db08f23a9c01058dcf9a381dedf5d', 'Tan Siew Leng', 'tan_siewleng@hotmail.com', 'f51e6a903bcf788bc851e968ece923363c084e4ce077c8d9f22de158e4078ac8', '7fb9871d7438d5ba1baaac600774bf6834b8115d', '0124725336', NULL, 1, '2020-09-21 05:41:04', '2020-09-21 05:41:04'),
(379, 'c66d76217cf4fba128e8c4147058c7ed', 'James Tan', 'Cy711james@gmail.com', 'abd5afaeed49bddbad7a3252526fe922c5e156f47b19b504df3c292c61fc523a', 'e1c12294939412b45ecd2ab7ad3989765881afe1', '0149045998', NULL, 1, '2020-09-21 06:48:44', '2020-09-21 06:48:44'),
(380, '1c2ed749f8b8c04844f9d3b7d2e4e0b3', 'Yap Thiam Hin', 'yth955@gmail.com', 'de2c2cb4877ce238a20b0b9231571e99fb84716b7f90df14e1305126388900a9', 'b11d9a2246585b097ce103052fc8d5b5e59eed11', '0192214443', NULL, 1, '2020-09-21 07:21:39', '2020-09-21 07:21:39'),
(381, '333d0a2b4de76456058c6b3a63b54a19', 'Kam Sing Ho', 'ksho@hotmail.com', '1a81e793494e65d7ee72c832f638c40ae7371a27b4d897295b5a0353b177077b', '604da1bc736baccdfb63748f1cc3afcf2d038c87', '01139787495', NULL, 1, '2020-09-21 07:51:35', '2020-09-21 07:51:35'),
(382, 'b133e70cfe70a642daa7f77641ceb289', 'LOOI CHING CHIEH', 'looiginger@yahoo.com', '7190aac48024b221754439c2ca1de89d311e0a520abc9dce30a0d214d4efb24f', '86f26575d63da993549023ff6ab9ed3aae913f9f', '0124368299', NULL, 1, '2020-09-21 09:01:24', '2020-09-21 09:01:24');
INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(383, 'c134c1549fe47829726897cad41b95a9', 'JG', 'Jaclyngohsl@gmail.com', '5d60bd9b29ddff5792a615424d241eb81e51d06838e4f56150a838a40f755b97', 'b4211fc1bef16806e3e7f89f6fbf230518c53d2b', '0125199970', NULL, 1, '2020-09-21 10:15:06', '2020-09-21 10:15:06'),
(384, '8e46ab691a193b582927d60787257cb3', 'Wong kar wai', '', 'ad032ae981ab16eec3e01fad108d4db277e8302edfb3c14251c5b58dd3b0bcf6', '532146e6ba729c638c48a81eef11b539d9cf6f4f', '0182754656', NULL, 1, '2020-09-21 10:26:09', '2020-09-21 10:26:09'),
(385, '982abe94b8debed0750f32c2f5c3d5e7', 'GARY THOR', 'garykhthor@gmail.com', '7516cc660fc6ce061b7fd402aea9a93feff2dbd89663b86aea2c95bceba12c3a', '2b0ee545407593b3b8f0bf8170a126ab7e940c8e', '0167777550', NULL, 1, '2020-09-21 10:54:23', '2020-09-21 10:54:23'),
(386, 'f5f882653085e9542b5cff4688571da4', 'Fook Tone Huat', 'fookth88@gmail.com', '7c9e0b68cc3a45bb86846568beba98a7b4209816e2abf9197e8b5f0ef82deab8', '76a47ddaa0ce32c8ee411548cda96efc66e4d7ae', '0174752022', NULL, 1, '2020-09-21 11:25:22', '2020-09-21 11:25:22'),
(387, '83767fc0e8470ee8b192e7a9a60b52bf', 'nelson lim', 'nelsoncherng@gmail.com', 'f288d368299c61c7c4ab5ae9dd75f574d497f3cec5f49c78d915bffd58228338', '4d74dc4ec13e3f4edfa16ab2baf56d1fa9adf252', '0128405820', NULL, 1, '2020-09-21 13:01:56', '2020-09-21 13:01:56'),
(388, 'c404142bd0c26fa06610b192d3c31194', 'Lim', 'p1gy@hotmail.com', '6d49963bf72bcc05ec103871827252f0821a02b67d85a633fefcbfb1fe4f51b4', '431b8dbb5e808e07f5ad5be9466fc7a7ffc10022', '0164926988', NULL, 1, '2020-09-21 13:22:58', '2020-09-21 13:22:58'),
(389, '2cb75973bd2a0832f4d0ca34283403fc', 'Tang siew liang', 'selin_e06tsl@yahoo.com', '2deede9bd925c702fd760ae31deea0fa19cbdd03969e5545534dc61c1e5db633', '56297e664665cd71f026179ef7bb6926a815de7b', '0125825118', NULL, 1, '2020-09-21 13:35:50', '2020-09-21 13:35:50'),
(390, 'fdadd273c5d18d7782a5b1491ffc18ee', 'Stephen Soon', 'stephensoon@mnp.com.my', 'dc1b31477ee2cd7fb735bb28dd4ed3f15747a6aa6f81dd2367f7d1b3a17296c2', '9ea12bffa4b365cf3a706eacd9d1ad426f0e99f3', '0167231440', NULL, 1, '2020-09-21 13:37:30', '2020-09-21 13:37:30'),
(391, '1b5b0421c666756dc580cf309825003a', 'Ong Ah Kwong', 'Akwongong@gmail.com', '9738fd99b3a46f592d522762c79f23f4c63184d33cf3607269f8ce02c68863c2', '7cb0497c9d5a20715b2a288b097d78efb4d733f4', '0164956671', NULL, 1, '2020-09-21 13:40:52', '2020-09-21 13:40:52'),
(392, '72b511ff03a47a742a003041f0d4c299', 'Chuah chooi booy', '', '8fc5350351e6126455d33df3ea2bc978a23401fdadcf35c89fb57d773f5b34f7', '1f3bc55484a021ad59395c2001c6940227a37504', '0164686861', NULL, 1, '2020-09-21 14:00:07', '2020-09-21 14:00:07'),
(393, '23f3c1c16448d14adae6d0e365f03790', 'Tony Ang', 'Munling@kalyx.com.my', '6f16d1e398e904fae6ba2bc7c6f22c8da31dbf26d7dc2bdee245e2bda68cbdf3', 'fd750401f6d9ac58aa28d8acce6849b5e5fcffa4', '0105195328', NULL, 1, '2020-09-21 14:08:00', '2020-09-21 14:08:00'),
(394, '6c68a6bff212447cac1283eb6b360fa8', 'ivan', 'raincococ@gmail.com', 'a40c856aa3e579be112ce018339f164c57b9b44246fe383619f15504d464d3a4', '257df96f4c7b1da49465b15abc15e7f0efb18ed8', '01126682568', NULL, 1, '2020-09-21 14:08:56', '2020-09-21 14:08:56'),
(395, 'c5df18a26082b7d39a64075c8739dc26', 'Ng Chor Wooi', 'ngcw9039@gmail.com', '837503b55cdc91fc118972e7448e77e300f1d1860244b8c5233f85ecdf5d8ecb', '1e95f605cd24eced2353313ca51d2138953d186e', '045824090', NULL, 1, '2020-09-21 14:31:12', '2020-09-21 14:31:12'),
(396, 'be8e5626a66bc2d3cbadc4456574ef3f', 'Hadden Lim', 'hadden.kalyx@gmail.com', '41ae1863e22c2cdae3ede08b93021ec8f7286e0a0cd8632fdd549b5861a64f73', '057f2a3753f11f10a9df24a06abb4ea53f888d1d', '0164190499', NULL, 1, '2020-09-21 15:00:40', '2020-09-21 15:00:40'),
(397, '975d2af20634919446204e29c72c3184', 'Suu Ann', 'sylaw2011@hotmail.com', '0424426642e0845af8f6b4530e6fc4b1af26b7b76c6116f90cc34453727044f7', 'b70009a2beec96f85160a544c794445beab46f57', '0192515418', NULL, 1, '2020-09-21 15:03:55', '2020-09-21 15:03:55'),
(398, '468fe0dadb46564d60dec8193fb2800c', 'Tai Lian Pong', 'tailp@ghtag.com', 'b32bb6c459395db0941d15d3ffcd29e950173440a2dd38850c45021fce3c41bf', '45b7ca8a3a3f7268d9e43227d681caac0a7e8bba', '0124927762', NULL, 1, '2020-09-21 15:06:57', '2020-09-21 15:06:57'),
(399, '47d63114c4cd44055ac761d76b12f73f', 'Teh Khian Beng', 'tehkb@ghatg.com', 'a797b56a2dfba2abd84e91e1223849b59a0b1a15be86a4d7c39705ca7509604c', '69307907709e03fcab169dfc122360e0c41f4959', '0124271614', NULL, 1, '2020-09-21 15:09:02', '2020-09-21 15:09:02'),
(400, 'fa8eb16a8eab0dd07671a7fc2f382be0', 'Steven Khor', 'stvnkhor@gmail.com', '6ed08ad6804a4280f4e1c99c76f1d6c6904476b0c08e467ac56e339ded6fa24e', 'c2c3bde46fbad1c5f1d3e5670dfe99013137f69d', '0124245725', NULL, 1, '2020-09-21 15:33:08', '2020-09-21 15:33:08'),
(401, '391d14ba1245537af69d2eff71278a83', 'Tan shin rou', 'roxy92.t@gmail.com', '3b2dc5dd5884f2696e12fe364c19f0af5236981dbc38a6f4cb0b8fc807041243', '5c30ef88a197d636237b6934fb61d4eb8940aada', '0125387282', NULL, 1, '2020-09-21 15:33:57', '2020-09-21 15:33:57'),
(402, '79128b6af7c44d4b14f910aa1886a636', 'LIM TZE LING', 'limtzeling@gmail.com', '2677820f7f6c197a82c7c8d77137d8d71d3acae09d665ff6082ba2b6ae7945e1', '68a14c37350bcffb6c063dc70de24c52ab2a2ea8', '0164766387', NULL, 1, '2020-09-21 23:52:11', '2020-09-21 23:52:11'),
(403, '034d8477dbc8be18118916fc92debf88', 'Ooi chai sin', 'tanmc_86@hotmail.com', '2d9fa03b5111a92948b34a9a3d985c95c27f5dd552ca35dc00428345c07b3abd', '1d637bcaf4f6f69d0d8de07e385174211b16d98e', '0194055555', NULL, 1, '2020-09-22 00:05:09', '2020-09-22 00:05:09'),
(404, '7911c13c558e587d217e05bb9607a0f4', 'Lim Chen Yen', 'yencpy@live.com', 'da32e3c6fb697b2da4792742825e51ad4a0cf110b8e5afbbf253bdf08e49353d', '1f7cdc3bf07a45ec7772b17b3d556eddbe778004', '0174456550', NULL, 1, '2020-09-22 00:31:50', '2020-09-22 00:31:50'),
(405, 'f84b6522700ccaa51890a793ecd0fd3e', 'Jimmy Ong', 'jimmy@nsr-rubber.com', 'c918a2759968f8bd29c285b3129fa66109e1859d67bb97fcb88b5da7481d361e', 'a2af0e923e1251a3a880d6e1fc8b7470e2da7a43', '0124386080', NULL, 1, '2020-09-22 00:49:20', '2020-09-22 00:49:20'),
(406, 'e2d29157d71ae00db46d8e2a83170f95', 'yeoh', 'frankyeohtl@gmail.com', '584e45b4444c9fbc6192490c888c3e6ad4be4fecb1808759531734b9d8d472dd', '3edfc4e3fb68b5422521ee750d14e4146ab6b25f', '0103824483', NULL, 1, '2020-09-22 01:35:32', '2020-09-22 01:35:32'),
(407, '2e26a0ff3e7698e693f61dbe8026cee9', 'Janet Chan', 'Janetcyh888@gmail.com', 'db7b5a226a4784b99f60efef7e7b02c3c41fcdc61fda6646407d4155f12434b9', 'f6f0a0bcc197a9ae5bcd7b05022152585399329d', '0126596025', NULL, 1, '2020-09-22 01:37:07', '2020-09-22 01:37:07'),
(408, '2d3449c5e83472ea9ff29fffbb745418', 'Heng YH', 'yakhoi-heng@hotmail.com', '46b9780f7a386a656d8a988e2c92cea3546a24f1bd361842a84200413c4d23eb', '4509e577d0df6ae58b3c26ef221b933d6cf6b738', '0164448678', NULL, 1, '2020-09-22 01:49:30', '2020-09-22 01:49:30'),
(409, '1993ac3085b54157b6b3c1768c100f65', 'CHNG KHYE SENG', 'angelsky828@gmail.com', 'ffaeb385b94937462adcde87929e604e65835f42b73d28fcb843194a81e26265', 'a5cc4f7ba64b1c0d72a8d27374868d44a0bb1655', '0164488661', NULL, 1, '2020-09-22 02:21:59', '2020-09-22 02:21:59'),
(410, 'd8032cef7a30271bc95bb22f3c6e1c97', 'NORMAN NG', 'clchngsky@gmail.com', 'c72eaa7eecf9156a4d7aeaf1655a8887bea470d745c287b85b16511a392d3299', '296064ab5a3d4d2d7db6b960cf7507b91581bb65', '0164444188', NULL, 1, '2020-09-22 02:24:53', '2020-09-22 02:24:53'),
(411, '25cd0fbf8578fef44b5db2b1f3f6f319', 'Goh ah kooi', 'akgoh8039@gmail.com', '561b35d3080b83bae6ff014bd9b143e32370b524871f1043fe91c521d5882847', '400732fe56995eaf623cdcfd28028916bfac8170', '0124138039', NULL, 1, '2020-09-22 02:25:00', '2020-09-22 02:25:00'),
(412, 'de60aee579841b6af63f6c10007480b4', 'SuiSui', 'ypkoay@yahoo.com', '5212d2a61a986658e969292a798430ce8724a3d5c59271f619c63a1e77d9d154', '1506871d9a66f28f40b5a33b71e4f286eb56a9a5', '0125114803', NULL, 1, '2020-09-22 02:36:00', '2020-09-22 02:36:00'),
(413, '5ac62dfb60c94f1e5cbcc02c1d13a9f9', 'LEE KENG ENG', 'richardlee73@Hotmail.com', '392619f64c017aa7617a60d0a72bf783e47dbd70c34f48502def32b44130a2eb', 'cb068da128f7c8d10ac758c5d594a3d952c429cf', '0194843008', NULL, 1, '2020-09-22 03:01:20', '2020-09-22 03:01:20'),
(414, '2cf5ab9c2b22e5cd1c38d175bc122208', 'Chee Lai Kwan', 'cheelaikwan65@gmail.com', 'dc874d81947be4e457bea0ca5a667b0833a053d7c32e42fd7ee4564ab288796b', 'be17c10eb80f9f6639e4c6bcf41c25e14779203f', '0197567828', NULL, 1, '2020-09-22 03:31:54', '2020-09-22 03:31:54'),
(415, '70525b9da206caaa4535d710f6fd38fd', 'Sim Jing Dong', 'jdsim111@gmail.com', 'ecdfebfff6c85bce85936a9b1d528b5be2540c928807b391e575bf441b2f55bb', '0d8a742fa5f3b5817e6df3cf455bdc5fbe692026', '0175660036', NULL, 1, '2020-09-22 03:39:21', '2020-09-22 03:39:21'),
(416, '95c738d33f3f252f3c97ba15f43f30a4', 'Liew Bee Lan', 'liew_beelan@hotmail', 'b8981ef5dfdf81c18343e240db3a437ac2cb3b5a98115cf1b4c31eb3c582f9f3', 'ad63a8797c43353e9d12a9ec03b793b7e9478fad', '012460868', NULL, 1, '2020-09-22 04:07:16', '2020-09-22 04:07:16'),
(417, '6831a659e9c083844f20bcdb0b2cc4ed', 'Liew Bee Lan', 'liew_beelan@hotmail.com', '2fd542d6471c675b3ecb3931e86b4d619837f7008fa5688cbe9b29b98210c7c6', '4167256b7ab665f96eca00b914b1ba14097ccc47', '0124608688', NULL, 1, '2020-09-22 04:07:56', '2020-09-22 04:07:56'),
(418, '0eb62e227f6f0346a15520147093f4c3', 'Chew Eing yit', 'ey_chew76@yahoo.com', '3d78a7e9d0f7444907642ce67d56e499d675f58b1ee707d7450f4cf77f2c9290', 'd2e4a8cf523eee387c7015fe958e7058fdd180cc', '0124276659', NULL, 1, '2020-09-22 04:13:16', '2020-09-22 04:13:16'),
(419, 'fb66c82e0515950bf2b00375f709c67c', 'LEE HONG CHUN', 'hongchun.87@gmail.com', '5ed3db6820d531901141bf235d94c71a45bf036958f29d1aab666a9919bb0b1f', '78ed2aad73eb4f67f2c5d68fea642d0928b538d7', '0164134567', NULL, 1, '2020-09-22 04:22:05', '2020-09-22 04:22:05'),
(420, '3a4c805282fb5fd8ff473c30b507fed4', 'Yong SS', 'ykyong31@gmail.com', '1449eb13921e4d007967da5b2fc6d3aa907e1a350526d0955c749179119f5822', '87dfe08193b66eb518c534fd200b873ace5472c6', '0194472943', NULL, 1, '2020-09-22 04:29:56', '2020-09-22 04:29:56'),
(421, '571648bb1c50fc6406172cb7d05d5996', 'Liew LM', 'liewliongmoi@gmail.com', 'a436286705a391ff23552c2df1c0e320109477ae0fd67c8bb8313d06319c10d5', 'ba61a003e2414794b4bb59ef426a9bd07808ccd2', '0174019819', NULL, 1, '2020-09-22 05:04:52', '2020-09-22 05:04:52'),
(422, 'a83d8e158c9ede6043ba6fa189834a14', 'ANG KENG CHUAN', 'kcang1112@gmail.com', '92a0237ce50ee60e949146e55d9ce28cef0acf07ff7c22c0afc4928e5e53bbe2', '615c3dc4e2ddd9fd5d620e87602106fd73a223db', '0174877107', NULL, 1, '2020-09-22 05:47:32', '2020-09-22 05:47:32'),
(423, '52d948e9de1235c78ac75d4758caada4', 'choong', 'ccs2814@gmail.com', '5799169e3be224bd19e60d983b89845f4b2872d4d1d177f78e3712dd67859751', '4278449b23afee299481d49a37b9c814a7ee4914', '0125252814', NULL, 1, '2020-09-22 06:02:46', '2020-09-22 06:02:46'),
(424, 'a50b124cf947d779952de0268733c55e', 'YZ', 'Syuanw12@gmail.com', '07a2e14d0c649f0db19849b738c53cad6ddf76a9e0978899713baef050918b54', '2a668ca7af32d1e4a706d9a4297ad0cf8c25c2a0', '0124637299', NULL, 1, '2020-09-22 06:39:06', '2020-09-22 06:39:06'),
(425, '636d0a8d84d3968971c2723359bfb93c', 'Ooi Pin Pin', 'pin pin.ooi7@gmail.com', 'fc01d4cf5f47899f7e1fea87ead4f70a88a4c2908a8c0c16153d080749497ca2', 'a3acc2ecd2603cf1d3d3be8370c2e7fff9f1f661', '0198883799', NULL, 1, '2020-09-22 07:09:06', '2020-09-22 07:09:06'),
(426, '5afcd5eb2b2b7f04732f97f26a40a4a2', 'Phuah Chin Hock', 'solariseeducation@gmail.com', 'b471446b045536e1ba8293db231487ff489c655c982b86218f7d67e9b691f6f7', '0e1890259bb14964427ce56fad79e81eedaba923', '0164508840', NULL, 1, '2020-09-22 07:20:32', '2020-09-22 07:20:32'),
(427, '34a19376b6744e01e0e6351ee64be53e', 'Ngoo', 'ngoosb@yahoo.com', '6208888c8dfe5328a9cc62755a69e3328722d5fd0aecf89a8cd25f24baafbfd6', '8a8f8df2cc90c33d1bb3cbf523ea48b201b48a6c', '0124776784', NULL, 1, '2020-09-22 07:51:37', '2020-09-22 07:51:37'),
(428, '685bed9e3c8da52cc0873c562840d71e', 'Tan Chee Chiong', 'johnlustan@live.com', '2ec2bee8477f5598173fc1180656fbd526d500963d3b01cca0aec269dfb6d8ac', '357cd1e95798eb9d639b531401251d30a1b02160', '01112413141', NULL, 1, '2020-09-22 08:51:22', '2020-09-22 08:51:22'),
(429, 'a4cd3f4fde4aadca8bcca5972173eaec', 'ooi kai', 'Yeansiang728@gmail.com', '881881ed34febad5f30ffb3a4a242d9d62c709599be4163accb02117f10b2e1c', '07a55cb739501514d4c78ff26432138e66eb63e4', '0174555896', NULL, 1, '2020-09-22 08:58:36', '2020-09-22 08:58:36'),
(430, 'a7b86adbb4fced6de74ef3f15a6b26d2', 'Kelvin Fun', 'funyongwei@hotmail.com', '524b2a6e92756c8d6b6e79f5f9fbc12145fbcd8790e260c75a84e96a3b774734', '0edf245cc31b9c4baf876a45cc76b4f2ef66c9b4', '0174439869', NULL, 1, '2020-09-22 09:10:04', '2020-09-22 09:10:04'),
(431, '568cc927367051c40be17fb9c0633eb8', 'Lawrence Chuah', 'ws_chuah@hotmail.com', '0f95cb6994d620909efb7354a7fbc2bcd080a5c9f66af158c028985f148a5665', '370efc93f82b3f091ffddd453916470162734a74', '0124520708', NULL, 1, '2020-09-22 09:53:20', '2020-09-22 09:53:20'),
(432, '4bb324faa326ff2e2016d6bec571ebfb', 'GOH Joon Tat', 'joon_tat@hotmail.com', '4e8696b4393cc423a2ea2caf13e18e3859a637675c6190d019f813e3cfe320d1', '807e55d66b1ddb3b3d268588afeb8667e1a1fdf6', '0194511988', NULL, 1, '2020-09-22 10:30:15', '2020-09-22 10:30:15'),
(433, '917850014a5035bb56d121288fcbed95', 'Lim Kean Hock', 'khlim8w6@gmail.com', '8362a5cef4c4182ee06ec2964f08310f6c7897df280cf99d61022618c82c8ece', '6127f285e9a9a23b7e3bfc7d12ca7357e6360f38', '0124928664', NULL, 1, '2020-09-22 10:38:27', '2020-09-22 10:38:27'),
(434, '1bca9784b7bc7e78d51a8fdce7e5660e', 'CB Leong', 'lcbleong@gmail.com', '2783063458f55f661e53f127aaadc1d5db1d4acf8e28272daf2a75168db7a1d7', 'cce3d1e1e2eaf6a9cdff83cea15aba9aa678e7a9', '0124565106', NULL, 1, '2020-09-22 11:54:04', '2020-09-22 11:54:04'),
(435, '10c217c5b7e780990b32551c216a67b5', 'Tan Pea Chein', 'gracetan8316@yahoo.com', '1857e1085cebe97f63b7c73c53310e87fc0c19dbc30a96d431d94eacc831334e', '83853a83351473334ddc52f76ae941fe2b02367e', '0174699566', NULL, 1, '2020-09-22 13:15:56', '2020-09-22 13:15:56'),
(436, '29fc0520f473855d0fe784bb86759ed2', 'Jenniet', 'jennietwy76@gmail.com', 'f53e1e0909c5cee3f43474ccb02a80cfb417318936bb2daa316959dbbc2c1038', '503db4eecfca22e6823d2381fff6f52815c3166b', '0175602876', NULL, 1, '2020-09-22 13:32:14', '2020-09-22 13:32:14'),
(437, 'c1c2c7dff86384f3bc0fa19938e7a6a2', 'Shaun', '7cswong@gmail.com', '4167cae133119f50b779fe6ff709395cb3af56eb570468218d5ae20daf4c10c8', '5df85e81bb4b9db2bb2c845064ea1d94b81b34b7', '0165531398', NULL, 1, '2020-09-22 14:08:05', '2020-09-22 14:08:05'),
(438, 'c3641fe7468b668c4aefdce8f038ff9f', 'Goh Chin Kwang', 'goh@dulub.com.my', '7c3b179a01f2f18384819a3783dba6288a023bc72f45430b63ccca74b3b82000', '886ff390576bb809c1a89e43efa870bece970138', '0124029503', NULL, 1, '2020-09-22 14:21:10', '2020-09-22 14:21:10'),
(439, 'a01c0ed747735d233163ba2f07526a9d', 'chin seak weng', 'waynechin27@gmail.com', '0b7878c392061758f0fa9dc66b917cac5983d0873a100dacf0175efd888fffb5', 'b47053b8dde09389a745c252e990fca677e08e36', '0125511170', NULL, 1, '2020-09-22 15:21:27', '2020-09-22 15:21:27'),
(440, '6154683dfa1cf4e058d6e2aef626dbac', 'Chris Seah', 'Chrisseah75@gmail.com', '1ee1acdcf10e76f0cea56b86d872459ea296bba7775cd054189e5e350bbdd7cf', '2d4f11b9f332e18af117cca3ad412ae674c5440a', '0124307007', NULL, 1, '2020-09-22 15:23:03', '2020-09-22 15:23:03'),
(441, '1ed6425bee3a8c1429982829b6c367e6', 'Ivan Ng', 'Khai288@gmail.com', 'ab55eb80a1a643748e097bc64a0d33a51a2069e774de28b63f6b1f4e92407731', 'dc6627a4483670470ecc6edecc356cc0a6776527', '0124283662', NULL, 1, '2020-09-22 15:40:43', '2020-09-22 15:40:43'),
(442, 'e89d8e8325ddea2be8f2bb241fcff884', 'Cheah kean eng', 'Keanengcheah@gmail.com', '151c315fcb6a8290c7bc2f260fc48472c55d6e626f9477d26f933fdec2bf807b', '7a3f3736f1e263e1e78951f9de7aa0823fb0c95b', '0124118027', NULL, 1, '2020-09-22 20:58:09', '2020-09-22 20:58:09'),
(443, 'bda5e50ab0d504d502247770fc447df9', 'JAMES LEONG', 'jamesleongsh@yahoo.com', '39104b840a6a9c2578a99ae6dcf37e96336d8661c8080faf65e75e410854982a', 'be2505f9697379f3ab8fedd1b06a3447ead50008', '0163080830', NULL, 1, '2020-09-22 21:19:26', '2020-09-22 21:19:26'),
(444, '4255ad5d12f56eb64142eca9e56fafb9', 'Ooi Suu Chin', 'scooi831@hotmail,com', '5ceaea31b586c7fdcb176731aff2c6fddc4c8afc7119c847a3c8426ba11acb01', 'c486585d2da285238762e131bc71c43437034064', '0124268831', NULL, 1, '2020-09-22 23:03:56', '2020-09-22 23:03:56'),
(445, 'a963ce5de2a2556776700f74d4ebfaa9', 'Angel', 'wpling8949@yahoo.com', '482c27b96e8b18925c4237a77e55b86e874a0621feb43cc3508714aace610219', '74a8097903d71bcb58a47b6907740e1ad191308a', '0165217533', NULL, 1, '2020-09-22 23:36:48', '2020-09-22 23:36:48'),
(446, 'c292875b9a394522e797ab0fd6de8f9c', 'Daniel Tan', 'boonhor8311@gmail.com', 'e41c1439798d01ea3823be8c1e71d31ef1158a61ea58d5a7fa0bb4d53a927dfc', '5ac4d3216e40cf7139523ce6fbfce7f484455293', '0164992273', NULL, 1, '2020-09-22 23:46:57', '2020-09-22 23:46:57'),
(447, '248b00f6c484dba525320c1645067027', 'SP Whong', 'Spwhong@gmail.com', '0aa651455d50ea5d1b042f257df71716302e2a7b19b00b21c735f63db2fed73e', 'b5136050844a86614fdc8701a04392ace9e5d15d', '0174502223', NULL, 1, '2020-09-22 23:48:01', '2020-09-22 23:48:01'),
(448, 'e50fdb0e7a17bde00c314b682491a9f6', 'Hong Ho Jin', 'pruhojin@gmail.com', '3b64a445c15b72a23ed20d2e68fe6053fad0cf9225ab445af917a14933787ed6', '8383c607402d60ce6fdd54ccfc83e872a2b0022c', '0124855010', NULL, 1, '2020-09-22 23:51:14', '2020-09-22 23:51:14'),
(449, '39d35eb962629c7b4025552137d98678', 'Anthony Ong', 'wzx_och90@yahoo.com', 'a079e2cce3b552c666d8ec72910f86b05290f9d9f97c816cca173521c7083e1a', 'd7cb2e7337e595e6b4939f58f15856dcaebfb53a', '0167089615', NULL, 1, '2020-09-23 01:24:02', '2020-09-23 01:24:02'),
(450, '4f926f82c3f628df4f9b767beeb7f22d', 'CHAN LEE LING', 'llchan@prismprecision.com', '8f6f9e81b9576e5c7e8e66c9280e6a98143397ee1db93e25b6b46cc997179233', '2126210b54d222e0d00014503f72ea7cdc141092', '0164901001', NULL, 1, '2020-09-23 01:45:15', '2020-09-23 01:45:15'),
(451, 'fe8e0f8061fd65e344904d5daf8d05b3', 'York Ng', 'passgo85@gmail.com', '9a8372d6cc67e9169591342faf2a3f0ef83f71d49223c18c0fcf93ec92620b69', '495c0de854a43914c5f5a1523f556153d9c3a2fc', '0124748138', NULL, 1, '2020-09-23 02:03:15', '2020-09-23 02:03:15'),
(452, '486fbe901dd0131c9ccb877d4f3953de', 'Ryan Lee', 'ryanlee_1514@hotmail.com', 'df2c32e59aff8e40208b469bf4ee0917f4f255cc3177e7081cb3d3c6d097c52f', '9ee1787aff5aa391f2b09dc14d460e91c20afda1', '0175433090', NULL, 1, '2020-09-23 02:40:25', '2020-09-23 02:40:25'),
(453, 'c45b2a4f27e94bfeb8b43fb36e139e48', 'Liz Lim', 'lizlim@hunzagroup.com', '810c4b9a7c4047eef62078277ff7703ca58667ac42682a85ce7e52f99ab11229', 'db886f05dfbf8dbb8f5e4ff17959af26cf771245', '0164988829', NULL, 1, '2020-09-23 02:51:01', '2020-09-23 02:51:01'),
(454, '4905521f7ee015faf2a9761c6c373cef', 'YEAP SOON CHWAN', 'SOONCHWAN.YEAP@GMAIL.COM', 'ea6a7cf0ea8c248685a41582d270305ddb7590d85fb61bb1e02ca864bf9f8103', '511b6f63c5bbe17c6a73a1796591fbba74c9e6d9', '0164567291', NULL, 1, '2020-09-23 02:55:03', '2020-09-23 02:55:03'),
(455, 'fd773d4bf6cf33df2740aae48139499d', 'Chong', 'lewyoonkim@gmail.com', '58af41831605dbbfe6f8d3ddfaed49d382f284e1ece34e895d9be2011628de1b', 'b2d4ef164271ba92a316181101dd848adbda648c', '0124709863', NULL, 1, '2020-09-23 03:02:06', '2020-09-23 03:02:06'),
(456, 'b4595b7dade76aa446445d4f31d23fdb', 'Soon Wei choon', 'soonweichoon@ymail.com', '1c88c7d848d33866185ce9e3fafcc987728a4af70e0a820745f42b6a527b4dd9', '3754e94e937277a08dccbcfadad5b17f8c8d915b', '0124299698', NULL, 1, '2020-09-23 03:04:16', '2020-09-23 03:04:16'),
(457, '55e19a5ccf66768c849f6c8cdbc55697', 'ONG bee keow', 'qqong@ymail.com', '019cbcfad2ecd5bc1ade27eedfcdfa03cba9e9fad13cafde8f74e1eed9a3fa7b', '0d73c41d060c8a6aff512c0b40966c8870ea9286', '0124706899', NULL, 1, '2020-09-23 03:05:28', '2020-09-23 03:05:28'),
(458, '026b451df4468d63cc0aa8e037fd8414', 'Nicole soon Teng yi', 'Nicolesoon@yahoo.com', '1e0f0dad2fa24e7e3c9b151179a75ad43ae2f516d2afb07cb0f8108dee26abaa', '0ee95bdeb5d9416df144489db84c104d31b31805', '0129885920', NULL, 1, '2020-09-23 03:08:17', '2020-09-23 03:08:17'),
(459, '6334361fe63b6643b935c0a7ab024544', 'Kim Beng Huin', 'bengh_kim@hotmail.com', 'f75fdf000ef39e7a635d97d369ab8897165a8636155a741090717efe0f99e4f1', '1a3d1c9276e7cebe5c447cf451954e1bdcd863d5', '0142569286', NULL, 1, '2020-09-23 04:08:20', '2020-09-23 04:08:20'),
(460, '1eea154fd730fcbae9377eb8ea4afccb', 'Khor  cheng jook', 'cjkhor22@gmail.com', 'ff0a13f447be7d6a2f1390088616bc2e9a6615942cce5396fb99daa4173d1878', 'f2cff665aab1b029b42e2bf00d5bdf703748e06f', '0125161686', NULL, 1, '2020-09-23 04:38:24', '2020-09-23 04:38:24'),
(461, '2ac4ae8e908722c4a44ce6dec63a9c61', 'OOI SAY JER', 'ooisayjer@orienta.ltd', 'c57026799474aaaf42dfdd4c18e1172839e5afd80448ab01df69b2a9cd9338a8', '6de80c53100a5507ee725ff5afac2e9bfa51a6b1', '0193130035', NULL, 1, '2020-09-23 05:53:25', '2020-09-23 05:53:25'),
(462, '990e03fb7c8b3b24019cb0e12779baaa', 'cindy khoo', 'cindykhoo94@hotmail.com', '8ba29c40cdd08e40187a9155bce29b0d07e702d235612b08738049ed128ba958', '8cbab7459a832c9c454f2988f7c677bb3a095420', '0125410749', NULL, 1, '2020-09-23 06:26:07', '2020-09-23 06:26:07'),
(463, '89ac2c21df4b8ad8128c418bcd8aa7a5', 'YEOH SEAN JING', 'stanic_yoyo1993@hotmail.com', '4ed4e3bccd88881fc2f0fef431d41948f928fdc01d10ee32ab8fcb09fff67f71', 'c5a67d2806e2ca0946b73703db69dd679c30ca2f', '0', NULL, 1, '2020-09-23 06:34:01', '2020-09-23 06:34:01'),
(464, '118b4f6f1b152a95b78701e48d4ea020', 'Yew Ming Hong', 'y.mh.94105@gmail.com', 'd3dfc94d7a061a52e2cd150dc6b7d167ee0b7eecbb4ca37b959b95e0fe50a9fa', 'd8f982967c61e7234eaa299fc540445de86aa3d7', '0165000076', NULL, 1, '2020-09-23 06:52:16', '2020-09-23 06:52:16'),
(465, 'faa505449fa2a4b846b0a6c1cad72ace', 'Cheryl', 'sssuang@hotmail.com', '5e36e6c2e570e8021f571dfee6eca2cdf397dab4abb7da1a5b5e1fc59de62aa1', '598f78aa3efcdb6e61ad33795379dd02f0cbebad', '0174170513', NULL, 1, '2020-09-23 07:12:19', '2020-09-23 07:12:19'),
(466, 'd828981c15155b10a3ef5640cba2bfb0', 'Chong Min Xin', 'mingxinz843@gmail.com.', '2a2c44d868ae13da35469c56012e6174b48e5d0910ea3321347d3ec11d08d175', 'a48d5befcccf48ea88361f5ab71563f469568824', '01111058129', NULL, 1, '2020-09-23 07:16:46', '2020-09-23 07:16:46'),
(467, '8dcac85d1d85bb9df395377eac2581c5', 'Lim Yee Liang', 'Limyeeliang88@gmail.com', 'a3fc20109839541d1a945cd07f41a7cae5abd81b42e93cf094a0a3e8e8bb6796', 'c69188a0d5dfe36404dba0374c7673585ecd840b', '0199284075', NULL, 1, '2020-09-23 07:17:27', '2020-09-23 07:17:27'),
(468, '1122db0ad76fb0abcdc998329ee92a3c', 'gabrielongjianwen', 'chiachinning@gmail.com', 'bf94b5f43cedb119baf97ffec924d2b32cdcf05f7875f9bd9e6d119aea4e338e', 'd3b88404be7d84cc7c2322e61740c2678036ca9d', '01127831569', NULL, 1, '2020-09-23 07:29:07', '2020-09-23 07:29:07'),
(469, 'dd070b3bfa2fd08bfd757c28bc1e3b73', 'Sim Boon Kuan', 'simbk1@gmail.com', 'a26bf2e3a0e0fcc0fcd7e6a925f51324008844544c98203d2496fde09a7ec7fc', '5623bc7ddda00e85bf963a75b42eb8ba51825f7e', '0124524012', NULL, 1, '2020-09-23 07:31:39', '2020-09-23 07:31:39'),
(470, 'b60c68f64fddedd7792efa3f96b8e277', 'Hee Li Ting', 'natsumiting94@gmail.com', '4dbab7a6a54aa8ec90816774030a408305c14f7f4acf5d619d4cd24d66fc50f4', '1a19688e484fd79a1112cf9cc5b775d712a639dc', '01110990940', NULL, 1, '2020-09-23 07:41:09', '2020-09-23 07:41:09'),
(471, 'a72cdc437097c3ee9f66ef2a0f69606a', 'Kong Shih Hao', 'kongshihhao@gmail.com', '331795bd25f139c3263ffe76cdd4a722e1737931f0820d20213299ec2e5dc3c6', '0747297a047b5338677d741271ffc13042c49a28', '0195477555', NULL, 1, '2020-09-23 07:53:24', '2020-09-23 07:53:24'),
(472, 'e1d1c7561cd7b9c8e1ca519357995b52', 'Chong Lee Ping', 'longrich88@gmail.com', 'c10ff5cc0f17a738a6f6e180c1e14744511232eed82a014e26a6872f0c103a62', '674be12d9d73105d41aa420b6b0ce0f56230a634', '0103999155', NULL, 1, '2020-09-23 07:54:59', '2020-09-23 07:54:59'),
(473, '2853c22e559d8d5a82b0830347e6d21b', 'Koay Gaik Kim', 'adaadaong@hotmail.com', 'e68d9258b9752e60cac054de34389604702f438fc9a067e9c2762e552fe8aca3', 'd065cd594654977f97c2ac9efae6f90e02095af7', '0165609890', NULL, 1, '2020-09-23 08:11:35', '2020-09-23 08:11:35'),
(474, 'd435cd339eca8d757a0c367abadc98a2', '申满珍', 'singmc606@gmail.com', '29337478ce4596349d9a6f0d6c5d8def058e3e146e084ff8f17b39aab44e5732', '6f01704389735e3d31798110851f4abb0a0af6e2', '0126066062', NULL, 1, '2020-09-23 08:28:19', '2020-09-23 08:28:19'),
(475, '4e3e5cb8e1cd162179fe440c2912a830', 'BOEY BEE YIN', 'boey_by@hotmail.com', 'c7a2247abefb04d196754abd131a56a3ea60caf0dfcf52548a5418c7ba3fad9b', 'c1df4536d3d594bb1fee71b8b53b81b57ff8eb45', '0164311880', NULL, 1, '2020-09-23 08:33:06', '2020-09-23 08:33:06'),
(476, '14b58844b4c93617c4c745f01c9e6291', 'Lee Hooi Pheng', 'maylee@ltsgroup.com.my', 'cc6be052497ed42e6450420e91b540517b87eed69d4dd966ff16a18724ae15ba', '48788b11fbbc48548bd6f09def636aeeb2c8c444', '0124029333', NULL, 1, '2020-09-23 08:33:20', '2020-09-23 08:33:20'),
(477, '161998a1b1df505bce88e704f128d49a', 'Lee Hooi Jing', 'hooijing@gmail.com', '7cd95ab082d35f60ffe8319ac24a850eb40b8796e2024f0ac0beb62740204b6c', 'dc20c0745960bac5ce8854daa1f523f330f009a7', '0124273333', NULL, 1, '2020-09-23 08:35:47', '2020-09-23 08:35:47'),
(478, 'd50bdc68fdc95783d9665c2b364f8aef', 'cheah hooi teng', 'lorraine@ltsgroup.com.my', '32e9a0c3cf07e7aa71afa3ab0f2822b219732b1ab66103d7accde7f9b64ab6dd', '723ade3447877c9f81258c451a75a190588727b0', '0124812033', NULL, 1, '2020-09-23 08:36:41', '2020-09-23 08:36:41'),
(479, '84065e48f4175a5457f02a984fc0b65c', 'LIM CHAW YEE', 'lim.chawyee@ltsgroup.com.my', '5beea933a378dfbfc928b92c1e24c1d25242964cabb354ebf636523568214ebd', '228b30dace643f7582ce42fb42e3ceefd2c97045', '0194131533', NULL, 1, '2020-09-23 08:37:32', '2020-09-23 08:37:32'),
(480, '51706d9400925d52dfa263ee1378c385', 'CHEN XIN YI', 'chen,xinyi@ltsgroup.com.my', 'b47fb13846b8f4e5994be73b91d0601a304e8b53145338001c7ccf6ed3faac0b', '112ee80951e23c69b38457a0e7c3e9482e817910', '0164024807', NULL, 1, '2020-09-23 08:38:35', '2020-09-23 08:38:35'),
(481, 'aba9c090a6a4dc2763ae659577032deb', 'LIM KAH WEI', 'lim.kahwei@ltsgroup.com.my', '3b43fbbe10ed7379d85678aa36005de8f897a81c016f4bff91b4be213922778c', '37c7f12e2c40ac0f68277eb9bba0ca885a22538b', '0125295381', NULL, 1, '2020-09-23 08:39:31', '2020-09-23 08:39:31'),
(482, '1f6ae61e4e8cba60b84067aff7742c2b', 'FOO PEI LING', 'foo.peiling@ltsgroup.com.my', '1333a28fac8c8ab5af79da59deb4819ff0e256b58e0e6d5f30efd2b2a0bd262d', '94d303a46ddb2bb49093780a85724306774dacff', '0187762799', NULL, 1, '2020-09-23 08:40:45', '2020-09-23 08:40:45'),
(483, 'a11d89b9374efa7d9ec0cbd9cad856c7', 'NG PEI THENG', 'ng.peitheng@ltsgroup.com.my', '87bf0533506827e3356bb98ece276a344e45a9a244206aabe96d51afb54ce87e', 'c8080a70c4e333ca629cebead4a52675a8dc5274', '0164402502', NULL, 1, '2020-09-23 08:41:54', '2020-09-23 08:41:54'),
(484, 'f80ccf21e172948fce781caa6b0de2aa', 'NG KOK CHUAN', 'ngkc93@gmail.com', 'f3d0ff1fda905d74c0689613205b705979b0f2c3475d909bc1f7feceb5058509', '4995b9024ac930ea6f2309867a0a02e3ba501a13', '01138507112', NULL, 1, '2020-09-23 08:42:45', '2020-09-23 08:42:45'),
(485, 'c64a2b598aeb984dd8de493c8b06e867', 'Tee Jia Wen', 'tee.jiawen@ltsgroup.com.my', 'e5144f371cad289b06eb6134ab8070efcf4950f61d89fd3d9a9a9081b036b9f5', 'b4eb4b1a520859276fcb877cf8764b4c8f17d2bf', '0125426688', NULL, 1, '2020-09-23 08:43:44', '2020-09-23 08:43:44'),
(486, 'd8d7dc11f04fb44e4454b68fa48072de', 'Siti Fatimah binti Abu Bakar', 'fatimah@ltsgroup.com.my', '397fcb033cfe168950d4fe8ad48bd2e416a2ac07b3e625e7ca2c87110d753b79', '1ee4043961acbaeef690e0941fda62e40346a07f', '0174596009', NULL, 1, '2020-09-23 08:44:31', '2020-09-23 08:44:31'),
(487, 'a02799d8061e7e5a815ceb3d14ec1b4c', 'ch&#039;ng im im', 'karenchng@ltsgroup.com.my', '8694f0e9ecdfb4162d41005cc256d6f6c7451a4cba94484f4d0c4038a5b7bd06', '83b9ad8cb927b206a3b2116f573fafbb89aac8ee', '0124350722', NULL, 1, '2020-09-23 08:45:35', '2020-09-23 08:45:35'),
(488, 'd5dc2cb6b2b6f02b180dadeb8d27611b', 'Khoo Ai Fen', 'afkhoo@ltsgroup.com.my', '88781559282a4c6cf88cc3aeb074bb0b4c42fc2c2e54472bdda690b1b4c8ea50', '59536b29b6188a6d766ef360f6402f672481c09c', '0124816533', NULL, 1, '2020-09-23 08:46:28', '2020-09-23 08:46:28'),
(489, '4359233a8ff9897a6bb474c90664f255', 'tong huey ling', 'hltong701@hotmail.com', '9dc778d8196af82d9638b220b258c50a040eaefe3df1ccd1c4b555d8ea0f3de3', 'e97fd7ee2565621912f8f09f7bd2b5ecc1db4a07', '0174420302', NULL, 1, '2020-09-23 08:47:41', '2020-09-23 08:47:41'),
(490, '9ef2a34e55a8991d611b34367583e66d', 'Tong Jern Yeek', 'tong.jernyeek@ltsgroup.com.my', '10fc4b51717699f65347ee0481f48e95693f2ee783d41a52abc4edd2b9a7c5ba', '8edf8f34f45e228b7a146c407df9a1e30cf151b3', '0194578033', NULL, 1, '2020-09-23 08:53:11', '2020-09-23 08:53:11'),
(491, '4469790410d7f9f7b7eb25b045f71a57', 'Lee Hooi Ling', 'hooiling@ltsgroup.com.my', '73f92008798a64cbf33024c46717bf9a98d82bc27e17ba70320916d5e5db543a', 'f6dda6496d3e1883d30d308827abda9d7c06c7a6', '0194774333', NULL, 1, '2020-09-23 08:55:14', '2020-09-23 08:55:14'),
(492, '309007253a798af8601606cef212db9f', 'Lee Sow Ai', 'salee@ltsgroup.com.my', '0054af35d2385c7485aaccaf0c23ad4d7e9dcd998fc04c9b00fe2bcb729537f0', '949c5e4c56e36003850e13225064337405c8abf1', '0172178033', NULL, 1, '2020-09-23 08:56:54', '2020-09-23 08:56:54'),
(493, '39180fc1805b2efbcde30fde6639d332', 'Hiiweehan', 'Hiiwee33@gmail.com', '49acc23aba6ab6566a73652b199fce77b9ea2cc26173c92a8019d76809b3265d', '84e1e78655c2cbb2af9bb31f086d3434ee41bf9d', '01128959242', NULL, 1, '2020-09-23 08:58:06', '2020-09-23 08:58:06'),
(494, '3d2f4a258bc3feae09ba2b739e18a68d', 'KHOR YEAN YEE', 'khor.yeanyee@ltsgroup.com.my', 'c3b0af95c896d924f562b3d93f5fb48383686a82bf87e03f08303d513597adf0', 'ea94bb81c659f99e02bb69dd732de038a06e8763', '0123145033', NULL, 1, '2020-09-23 08:58:59', '2020-09-23 08:58:59'),
(495, 'f8b8ca957d98608ae0acdd52781763a6', 'NUR ZULLAIKA AINA BINTI ZUL ZAMRAY', 'nur.zullaika@ltsgroup.com.my', '2b38721b16ed799a5aca07fae83db425205b8d8b278215f3c2b9d039180e9925', 'd265bf691fe8f2db63e9cca932f033ad90ce0676', '0134154125', NULL, 1, '2020-09-23 09:00:33', '2020-09-23 09:00:33'),
(496, 'dc89114764f3c6ee92c439c8d4f3a9f0', 'Adam Gopalan Abdullah', 'adamgopalanabdullah@gmail.com', '165658dbcbfecd1a7c49389ce3d0d1c1828b7fa46590fb6c941ae05f4c5f276f', '7804661956efc603f850607e8257c0def57c0365', '0174080906', NULL, 1, '2020-09-23 09:02:14', '2020-09-23 09:02:14'),
(497, '52b53293f14ad91d6154f88fd4a7d6aa', 'Vincent', 'vincent_tys@hotmail.com', '6481880379ce3f4576970f90f5efffc8d6e37279df98a72c79c495a519419e70', '30300a86c99af3b42eb92d7dcc5056fdf77123fd', '01116553260', NULL, 1, '2020-09-23 09:02:27', '2020-09-23 09:02:27'),
(498, '2ede3409fa1fedb1ed9c50647265d053', 'Baidrulhisam B Abd Hamid', 'baidrul@ltsgroup.com.my', '4abb9b2ead7de028654d1ac19c930614b09a6a81fc30f18f7282353a9b98e873', 'ff4f3830400bc43b152d4f6ded635d5a3efd7b38', '0124272333', NULL, 1, '2020-09-23 09:04:53', '2020-09-23 09:04:53'),
(499, 'ebbb128520590fed8461af2a36491c0c', 'Jennessa', 'jennessayong@gmail.com', '1997a01ac4e6ae7339b8e43b84bc802327c6fa2eb2fa9ae082a3bddabcea9a85', '7db522414fe7ea0f1e3661b14d1e9637160db2ad', '0167225354', NULL, 1, '2020-09-23 09:36:45', '2020-09-23 09:36:45'),
(500, 'c71794fefde4e697960b596a80ad7acc', 'Desmond Owe', 'desmond.owewh@gmail.com', '4cde7a53d0e6d1e5897b05ab52486764bea252446c26369cb81715f4d91df01b', '6b5de5dfdc45155b8014b6cb6b4ebee0e9970730', '0126290168', NULL, 1, '2020-09-23 09:38:11', '2020-09-23 09:38:11'),
(501, '7c67df293ae16401471481545c5050c1', 'YAP CHEE CHYONG', 'simonyap3650@gmail.com', '857432c3c1423d3495e166b63f4f6e98fd58b985c9e630b8b5e24a89431752e6', 'b8f8a8b5e0e3c94646e5d7a57224d2cfed164cad', '0164077880', NULL, 1, '2020-09-23 09:48:39', '2020-09-23 09:48:39'),
(502, '8058d2cdf9a77aa478805ae0afd436fb', 'Lim tun chen', 'Tunchen0401@gmail.com', '9b9eaf722d0ea8a56ba9636b826c465376b9b1414319afc4d6c1d66614bcfb06', '5315825bd85e0024ca0bf82cbf3ca92a6fcb8d31', '0162617773', NULL, 1, '2020-09-23 10:36:04', '2020-09-23 10:36:04'),
(503, '26a6ecc4a5f88bb82f4e8d1a49e5c7f1', 'Chen soon chui', '', '3b0e25bb704ca6fa8782b3d44d515df4c8220d83eea1535316a6629b831c6d1a', '676c056f51308b2a652971a9bf79e2affadeced9', '0164086277', NULL, 1, '2020-09-23 11:09:26', '2020-09-23 11:09:26'),
(504, '244d26f7fa33644776a9fadd8945e5ca', 'TAN WEAY VRN', 'tanweayvrn@gmail.com', '1504a6a3f84179b32eb301d9ea9248784c8c695c5498621a2432f5759b760efa', '32fe3f07273a7daf08ddf8cb5b79b94c5c558716', '0125281637', NULL, 1, '2020-09-23 11:28:17', '2020-09-23 11:28:17'),
(505, '7e3ca49fe8bd5b27ce40808126ad9908', 'Yap Hoong Sheng', 'h.sheng6485@hotmail.com', '66ddc30b263365bb1d079b0238d18dc05287235fa474c3dfe45b884d96202d75', 'd97b0018b3c3fc1f44eedfff0bf3ecc4c2298f4f', '0166109187', NULL, 1, '2020-09-23 11:28:26', '2020-09-23 11:28:26'),
(506, 'a148d8d99efebdd70e5ec582c550c7c0', 'Choh Moi Chen', 'mcchoh88@hotmail.com', '84ee1e849d23bce2b2c01d03ff56a442fcfdab083b6f3266271035d4586cc5c7', '414e6a491c314f22ba74ca15178f00eb5dec0bf6', '0172003553', NULL, 1, '2020-09-23 11:28:36', '2020-09-23 11:28:36'),
(507, '6ec14ee8ef47179d7b0dafc02271fb27', 'Ng Wei Chyet', 'Wchyet@gmail.com', '59206c7449752ac4e3195fab3f138b407bc4a1afd7b775713d2bbd3503cfa55a', '05e4a84127bba616769592745f42c821bb6bb817', '0124736291', NULL, 1, '2020-09-23 11:28:45', '2020-09-23 11:28:45'),
(508, 'c74b9e14facc4274da3440b8aa1ca2e5', 'Scott NG', 'scottng94@gmail.com', '39b78bf25ac998f0ff343141a2b41fb352079f844657c8156e776e814abc62d0', 'c5b0d959a82d2ad6dae1b1d279ad8d81d8c90038', '0125620739', NULL, 1, '2020-09-23 11:28:58', '2020-09-23 11:28:58'),
(509, 'f2add45d61dcc7c6c93a250e6d4d4b7c', 'The Phaik Sim', 'psteh@gmail.com', 'ab936749a470577359e7667aefdcac69e7cc5529657d8a51af1bbda47a75350a', 'a87e1b35699c7005f3dce64fa46b6e8f83a3f298', '0124108209', NULL, 1, '2020-09-23 11:29:33', '2020-09-23 11:29:33'),
(510, '4362703ef6db581c84217b1fdf2c8580', 'LEE SHUAN HOE', 'shuanhoe@hotmail.com', '9612afc4b5ed1407068ff4470d5bd9d8d522863e00bb421c58461670558cbfc6', 'c225ccbf47ad79b394aa9b7a791fb51d49cc6698', '0124500748', NULL, 1, '2020-09-23 11:29:41', '2020-09-23 11:29:41'),
(511, 'b6fcd48457d1b7f61a9240ef78e36e57', 'Wesley lim', 'wesley0925@hotmail.com', 'd8de89803fe4a52e4a4cd2fe0ad4d643f37dcf960433666faf6f11e19d5bac05', '499d535a23bcdd937a9499bb615e3215de1f4765', '0135333975', NULL, 1, '2020-09-23 11:29:54', '2020-09-23 11:29:54'),
(512, '256427f3091ec210fd8c8ec50d99e21d', 'Loo Berine', 'berineloo@hotmail.com', '59d1a8031608e54ee3ddcf30283ea7a274f3dd6624529cb4c1878157b0e61e7d', '7a15da9ced552dce6eb836da9b0d0d6eccef3a18', '0124808848', NULL, 1, '2020-09-23 11:30:15', '2020-09-23 11:30:15'),
(513, 'b01e520143935d3688c3b5bfbaef2066', 'Weimin', 'quekweimin@gmail.com', '2d789d3a12254aa37f1407f12402f09a2376c4b2b29b2c6faa6509cf797a5a3f', '04c18a4bd443320736d4d2f065932700de9be062', '0127123639', NULL, 1, '2020-09-23 11:30:33', '2020-09-23 11:30:33'),
(514, '22b60610b919a916f041d4fb42982ec2', 'Eng Ghim Boon', 'boy_boyz168@hotmail.com', '9b2d621f3feb706a6c048db5b189e2f38fcc80866e3ad34e8b708cffd05e7c77', '7865e9e2214c6a363d4d8394d1bdf462087fc839', '0103970663', NULL, 1, '2020-09-23 11:30:37', '2020-09-23 11:30:37'),
(515, '0828ef419ff0e7547957c931488f0deb', 'RYAN', 'tengkim@hotmail.com', '08086d0c4db875a0d9b9055c017805009ddee2640f71430b3dcc5f7920a341a4', 'af1ae960962ad77f1d49d30d84c65cfb6a45dc2d', '0194717516', NULL, 1, '2020-09-23 11:30:56', '2020-09-23 11:30:56'),
(516, 'd448a10e783d954bd6076f8c820708d1', 'Ooi Gim Siew', 'GimSiewOoi24@gmail.com', 'cf0232ee9a10ebe2ba0896bdcdb6ecaaaf5e95c62b0298f7c68a0eb59c715e5e', '36baacfe3b0a0cea8cb3e3a1e11358315f18442a', '0124976648', NULL, 1, '2020-09-23 11:31:19', '2020-09-23 11:31:19'),
(517, 'b0835080882c7938bea6da0b9988527c', 'chee poh chai', 'ivanchee85@outlook.com', '674d52453f24bd22ef8b0bda68da8004cadd9cf0da86582c02a4fa26808316bb', '54ce7193f8507bc30d2ba04a32e865c806897f3a', '0124755013', NULL, 1, '2020-09-23 11:31:22', '2020-09-23 11:31:22'),
(518, '41dbead031d089fb33e06eb43f013d89', 'Chris Tan', 'tpenghow1990@gmail.com', 'edd84c4ec7a1e719e45862cce6a81102dc14d0a2f84e7a1e30343426d79bb977', '43b4731b6f7c20f67159ea97321dfeaf1bc03198', '0126971990', NULL, 1, '2020-09-23 11:31:23', '2020-09-23 11:31:23'),
(519, '85471554a4ab6cdcaeb3afc75486dfe6', 'Chong Yin Chin', 'janechongyc@gmail.com', '714646f0da0cde8b73164bf51c547d49efb48c606d1b744eeff4f68ec88355b5', 'fefb5b6f5a02c83623682292536dbf2c82852535', '0192737618', NULL, 1, '2020-09-23 11:31:24', '2020-09-23 11:31:24'),
(520, '1b19360e73f2e36a962ade3a039f2a6c', 'Cheah Chow Chin', 'cchin1983@hotmail.com', '9f3b7d0067749d33855e13e4484d2b6c6b11e301d96a0c36cde2fa89c0fbb2b1', '17c9cfc642908bc3880c751bfa8ba4d56ae1c7dc', '0125891639', NULL, 1, '2020-09-23 11:31:28', '2020-09-23 11:31:28'),
(521, 'd393c4ca84207e8bde0e000391b6402c', 'NG TIONG HIN', 'ngtionghin@hotmail.com', '97bb31dae1af8ea4db2243bf83c4d7bf970acd0863694f17eae3324441cfa038', '8e57f49b152242eedd4de0c46e9bcb6ca016429c', '0165428320', NULL, 1, '2020-09-23 11:31:36', '2020-09-23 11:31:36'),
(522, '3f5b3ba56e2dda2a8d62cf52d7b571a1', 'Koay Hong vin', 'koayhongvin@gmail.com', '046649f47c9741ec5061bb7ab3312a08802f5cb5ac832c9c12450133e1559f39', 'a8ad9a410122176cfcd0c4d6f9b61d074bbfa112', '0175491349', NULL, 1, '2020-09-23 11:31:52', '2020-09-23 11:31:52'),
(523, 'a305cd58d79f20e55643b2c895b54c11', 'Yumi', 'Yumi_1219@hotmail.com', '8d68cd8e99f79118a6609d0b682a69a0e29d96a90a71b6d5125f4c8ac0c1ac5a', 'fd72905452407e5b8974a9038c11a37f282c4c15', '0182683265', NULL, 1, '2020-09-23 11:31:53', '2020-09-23 11:31:53'),
(524, '38a5f2cb40e8b6415aca12cef192e413', 'Ho chee sheng', 'hocsheng@gmail.com', 'c0fb9f583886b7f28844f563d8011860513b39fc41ed03c2bf551147eb883245', 'bcdaade0f63731016033d8b6381f567b488d9e32', '0124408005', NULL, 1, '2020-09-23 11:32:01', '2020-09-23 11:32:01'),
(525, 'cb37763205e3c87f654fcd803312624d', 'RICHARD HO', 'RichardHo@hotmail.com', '3fe607252b4a7f7c64ddb37aaf3fba48e6217efe9722a431e4168e382e3e889c', 'fcdbc1b31f5b47590f607fe2cb14465ad5058e32', '0199506808', NULL, 1, '2020-09-23 11:32:04', '2020-09-23 11:32:04'),
(526, '98e2076d8472fbfe0587347ffc0ab2b3', 'TOH HONG MING', 'tohhm99tt@gmail.com', '378d5e66bef622c22150a6d980afbbcd2dc8a42ec5895eda9327d7e36de5fbeb', 'b6372c6f792787a62ef0a58011a76af3a43e15d1', '01112815683', NULL, 1, '2020-09-23 11:32:11', '2020-09-23 11:32:11'),
(527, '745c6a62710c07756a4c3debf3ba7b95', 'Elisa Tye', 'elisatye@hotmail.com', '0c7c52a0e5f9d50d34771ee0c459cef5ca312d9e837ab737b0174707c658c145', '5bc401b147089563ed9f02549c703aaab15ecc1e', '0194044263', NULL, 1, '2020-09-23 11:32:16', '2020-09-23 11:32:16'),
(528, '74113584ccf500a21cb34619c10fe729', 'Ooi Pei Wen', 'f1wen88@hotmail.com', 'deec1933a2cfa17bfb521d60cf6960a7ff51d9b60e050dd9472b067ff7e3a13e', '508f6495e2e1b37c1ad75f5b16d101a8c9efab4e', '0174646031', NULL, 1, '2020-09-23 11:32:30', '2020-09-23 11:32:30'),
(529, '82029f3ef5b0f39ebbbf433234210a4f', 'limkarchew', 'chewlim1989@icloud.com', 'c1fe3428efeaf9405ee5f1b6d1ecf258add7ee9dd0dcccbef9931165fed50c18', 'b8c4290a391b632b491d0bd48cb6f23306c9fc02', '0125077417', NULL, 1, '2020-09-23 11:32:55', '2020-09-23 11:32:55'),
(530, 'c9ce3a96cfca09846fa9338972ed03d8', 'Tan Pei Neng', 'tanpeineng2005@gmail.com', 'af9a8201c1c1a7a6af491dba7e9d42d8efc1f943a111a485554260b7561118c6', '68fa7e4c61ae2c38c55800f3e71e03e6a5ddc513', '0168548869', NULL, 1, '2020-09-23 11:32:59', '2020-09-23 11:32:59'),
(531, '7377a48ed24662b6404fde5357c53682', 'KHOR WEI ZHER', '', '30cd13967d568afb62e551c45ce8a8e075d9d73e9008de2114771a014288f692', '7e332987faa3b91ccd6feb46081156029679d381', '01169351731', NULL, 1, '2020-09-23 11:33:10', '2020-09-23 11:33:10'),
(532, '34f572e9a6fd4f0b55cc31f1c834140b', 'Samuel Ng', 'snwy95@gmail.com', '6b17f055f233b275349e57c2220a55403df030e367365a1a9ff5fb6beed44fcd', '9fd6ed3e4ec0817e0e27cfd3d982361c5dc1bddc', '0173685280', NULL, 1, '2020-09-23 11:33:10', '2020-09-23 11:33:10'),
(533, 'f36e5675fec958a9af5a762cbe5b78c6', 'Cheah dini', 'cheahdini19@gmail.com', 'b04ff6307272ddc31d02a999a18018cdde65b689af2b6e32e65ff5b702522d1f', 'ad6ee1a79087340660a4d165037701ec74189f32', '0174183258', NULL, 1, '2020-09-23 11:33:22', '2020-09-23 11:33:22'),
(534, 'f081c2053ee68533972fe5b7594d6115', 'Koon Leck Chen', 'limpepyang@gmail.com', 'cf849a39e73e466fa430ab269eb56ff9cbea2694b85ed3372489576ebebdff86', '57277b0797b6acaa70c79cf273129175d326e86a', '0165800191', NULL, 1, '2020-09-23 11:33:34', '2020-09-23 11:33:34'),
(535, '658ec2139ff94b2f917c4fa3f4fa900c', 'XIAO JIE', 'chunjie80@yahoo.com', 'a528a1243a9388623019b67cf6e0b0d95cdeb50f323926190b6da26e02e3cc1f', '418661e02e09c4b3956d738cca3ea130ae91f8fb', '0182235283', NULL, 1, '2020-09-23 11:33:47', '2020-09-23 11:33:47'),
(536, 'dc8c4130d0847e094e3f47e19fc946ba', 'Hew Zhong Lee', 'zhongleehew@gmail.com', '3706ba163435df3f9a1d01aaefd19150b06f2c3480651101a460361707f48899', '5932ed456b4128790ad5e401d8361c48e330d01f', '0182265228', NULL, 1, '2020-09-23 11:33:49', '2020-09-23 11:33:49'),
(537, '34799ca021bd3f70c12f9515369c6083', 'GONG YEE CHING', 'gd1231b@gmail.com', '58099cdbe5b2a4b15e1cb5f1ef842cb550ebf38c2c54abe78ebc95bb2f07e55a', '23f3b1a7b7c7ff1c6e4125b5819477918e655db8', '0164146657', NULL, 1, '2020-09-23 11:33:54', '2020-09-23 11:33:54'),
(538, '13728731ecc5ece557cca6d3d6c14d95', 'Ong Keng Leong', 'ongkengleong@gmail.com', '165361fed45dcbe53ce505d05cbae12d9ebba060bcb8c1f134959fe80ae25822', 'd685f38be205ba9adfb3ce21f6483f355c4cfa24', '0125868325', NULL, 1, '2020-09-23 11:34:30', '2020-09-23 11:34:30'),
(539, 'c50dfd332e975b450a39a41a209b80b8', 'Angie Ng', 'cycy_n@yahoo.vom', 'd076284adc46a75f542a9fdac21e70bb5e06af5941f107ecb66d1ef282e3a1f1', '395cb5b1de38b2a107a6edd9c8831beb0c084d11', '0125934891', NULL, 1, '2020-09-23 11:34:36', '2020-09-23 11:34:36'),
(540, '8279c1fb332802f946947b3247c8322c', 'Chen Seok Ling', 'seokling1234@gmail.com', '1774958cd3282d62cb0ae41e52e58df1d55e091c27d5d8f0446bc3f492bcd9de', '4936234935b98589c411f2997791ea3faf611d42', '0125223480', NULL, 1, '2020-09-23 11:34:43', '2020-09-23 11:34:43'),
(541, '2fba4bbe640d4b52c0152bca40a4cd5e', 'Zikai Ang', 'zikaiang19970810@hotmail.com', '367ae6425f59e0bd2a9bf42cee7bfb5fbc7907d5fe28b27a97b45e45b7ba5fd1', '44d30a10792230b68f3aa02e85f71e398d994ef3', '0167070873', NULL, 1, '2020-09-23 11:35:46', '2020-09-23 11:35:46'),
(542, 'd528ecd3326e9d0fc770d3a00830f5ef', 'Khaw Choon Cheng', 'cchengkhaw@crownww.com', '736178eaf1f333d92d1377ee2ce8689deb5d22343179d3a901c98967e0025b65', '28a5a17eb62fef238a78b915a428d6a17507d3a7', '0164229723', NULL, 1, '2020-09-23 11:36:01', '2020-09-23 11:36:01'),
(543, 'fd7008e38b716d26ec362e6ca1b9c882', 'SIM LI NA', 'tangyunyi@gmail.com', '6cef23d4b6d7ff6b5cab431f7ca32f9d2ce8bdf0ae346f4f855fee698be95d3e', 'b11e96ad332f0988e573ff228511455cda1b10e5', '0125542551', NULL, 1, '2020-09-23 11:36:32', '2020-09-23 11:36:32'),
(544, 'f2f1c799411af71b969400e5c45e4f5f', 'Lim Ley kok', 'leykok_0919@hotmail.com', '2dd923aa05d630e8499a0cb30feb7e9b5657304a7b009b6053697ed17797812b', '2647e82ac4e9895d912de10657dc3cbc5c4c3fa5', '0164030987', NULL, 1, '2020-09-23 11:36:42', '2020-09-23 11:36:42'),
(545, '7da7770f2cf549f875b9901d3ef06580', 'THERESA', 'theresayong1818@gmail.com', 'd40b0cd671e73b3c3f3b09841c2918b2ae191d9ef9403e20347cc2440029dd9e', 'f51c4c21843bad1bbd922d8d79093b8260e0687a', '0124215380', NULL, 1, '2020-09-23 11:36:59', '2020-09-23 11:36:59'),
(546, '7bf856161ae960f8265cc12c7f2aa3e3', 'Wong yun dai', 'yundai99@hotmail.com', '3f31058c79d48ba9c32fcfcbf1fd1255c41b3979f7d515212fa7ebd0858037bf', '745a7c0bad12f5f89675fb23c3bca943ee270dac', '0198691803', NULL, 1, '2020-09-23 11:37:29', '2020-09-23 11:37:29'),
(547, 'ad0c26b56948e557ba331c096bc3bf3f', 'ivy', 'ivylim6391@gmail.com', '5f2a1ae1c7b5bdd62aa911449f1fa397e3d00f116ec3bcfadb1af8bc627d64cc', 'ca39e33c4970a1dd5d5db3aae0bde2a985a3c4eb', '0162252134', NULL, 1, '2020-09-23 11:37:42', '2020-09-23 11:37:42'),
(548, 'f0c17e385cf50a0eab0433b79e61cdea', 'Leo Foong', 'leo_6301@hotmail.com', '89d543ba50f6565cf3ff215555dc0da609d557f9bcb8b57c57b579100499be7e', '1256c48659943d68e7623e22b072330f865896ad', '0175432593', NULL, 1, '2020-09-23 11:37:57', '2020-09-23 11:37:57'),
(549, '820f0767d12595c1a37c4acfa02a9144', 'Frankie Tan', 'freemankie79@gmail.com', '9186d5adfcc2b4146ccf4e7dabbc47d0367ee5275012b80a3361c3c653f92209', 'c1b3536109b3a3c64eb132378bcdaa33ab1ec295', '0124941669', NULL, 1, '2020-09-23 11:38:01', '2020-09-23 11:38:01'),
(550, '08c936a8fe751ed72a6cdf2938f234de', 'CJ', 'happylike973@gmail.com', '8774ea98eac96d5857f26dd3349c918b66612f5a778051f5deee4a513e78e8be', '4682060599efb19cb5ad9449c57158912959272d', '0198931478', NULL, 1, '2020-09-23 11:38:30', '2020-09-23 11:38:30'),
(551, '95c382f18e741de23d033eb1b012d0ca', 'Aou Tze Yang', 'aty__87@hotmail.com', 'fb30d1ca7f9f765866f8db944cb5cdbe90b51a592d0324db41b822e5fc00c7ef', 'b8cfb9bc038198ec595585c65a51b1fb9c72b47f', '0164686387', NULL, 1, '2020-09-23 11:38:39', '2020-09-23 11:38:39'),
(552, '58f764678447c72de8b834e26c968229', 'Chu Wen Sen', 'wensen_chu@hotmail.com', '2b37b8830158290286bced15e798dc5da159f65fed81ea02b37209daeced7c75', 'e95bd33f03534c9ea88f09393ce7a0ac3572d5f3', '0124579020', NULL, 1, '2020-09-23 11:38:41', '2020-09-23 11:38:41'),
(553, '046a0e4fa4f3356ee32108ed5e709ff2', 'Kelly', 'ling_1242@hotmail.com', '9ce5582c13f75c1e689baa54a6bdc35fd21e0b92f629162d7c8b5aa2772bbcce', '3890ff37be2c7317b3b6a8f521ff0fed3c8ea827', '0184606485', NULL, 1, '2020-09-23 11:39:18', '2020-09-23 11:39:18'),
(554, 'a4bbc19e80b9c837ac0bc2263d4c59ed', 'Kang fang mooi', 'fangmooi1@hotmail.com', 'e43b512645e5695cf236dc4325bde46453f3a70dfdd4e09bdedfbd6c159d9d5b', '57fdfeb2c5d161b4ee83ae920f8b0f5111f9275f', '0124943428', NULL, 1, '2020-09-23 11:39:31', '2020-09-23 11:39:31'),
(555, 'a2ecee3c18c2e6ef239bed0a8fc7a2b0', 'Jx', 'jiaxin5199@hotmail.com', '99ca0e8189c24691cf2a425124ae83498609999e5c7da6e21c2ac41c79ef8d4a', 'd7f9fc9562eb876bff79413999c965efe39fa967', '0125774660', NULL, 1, '2020-09-23 11:40:46', '2020-09-23 11:40:46'),
(556, '4cde87e23cb157653b63018ac9c630ca', 'Yeoh Su Ming', 'yeoh_@hotmail.com', '70fa9ab4c128e8fa93d7e1fea2de4191cece18cc0be2937f8dabcd487b74c11c', 'bd628f7e5a78f1864b42fd72c247d73f253d737f', '0164557861', NULL, 1, '2020-09-23 11:40:58', '2020-09-23 11:40:58'),
(557, '3f227e37feb4b82740516fc1df000c47', 'Chua chai hong', 'dac6282@yahoo.com', 'a0792a7ad4e94138f0aff2f33ba97bf2c5783b4a206e59faa6d52f06bafd5eb8', 'eb3caaf5309f3f0be4bcdfd35bbd20002046c265', '0124726082', NULL, 1, '2020-09-23 11:41:02', '2020-09-23 11:41:02'),
(558, '785bb047683779643ec64c7ef050a53b', 'Har Khai Chung', 'Krayeon93@gmail.com', 'a14fea165b941ffea727b01a3adccd05d02ad8dcc9af88829a3eb6e8809ea25d', '8e4e073665b08c9d787e5fca1f6f0bf1d0ce2cb2', '0182520100', NULL, 1, '2020-09-23 11:41:04', '2020-09-23 11:41:04'),
(559, '9a4e93959701444ebb9601991a0f1f28', 'Lim Chin Chwan', 'Shereenlim@yahoo.com', '6dc6ae8769f6088f9d2c8cb7bb3e5d8a5b76c200456a3d1273bfef4f94660cfa', '1adc0dcdd51d9532636d7ac73d9dd549d3acef49', '0124956339', NULL, 1, '2020-09-23 11:41:21', '2020-09-23 11:41:21'),
(560, '2c85340ca4ff52fc1c8f76843b552dd1', 'anders', 'gopkm2323@gmail.com', '1b13f44b678f9fd308c72432f06f81a2dcff1ad2fb35e34e535889bc91b6ccc7', 'ab848d9a54695c4f1c40d44e523670acdd74a996', '0165051053', NULL, 1, '2020-09-23 11:41:29', '2020-09-23 11:41:29'),
(561, '2d9297cb6bbdd5ae84e6f1dcb99c436d', 'Chris TAN', 'chris_tck@hotmail.com', 'e4dcf1a275bb9a06d2257991000dcba4ede3fe5a4a27eb1baf878dda4d391c23', '465a91f8765447d40bef9bdb1361c0a4e6b0f58d', '0124097115', NULL, 1, '2020-09-23 11:41:52', '2020-09-23 11:41:52'),
(562, '02aeeefee66df7b3a6de95212e51d64a', 'Wee Feet Lee', 'pse.nkc@gmail.com', '2cb168ed96161dc9c75756c12cc11b553e0672e1f6ab849f188e1cbf88a0be48', '91c0b8080c6cf8fcf041fa5ec3da12d347ac2938', '0162638696', NULL, 1, '2020-09-23 11:42:00', '2020-09-23 11:42:00'),
(563, 'e4a80552461f29dcfec1e811a280be5b', 'SteffiBoon', 'steffiboon@gmail.com', '8e9bf0ffbb013fe3798f18e50b887798a3cec0e960eb70c26138059b19319ce4', '866e90576e2b8ec98a37a59c57623d4f89c7b82b', '0135882428', NULL, 1, '2020-09-23 11:42:42', '2020-09-23 11:42:42'),
(564, '07cce3ec41c83b07fa052c24a25fc66e', 'CHAI CHANG XIAN', 'changxian.chai@hotmail.com', '918f88ce9fe06d90510177e560fb8a39d10b3dbfda97f00f1ef0f260d7f2370c', '62d13dd77a31641b8531693623b395f239b8cedf', '0147048793', NULL, 1, '2020-09-23 11:43:31', '2020-09-23 11:43:31'),
(565, 'fbe48b6bc550f7549457d94f7425fffb', 'Nicole Lee', 'sindylee08@hotmail.com', '9e6619aaef460d5466bbcca020a70a74323b1205d842ae6050a929a0314ab7fc', '9a31d3db14c2f262f833a8db61cf8be58dc5ed18', '0176850086', NULL, 1, '2020-09-23 11:43:35', '2020-09-23 11:43:35'),
(566, '991efc432c64ea07ead8568cbc079ac1', 'KANG WEI KOK', 'Reo8378@gmail.com', '4f533211f371dd15cfc4aa66d06547adf51b00374cd85ddad8dbe787f084c099', '4882ef6a31bc7020aa57bd6d2aa617f266a1d899', '0174093788', NULL, 1, '2020-09-23 11:43:37', '2020-09-23 11:43:37'),
(567, '0bcee27007e0e5d34729e76183f9ace5', 'Tee Chee hoong', 'teecheehoong@yahoo.com', 'fd860ba5b235b91b293dbc708b0daed8ee821d934c2ea849e70828a6fff6ff46', '3d840274bc587e2703dea29828828b25b4a83805', '0174148265', NULL, 1, '2020-09-23 11:44:02', '2020-09-23 11:44:02'),
(568, '36da1d9d30998a262352af3a7f8b6fcf', 'SS Tam', 'sstam@nuracorp.com.my', '9de5d50cdae4564cfdbf8682ca3015434dbb69308032c63389e201c2a5360f5c', '84af2ad847f5bfe6cb9f4bce2589b1e5495aaa3b', '0122199938', NULL, 1, '2020-09-23 11:44:15', '2020-09-23 11:44:15'),
(569, '076740965c6b83dfd877a6d6117d70e7', 'Loh Yi Xuan', 'lohlohyixuan@gmail.com', 'ab0051e7832c54bab263c529900fe5af4822c03b80c0fd2c31c219cdeca5a2f7', '4e152ae5f19e2cac48f1354a150989af0370f4cc', '0129246723', NULL, 1, '2020-09-23 11:44:32', '2020-09-23 11:44:32'),
(570, '0d6d74628ff2db99150a9ecb591f8d1d', 'THUM CHEE WEI', 'Cwthum1024@gmail.com', 'a297cbe798190b4fa9d5bf85a2a1bb60732abc0dff9edf542df0142e659e9025', 'ff1f2bd9c9a770f53d0d8b30e2dc1b67cb021593', '0125480313', NULL, 1, '2020-09-23 11:44:35', '2020-09-23 11:44:35'),
(571, '3870bf5a9443d4e97b1a303ddc6c977f', 'Tiu fung ling', 'looling82@gmail.com', '08e3c48888d24c5690aa7609a0fcb6b5a69d1e3fc7839162aba0554538f0fe56', 'a8e88f7677253346229a0fc4923cf3bf55fcb67f', '0109311961', NULL, 1, '2020-09-23 11:44:47', '2020-09-23 11:44:47'),
(572, '0ee3c8f2a4366f048b5e29840dc36a6b', 'Bryan Ang', 'bryan4148@gmail.com', '700420988bfe51afc1fb2a40edb015ec8eccb55e68fd0dfdf1a88f918c15a589', '6a6c30e56b81f6aa9e04d70a10eff03d26578734', '0174148017', NULL, 1, '2020-09-23 11:45:06', '2020-09-23 11:45:06');
INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(573, 'd8021dfc24598da45a97a9af7fb27bf8', 'Ching Kai Jung', 'kjching5@gmail.com', 'ce65d3b29ccfd3354d725e91a80039ada67f6df389fe866f06a746bee2049f83', '0fb884b8b3ff1bad4e73a33f8132bc3e0ac2010e', '0175910383', NULL, 1, '2020-09-23 11:45:30', '2020-09-23 11:45:30'),
(574, 'fe2dc829df1933c58511312e9258cd93', 'Leong Wai Kin', 'mrnobody534@gmail.com', '8b3cf37cca0030185b72ffc99002cdeae40e37500e31130a977d44707e832257', '01279e1348cdbb6a5de8bbd9aea3276131cf9d6d', '0103904865', NULL, 1, '2020-09-23 11:45:32', '2020-09-23 11:45:32'),
(575, '0805f41851b7164a86f66c8b1508f114', 'Chua Kah ming', 'sharine_chua@yahoo.com', '75085e49b01cfaf14ebc344fad37a5b87e8c4bd5ee6b5529822c0d2bb8497e39', '9b559e3d294452e288ce4e3599a89f7257716902', '0129830230', NULL, 1, '2020-09-23 11:45:35', '2020-09-23 11:45:35'),
(576, '83bbeb52ccc985a280c2fc3cc051dc6b', 'Tang Chang ji', 'Changji_1992@hotmail.com', 'ec1f96c56eb0b8a6cf6754cd1d2fab879cf57b5eb5ec370ab0e029f95120d0fa', '08b8343781353bfecc709514569c94d335a94ac7', '0174487978', NULL, 1, '2020-09-23 11:45:44', '2020-09-23 11:45:44'),
(577, 'dffa1653ce7b27e88a23b62aa54bbbcd', 'Joshua', 'Joshua 1226w@gmail.com', 'b05af9a24ef9642fd08b876259e581b4571178a083d411f08c437bf8fcc6e37f', '9e91d64b32442e57d9cd9f686eb26829073a038d', '1226', NULL, 1, '2020-09-23 11:45:51', '2020-09-23 11:45:51'),
(578, '28bcda7afe8c0987c29791adbcd8f87b', 'Tan yen thing', 'Yenthingtan@yahoo.com', '882c11946df9d0e81d01a4948a920831f69122817808d9e5f8673609b5b0be4c', '6e1f9b497b473e70923185714d1c413af2344ebb', '0124956491', NULL, 1, '2020-09-23 11:46:08', '2020-09-23 11:46:08'),
(579, '2426561d8cdf18a2e15911defc95c0f0', 'Mandy Tan', 'mandy_tan61@hotmail.com', '3405961315f70e05cfd9f7c4b9ab46f7695612efa65dac2f2359bd34d771d360', '57389de1fab86714906a596b645a7b941a44e13e', '0103899808', NULL, 1, '2020-09-23 11:46:13', '2020-09-23 11:46:13'),
(580, '5b74b4eca6d90188f343c616d6596554', 'Lee kok wai', 'kokwailee.stellar@gmail.com', '0ab46bfff2789e7a0d9d4285eb164dcb53a990e71cba4845b3725065e2468491', '7518fc8b2f96c98c19448d8435f0fed4c962d6f0', '0177486198', NULL, 1, '2020-09-23 11:46:20', '2020-09-23 11:46:20'),
(581, '57cc51bb4afec0d56c3d2a9c4e3e26fb', 'Alice', 'saupeng1006ng@gmail.com', '753f6c5fb3911f3e2e4a7d078790c87ab2c676b9d981a6e52bdcbca9327d5c88', '3d3bee702b7a9ac6eeb9d309b431a52788585102', '0177229038', NULL, 1, '2020-09-23 11:46:41', '2020-09-23 11:46:41'),
(582, 'd69e0d4e88c0ae6d407e2bb05abb3b29', 'Lee Yean Hian', 'hannlee11@hotmail.com', '59c14d31242f3f909e401c40b117f5fe25b041ef62cd029e26f66f366118cc96', '87ab23b9e545a8e94535fa3c7882b68331c9d282', '0174885458', NULL, 1, '2020-09-23 11:46:43', '2020-09-23 11:46:43'),
(583, '69184d884ca118ccdf93d8a77205bfc2', 'Lim Li Qing', 'quennie-gdluck@live.com', 'aec4d4154bfc2bc639a6cdb82205842244ac150b1cc5142b18da9cb788e7745c', '763b4c3cc0a333ff2b58a090e7be679375c75f4d', '01120007318', NULL, 1, '2020-09-23 11:47:03', '2020-09-23 11:47:03'),
(584, '068a07bbf76eac358a91e4e4636800fe', 'Woo wai mei', 'ymeiw13@gmail.com', '2ae93e85f5be62b902d070362e677100862a0086b4cb58d6fae7db250a3f2adb', 'f676ecf54fcf5cde735bd94a935d6d51ff055b92', '0126491818', NULL, 1, '2020-09-23 11:47:44', '2020-09-23 11:47:44'),
(585, 'eda5cac7f5ee3c22289b77d092354df8', 'Tan cheap lee', 'Cltan388@yahoo.com', 'aa9d6c3241601e37e2d808d31560b1548c27cad9ce28c438bcc3ef713e1c862f', '1f7b0bf26f0eedeb2be495ff09ad105723e6362c', '0194472912', NULL, 1, '2020-09-23 11:48:04', '2020-09-23 11:48:04'),
(586, 'b756b483f1be6cbc60ac12c0f7d1db06', 'Wong peik fong', 'mikiwong86620@gmail.com', 'f0f708cca81b318eb912bab5cba8b0bc1e5b79d7f121cae9a240365e51b35a32', '3bf93f020cecbfdeb7502f13da966c154d7e20cb', '0165122219', NULL, 1, '2020-09-23 11:48:15', '2020-09-23 11:48:15'),
(587, '7d9c57c53d22c307083485b68b9372da', 'lau', 'stevedom927@hotmail.com', '47ccae2eb98a5f41ea25af72554f492abf81e89e7e218d9d5831f1b199b0fe55', 'ccbd7865824a012f2c811a63b724f6ca360b1114', '0125670921', NULL, 1, '2020-09-23 11:49:20', '2020-09-23 11:49:20'),
(588, '99e33e60834091e27f722bfc9f72f37e', 'Chim Wen Hong', 'chim_wh119@yahoo.com', 'd05a1f4767e8ee68ca53be33edb62974b22c957f186f7a4ca543679c367d7a51', '146d0c1b0049db719f477ea7229b1ee7c8fb2b4e', '0164839788', NULL, 1, '2020-09-23 11:49:43', '2020-09-23 11:49:43'),
(589, 'f601c86f06519508cc1fd4608087e9cb', 'Tan Sho Pin', 'evietan417@gmail.com', '319b3b0634eabf5cfb1f007af3c24921290c0a339f6caf76bbdfc545d66e3838', '79380f9e4a0063fe8282c696dd53857a9136ee45', '0174179308', NULL, 1, '2020-09-23 11:49:50', '2020-09-23 11:49:50'),
(590, 'f3ddc28d266cdbd9576e86d76f8d11df', 'Ong chin pao', 'sally45401@hotmail.com', '57a2ce87518bd0c95db3cd89c7b08c7c15055f30783ebed1a600f82a5ea723d5', '0d5c82664d3cfcca5aa16ac4205dbe3e8e15fbcf', '01110692231', NULL, 1, '2020-09-23 11:50:41', '2020-09-23 11:50:41'),
(591, 'aa6b59ce5ac0fe2112e1b8782c7f8d9e', 'Soh lai heng', 'soh5528@gmail.com', '936467600f49d8a5e00f0046863b4e95649bbd5162b64560a709090df77f3d2b', 'f5b7748c55366d505b1cfd26a2f3ea936dda4f6c', '0124266889', NULL, 1, '2020-09-23 11:50:42', '2020-09-23 11:50:42'),
(592, '14cf2ed82c1a1706380e6f697a66281e', 'Chong Yu swam', 'goodswam123@outlook.com', 'dd364ebcdb7351d854c531830b072efaff9064fc0f010dfb813ff02b909ee90a', 'dfbf9b70f1c0d75941347c3f2a40c57b9cbdb7ab', '0102498672', NULL, 1, '2020-09-23 11:50:44', '2020-09-23 11:50:44'),
(593, '40d3307617b61b5182f687ef47a8a297', 'Bryan', 'Bryan0424@hotmail.com', '88ad696a99b0e1d12e1f0aff91a210e3dc8934faba3513f203be8be93d60d2f6', 'f0abb7f44565b62e006c00fbfa5957fef3e925d5', '0179801772', NULL, 1, '2020-09-23 11:50:44', '2020-09-23 11:50:44'),
(594, '1c4468bafc6a39e09a53c05b5f3e69ed', 'Yap Teng seng', 'Ytengseng@gmail.com', '0b7a80f0d317006511d187465bdeffd6cc0587f9d0819a4750c50e26cfb9f407', 'f87af72d961e6fe362ff665922ded2498a9b6d54', '0167112818', NULL, 1, '2020-09-23 11:51:07', '2020-09-23 11:51:07'),
(595, '161173e5754bad2c404d132d78e94b71', 'Tan li ying', 'liying0125@yahoo.com.my', '3dcb7385e46d6031c2c7904f6fed3a024eef1bab5a468b42a5cf40a0c955682a', '9e68b86fb91d218828b0e9682eb29b1716d0c31c', '0194998313', NULL, 1, '2020-09-23 11:52:06', '2020-09-23 11:52:06'),
(596, '66384c318663f6bbbeed2f43c52255d6', 'Tan Chee Kuen', 'jamietan0711@gmail.com', '61df302d15af0d3098229dacf0c1c8a2ae63077b1eb8ea6c6b8961b80d7e2bfa', '323c0765d7e1fc264dc48052d0c8373bf2719fb9', '0126196567', NULL, 1, '2020-09-23 11:52:31', '2020-09-23 11:52:31'),
(597, 'af76719fa5f6e769663bde9cc7183ffe', 'Tong oon tat', 'tongoontat70@gmail.com', '324b2bb221d48cf50adb0bc9712a375eb31520453f4f42234eeb51c8164d35ec', 'e7a9048e35098af2558f2a30a8c74377cc083151', '0164593783', NULL, 1, '2020-09-23 11:53:02', '2020-09-23 11:53:02'),
(598, '40d252865b2ad043f42c52d6ab1a9414', 'Tan Hin Khai', 'ansontan93@hotmail.com', '643aaa16feb4ea7b5c67478655d7e095cdec04114df48022b1b1f66c817a64e4', 'daab5158f19d68a133f718c6d11c473bd1842341', '0164428847', NULL, 1, '2020-09-23 11:53:07', '2020-09-23 11:53:07'),
(599, '991507ce911b65ac3df112859a5e472a', 'simweijie', 'jacksim1818@gmail.com', '78c1f407068c626c166c1e50df678dd30f4b7758426709d8a77525ecb834e027', 'ea54b920530a3d9cd9408e5d8469c61b54404af9', '01112447318', NULL, 1, '2020-09-23 11:53:27', '2020-09-23 11:53:27'),
(600, 'b594cc05071ee634800e9cf7d023a306', 'Danny', 'torres456@gmail.com', 'f1501f10fdc89a5b4c2bd0c707c0434f7087c0bf7d4172e45ce10828483d2d28', '7912121bbfa15497a353aed8cebffea18efafbf9', '0143017694', NULL, 1, '2020-09-23 11:53:38', '2020-09-23 11:53:38'),
(601, '2c6c0d00c4e87218b302c2ddba99ed7f', 'Cheang', 'cskipc@yahoo.com.sg', 'e038b5bf3251cda2c8e5e2f5beb62746e54b7aba959a892ef083947343f96f12', 'c631910d94f0bfc97b16208ef3bc5eaa00493320', '0162088586', NULL, 1, '2020-09-23 11:53:41', '2020-09-23 11:53:41'),
(602, '5dac1ec380c084c5a4a590724c964dda', 'Tan kar chong', 'alextankarchong@gmail.com', '6f6ed853f2664de4644345907ed1bd104153f3873d6a87c2125b9d363b2ad9b6', '5aaf534766a08cf9932bf07ffff05aaa6d3f54ea', '0164215416', NULL, 1, '2020-09-23 11:54:39', '2020-09-23 11:54:39'),
(603, '70d9e0c4ecbf86a25ee5c2980875a3fa', 'Boh Seok Yun', 'yunaboh92@gmail.com', '6603941264f01fdbf87ccaaccec45787a7a9653d7c380a94340f2e69158dea57', '860e61c4a17ecdb1220cbba31228f723f1fbb7b1', '0105626189', NULL, 1, '2020-09-23 11:54:43', '2020-09-23 11:54:43'),
(604, '9738ab269f264b620e048eda6a9c2b5e', 'Aifang', 'jchris2210@gmail.com', 'a29f926bf6f08aecca55e1f18d28c9c0901e9a9706c9b97a11a9ba965f5c67e7', 'fe8b97160101dd498d53478678b7f4949989676b', '0164075717', NULL, 1, '2020-09-23 11:54:51', '2020-09-23 11:54:51'),
(605, '1d5fed64082509ddef477efb319352d0', 'anna goh', 'Shumaygoh@gmail.com', 'fdc0cb06ad082bb87ee2561410c62813207c93dcd2318ad1fea6563ab71f60ab', '698375191484e462c3cfd826ae56b4efd14de7cf', '0105055800', NULL, 1, '2020-09-23 11:55:24', '2020-09-23 11:55:24'),
(606, '1bb589d3d98df02f5cd2c63510d4e66b', 'Alvin goh', 'gohalvin820@gmail.com', '4f0fd436972722408d39bd82e98be0e72fbe7cd46abc54234ab98a92a808f5fb', '1a061a329efc99e51a8b772af04e88417aaf8a95', '0175580409', NULL, 1, '2020-09-23 11:55:28', '2020-09-23 11:55:28'),
(607, '3ee28f656c1a10709f900963f1ee9a09', 'Lim Zi Hui', 'zihui2424@gmail.com', '057bd19d403635e7640398528ae5f1e4b407da909f45ed5f20a8795037365b14', '036ca9f69595c9899a43e09992a43ef0926d16d4', '0164931193', NULL, 1, '2020-09-23 11:55:43', '2020-09-23 11:55:43'),
(608, '2a756f728f36cd5355ac68daa8250bca', 'Teh Eng Meng', 'mengengteh@hotmail.com', 'ba10a5c6def8eb55c29c24d0c05551dc6c557bfa77512b9009177b461576d92d', 'b1e0820eccc74ffa087fa4b560e0725ec1f2db99', '0177001399', NULL, 1, '2020-09-23 11:56:01', '2020-09-23 11:56:01'),
(609, '81495092c3f8883999d448ed712eef91', 'alex teoh', 'alexng0110@gmail.com', '7db603b0304253258db1be1d749b3342ade72b442a588d339caef32482121c7e', '770370f13887a881f8bea69dd55ae6e3219c164e', '0103690061', NULL, 1, '2020-09-23 11:56:10', '2020-09-23 11:56:10'),
(610, '81a77cdb1461296473b9152d1b084b90', 'Choo yun ding', 'chooyunding@gmail.com', '45927d4f9cc1496a3b1c9aab70cfd740664fbbd73b236f4e0b06d2c92ba27d0c', '5a4f5f8f87b8723d8a57ca6bab507ac5dadaef78', '01136312023', NULL, 1, '2020-09-23 11:56:37', '2020-09-23 11:56:37'),
(611, '6de1a64f50837603ccd6da8dfc8ac149', 'Steven Lim', 'stv.cslim@gmail.com', '79360a2b744795e8ca7b75712abd5492a94b1e5a7a2b34c5e8581333dae978c0', 'fa1a5825c540e5a1390533331af97544f75761bf', '0164457640', NULL, 1, '2020-09-23 11:56:42', '2020-09-23 11:56:42'),
(612, '85936a7225d7fcd4bd07a5658d646457', 'HON JIN KANG', 'raymondkang0068@gmail.com', '91ce963717436cdb9d08fdadd055a0f4b728fbfd485c7bf29558faba19fe4b5e', 'bed38fe2678bd31772946dc9aad66641b201eb33', '0137309360', NULL, 1, '2020-09-23 11:56:44', '2020-09-23 11:56:44'),
(613, '58d7a45323761f49246f69e4b239883c', 'Janice Wan', 'janice.htg@hotmail.com', '866a549b18da5148cf8f3e625f02c914885a7ba7d7cf534f9b503656841f7f21', 'a7ef5d5c6dbf0d86d0cc72698dcdd4d7e470df78', '0166444418', NULL, 1, '2020-09-23 11:57:01', '2020-09-23 11:57:01'),
(614, '7ce5b0b04fb6e1e23731e2849c334439', 'Wong AC', 'wongusm@gmail.com', 'c0c94ed4e7ef2d9f44f74b45cd40ad7a2da5856c03b32b814734e3abdee63117', '6621bb78b2dd98f4a13860d25b26e9191d359e18', '0124290421', NULL, 1, '2020-09-23 11:57:46', '2020-09-23 11:57:46'),
(615, '16b1a616b6ae4165ec11cfe871c05379', 'Tan Bing Huei', 'tbh.com.my@gmail.com', 'fa498dbc16fcf399e8763e9ef6e7be4394410c18bae89c6b792edf0044ae5403', 'a50af632f46bf73d3789a380a16c0cd235d9e5a3', '01131832471', NULL, 1, '2020-09-23 12:00:06', '2020-09-23 12:00:06'),
(616, '3c1de68e7ac4a80b4b327f1cabc6afc8', 'Jane Lim', 'Jane_limtt@yahoo.com', '2ca75b1763419ac13d4b2d097abb56adf440fa608a8c9f218b3283055d75cbc9', 'becc74912b24fc73205f0b29e8a54da4c1b2e2c8', '0124307895', NULL, 1, '2020-09-23 12:00:20', '2020-09-23 12:00:20'),
(617, '3e6f07f6532ca0756f798669db0daed5', '陳竹纬', 'accounts108@lktfood.com', '0b7ff061efc3b66ac95f6810b31f7ff7dae3dda2b9d69bacf323a7e2ae9dfef0', '992eb86857dbb54dabc4622d6b1fd3dccfc6a484', '0108817292', NULL, 1, '2020-09-23 12:00:53', '2020-09-23 12:00:53'),
(618, '468382ea5c880a28625b2acb779d2773', 'RICK OOI', 'dprick88@gmail.com', 'e62722fdb20be07765ef7aa681669296b293fde10ca8eafa1e61cc281cd674b4', '1ccc97ebf1e5da4e97f9b5bb75cc837f07294598', '0164457371', NULL, 1, '2020-09-23 12:01:04', '2020-09-23 12:01:04'),
(619, '5e528b0ff2c737c6d966aed5643f7859', 'EMILY CHNG XUE MIN', 'sabrina.leow77@gmail.com', 'ea234cf17a00bbdddfd58839c34f2f3dda479f6ee768d3ae8784460ca672a990', '79485e9494df8587ff07f2178fa9b44269cc8747', '0184000342', NULL, 1, '2020-09-23 12:01:29', '2020-09-23 12:01:29'),
(620, '5a05961c1f8edd21e4e329779b3cead2', 'peggymoi', 'Peggymoi1984@yahoo.com', '3af90303c80c52192996f2dea495393aed327366d0725f29f542ea947fc6aab3', '44ee54fda7869ddf8b575dff40de5e8af260fa7d', '0129663382', NULL, 1, '2020-09-23 12:01:44', '2020-09-23 12:01:44'),
(621, 'b944a2055803b53acabda2efd09e34a5', 'Sim lay ching', 'Cl_sim88@hotmail.com', '0ff17deda9ec9f72ccc84e1ed8c1eba8d6da41b264d229c620a07e74155265c2', 'ce1d97c12bf5dd97fdca5805822cebd9846bb929', '0164053461', NULL, 1, '2020-09-23 12:02:10', '2020-09-23 12:02:10'),
(622, '2afb1fe4127c91c9aa39ab0b12db9407', 'Low Sze Han', 'Lszehan@yahoo.com', '380de8c38165d1f4e918882fb97efae4f02c522d959591304aee855bca1a3077', '717a607308be8fd47e49da486ff9f1fb2d61f3bf', '0126795850', NULL, 1, '2020-09-23 12:02:12', '2020-09-23 12:02:12'),
(623, '78bd831416b20b7dd03eda4a157c4a1d', 'Karen Ho', 'Karenho2001@gmail.com', '461c03530482393a7c3af11989ae203638954bf65b125eae66c187569199801a', '08e839d66df0e07c8aef8e4f5f03dd73b8d9bf89', '0122365744', NULL, 1, '2020-09-23 12:02:25', '2020-09-23 12:02:25'),
(624, '0a0aef3cacdc6af297d017093958c439', 'Danny', 'dannyxiu988@gmail.com', '37ca103dd5912e140d81df7fbf8ab68fe81e160de0f2c5629d18f1e933f118b2', 'b74c21832ab277fff83bddad71e9c0327d813fd2', '0124552518', NULL, 1, '2020-09-23 12:03:31', '2020-09-23 12:03:31'),
(625, '9ff024ebc2aadb052f8cda8c2d18b11c', 'Lim Khai Ying', 'kylim1994@gmail.com', 'c64a232b5594ad36dd9c2731d727cd4deec9cab71618650e8463e4c38e910b2c', '27a3ce1edafd3f3f1ee06df803f8b0716d089bdb', '0164054935', NULL, 1, '2020-09-23 12:03:37', '2020-09-23 12:03:37'),
(626, '8946fd4b1479dfe71e28a49cd1bf4e1e', 'kee fang Ng', 'kf1140@163.com', 'fb9fd5a1de0552cf68b8497de13639403532beac963e02b88afbeb7c2595a2a6', 'f59f894bfc5abb60b4da6fffb39e2c3ecb34c9fb', '0189887571', NULL, 1, '2020-09-23 12:05:16', '2020-09-23 12:05:16'),
(627, 'a9407ad641a7249fe4b4d1d73ab044fe', 'Koay', 'koayeeling@yahoo.com', 'b1775f522f73a175dad8c8b98d5ce6f8acc72e06979bfbae3b501a335cdfbb41', '20918ec26c89357dab352810fe453e24da1f5624', '0124775374', NULL, 1, '2020-09-23 12:05:23', '2020-09-23 12:05:23'),
(628, 'b743fccff083cfc73c924e5c91aca680', 'Hia Hee Hock', 'Peterhiaheehock@gmail.com', 'a303d4282bf9aceba6c87a5843cbbab117629dd05a839beef42fe2e3cf0b843b', '15ab37b6fdd35ab607764403d09bed6ecc747cdc', '01156615215', NULL, 1, '2020-09-23 12:05:35', '2020-09-23 12:05:35'),
(629, '3581458eff4f598a6da2758ac65672bc', 'Cony Chan', 'kirorochan13@gmail.com', 'bf80273052c27b0ccbafe5daf0f5a461c4990713f8f6bcc7f542c81381be6668', '8039d5ce8541bbfc147aed1ede54a8f276954f7b', '0164014890', NULL, 1, '2020-09-23 12:05:44', '2020-09-23 12:05:44'),
(630, '4a1509ca0467e95488b63f91a3a23722', 'Hoong Ling Tze', 'hoonglingtze@yahoo.com', '68ab7d186a160e22657bef672e2ab31c35de7ff88fe4d193acd8f5f523efb535', '44621950c1f0d2661935d161ec9783e62f83dca9', '0164400126', NULL, 1, '2020-09-23 12:06:18', '2020-09-23 12:06:18'),
(631, '9195d9be4f3c24af2a487716f2f0cf0c', 'Wong Tan Ping', 'sally731@hotmail.com', '77bfa776bf3f891704989db4a92ecdddb1ac352341a6d76bca086712b06a9cd2', '67fe9ae10f1da50d053d9fbfc3fb19847f5328c5', '0164886877', NULL, 1, '2020-09-23 12:06:24', '2020-09-23 12:06:24'),
(632, '0e9acf52717998a2e8d445747f5a15e0', 'Sylvia Khaw', 'myhomelife88@yahoo.com.my', 'b72f7ecd91ab825903805585aa0e7a4bb2e1b2e0b11d7aaeab9969a5d8cd0ac3', '4687002f53c09b11d005dc69886af5390325c451', '0164191198', NULL, 1, '2020-09-23 12:06:27', '2020-09-23 12:06:27'),
(633, 'bb3e4abdb77a2574e0128a7d1d1f3e03', 'Lee chew ee', 'chewee-0903@hotmail.my', '087700c1916268a070f27c6a7e82c99667bf170b74c8036049b36049b90273d5', '656efbb7e7fcb1b0a451d725362a7847042a547e', '0175597155', NULL, 1, '2020-09-23 12:06:40', '2020-09-23 12:06:40'),
(634, 'c5e6c034116d64b6a07ae3c08150ef6e', 'Roy', 'xyan.1047@gmail.com', '0a066a50da5354478493f3705b0d936d75b79084d35eda1658597c78049d6d79', '4f16d77a8b405cc305dbd11a631462ca002395de', '0162345559', NULL, 1, '2020-09-23 12:07:00', '2020-09-23 12:07:00'),
(635, '7c6fdbf0914b43ec542d5c7153ac2b61', 'Kow wen kei', 'Catherine52037@gmail.com', 'dc5ccaf0298490958061aa909bbfe803307acf6b6d36ef515ee93270bdcaf3bf', '9e087fb18d07a0b25f46b3088f530b1628071c99', '0123143592', NULL, 1, '2020-09-23 12:07:20', '2020-09-23 12:07:20'),
(636, 'c6106804d3ad934551689f0253c4f445', 'shirley yap', 'shirleyga611@gmail.com', 'd68439292bf462f3cdce0afdfab15b35bc78532affbff12c1408ff3ba22b8229', '0e407678bfeaaf8564b0a042927349d3389b1dbd', '0122012605', NULL, 1, '2020-09-23 12:07:54', '2020-09-23 12:07:54'),
(637, 'f2ae802bec5dd0969c0325a9d1646ae3', 'KUAN YIT PENG', 'kuanyitpeng@yahoo.com', '1105c6a71927dcf6441004988180c411e8e6101ec4b2524f0c48b41dbff73774', '8401892c4cc3cccd55f672eb99fe3c3752f90e2d', '0194107695', NULL, 1, '2020-09-23 12:08:51', '2020-09-23 12:08:51'),
(638, '99faafdd1bc0fb1f798273cc438e3bb5', 'Loh Khim Soo', 'Ksloh98@hotmail.com', '3e1ccab2c31a30a46378e8bd55fa077ff04794fd9cb10e68f339b54481646497', '66ce5a0235294544dc2cccdafaa03277a94d6280', '0194140091', NULL, 1, '2020-09-23 12:10:00', '2020-09-23 12:10:00'),
(639, '2739af61d04fb789f624261a9f1d00d2', 'loh xue yik', 'xyloh1103@gmail.com', '6629768172c0658ecf416fce2ae36f48cbe6f746d8fa5f643f814e34cd775801', 'da96b10b4226b4da64f04636df0b2addf7acf7e1', '0175352689', NULL, 1, '2020-09-23 12:10:05', '2020-09-23 12:10:05'),
(640, '9f16b3fe5b5de44d9ab2a9bbd70ed57d', 'Yukiko Yeoh', 'yukikoyeo@gmail. com', '4966561efaa18beb6bb99d8624d86b452a3a3df124476ed09ead787e23257737', '5ac74de6ebe16f4d0d667762a9f14ee511d3e88b', '0192268630', NULL, 1, '2020-09-23 12:10:52', '2020-09-23 12:10:52'),
(641, '587b8623519a91014dbc948755b9a8c1', 'Ho', 'tsuichin', 'c42b99bdd060dce821a8b43d06bb8a41758dac4c7b33c185ebf966dae5ddcd3b', '7567edcbb3d551cbe931c02400771e12597ff5bd', '0143000718', NULL, 1, '2020-09-23 12:10:54', '2020-09-23 12:10:54'),
(642, '669057c2d9f2ce006330350679f5063d', 'Cheong yutheng', 'cytheng611@hotmail.com', '864befd8eedac0858cb3a6b40387c942fac6e23a54b504c0e47324a1503b0c7d', 'b9537753dd2612d8ddc7b903b149c4d3adae174b', '0164800086', NULL, 1, '2020-09-23 12:11:16', '2020-09-23 12:11:16'),
(643, '8a28db5d2cff742208dd17cbf9a4f7d1', 'Darick Tan', 'darick11111@gmail.com', '708f9e5dcfee1f489d7e9ca91df5b56aeaa0f443417fa369181e0d5c4488f8c3', '9aaa71a9f6c6b6c8f0f1c53d8dc0a272184b1b47', '0122147891', NULL, 1, '2020-09-23 12:11:20', '2020-09-23 12:11:20'),
(644, '345ac386d817ffd1ce8a860365d3dde0', 'Ivan Tan', 'smtan98@yahoo.com', '8bd9a74198ad68bf6877f87d8ec0c8fd27673ffb17b05dd516ba4ce4f9b49b82', 'dfeb0bb541e2ac92633d833f25b7f3c09b98b95c', '0124285164', NULL, 1, '2020-09-23 12:11:48', '2020-09-23 12:11:48'),
(645, 'b25bb7f01d7a6d356d7c137678f6f3c2', 'Lim chih seang', 'Obfunny@gmail.com', '05a09f5a403b0ba470a4c8f64a46971ac5bf96f1224ffdb9907f923d3b8ae599', 'e9f8db1e08650d50f569dc989dc978e288c0e6bd', '0164148309', NULL, 1, '2020-09-23 12:11:55', '2020-09-23 12:11:55'),
(646, 'dbec4e9181f365feb877ae862124cb45', 'Leow', 'Leow222@hotmail.com', '355b7bbaa148f6e4b7aeed45c01605172fd08094be39b3e02ebbdce01f63dd5e', 'e882864f73f46c12021db2576f1e76f518036f59', '0146020306', NULL, 1, '2020-09-23 12:12:30', '2020-09-23 12:12:30'),
(647, '5f466b911cf8375f21b51f98084ae430', 'alice', 'alice-hm@hotmail.com', 'a65a04f2e02ba7ebebf7217d4301aaded6cc818b8a6700264126aabe847077d6', 'fae0291610c33c96a6164981b5258c5820e4e294', '0126545537', NULL, 1, '2020-09-23 12:13:11', '2020-09-23 12:13:11'),
(648, '880593c35d0b9dc7b61dde66c1302066', 'JESSICA SOO', 'chewleichew01@gmail', '3a028858576859cb7c8224f9cfcef0f2c1a5cdef98fd2dc0a402e56506d03441', '2fab15701419298ec3b520cabc8c24f010e45b94', '0127746638', NULL, 1, '2020-09-23 12:13:11', '2020-09-23 12:13:11'),
(649, '26f67c24c34c6cf187534674fb2d1cc9', 'Ooi Kok Leong', 'donny940417@gmail.com', 'a9d2f143341fb240a8cfa6d23cceae20133378b0a1d942ea6b1d57c12a57d805', '547131c7ec8c89888f13fb0db056c86706b4b658', '0124988913', NULL, 1, '2020-09-23 12:13:36', '2020-09-23 12:13:36'),
(650, 'e6e3d831fad385a5031b38116ca8e57f', 'Soo', 'glyden.swf@gmail.com', '9b716ad43dd9e2fa086ace4e947a9d65b796a2e416eabb19eeb054224853bdb3', '53ade35c17f67a60cacf0a45cb05e482245c56ce', '0129118936', NULL, 1, '2020-09-23 12:13:43', '2020-09-23 12:13:43'),
(651, '81b4e3963058c9b4bb6113e6da2d7276', 'Duncan', 'Duncan98008@gmail.com', '9d64d0eb7c4cb4e458edc1e506d4b26f9be39f3bc56101cc6073b23405cb75e9', '6f9176d3508546fa9dfa841ef87364ab171cbaf5', '0149493435', NULL, 1, '2020-09-23 12:13:57', '2020-09-23 12:13:57'),
(652, 'ed1a626eeac2b33ddcab4877e1b14530', 'Aliese Teoh', 'huay5513@gmail.coom', 'dacbd618ab9bd5fc570f554d94772768ebb7e1a6163dea51487c9c8cfc82f514', '72f3a8de046e8ddee9905a89b74a3a4848ca0b04', '0125338598', NULL, 1, '2020-09-23 12:14:19', '2020-09-23 12:14:19'),
(653, 'a2d7c67aa07d1295c82bd0df33321b0f', 'Heng Yok Seng', 'yoksengheng@gmail.com', '7e1395c66a57fc97709cc46ccc4d67eae6fd0831e63cdda98067edffbbb9f304', '950388d2f548d0553e78bd8ca7733aaa2c28b57f', '0175696921', NULL, 1, '2020-09-23 12:14:34', '2020-09-23 12:14:34'),
(654, '93db5ebd9c7daf0a6ad01abe75595537', 'Yee li Lim', 'Gracelyl2003@yahoo.com', '863d2027cdb41b6b8c3633623ba8f409869f1b1860c1728dc0cb1694e0b2268a', '57565f09838229ad0cf376065902f309dbdbd8cc', '01128440546', NULL, 1, '2020-09-23 12:14:41', '2020-09-23 12:14:41'),
(655, 'c49d0666ba11f599f584d3f9e19b06f1', 'Chiaw Wen Chaw', 'wenchaw1015@gmail.com', '1dc020d5772088644dbae549c992ef1062210b30cee7ac019d4701d2cd778dcf', 'be132978b10b20dcc4eb300565cba76c001f8bdf', '0124487186', NULL, 1, '2020-09-23 12:15:08', '2020-09-23 12:15:08'),
(656, '13fd254a656809025cd37ec9fdc79604', 'Ng Sim Yee', 'ngsimyee44199@hotmail.com', 'ee6c7945a922bed551e9cf2a23c25f120f8ba21f4226b008f1d29e5b0d72d659', '31b10759b8b6650068ef78c1aefb7b5914db743d', '142483356', NULL, 1, '2020-09-23 12:15:13', '2020-09-23 12:15:13'),
(657, 'cd63bea71abb49ee84dfb55d7911fcda', 'Mavis Ong', 'evelyn.ong.mavis@gmail.com', '7992a4ce2da20ecfa69548ce689aee99180504d0ebe64e689d9ca4daaf20c13d', 'cdab063964f7764b33de8de99548812eaf34dbee', '0174362588', NULL, 1, '2020-09-23 12:15:37', '2020-09-23 12:15:37'),
(658, '1d0e8edc40bbae28bb7685aa4994379b', 'Tan Yee Theng', 'tanyeetheng0908@gmail.com', 'a2164b3704c72887e82819f421bd3603fa977bbdf5de31a33f6e750b234bfdf1', 'eb613ae2c401bca893591196ac559de85fb82ee5', '0126055599', NULL, 1, '2020-09-23 12:16:47', '2020-09-23 12:16:47'),
(659, 'ba78e9c1ae1088a014cb11194db197ff', 'khaw whey ern', 'wheyern@gmail.com', '539b3e4e0f40a8e05840555f245cdac08b0fb6c3789e4b805895792d24b8c2a9', 'c6d409b0ef1fdc97c1bbe3fe669cb4606de2f65f', '0164412669', NULL, 1, '2020-09-23 12:17:16', '2020-09-23 12:17:16'),
(660, 'c770e1ee6dc16bff9dafab781204f6e2', 'JASON TAN BENG KOK', 'jasontan74@gmail.com', '6996e05d23028c0e7d7ec2da932aea903b49c10cb34d9dca38160dd161c9f846', '15f0f04f773571426a0a456ee048cb347a24c655', '01133930138', NULL, 1, '2020-09-23 12:17:35', '2020-09-23 12:17:35'),
(661, '66bf252ea060ec4201526adb0b3d604b', 'PHUAH KOK CHUAN', 'caseyphuah65@gmail.com', 'befd9704f0a2ec564898b4acebc61cea3dbc56f4f7f4a1a2c4b7f97b7ebf5166', '29852d4683c4e171febe4517c887a25d11174ce0', '0129729182', NULL, 1, '2020-09-23 12:18:05', '2020-09-23 12:18:05'),
(662, '82465fbd41a6e0a8dadcd4e807a55650', 'Ng Pei Jia', 'peijia27@gmail.com', '5ca41b7bbbde3a3804b1e5773f560fd99d18f82223ca8ec1e76e844d1b508685', '8b7ef500cf39840082e305f1076b4d010bae06a9', '0125938609', NULL, 1, '2020-09-23 12:18:34', '2020-09-23 12:18:34'),
(663, 'bf49131664f0ce5308f0a418e317716d', 'Tan Tee Wee', 't_teewee@yahoo.com', 'f036eab860a65b33c91986997465ffbd9fbb471e24c368c02364119019f644a2', 'fd41bd8e03187800d469c521f435443ac5aa2b7c', '0194755390', NULL, 1, '2020-09-23 12:19:11', '2020-09-23 12:19:11'),
(664, '2308b1be517dfc25a3d3675ef0617569', 'Ooi Sin Ju', 'ooi_sinju@yahoo.com', 'bc81cb0296afe83b82d69b594fb7ea8e41286d39178a0945345553eee71a1c17', '65900f8c770736f9066a723a1ff73f9c9bec2bef', '0195433555', NULL, 1, '2020-09-23 12:19:20', '2020-09-23 12:19:20'),
(665, '07b749687b2d48d737d0eee728f156dc', 'Hah Shau Ling', 'hshauling@yahoo.com.sg', 'd16a8c7a5b9e37ce02eac157a27c150b9f3189a9102c4e54aa58118a1d2b8928', '7a53fa8c188787991a870a95529e565e0494d794', '0128087129', NULL, 1, '2020-09-23 12:19:38', '2020-09-23 12:19:38'),
(666, 'c56d2694d7c3683d4f05c0cb1bab2acf', 'Ching Yong Heong', 'cyh931016@gmail.com', '7d83922cdd7078d95e4f659fcf24da0d5378d6351a1f1ecdefce2a59009fa37f', 'ffd377b2d816cc8b592ed833bed8aaba1e348668', '0125101993', NULL, 1, '2020-09-23 12:20:21', '2020-09-23 12:20:21'),
(667, '61685417532209e6979ffb50841a81b1', 'Benjamin Ong', 'benjamin_chinh@yahoo.com', 'd839a00d16a275b51eadb75c2ec56ab08c03746c9566853ad0cd8ad53120465e', 'dbdbfbd285278d26b0a99112b485a7a3278942a9', '0174960788', NULL, 1, '2020-09-23 12:21:32', '2020-09-23 12:21:32'),
(668, '331324de7cbbad511498773f2781cddc', 'Joycelin H’ng', 'joycelin1998@gmail.com', 'ed985d15e531c9c7c078f29cd31e91ae6e6d9fe3dff5c68a889546f46bf61199', '1e477d358edb6fb1fb8822f9deee0d07c22a61e7', '0174528450', NULL, 1, '2020-09-23 12:21:40', '2020-09-23 12:21:40'),
(669, '55e047a5372429d25f698c8f92b5d46d', 'kelvin chuah', 'kelvinvideo@yahoo.com.my', '3d16e0f15502709e0e914e5b054bc544b950a3d5cb205873ecdd1550f88cdaa7', '3027d6664bdd3d1dfbfd696bba7b5ef8f85155b9', '0198883014', NULL, 1, '2020-09-23 12:22:21', '2020-09-23 12:22:21'),
(670, '27591e18d6aed1593e10e4a48b5ba53c', 'TAN CHEE FOOK', 'ingtcf@gmail.com', '52f67ed2a58f7cd019b29f7c47e09033fbd31fc73c6c52b387da20a04244c6f0', 'c10b33f77e85188ff4738e6b237678d4eb15bbcd', '0195589605', NULL, 1, '2020-09-23 12:22:44', '2020-09-23 12:22:44'),
(671, 'e740aa55bffbf4a539a1da518607110b', 'dawnson chong kah yaw', 'dawnson_chong@yahoo.com', '082a2a87dc954a7cd4cdef72f1a1034af0ad15ffd78e6bfbe773f7509817e946', '61ca16d4f66f8ebbb2aff5ee79aeeba2e3453c18', '0102228485', NULL, 1, '2020-09-23 12:23:33', '2020-09-23 12:23:33'),
(672, '113a6f1a101e3424f22e3265d99dec75', 'Tey Lay Chu', 'teysummer07@gmail.com', 'da76efffca7c153816ce8d358f2d310816b4b840e576d8255e5a9afd6e25acdf', '2c1c879cfffdddacbebc29d689acd3f31eb0b43b', '0127832693', NULL, 1, '2020-09-23 12:23:36', '2020-09-23 12:23:36'),
(673, '89965e67227499d9199b1dea486ddaf8', 'Edwin soong', 'edwinsoong@gmail.com', '167bc3f4c4acf409c20763082024f5de7bba9313c2f5a24b0edf2763b1e48a80', 'c846d7b8f7279b6c9b16c178e120d3c333ef6558', '0174769188', NULL, 1, '2020-09-23 12:23:43', '2020-09-23 12:23:43'),
(674, '70569218dadf9158f14aaf1772da0a47', 'Lim Hui Wen', 'huiiwen1003@gmail.com', '6385aaee4358270c048ecea8cd0804e552f8fbe440cde2b8d61eaa155929a02e', 'b70cf35ae8cf7b466e8344c4cb35084a2276d450', '0169203106', NULL, 1, '2020-09-23 12:23:43', '2020-09-23 12:23:43'),
(675, '9cf7c7de0017e2eb914b51cd7c48f405', 'Chua Wei ling', 'weiling1010@gmail.com', 'ad4cefddc18d805b797c65abaa563839a0a1cb5eaaa6493d84f0021c2e9b9bf7', '2bfb99dc6f4092997d286f86b26bff92e7568b25', '0124101038', NULL, 1, '2020-09-23 12:24:07', '2020-09-23 12:24:07'),
(676, '4c29556abb27e957c11bead4e29593bd', 'Janice', 'clcjanice@hotmail.com', '9830bedcaeff2d4340bb0812f1c1b5f6f46875cae5aa39b2dd13abeaa178eba0', '3cb67a2e412e123f4daff5e7bbc3fc0a1e19a711', '0164516057', NULL, 1, '2020-09-23 12:24:17', '2020-09-23 12:24:17'),
(677, 'b96643fdbdc5e92711938c5f5330d2c9', 'Mei Sin', 'meisin424@gmail.com', '8c80e0914ab7659e9c9b498629f0a265a9385451c31a57a8b5973a331dac7db9', 'e7eb7e96d00a258abb3c54cc62413383beffe34b', '0108773721', NULL, 1, '2020-09-23 12:25:12', '2020-09-23 12:25:12'),
(678, '91b3c44fa48c7182f108c0f466308528', 'LEON NG', 'woonlai97@hotmail.com', '322fc37aaafd47e280d68f4978f1dc32c0585a04d2fc4e071bec5b09b7881270', '92ef6396cacecdd9d4611e1efa274008f42c8449', '0168894421', NULL, 1, '2020-09-23 12:25:22', '2020-09-23 12:25:22'),
(679, 'b5e3792d649dd039e952a93e0078ad09', 'CHAN MEE HAR', 'shirenachan@gmail.com', 'd264be1d233fec016c09ef3242aed7f73ff47ffe016a26ff07861265d3e67f70', 'a7caa9d9dfed8f4ea41fcc8912e29453a51d9c3f', '0165509605', NULL, 1, '2020-09-23 12:25:39', '2020-09-23 12:25:39'),
(680, '6e187e1ddd16f68b2fc9fd7cc8ea061d', 'Chew ai ling', 'ailing_chew@hotmail.com', '60a924b277a252aa8bcfe9b102de1cfd0745abda6f6a4f229c9adeaf4ea0a1d0', '04de92dd707757f3b2c8f85d2b9ac274be76683c', '0102185012', NULL, 1, '2020-09-23 12:25:55', '2020-09-23 12:25:55'),
(681, 'dbd1ee442cbc9e45b63ca26623354c3b', 'Loo', 'seahfangloo@gmail.com', 'a3454aa26941f67e03e80029fb9162afec9a7154b61bf6dfb726cfc68f01561a', '3f5775668412cbacea0f13ac37b33f28b7feb1af', '0175270456', NULL, 1, '2020-09-23 12:25:55', '2020-09-23 12:25:55'),
(682, '5f9639ba8f7c65884c6709a81e5dcbae', 'H&#039;ng Khang Chin', 'khangchin.hng@silitech.com', '6437de11842ea8185adcf194ef4acd96aae12fec69d505e7c4e909361a795656', '7818853f3c92725ac5d1c041133fb5010953d0e1', '0124450765', NULL, 1, '2020-09-23 12:26:18', '2020-09-23 12:26:18'),
(683, 'c5825358e2068b747b2dd1cf1d01e78f', 'Low Chee shun', 'Jacklow_lonely@hotmail.com', '99c0ff828f65d0894e6c9f1535359f96a139768577ad9d072b15f0eac0f27c04', '20e482c7d60e4b9f7d9c515713499514f091c756', '0189837455', NULL, 1, '2020-09-23 12:26:50', '2020-09-23 12:26:50'),
(684, '46e476837bb5f01a78853355d93b98c8', 'Wong', 'scy_wong@hotmail.com', 'e2a5f2c639f3255583272cf63c0e47b401a7a4a7af72b10bf147fa0259d33b41', 'fa4fdc5020c9d5bfe42a3426eef6701f2f7e978d', '0193563665', NULL, 1, '2020-09-23 12:27:03', '2020-09-23 12:27:03'),
(685, 'd90619a825d92961f3a3e8ce211f71cc', 'Choy Chan Mun', 'choychanmun@hotmail.com', '113dd67d077a754ec87a7521371ce6882fc626859560ea3fbf93cee73d638b98', 'b14dba784cf7c4880fbdc6cbeb0e207ffc9b545a', '01265347353', NULL, 1, '2020-09-23 12:27:06', '2020-09-23 12:27:06'),
(686, '82251f34050b70019020eb316bad964b', 'Tan Tee Chun', 'darrentct@gmail.com', 'fb4e550d5005c707679d4a1160d2b380872bd66e03d3f66f97455870332d0ec4', '7eb38d5b7b3e39dfdb9f99a091bf03de4e0f242a', '0194585887', NULL, 1, '2020-09-23 12:27:33', '2020-09-23 12:27:33'),
(687, '9fc216947df4be982ef5762dc01ef93f', 'Yeoh Tick Sern', 'jacksern96@gmail.com', 'b2c32944c9383d1b41813ea5738d598cf67f1ff3b2a4fd24b26b8ff54efed902', '317cbe49e51da7d5c7a335da3eaa1654670bbd6b', '0125201178', NULL, 1, '2020-09-23 12:27:58', '2020-09-23 12:27:58'),
(688, 'f5a3c4589c94de280fbb410fc55a6e30', 'Cheah Lee Sun', 'sunzsunz@hotmail.com', '3f9f39f4cb8a44256292fa5141048667a9ff9d177537595e6736dcb19b55b365', 'fc847d66fb80f224d3a44e97b84d9129eaf8f944', '0164275880', NULL, 1, '2020-09-23 12:28:43', '2020-09-23 12:28:43'),
(689, 'e149cd25070e9e0c8f3f48deb4fdac73', 'Julie Lai', 'Julie9633@hotmail.com', '8a4394e650a152aa53d83bd81800427471c8c7282d626f9f2b9a3fc996d73c29', '23e4188de708c7217f88354d04a4bf92654451ca', '0162986266', NULL, 1, '2020-09-23 12:29:48', '2020-09-23 12:29:48'),
(690, '9f35147ee65099bebf60beedcf31020b', 'Yap Chee leng', 'clyap5387@gmail.com', '7ebe95d336e5c6b5e214f460786e39eef540f9b7e912266b7e6f158a2302ec04', 'd4e6d5c1041bacf26d88971775238b64981484d3', '0164865391', NULL, 1, '2020-09-23 12:30:12', '2020-09-23 12:30:12'),
(691, 'd7e7e558b146081405bef7012cc88ec4', 'Wong Soey Fun', 'wongsf55@gmail.com', '7319031b65cb36e579cdaa5f28e9c56635ae3d11c4098352f8f219cff0690e5e', 'ddb42ab1c6fee6abea2688edf87de64508288193', '0164533390', NULL, 1, '2020-09-23 12:30:13', '2020-09-23 12:30:13'),
(692, '2fa2e1953af17f5b74bdf7f56ca2cdf9', 'Low Jin Siew', 'jinsiew_aia@yahoo.com', '589805995f6cf60d8e9872dbf70e00ea94b380047fea62c8d6db830d4b736b9e', '343b088a78ba652fecd4f028667cc398d0b9d8d8', '0174826680', NULL, 1, '2020-09-23 12:30:28', '2020-09-23 12:30:28'),
(693, 'eb41f97abe614d5479337a674f3e9da3', 'Jimmy Lim', 'huachai2005@gmail.com', 'f9869336980e77f670b402276f983aa8f407102428b3f515dce7b412074c3fda', '85782816e3ee16f450879756f1839c33d5bf9bc8', '0125545663', NULL, 1, '2020-09-23 12:30:56', '2020-09-23 12:30:56'),
(694, 'bd7e15af31988e0d376201916dd52eb1', 'HC', 'hcsia@yahoo.com', '6058727a4adaee1a4e9d3aeb312caed20f3e4d66255d583231ad3d3677e703d2', '115d6613c3a6e8819f7076b0d08a150839d13dd2', '0174560765', NULL, 1, '2020-09-23 12:32:07', '2020-09-23 12:32:07'),
(695, '7f3b900cef3c948c6a9dd0e360017e09', 'Lim Wen Jie', 'lim123lim321@yahoo.com', '732f9a7dcfeb6b8fbca4ccedf7d9b849717c1225dfb96e31e7bae91d1760adf5', '88460f199c0322dd01b6fcb4350220bd8d88ca50', '0124015985', NULL, 1, '2020-09-23 12:32:52', '2020-09-23 12:32:52'),
(696, '7c750f9c692c078af4d98564925b93cb', 'Shermaine', 'shermaineying952l@gmail.com', 'd979f697f04be31c3a2390c9c45f297ff4aeba71a048881a09b62f13c362d2af', '76c752479b594551f43d673f556eb84a459d422d', '01111601343', NULL, 1, '2020-09-23 12:33:15', '2020-09-23 12:33:15'),
(697, '925a648f0a2eee3055fdf115bfbcf648', 'Chan pat see', 'patsee@supertours.com.my', 'bb2d53e91d5664384761ad0977f7e1712b23a6bdf64092e4932787654dad0c6e', '2650022e537b354bdba389afbb19b1f41e437c63', '0164208202', NULL, 1, '2020-09-23 12:34:32', '2020-09-23 12:34:32'),
(698, '94cc7600fa7012ba16aabc859e5b08bb', 'Ong how yong', 'Howyong4584@gmail.com', '894d5a24926f5159d1b4511d7f2a7f03cdfbed5400f4dfc6b25ce4448c2d4218', 'c9ef7b3c94652e5431a078135b6cd381e3120459', '0125919782', NULL, 1, '2020-09-23 12:35:20', '2020-09-23 12:35:20'),
(699, 'f0f543c41a434d0e9caffe0f3adba9f8', 'YEW SHENG KUN', 'shengkun94@gmail.com', '392a297a09ac6e8a0cb676144a83fcd14913a14f90a5e4b124bb025a42545ef4', '3426761e368b69b01b0306f4b499875d7090132b', '0147582151', NULL, 1, '2020-09-23 12:35:35', '2020-09-23 12:35:35'),
(700, '2383d1f817e4b60c6447b315ac91414e', 'Goh Kah Chin', 'kcgoh81@hotmail.com', '6be00b99734b3aa59162c7af91a9f61851da88f1e0fa647c242ac122e2482738', '4fd28e9480665e999131ed9ab3284955abebf604', '0164926058', NULL, 1, '2020-09-23 12:35:43', '2020-09-23 12:35:43'),
(701, '50005abcc7629550919e72092001f959', 'Khaw Jun jie', 'junjie7883@yahoo.com', 'f6a10e693e0d56fa0d687855e49b1d0173233954fc0d2e722ebef6a53699bde6', 'eb8174880418de0966e089fec20d80ff92fe6f5d', '0198578815', NULL, 1, '2020-09-23 12:36:02', '2020-09-23 12:36:02'),
(702, 'f04cc4a0c624b6215d1b0a001aa6f0d0', 'Angie', 'angie4947557@gmail.com', '14cf39ac92683c0c3a050bb5a06efa9ca7fd9fa3f919f9c7265f6c8102259c59', '05739d00f346092c4a2b2c48462b9782bd0428ad', '0124941981', NULL, 1, '2020-09-23 12:36:07', '2020-09-23 12:36:07'),
(703, '210dd9f138e5fc9745632ef8ae8a47b4', 'Ooiyujie', 'ooiyujie5348@gmail.com', 'd2dfa51bc67df235f185c34f14fe36467c6e315fc0071995f2d17970dd1b646f', '1c8cc17ab4b1f55051db45a54219da574907e4c2', '0174329730', NULL, 1, '2020-09-23 12:36:37', '2020-09-23 12:36:37'),
(704, '4b8740ee394db55055324551b88643bd', 'Chris au', 'Chrisaws@86.gmail.com', '5328e9aa23b997721b30b1f4e7670af1cd78566296195bc82d6135d7390553e4', '9a23b46f0b0fe19eaec3a6b3006b44d1ec7f3a8e', '01121593654', NULL, 1, '2020-09-23 12:36:40', '2020-09-23 12:36:40'),
(705, 'a81aa7f2246c1eb9efdb393c0cefc77c', 'Francis Lim', 'odaryuichi@gmail.com', 'eda2acf493c2af030ed4621691f489be314c9067ae65057584789b7098931e75', 'a4394bf1084ee3cb7aca237ddc44a6bbba10b191', '0122515566', NULL, 1, '2020-09-23 12:37:11', '2020-09-23 12:37:11'),
(706, '744e2a1b456a2aff5f975da53e2433a1', 'Vanessa Ng', 'venessa.ngec@gmail.com', '3553b88ae2c219de1f14b1528227d52330a8c1d9c9b16adfa28a843ab5ece3ae', '9363fe85759ef92665c5722f97dbfd2c51db54d5', '0174051241', NULL, 1, '2020-09-23 12:37:56', '2020-09-23 12:37:56'),
(707, '2a5ec2e27eb2260e1bad080b030592d1', 'EELYN CHU', 'eelyn88lynn@gmail.com', '9de893a6da78745cf41a8179acbc1b5214aeefbae3aed2d00daf1ad721ef2fb0', 'e51be73cc907df48d65803e411fbc0dcf72daa6a', '0169532138', NULL, 1, '2020-09-23 12:38:29', '2020-09-23 12:38:29'),
(708, '7c3f49f36306926986aaa2046d7e204a', 'Candy', 'Phooh128@hotmail.com', 'ebe7ec64dacb77675453ede7a1496b61caebcaa0b810883978fb6f1f2024b63f', '9f8ae1889a13be05b674666a27fd43f4875bc15b', '0174609418', NULL, 1, '2020-09-23 12:39:10', '2020-09-23 12:39:10'),
(709, '97ece5064d5d2f4a111ac72be969ad22', 'Chan WAI chin', 'Chincw82@hotmail.com', '5e8deedef2a316aba88de94b1dc124d0684b1f28c06eed0513508ec2fe34c018', '5d05dba3776bf3d17d9dade0ba6fe12d81fed40d', '0126900028', NULL, 1, '2020-09-23 12:39:12', '2020-09-23 12:39:12'),
(710, '7553a6df516faf65288fd0c2f9d4b06a', 'Zoe Lim', 'infinitetrading@outlook.com', '3c0dd92d371fcd3ab56c8839115501796828d2487cb52c26213fbab6d9c238c7', '6c5db29e3d3a733e4e884b4fc02ef712fecfa42e', '0175581128', NULL, 1, '2020-09-23 12:40:29', '2020-09-23 12:40:29'),
(711, 'da2a74053f53a7975f65a6a02e9c0aea', 'HNGWEIJIAN', 'Hwjcyc@gmail.com', '437b1181a6cc62d93b0322760260707e7ed5c8df09670c985900be61fb79b4c5', '463e831e288d3fbfd3e9ffec2190c788b6cee4c5', '0125285012', NULL, 1, '2020-09-23 12:40:37', '2020-09-23 12:40:37'),
(712, '865191f297cc62fa6895b51ba3447f32', 'Coco Toh', 'tohwl88@gmail.com', '90fa5f2d9af7d1a1b8d4b214d5d9deb46fded75b77877e14c6ae011d375c57c9', '5e75266e6ece4d9e0b3b565bd3e8f93fbba0b515', '0164412337', NULL, 1, '2020-09-23 12:40:43', '2020-09-23 12:40:43'),
(713, '77e85df22a888809affe018dcfbd3bed', 'KH', 'ckh6065@yahoo.com', '0b5110102670adf449ae7e347724de6b0c92041e0bff1e2373d2c1ad9c8b000a', '3ca999487c38efe58650a0d64555089774f46bf0', '0199101525', NULL, 1, '2020-09-23 12:40:48', '2020-09-23 12:40:48'),
(714, '55a55ffd85896921ff9ab0958a91e48c', 'Lim Jhun Hou', 'jhlim0413@gmail.com', '31529ebf3512e4008191976a19ac211d77f90969bf18362e687d45f53cd145e6', '09f0496d31fd4c36cdcf70d1078a979f56b756f6', '01164463458', NULL, 1, '2020-09-23 12:40:55', '2020-09-23 12:40:55'),
(715, 'db9a59883d68527306f9ddc5c9df7379', 'Loh Boon Zheng', 'booney_22188@hotmail.com', '71ec35386e240d83ddec43cbed50c82e25d0eecad5875206721716ec0b7d84ed', '5193b2ebed2fe9cfcd73161caa2a089084f15ed5', '0124828858', NULL, 1, '2020-09-23 12:41:02', '2020-09-23 12:41:02'),
(716, '69cff4b74eb032f2c060f3655f71d3e8', 'Daniel Por Ching Siang', 'danielpor99@yahoo.com', 'eb57a9f34bed427e73fabe401be30b7f6fba9b6f73ad09b661118e84afe9f579', '9e2b102ab801af48b7c563bde59d1d4c53c87237', '0182298011', NULL, 1, '2020-09-23 12:43:00', '2020-09-23 12:43:00'),
(717, '6e7905dbdb4d2d17e3d055cdaaaf4f05', 'Mah yan ruu', 'mayplemah@hotmail.com', '1dd97e5581d1a24b1f84f73b5a02c039504b28a63f1f4365d1cb1b6ac879cbfd', '205288531366309fad38bfe5aafd09177505785b', '0176042159', NULL, 1, '2020-09-23 12:43:28', '2020-09-23 12:43:28'),
(718, 'ae0be039a8a2fbeecd93737efafdd0a2', 'Polly liau', 'pollyliau@yahoo.com', '274ba131fec572eab532153dc6a79e058fe0a8845669e55d2f3a1df0d441337c', 'db131f8106b6743ac1e5c1b7220dcccb86055d68', '0189094744', NULL, 1, '2020-09-23 12:44:24', '2020-09-23 12:44:24'),
(719, 'aed0e4f77ae346ced9206ca1d1df2d1e', 'Wong eng hua', 'Enghua0805@gmail.com', '3285c53021dff5bc64e3468feff608198291714b72be4f16745e3bdb7e9b5adf', 'e97583f3a04091d7df28ea09b7b62a501b0bebc5', '0125880197', NULL, 1, '2020-09-23 12:44:29', '2020-09-23 12:44:29'),
(720, '544d5b0a0a9f42167b9bafbb2a5c2d15', 'WONG KIM SIANG', 'kswong33@gmail.com', '02a1f438a5a37fca21e6985e66a9f93053e16632570d7debde0621cc9e2848eb', '8565c329e6f7782f031835f69b7e592de954a978', '0194427299', NULL, 1, '2020-09-23 12:44:32', '2020-09-23 12:44:32'),
(721, '6513e6f557c653f0a24d813010562243', 'Lee Shu Phay', 'shuphay88@hotmail.com', 'fd8df82a4b56c091fba4b06053ebe16bf965c4d5dc325d9744b5683350f23f07', 'c45ab1ee68193af037cfdd434b0e26a1eb6e02d9', '0124196738', NULL, 1, '2020-09-23 12:45:32', '2020-09-23 12:45:32'),
(722, '31c79806d1d94dee48927164f2bab525', 'ah liang', 'khor_liang@hotmail.com', '07b619d7e1fff25ca04297022bd473b0afb8786cadd3962098c89fe01fe79e24', '29c41ac10efae68870ad22def9f9e60b47f14501', '0186684092', NULL, 1, '2020-09-23 12:45:55', '2020-09-23 12:45:55'),
(723, '41bbcd701abf564362d91b259244a2e1', '罗月清', 'ycloh@live.com', '25ce7527f64706fbddd3dce6eb85ebfe0924651c7bd9e35926e8a9dd14507abd', 'a37c6099f52bac7e148a0e9bd1fe7502cdc585c0', '0164102366', NULL, 1, '2020-09-23 12:46:49', '2020-09-23 12:46:49'),
(724, '730e6e6995dffc78cec6b59a7d59d39d', 'CHOW KAR SOON', 'craftks123@hotmail.com', 'd2d56d2f32a750e5df3638e0a2aae7bf7c9154ab94bd1c98819ba75ee8aa6e17', 'f9f75c60d9add437dbabc4aedba6e2d7be08ca9d', '0193276877', NULL, 1, '2020-09-23 12:48:21', '2020-09-23 12:48:21'),
(725, 'f8b5129b471098f9e2a6945624304548', 'JY', 'ang_jy96@hotmail.com', '15bd8262c7f72016e023476864cb5b84cd1c4784219a8ce01227f63d4e870654', '28c919db3331050600c05193dba536a06140c5a9', '0169876689', NULL, 1, '2020-09-23 12:48:22', '2020-09-23 12:48:22'),
(726, 'c8dcae6fcc387c71fa1543e83d31e8f1', 'Lim Poh Geok', 'pohgeok_lim@yahoo.com', '0de0f2641c39201c29a6782eef80e0ee513251bce397ded354b2f81d645cff83', 'c25c550c46e2c33c7bdeafffb9106b4c9c19d490', '0127692502', NULL, 1, '2020-09-23 12:48:37', '2020-09-23 12:48:37'),
(727, '3205fecc0de4d48f631fcc7417ddb20a', 'Anthony lim', 'Yeowgiap@hotmail.com', 'ac7899bc41e356e94be1f88089e6719a5ec962c2d48c71ca822bd4c045d07584', 'e780a7e998afa3092352170d03318bf00b562443', '0125218249', NULL, 1, '2020-09-23 12:49:50', '2020-09-23 12:49:50'),
(728, '1e807df8da20ed224b69964625370a55', 'Chia kui jin', 'Heibaby253@gmail.com', '825d921705e1aa2b932f4f1f56b2cedbac63f1071ea00982361a79047ee13a48', '52120e525cd25f753c8af8e51cb83c8e7d1125a8', '0167982738', NULL, 1, '2020-09-23 12:50:16', '2020-09-23 12:50:16'),
(729, '7b20aeb9ee3eca16bb56e7587df31316', 'Chua kheng wei', 'adchua45@gmail.com', '241c274ab74f3a0003dea8ad202a5ba44147b20fa400c6a861f3497f4bdd08c4', 'a0ab07ca69aa65ca51b5c287db9943b9641f42cf', '0124956123', NULL, 1, '2020-09-23 12:50:35', '2020-09-23 12:50:35'),
(730, '8f9255de7d87356b25d60bbcc1297541', 'Teh Yoon Wei', 'yoonwan@hotmail.com', '1fbd589def09be18d2e7fab311d0a7a6da631040e7a578bd751827de68fc618a', '499ba7634722509a8695806b7728e42397a3f41b', '0164603754', NULL, 1, '2020-09-23 12:50:56', '2020-09-23 12:50:56'),
(731, '2b1c55e971271927e1977915f136ad66', 'Keith lee', 'Stupidbie9933993@gmail.com', '9f82adba21dff0783d507ee49753bf1ab1e167d36ef80ed5f3552806a6e4b37a', '2bec284734d5aedefe93390fc8cb13440afd1372', '0127963684', NULL, 1, '2020-09-23 12:51:13', '2020-09-23 12:51:13'),
(732, 'd2aeb24b1b900b5f0835fbd4fa5e8bc6', 'Hng Shi Hui', 'mshng00@gmail.com', '88d0482bc3564ebfa5ca24cb1a95445337a2421512394d96fdc07c54a6a73633', 'baf87152e034994514819d3551b7f2b24c2dbd92', '0174537972', NULL, 1, '2020-09-23 12:51:13', '2020-09-23 12:51:13'),
(733, '35364a81c14f982fc15d854d6e88109f', 'Khaw Mooi luang', 'yeeyu_1010@outlook.con', 'd542d2284246b9a4521612e6df4093fa65e0cb033b4f84ce6e4cf8e21d48c538', 'daa425b051a5bb2c95ce79595badc8b46494abe0', '0195406190', NULL, 1, '2020-09-23 12:52:38', '2020-09-23 12:52:38'),
(734, 'a059f8fcac121464525d5c9bb9e4f964', 'Christine Ong', 'joseph.ng.iso@gmail.com', '90f0563fc7c651e42b9d98307c983ac7c7edd097b838fb02511ec23e3dc73be8', '7ad45c0f16d6c938292393f56a95aea4eec9013b', '0164337448', NULL, 1, '2020-09-23 12:54:27', '2020-09-23 12:54:27'),
(735, '7c03bc5f9c2eeda9f3957cb0b8358925', 'Ang Seow Ean', 'Aries_ang@hotmail.com', 'd2ee552a3adae2dfb27c523a219f05cfd22662096e8b37c9dffdbd186267ef2a', 'da7a4bb8ae5f6a9f18b20ce80ec8c6fde46631f1', '0125560033', NULL, 1, '2020-09-23 12:55:29', '2020-09-23 12:55:29'),
(736, 'd738ace19d8258f3065aed8d5c002d8b', 'Choong Li Ting', 'litingchoong80@gmail.com', '2423e1adbf6312b4a1474636dcb21e3697dae037958be9271221cd39cf0d6e35', 'ede8013635116dd040d71c7258f0232268097960', '0164206391', NULL, 1, '2020-09-23 12:59:09', '2020-09-23 12:59:09'),
(737, '66fe37bea59dad1619a0273f7a094b5d', 'Chua', '', '3b50db7c45263aee3d6c432ebb89aaa60c8c342bc27a113cc2d22da19f8e417f', '9e9378a23bacc0e860e5edda446f0e2b1ac3910d', '018', NULL, 1, '2020-09-23 13:00:00', '2020-09-23 13:00:00'),
(738, '9fda7dabc27b60ca628a1cdf8adedc5d', 'Yeoh Sean Jing', 'sean93_survey@hotmail.com', '8b2bcb2ef4b89778a55b5f0d791acce763b71b3fc9f62914ee935bb3da7a0ae1', '61d5821eb66b681854e308a202fea30444db7625', '0174563161', NULL, 1, '2020-09-23 13:01:40', '2020-09-23 13:01:40'),
(739, 'a3c441d7d60c74dc7024970e4ae8ee9f', 'TAN YEE HOOI', 'yeehooi88@hotmail.com', 'aa9765f03b47e57f8b0d51dcdf6dbb8ac4443243fba6d9a1d35f6482c04f5ade', '741a60ac6e7c6ea3e7dbc620b909082431ab5387', '0124274556', NULL, 1, '2020-09-23 13:01:44', '2020-09-23 13:01:44'),
(740, '21526cc8a17566f3f7de1b75fab88e2e', 'Soh Fai Yee', 'fysoh9580@gmail.com', '3cad553520eb4bc9913a5f0e0655b417cd2906f08e1b5534467bfbaebf7b9ad2', '411848ab3818ca13350dd922471f9e4b8801f849', '0122343750', NULL, 1, '2020-09-23 13:03:06', '2020-09-23 13:03:06'),
(741, '8000dec4cbe9a94593f0ec113bd0b92e', 'Lau hun chuan', 'Jacksonlau2020@gmail.com', '40e213c49db7fdabf6707d0211720d7593da0fe0f4ad5fe71b539198c640a926', '0dcca501c78f8bc463799443ed9a0c64d4f3cb2a', '01169373083', NULL, 1, '2020-09-23 13:03:31', '2020-09-23 13:03:31'),
(742, '69673176921512715dccf77915c5dcd8', 'Teng Yu Wen', 'letitiateng98@gmail.com', '8f27eaf04eae549460ccceb7fbe8a8344bbed240a4b3aa8b3fae889f6c0903f5', '40b5266ff1da1ac4c1c31544bda0d76159bb235d', '01126841993', NULL, 1, '2020-09-23 13:03:40', '2020-09-23 13:03:40'),
(743, '8eaa009c3cc945bbc0b6ac022ddf8327', 'jason', 'boyjason9589@gmail.com', 'aec1b34dfcb4138e22c812499a2c97accb54a037b3f41b5350c5082f9570593f', 'd6a9b23dcad91f1947f1ff9a5bc89fa77e573fe4', '0187779589', NULL, 1, '2020-09-23 13:05:06', '2020-09-23 13:05:06'),
(744, 'cc90c103a6ceb6a31ed40b71d16c9fe4', 'Tee Ong Chee', 'octee80@yahoo.com', '0441a4da225fa45df8a612583ea6b20077dbabe02a14841a74127e621dfd2055', '39762e81bf7040b0a3dc62425489ce80ebca9559', '01156881636', NULL, 1, '2020-09-23 13:05:47', '2020-09-23 13:05:47'),
(745, 'fce06de23e11542fc4ed64c23019e3fb', 'Goh seah keong', 'skgoh82@gmail.com', '11fe205f3b4fe20d8fc0286f6bdcd4dc5a559355cba6a507ac8092c17f0a8d68', '062ec69313a900586aba68e4d54d2c057ecd2735', '0124237077', NULL, 1, '2020-09-23 13:07:17', '2020-09-23 13:07:17'),
(746, 'f5d741858a2ff43f8372a74bb1866063', 'Soo Nak Kong', 'Rachel88celia@gmail.com', 'f44eea5371374d2a0c5dc9a3b3a5a012972acacad38c6a4258728574fb9b36e4', '8ff30a5defb026a6c56f4f4c7d0109d7688fac98', '0129538429', NULL, 1, '2020-09-23 13:11:57', '2020-09-23 13:11:57'),
(747, '512a58d642c4ec6674d18ea287c5ce83', 'Cheah Jun', 'juncheah0303@gmail.com', 'b72b911584f4a4fe1a4013245813d188ec5811ef9cc948d1bb22585bc82883f1', '74adf498de7d293c575f637d1654588e7eb18276', '0122628652', NULL, 1, '2020-09-23 13:12:25', '2020-09-23 13:12:25'),
(748, 'e3b75bbb4a6415381c26002efb730e46', 'Cheah Yew Lai', 'jasoncheah10@yahoo.com', 'fb1c7a001bbaf911838a9b1daf98dcc891fd36612c208ad33643be0b8f7a42d4', '5f52c05132ef72287a00bac11b18481edfc27701', '0124874953', NULL, 1, '2020-09-23 13:12:51', '2020-09-23 13:12:51'),
(749, 'cdcf09347b6a1bd53adfb534ee5bdcc6', 'Ng Yuit Theng', 'cheahjun0303@gmail.com', '49c4a3ac9d17319f09e3c18dbf770fbd0cf32ff0753de399e1b56b162700a118', '55990d201d61b3e45aba1e93f3b422f2a793955a', '0164803328', NULL, 1, '2020-09-23 13:13:09', '2020-09-23 13:13:09'),
(750, '9fbcab8bb4777e8de0f5012cae6332b6', 'Chong Pui Yin', 'jncjenny93@gmail.com', 'f941adc8d1eea4e700ec7fc1b0521c0fec95bc477d7afe648c1635b4214703d3', 'f3d1418484f896c5e597d1807f8b76736da1164b', '0163901006', NULL, 1, '2020-09-23 13:15:13', '2020-09-23 13:15:13'),
(751, '52bcf1051514889a0d98fc4060a14a27', 'Koong chun fei', 'kcf921392@gmail.com', 'f394a54d1929bcffb4ba0e35f701973148d29796dd77e763f163834e8f34f449', '748d8ee65a5603042c70b999e9da1169a62fa112', '0174260700', NULL, 1, '2020-09-23 13:18:25', '2020-09-23 13:18:25'),
(752, '0248c2bf54147c29cdff746d59528a74', 'May may', 'ymayhor@gmail.com', 'c6a946a5f1ed618a2addd55544f0ab1a3eaaba853d34aef5c56e5871ad4ccd78', 'ed855524a5a449d8fe3bbace7d62fb2954fae6cf', '0164490892', NULL, 1, '2020-09-23 13:18:32', '2020-09-23 13:18:32'),
(753, '137fee278f41792b35dcb54838fb4766', 'Ng Yit Khuang', 'ericng7424@gmail.com', '73062d182a903bc4dd3e813fe20acb1e78412f3990b838a4ac4504d9d4115fb3', '0983e17bd4a1ed7a65b1b1d29fe9aba04d5ba848', '0196673448', NULL, 1, '2020-09-23 13:18:47', '2020-09-23 13:18:47'),
(754, '1564ab7127313acd8f4cc232582f4b9c', 'Qian Hooi', 'Oqianhooi01@gmail.com', 'e14557e2070070d51b91d9304a3e8010e276a91048e4ac058508e61c82482c58', '2bcbbf6aa52fb1b43ddfdc5ddc8812fd0ab199f4', '0142495313', NULL, 1, '2020-09-23 13:19:14', '2020-09-23 13:19:14'),
(755, '2bf4f61f36102df246208986c98bebe4', 'Pauline Cheong', 'pauline_cutepig@hotmail.com', '1a61fc7e1985c3fb4818e3ad06db3e8c2b3b7d4aed31681718650277f6e94bff', 'be55243e12609d2b4fd54ed80fc5c17935922cf5', '01156265012', NULL, 1, '2020-09-23 13:19:42', '2020-09-23 13:19:42'),
(756, '614845251618b1f83ef51d45c389d569', 'Lim Keng Boon', 'kblim.ing@gmail.com', '672de9446b1313e4f2b6cda44209597366162359ed64d22d30e58120ad4f1d2b', '1e3d7b3daeddfc0291e0ea4ef71301ae39b2ffd2', '0122067016', NULL, 1, '2020-09-23 13:20:18', '2020-09-23 13:20:18'),
(757, '1b7d66801327b16b956114f8f884059b', 'Yap Kean Leong', 'Yapkeanleong@yahoo.com', '7dfa8ebe4436c615c0032db9a1c5b7f69fc29049b294b7f86cdf85f08a7d817b', 'bfcfadf7a8854064a284c18a27df7ebf69417c65', '0122310963', NULL, 1, '2020-09-23 13:22:15', '2020-09-23 13:22:15'),
(758, 'bdde8616afb4fc5fb43f645d3496d17b', 'Lee Jia qing', 'Leejiaqing3456@gmail.com', '001f6f64a6c9fa41f8ad25065ba3cbdca5d79331e98c62d29832c3382131ec79', '843ce79455ffe528c90554f0b6617a1635c17d6c', '0175561685', NULL, 1, '2020-09-23 13:24:07', '2020-09-23 13:24:07'),
(759, '8ed3a54c0046f7165fa12e31b760e905', 'Ang Gaik Hong', 'ghang6095@gmail.com', '98be757c3aac6a4f93d0dfb30da0100dc9eae0469e921c1b8825ce32fdbfc383', 'df0849d500806408dec0a761b8193d94d2ee7820', '0174006095', NULL, 1, '2020-09-23 13:24:59', '2020-09-23 13:24:59'),
(760, '3def8b8d9cd39ffdf8b716a1beb9149b', 'Chew Shi Min', 'tcl5971@gmail.com', '57c8523cc2774b74a3f97ebae8fb2d3279f041d2164158ac8ef7036bd7685a1e', 'a4f2ffee6a771c840d198d70d5c77b9b7f7c7e09', '0164935971', NULL, 1, '2020-09-23 13:26:32', '2020-09-23 13:26:32'),
(761, 'fa656c7bbdb0aaac56269b3539d678ea', 'Voon Mei Fong', 'a19102@tsunjin.edu.my', '5d3c39d80dcc101213471c095d9cf2717cff34ef15fd3fc69a3bcc59a164618b', 'b3f32f4ac38cedebeabc5840ea7754b3ac20fde0', '0163866136', NULL, 1, '2020-09-23 13:27:03', '2020-09-23 13:27:03'),
(762, '4a9f702f824096e8598c7c831513bdb1', 'Kim Lee', 'caipinglee@hotmail.com', '0819092c35e8d098c0f6893aaa86d126ab71dd0fe96ae8acbc81f7c86b6a8264', '353df9e64fb1486d67b540578517b3d706611218', '0164421652', NULL, 1, '2020-09-23 13:27:03', '2020-09-23 13:27:03');
INSERT INTO `userdata` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `user_type`, `date_created`, `date_updated`) VALUES
(763, '4ba8e6aff22e449da401d1c3fd8ac3a6', 'YONG KAH POH', 'mandy7313@hotmail.com', '8240e605a0ee3dc0615b5b1bdbe4b2d56e4615d523330c57a9cc8597fc580dda', 'ea1531320575f18d597dcce924a8c228c46ad4f6', '0164273557', NULL, 1, '2020-09-23 13:27:25', '2020-09-23 13:27:25'),
(764, '8d88b0795715f3b6952bc284a05bc7de', 'Ooiweiloon', 'Loonloon0707@gmail.com', '48fcc2badfd21024671900ffa4be94fa43b2c3e43d198137de689af9c296d9eb', 'e55c55d8cae778059dbeeb73e4aa71b209e619a9', '0179742217', NULL, 1, '2020-09-23 13:27:44', '2020-09-23 13:27:44'),
(765, 'f7c4fa55565d4a04ccc3da445e9e3530', 'Yeoh Wei Sheng', 'Lee46364@gmail.com', 'b263a0c9b7d5641ccaf686397af106e6094112253a2ad802c82bb1b438b8fa5c', '315394658f78bc6ef9721a324b4ac62e994f07cf', '0184061932', NULL, 1, '2020-09-23 13:27:54', '2020-09-23 13:27:54'),
(766, 'fada2e411915baeb08862c29a81be6b1', 'Carrie Low', 'Lowyokesim50@gmail.com', '75e77eb9fca5440f72db5112ddefc08a0883eaa4f0c1cdaa0bbee45a54a62eb3', '21d84065b2a07c5cebdf9e80b29ae5a6ea4ec25b', '0132210265', NULL, 1, '2020-09-23 13:27:55', '2020-09-23 13:27:55'),
(767, '22e0dead6bea3ba7b3ed8052993e6b0d', 'CHAN MENG WAH', 'cmwsteven@', '58a15b1524950fba9b791225c7a722af3b7f76fea727a885073618bfe748a2a1', '1ca256c0d73404723bf0edd99c26966bc8cbc7be', '0164217634', NULL, 1, '2020-09-23 13:28:34', '2020-09-23 13:28:34'),
(768, '6cc25833b98a5c8e4747d6c1ea3bbf0f', 'CHAN MENG WAH', 'cmwsteven@gmail.com', '2ba5ad8fe1b8b4461e7f1ef84badd069b78f5e74b512c7a82232c2b2a331cae9', '41e5ec31909d0c8d299724317e823fefd74bff9c', '0125773232', NULL, 1, '2020-09-23 13:29:36', '2020-09-23 13:29:36'),
(769, 'a48a96fc4fd543528b4405127efaddfe', 'Lee Bee Im', 'lee@49364gmailcom', 'e576845ce6e9d5fedc461d2dccc2d5ee926b33f590dc63ab43e0150e490b5b0a', 'fa3a99d72b98f5fab805cf37c9fd863791114e21', '0164304530', NULL, 1, '2020-09-23 13:29:53', '2020-09-23 13:29:53'),
(770, 'fb7ffe464c4253661e9e8871888a48d3', 'Tan ming huang', 'minghuangt@yahoo.com', '8154f4655c3bc4d04c14b41d42ccb55ec62827d71f445ae3610e3cdaf271121d', '1a2335d3fbb3bbb5a7bb07c7f04ff386c1a48ae8', '0125720998', NULL, 1, '2020-09-23 13:30:15', '2020-09-23 13:30:15'),
(771, '27d02dc9a5d2399d40a88c80916c7c7e', 'Eunice Tan', 'eunicetanbeeching@gmail.com', '27716fa0fa080dfe187554dccadb16cc3777859ef24935296dc82b6ec4b6cdb5', '02b829bf1590cfd6e75662fba7438fe00af89b32', '0174323068', NULL, 1, '2020-09-23 13:32:03', '2020-09-23 13:32:03'),
(772, 'd7889b1e014ff0ca8a96ae8aaf84e9f3', 'Lee Sook Kim', 'happykim1012@gmail.com', '0b7c2d3caac91ff52562becdbb73732b813f5c598eebf989c3405aeabd11fec3', '9e65fca979cc03f61390c426ebbd2d0ca10246cc', '01112543101', NULL, 1, '2020-09-23 13:32:40', '2020-09-23 13:32:40'),
(773, 'bcb6d8f781202b1c0d03f59cc4a07258', 'Tan Yew Ping', 'Savindtyp1987@gmail.com', '6fe30b5f26a7edccf5731019509e88ccd2af224ecf7134e1e41b2297dac6dc27', '736f381ced58d0fac7220f4b44b362da7307fb5e', '0127779123', NULL, 1, '2020-09-23 13:33:51', '2020-09-23 13:33:51'),
(774, '10a195ada567837a06dcfe1fd2233b42', 'Teh Chee Keong', 'ckteh83@yahoo.com', '3d87533546e5dd1971f94ff5e946fd03e0ed15765970dcbab6f8dc8718a9b176', '70e21c00ab87824338a73086519fa634993a3003', '0174047878', NULL, 1, '2020-09-23 13:35:15', '2020-09-23 13:35:15'),
(775, '1a47e93a5be6bd11006baa5c008a16ea', 'Wong Hoe Hong', 'hoehong@gmail.com', '38a31f2888787f6a4dbed89130ccfb12e51becafb84e9eb98f773c89d6e95c86', '16b5b06d82d2f924770a2c4f7b52ff5afac71dd4', '0125055038', NULL, 1, '2020-09-23 13:35:51', '2020-09-23 13:35:51'),
(776, 'd97d91f83fec647f84eef131f256f486', 'Shally tan', 'shallytan4672@gmail.com', 'b621bd5007666c0cf7fb973b253af5ac6f1edc53cff233ee8cf2e6ea74324cb8', '9686c180650bbfb5e223dadabb0346109b6a5709', '0164739883', NULL, 1, '2020-09-23 13:40:09', '2020-09-23 13:40:09'),
(777, 'e493d7a7a1a016adf1ec57acfab50e4b', 'Lim Soo Chiu', 'aaron96113@gmail.com', '76a556d974c09dd240f36785c3d7eb745c25f3d989f072eb75dae31e9426bc8e', '440d138a9531edb26b72bb03a8f826f0746a8cfb', '0164716659', NULL, 1, '2020-09-23 13:40:35', '2020-09-23 13:40:35'),
(778, '3d57f68ff364d5f23bdb72ef5eaa8ca4', 'Ng Huey Leng', 'qoosnoopy28@gmail.com', 'b75c9501f74c552a33ad17492300946814cb5c49478aaf62ee44da26a91c82df', 'b5d00c5df83b28b12a0e115f4387358d6bd51cad', '0194716743', NULL, 1, '2020-09-23 13:41:13', '2020-09-23 13:41:13'),
(779, '1f561624f0c41d107c01153ad400d8c4', 'tan kwai fang', 'Wongseeteng@gmail.com', 'eb1b9d1153e5afc8b4297fbe525e5f7a531ffed2c34aa8fef161742fec033ae3', '92e1978e2a8e8df97fdadc464e93388bdc56b2c3', '0165944240', NULL, 1, '2020-09-23 13:44:06', '2020-09-23 13:44:06'),
(780, '6532e0fb514c442e03c4cb86631f3ce7', 'Leong Hui Yee', 'leonghuiyiee@gmail.com', '34601d33335113aca704fd01298433ca05a611d9af52dcb43597e440d1eba6c8', 'c4c6a41e601d966480bbe391aea254b1f1ca3717', '0183918004', NULL, 1, '2020-09-23 13:46:31', '2020-09-23 13:46:31'),
(781, 'bc3f4b17427929c71d276014a7f32502', 'Lau Kim Hiang', 'Kimkim151264@gmail.com', 'b6e7a30527e21474871b770c776be44424bd00b78bdb64693f1945b70db2d27b', '00ecd457e24f511cae979544401d9c2b741bd5dd', '0164770839', NULL, 1, '2020-09-23 13:49:23', '2020-09-23 13:49:23'),
(782, '1040732026f54f0295f0090c8dff387c', 'Melissa Lee', 'meeishan2000@gmail.com', '44e1a34adeb1f2447847f3a19c4873c4d4400da37798d2a86d0d1deb5f21326f', 'da40d709eb74c7c8b71c64485796c461543cfb31', '0164591221', NULL, 1, '2020-09-23 13:50:23', '2020-09-23 13:50:23'),
(783, '5bb55203ed9b14943efb1182409ddac4', 'Shi Yin', 'shiyin1997@hotmail.com', '17cd88bf2ecacd0241a6b3a82fae5cd19ba673b1c2c36b738d51477630e15bef', '8b45eef899de5ba3c69e7c9720d85c948f3914c9', '0164453705', NULL, 1, '2020-09-23 13:50:47', '2020-09-23 13:50:47'),
(784, 'f7f1a34629e0cf85b9f2dcd2e7fb5fbb', 'SAW KIM SUAN', 'sawpeipei79@gmail.com', 'd572f2d1498a32b18b546206fb6c27f5e072d31ff22b2aa4f59f729869ae1a35', 'f583da237f7ae8e22af3bc93f7cf866b2616c665', '0165382766', NULL, 1, '2020-09-23 13:53:55', '2020-09-23 13:53:55'),
(785, 'b238f6fe2e61e5c451065dd9d4478590', 'Lim Wei wei', 'lisalw1978@gmail.com', 'ed90cadb46d559f75106b475333a624d7c17288c3953c9d31fefe10a4b5d4735', '6780994316735a1849a7f89e64371be4cd85da94', '0177782138', NULL, 1, '2020-09-23 13:55:00', '2020-09-23 13:55:00'),
(786, '7786506f029c9add61827e4d947e8c81', 'Ang Beng Sen', 'wilsonabs218@gmail.com', '4979e07c0d5e6d476fef36e6a9b9be478ebcb4703d24de53592b066626040a45', '5a0d33177b148d256e8ec01009e8e874045f9f76', '0125091841', NULL, 1, '2020-09-23 13:56:32', '2020-09-23 13:56:32'),
(787, '3621edaebfea1941bca05f2c8a0444a3', 'soon choo chik', 'soonchoochik', 'd75ee4b91ccdc4ae60a46285d5a30ff0a2b1032f231457303c661ac562c88fd7', '6bd471f6a718b6eb18708f1c8f23d477b45c6baf', '0126905489', NULL, 1, '2020-09-23 14:01:02', '2020-09-23 14:01:02'),
(788, '212bb159c589bf20bbeaebb0b92067bf', 'Foo Chin Giap', 'darrienfoo@gmail.com', 'adbca56a9cab00ee765ac1692036bea1497863f0ca790aabfccb88e80f6fe47d', '8dcf31e5e6eece5ac4816838b4622805aa8a4c15', '0162261013', NULL, 1, '2020-09-23 14:01:03', '2020-09-23 14:01:03'),
(789, '4b0b9f906ead8cc50e871c90952d64a4', 'Cindy', 'helen649431@gmail.com', '2a38ad36e024330aac3d3d03444ae84e7b05d8a049ae8d1460221c4a7cc02183', '1850ccbfbf5ea616883f902d7498fe10247a0db7', '0182184978', NULL, 1, '2020-09-23 14:02:30', '2020-09-23 14:02:30'),
(790, 'ddbe596c4d750fe463c697508a332a36', 'Teow Wei Seng', 'weisengteow@yahoo.co.uk', 'db55f10c403c3d99e388d6ef5ed8dc7c20c1cd52db962171caeb209b83d8872a', '256bf3d6a481b9d9923831c8a2beb8d2d870c9a4', '0164084414', NULL, 1, '2020-09-23 14:02:56', '2020-09-23 14:02:56'),
(791, '6ec9dc007221ff01839313dc1abde8e4', 'Ann Tang', 'anntang77@gmail.com', '9cd2d6b590fb9aca507617e87f840459ecd7c5780677a46602937f205b9ed0c2', 'b550fff4e8fbbd0878f4e4d0c142749f03b5647b', '0124516349', NULL, 1, '2020-09-23 14:08:05', '2020-09-23 14:08:05'),
(792, 'd4aa30b56db498d692d72f2a49b8c770', 'Lau Hui Ni', 'huini0807@gmail.com', 'e77f1ab3acd6e66d6d86f26c99adc6208d2e9a0a96225c70e5815d12ac569c4f', 'eb832462b5627f2194009f92ed9d52ff551236b5', '0175902493', NULL, 1, '2020-09-23 14:09:36', '2020-09-23 14:09:36'),
(793, '5acd690694f70d23d0535d3ce50a2c1f', 'Poo Seng', 'pooseng84@gmail.com', '5a1110e1d33e332cc3ae1cd29be263b28d764e51a76829f84f52c6bf856bad3a', '4e4643244576c96bffd33fcf54a422b193a8ced9', '0106680323', NULL, 1, '2020-09-23 14:10:19', '2020-09-23 14:10:19'),
(794, '4e09d439b0fb181beca700a442254cf5', 'Ooi Chin Ting', 'octchynting91@hotmail.com', '5c146e7d83a08f3fb5df76bc14ece4d71a3e517f548c5946242c0b1060bf3af1', '060c0146c1c11911179e6fdfd2bd73c9cc02b4cf', '0124479812', NULL, 1, '2020-09-23 14:10:23', '2020-09-23 14:10:23'),
(795, '89b761655c8106ed34c0b9650c7836af', 'Ong Poh Im', 'ongpohim911@gmail.com', '62e04062838db48e5b6c709eb73ad1ebe7b3f21370c107509174dd8c86c2ead2', 'ba0089df29a874e50bf4ae1daaa573115f598885', '0105325077', NULL, 1, '2020-09-23 14:13:28', '2020-09-23 14:13:28'),
(796, '2e4b5de7344e2e22ca06523f6756602f', 'CHENG LAI CHIN', 'L1q1ng@outlook.com', '5d4b2e67f1ae138272685cb2237dea41ab555bf3b8d4d5cd96a0fa2bf127d769', '6c5b62659e047694e2868ab6681177e238dbc3f5', '0164782958', NULL, 1, '2020-09-23 14:13:48', '2020-09-23 14:13:48'),
(797, '745abb50c77f3cc2d4f8546095f66a40', 'Tan Chun Shiun', '', '6b9a35263f091458b3775d7017d670ae85f169bf8ba27eee4ad71d62b7041ef9', '1883f027b3a623da04c910a8ed84dc6460aaa205', '0176720283', NULL, 1, '2020-09-23 14:20:07', '2020-09-23 14:20:07'),
(798, '30ef407facec34151c53717d65fbf6c5', 'goh yik choong', 'gohyikchoong@gmail.com', 'a52f43804af7cfa44ab1ab77a73dad57a8a1b4b29aeda369c751b3b995fee699', 'f23f84b42d324a8938cce54320d22698034d5c05', '0174576737', NULL, 1, '2020-09-23 14:21:34', '2020-09-23 14:21:34'),
(799, '16debcd06292ad787f302e8bac8d807a', 'Alexainder Saw', 'alexainder.wintech@gmail.com', '4f80ed34db95d69632ad833474dcd5de1518b1d62477789c73d7661d2e398243', '412f55cb8ff5006f18bc34797359efb752e73769', '0164937823', NULL, 1, '2020-09-23 14:22:36', '2020-09-23 14:22:36'),
(800, 'a01228f7e3a32c44a7ec7eee642ff6d0', 'Tan Liang Sun', 'sun4679@hotmail.com', 'e1fca5a6ef8d5fb04da13cab521d8b7e2a7e93b58ba3b159a30d490b2252e788', '3de42c98447f6bdeff051af3aeb0de7e1ca3e2d9', '0174437218', NULL, 1, '2020-09-23 14:24:41', '2020-09-23 14:24:41'),
(801, '89d7fea15af79012fce6da726f1963a7', 'CHEAH NAM SHIN', 'namshin_0524@hotmail.com', 'da53922d26734436e1dbdd335194fd59a9717f0067810a38737cad97807701e6', '3c0919ed24ff00b1be881809279a6ed7610eb469', '0164572842', NULL, 1, '2020-09-23 14:24:48', '2020-09-23 14:24:48'),
(802, '8f700ec762e6e6ec6cd583ce32e95646', 'Allan', 'allanmq@gmail.com', '5f5b669ea8add45c8e0c7da9c9353396e03852c955a06530b670615be06e437c', '6fcb100160443ea4fe8d1b3a6260a666467e9373', '0195178381', NULL, 1, '2020-09-23 14:25:44', '2020-09-23 14:25:44'),
(803, 'e741867c2a0594573a368a2fe491d357', 'LIM ZHENG CONG', 'zclim94@gmail.com', '8ad923b65d529acb559bc18c118369a1a405edc74c9b350deeb45df99024b923', 'f72f094721f20dec4d97c4745449286ca0470f87', '0167468745', NULL, 1, '2020-09-23 14:29:25', '2020-09-23 14:29:25'),
(804, '299feadaf544cf033c366786de5bf8ca', 'Anna Khor', 'idktuna@outlook.com', 'd5d59677f74fbdf95eaf094a0347b322becbc7a6f84dc5f3ee31b3889f35f3d5', '8b66e413bf50cd1cf68f0795d4d61c755d02c603', '0127185897', NULL, 1, '2020-09-23 14:29:54', '2020-09-23 14:29:54'),
(805, 'b9f61b2ae2d6c103f8cf459462e081b3', 'OOI YIH KHANG', 'huangyikang78@gmail.com', '3780a5d983e1295a1b01ddd66a12f87819b22feae5b2cf5e0220470dbbcbbeaf', '4f8f795e9c77ed5c39e35bf8849934467440f929', '01137125762', NULL, 1, '2020-09-23 14:31:38', '2020-09-23 14:31:38'),
(806, 'c5bcd8d23c72a1008c52ec5b2a8658b1', 'Liew Jane', 'janeliew06@gmail.com', '40b8d1e1444a1a350b7644a978c5db11824df41bd282125565d81dc9408a24a2', '91aff1f1e9aa95e968273c7bf4d565bbe5c847b0', '0167157478', NULL, 1, '2020-09-23 14:32:28', '2020-09-23 14:32:28'),
(807, 'b30d8233ea6d7a9fe0180e2aaa4610aa', 'NG YEOU SOON', 'veronhooi@gmail.com', '7deceb32ad269ae70f6fea25d0d9b98d5d1aca878a0dea3f875c8df8ef971ac5', '892e7afeaefc489febc0d897ded9ad883bd429f3', '01136003634', NULL, 1, '2020-09-23 14:32:42', '2020-09-23 14:32:42'),
(808, '78d1216e97b8647b19e58193e67ddf75', 'Chai Poh Chin', 'diannechai@gmail.com', '0b02ae75c63c0af15417bcf8b93849ae3e2f280258215b5d4c1b683417e24997', 'e82cff6ba2f14122c0d4398fbc2ef7d5c50b3172', '01111942768', NULL, 1, '2020-09-23 14:34:01', '2020-09-23 14:34:01'),
(809, 'ea6e27b43813c30bf2a356dbb830bffa', 'Chuah Peng Seong', 'Chuahpengseong@gmail.com', 'b57ae869c6eb8ee607a2beb8ab451bcedab2bfbd4cbfe223813f305f43b65c60', 'ea7898021ed149e140894b823d7f5e1ae186286b', '0174227088', NULL, 1, '2020-09-23 14:35:14', '2020-09-23 14:35:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_share`
--
ALTER TABLE `live_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lucky_draw`
--
ALTER TABLE `lucky_draw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `sharing`
--
ALTER TABLE `sharing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_share`
--
ALTER TABLE `sub_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `title`
--
ALTER TABLE `title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `userdata`
--
ALTER TABLE `userdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `live_share`
--
ALTER TABLE `live_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `lucky_draw`
--
ALTER TABLE `lucky_draw`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `platform`
--
ALTER TABLE `platform`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sharing`
--
ALTER TABLE `sharing`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `sub_share`
--
ALTER TABLE `sub_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `title`
--
ALTER TABLE `title`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `userdata`
--
ALTER TABLE `userdata`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=810;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
