<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Userdata.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUserdata($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/ticketView.php" />
<link rel="canonical" href="https://gmvec.com/ticketView.php" />
<meta property="og:title" content="Registration Completed | 光明線上產業展  Guang Ming Properties E-Fair" />
<title>Registration Completed | 光明線上產業展  Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展  Guang Ming Properties E-Fair" />
<meta name="description" content="光明線上產業展  Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">

<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 gold-line"></div>
<div class="width100 same-padding overflow min-height-footer-only text-center">
	<h1 class="title-h1 text-center landing-title-h1 black-text">谢谢您的参与！<br>Thanks for the Pre-Registration.</h1>	
	<div class="ticket-div">
		<div class="ticket-left-detail">
			<p class="three-div-p">Name 姓名:<br><b><?php echo $userData->getUsername();?></b></p> 
			<p class="three-div-p">Contact 联络号码:<br><b><?php echo $userData->getPhoneNo();?></b></p> 
			<p class="three-div-p ticket-margin-bottom0">Email 电邮:<br><b class="text-break"><?php echo $userData->getEmail();?></b></p>
		</div>
		<div class="ticket-right">
			<img src="img/ticket.png" class="ticket-png web-view">
			<img src="img/ticket2.png" class="ticket-png mob-view">
			<img src="img/ticket4.png" class="ticket-png mob-view2">
		</div>
	</div>
	<div class="clear"></div>
	<div class="width100 text-center">
				<!-- <a href="index.php"><div class="margin-auto clean-button clean register-button pink-button" >Back 返回</div></a> -->
				<a href="logout.php"><div class="margin-auto clean-button clean register-button pink-button" >Back 返回</div></a>
				
            </div>

<div class="clear"></div>
<div class="width100 big-co-spacing">
	<div class="index-left-or">
		<p class="index-title-size text-left-ow">Co-organizers</p>
		<a href="https://guangming.com.my/" target="_blank"><img src="img/guangmingdaily2.png" class="index-logo-height index-guang opacity-hover" alt="Guang Ming Daily 光明日报" title="Guang Ming Daily 光明日报"></a>
		<a href="https://www.zeon.com.my/" target="_blank"><img src="img/zeon-properties2.png" class="index-logo-height index-guang opacity-hover" alt="Zeon Properties 益安房地产集团" title="Zeon Properties 益安房地产集团"></a>
	</div>	
	<div class="index-right-or ow-text-left">
		<p class="index-title-size text-left-ow">Participants</p>
		<a href="https://aspen.com.my/" target="_blank">
			<img src="img/1aspen.png" class="index-logo-height2 index-mah opacity-hover" alt="Aspen Group" title="Aspen Group">	
		</a>
		<a href="https://jesseltonvillas.com/" target="_blank">
			<img src="img/2berjaya.png" class="index-logo-height2 index-cod opacity-hover" alt="Berjaya Land" title="Berjaya Land">
		</a>		
		<a href="./ewein-city-of-dreams.php" target="_blank"><img src="img/3cod.png" class="index-logo-height2 index-tah opacity-hover" alt="City of Dreams 梦想之城" title="City of Dreams 梦想之城"></a>
		<a href="http://www.hunzagroup.com/" target="_blank">
			<img src="img/4hunza.png" class="index-logo-height2 index-berjaya opacity-hover" alt="Hunza Properties 汇华产业集团" title="Hunza Properties 汇华产业集团">
		</a>	
		<a href="https://www.mahsing.com.my/" target="_blank"><img src="img/5mahsing.png" class="index-logo-height2 index-mah opacity-hover" alt="Mah Sing Group 馬星集團" title="Mah Sing Group 馬星集團"></a>		
		<a href="http://www.tahwah.com/" target="_blank">
		<img src="img/6tahwah.png" class="index-logo-height2 index-tah opacity-hover ow-bottom3-logo" alt="Tah Wah Group 大華集團" title="Tah Wah Group 大華集團"></a>	<a href="https://www.jadigroup.com/" target="_blank">
			<img src="img/7tamanjadi.png" class="index-logo-height2 index-tah opacity-hover" alt="Taman Jadi 嘉利发展有限公司" title="Taman Jadi 嘉利发展有限公司">
		</a>				
	</div>
</div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>