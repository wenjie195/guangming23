<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminReviewUserLogoTwo.php" />
<meta property="og:title" content="User Logo | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>User Logo | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminReviewUserLogoTwo.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">User Logo</h2> 
    
    <div class="clear"></div>

    <?php
    if(isset($_POST['pic_link_uid']))
    $conn = connDB();
    $userImage = getImage($conn,"WHERE uid = ? ", array("uid") ,array($_POST['pic_link_uid']),"s");
    $uid = $userImage[0]->getUid();
    $picture = $userImage[0]->getImageTwo();
    if($userImage)
    {
    ?>
        <div class="left-video-container-div overflow">
            <img src="uploads/<?php echo $picture?>" class="first-logo project-logo opacity-hover" alt="Update Profile Picture" title="Update Profile Picture">
            <div class="clear"></div>
            <form method="POST" action="adminUpdateProjectLogoTwo.php">
                <button class="clean-button clean login-btn pink-button" type="submit" name="picture_uid" value="<?php echo $uid;?>">Click to Update</button>
            </form>
        </div>
    <?php
    }
    ?>

    <div class="clear"></div>
       
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>