<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

// Program to display URL of current page.
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
$link = "https";
else
$link = "http";

// Here append the common URL characters.
$link .= "://";

// Append the host(domain name, ip) to the URL.
$link .= $_SERVER['HTTP_HOST'];

// Append the requested resource location to the URL
$link .= $_SERVER['REQUEST_URI'];

if(isset($_GET['id']))
{
    $currentLink = $_GET['id'];
}
else
{
    $currentLink = "";
}


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminViewUserLogo.php" />
<meta property="og:title" content="Project Logo and Link | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Project Logo and Link | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminViewUserLogo.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Project Logo and Link</h2> 

	<div class="project-logo-big-div overflow">

        <?php
        // if(isset($_POST['user_uid']))
        if(isset($currentLink))
        $conn = connDB();
        // $userImage = getImage($conn,"WHERE user_uid = ? AND type = '1' ", array("user_uid") ,array($_POST['user_uid']),"s");
        $userImage = getImage($conn,"WHERE user_uid = ? AND type = '1' ", array("user_uid") ,array($currentLink),"s");
        if($userImage)
        {
            for($cnt = 0;$cnt < count($userImage) ;$cnt++)
            {
            ?>  

                <div class="project-logo-repeat-div">
                    <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                        <input class="aidex-input clean"  type="hidden" value="asd" id="user_uid" name="user_uid" readonly>
                        <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                        <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit">
                            <img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b">
                        </button>
                    </form>    
                    <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                        <input class="aidex-input clean"  type="hidden" value="asd" id="user_uid" name="user_uid" readonly>
                        <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                        <p class="text-overflow link-preview">
                            <button class="clean update-btn hover1" name="submit">
                                <img src="img/edit.png" class="edit-btn-img hover1a">
                                <img src="img/edit2.png" class="edit-btn-img hover1b"> <?php echo $userImage[$cnt]->getLinkOne();?>
                            </button>
                        </p>     	
                    </form>

                    <!-- <button class="clean-button clean login-btn pink-button" name="">Delete</button>        -->
                    
                    <form method="POST" action="utilities/deleteImageFunction.php" class="hover1">
                        <button class="clean-button clean login-btn pink-button" type="submit" value="<?php echo $userImage[$cnt]->getUid();?>" name="pic_uid" name="pic_uid">
                            Delete
                        </button>      
                    </form>

                </div> 

            <?php
            }
            ?>
        <?php
        }
        ?>

    </div>    

    <div class="clear"></div>

    <?php
    // if(isset($_POST['user_uid']))
    if(isset($currentLink))
    $conn = connDB();
    // $userImage = getImage($conn,"WHERE user_uid = ? AND type = '1' ", array("user_uid") ,array($_POST['user_uid']),"s");
    $userImage = getImage($conn,"WHERE user_uid = ? AND type = '1' ", array("user_uid") ,array($currentLink),"s");
    if($userImage)
    {
        $totalImage = count($userImage);
    }
    else
    {   $totalImage = 0;   }
    ?>

    <?php
    // if($totalImage < 4)
    if($totalImage < 8)
    {   
    ?>
        <div class="width100 overflow text-center">
            <form method="POST" action="adminAddUserProjectLogo.php" class="hover1">
                <button class="clean-button clean login-btn pink-button ow-mid-btn-width" type="submit" name="user_uid" value="<?php echo $currentLink;?>">
                    Add Project Logo
                </button>
            </form>
        </div>
    <?php
    }
    else{}
    ?>

    <div class="clear"></div>    

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>