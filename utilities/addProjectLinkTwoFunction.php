<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Image.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

$picUid = $_SESSION['pic_link'];
$userUid = $_SESSION['user_uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $addLink = rewrite($_POST["register_link"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $picUid."<br>";
    // echo $userUid."<br>";

    $imageDetails = getImage($conn," uid = ?   ",array("uid"),array($picUid),"s");   
    
    if(!$imageDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($addLink)
        {
            array_push($tableName,"link_two");
            array_push($tableValue,$addLink);
            $stringType .=  "s";
        }
        
        array_push($tableValue,$picUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"image"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            header('Location: ../adminDashboard.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }
}
else 
{
    header('Location: ../index.php');
}
?>
