<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Winner.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $name = rewrite($_POST["update_name"]);
    $contact = rewrite($_POST["update_contact"]);
    $prize = rewrite($_POST["update_prize"]);
    $date = rewrite($_POST["update_date"]);
    $uid = rewrite($_POST["winner_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $userDetails = getWinner($conn," uid = ?   ",array("uid"),array($uid),"s");   

    if(!$userDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($contact)
        {
            array_push($tableName,"contact");
            array_push($tableValue,$contact);
            $stringType .=  "s";
        }
        if($prize)
        {
            array_push($tableName,"prize");
            array_push($tableValue,$prize);
            $stringType .=  "s";
        }
        if($date)
        {
            array_push($tableName,"datetime");
            array_push($tableValue,$date);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"winner"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "UPDATED !!";
            header('Location: ../adminViewWinner.php');
        }
        else
        {
            // echo "FAIL !!";
            echo "<script>alert('Fail to update');window.location='../adminViewWinner.php'</script>";
        }
    }
    else
    {
        // echo "GG !!";
        echo "<script>alert('ERROR');window.location='../adminAddWinner.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}
?>
