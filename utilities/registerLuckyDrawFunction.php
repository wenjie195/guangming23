<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Luckydraw.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$name,$email,$contact,$state,$location,$userType)
{
     if(insertDynamicData($conn,"lucky_draw",array("uid","name","email","contact","state","location","user_type"),
          array($uid,$name,$email,$contact,$state,$location,$userType),"ssssssi") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $name = rewrite($_POST['register_name']);
     $email = rewrite($_POST['register_email']);
     $contact = rewrite($_POST['register_contact']);
     $state = rewrite($_POST['register_state']);
     $location = rewrite($_POST['register_location']);
     $userType = "1";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $email ."<br>";

     $emailRows = getLuckydraw($conn," WHERE email = ? ",array("email"),array($email),"s");
     $existingEmail = $emailRows[0];

     $contactRows = getLuckydraw($conn," WHERE contact = ? ",array("contact"),array($contact),"s");
     $existingContact = $contactRows[0];

     if (!$existingEmail)
     {
          if (!$existingContact)
          {
               if(registerNewUser($conn,$uid,$name,$email,$contact,$state,$location,$userType))
               {
                    header('Location: ../index2.php');
               }
               else
               {
                    echo "<script>alert('Fail to register.');window.location='../registration2527.php'</script>";   
               } 
          }
          else
          {  
               echo "<script>alert('Register Failed. This contact is already used.');window.location='../registration2527.php'</script>";   
          }  
     }
     else
     {
          echo "<script>alert('Register Failed. This email is already used.');window.location='../registration2527.php'</script>"; 
     }
}
else 
{
     header('Location: ../index.php');
}
?>