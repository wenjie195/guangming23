<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Sharing.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewSharing($conn,$uid,$user,$title,$host,$platform,$link,$remark,$file,$userUid,$status,$type,$titleTwo,$whatsapp,$phone,$offlineContact)
{
     if(insertDynamicData($conn,"sharing",array("uid","username","title","host","platform","link","remark","file","user_uid","status","type","title_two","whatsapp","phone","offline"),
          array($uid,$user,$title,$host,$platform,$link,$remark,$file,$userUid,$status,$type,$titleTwo,$whatsapp,$phone,$offlineContact),"ssssssssssissss") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $user = rewrite($_POST['register_user']);

     $getUserDetails = getUser($conn," WHERE username = ? ",array("username"),array($user),"s");
     $userUid = $getUserDetails[0]->getUid();

     $title = rewrite($_POST['update_title']);
     $host = rewrite($_POST['update_host']);
     $platform = rewrite($_POST['register_platform']);
     $link = rewrite($_POST['register_link']);
     $remark = rewrite($_POST['register_remark']);

     $file = $timestamp.$_FILES['file_one']['name'];
     $target_dir = "../userProfilePic/";
     $target_file = $target_dir . basename($_FILES["file_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif","pdf");
     if( in_array($imageFileType,$extensions_arr) )
     {
         move_uploaded_file($_FILES['file_one']['tmp_name'],$target_dir.$file);
     }
     
     $status = "Pending";
     $type = "4";

     $titleTwo = rewrite($_POST['register_title_two']);
     $whatsapp = rewrite($_POST['register_whatsapp']);
     $phone = rewrite($_POST['register_phone']);
     $offlineContact = rewrite($_POST['register_offline']);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $user."<br>";
     // echo $title."<br>";

     if(registerNewSharing($conn,$uid,$user,$title,$host,$platform,$link,$remark,$file,$userUid,$status,$type,$titleTwo,$whatsapp,$phone,$offlineContact))
     {  
          // echo "<script>alert('Register Success !');window.location='../adminDashboard.php'</script>";  
          if(isset($_SESSION['url'])) 
          {
              $url = $_SESSION['url']; 
              header("location: $url");
          }
          else 
          {
              // header("location: $url");
              header('Location: ../adminDashboard.php');
          }
     }
     else
     {
          echo "<script>alert('Fail to add new zoom');window.location='../adminAddUserProjectVideo.php'</script>";
     }   
}
else 
{
     header('Location: ../index.php');
}
?>