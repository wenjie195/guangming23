<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Counter.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $counterValue = rewrite($_POST["counter_value"]);
    $counterId = rewrite($_POST["counter_id"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    $counterDetails = getCounter($conn," id = ? ",array("id"),array($counterId),"i");   

    if(!$counterDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($counterValue)
        {
            array_push($tableName,"amount");
            array_push($tableValue,$counterValue);
            $stringType .=  "s";
        }

        array_push($tableValue,$counterId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"counter"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            echo "<script>alert('counter value updated !');window.location='../adminViewCounter.php'</script>";  
        }
        else
        {
            echo "<script>alert('fail');window.location='../adminViewCounter.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!!');window.location='../adminViewCounter.php'</script>";  
    }
}
else 
{
    header('Location: ../index.php');
}
?>