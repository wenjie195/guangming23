<?php
// require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Sharing.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $updateLink = rewrite($_POST["update_link"]);
    $updateCode = rewrite($_POST["update_code"]);

    $sharingUid = rewrite($_POST["sharing_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $liveIdDetails = getSharing($conn," uid = ?   ",array("uid"),array($sharingUid),"s");   
    
    if(!$liveIdDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($updateLink)
        {
            array_push($tableName,"link");
            array_push($tableValue,$updateLink);
            $stringType .=  "s";
        }
        if($updateCode)
        {
            array_push($tableName,"offline");
            array_push($tableValue,$updateCode);
            $stringType .=  "s";
        }

        array_push($tableValue,$sharingUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"sharing"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            header('Location: ../agentDashboard.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }
}
else 
{
    header('Location: ../index.php');
}
?>