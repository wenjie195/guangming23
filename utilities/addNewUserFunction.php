<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone,$userType)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","user_type"),
          array($uid,$username,$email,$finalPassword,$salt,$phone,$userType),"ssssssi") === null)
     {
          echo "GG !!";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $username = rewrite($_POST['register_fullname']);
     $phone = rewrite($_POST['register_phone']);
     $email = rewrite($_POST['register_email_user']);
     $userType = rewrite($_POST['register_account_type']);
     
     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";
     // echo $register_fullname."<br>";
     // echo $register_phone."<br>";
     // echo $register_email_user."<br>";
     // echo $register_address."<br>";
     // echo $register_user_type."<br>";
     // echo $register_password."<br>";
     // echo $register_retype_password."<br>";
     // echo $password."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";

          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    // $fullnameRows = getUser($conn," WHERE full_name = ? ",array("full_name"),array($_POST['register_fullname']),"s");
                    // $fullnameDetails = $fullnameRows[0];

                    // $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                    // $userEmailDetails = $userEmailRows[0];

                    // $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
                    // $userPhoneDetails = $userPhoneRows[0];

                    // if (!$fullnameDetails && !$userEmailDetails && !$userPhoneDetails)
                    // {
                         if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone,$userType))
                         {
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../profile.php?type=1');
                              echo "<script>alert('Register Success !');window.location='../adminDashboard.php'</script>";    
                         }
                         else 
                         {
                              echo "<script>alert('error during register new developer');window.location='../addNewUser.php'</script>";
                         }
                    // }
                    // else
                    // {
                    //      echo "<script>alert('error registering new account.The account already exist');window.location='../addNewUser.php'</script>";
                    // }
               }
               else 
               {
                    echo "<script>alert('password must be more than 6');window.location='../addNewUser.php'</script>";
               }
          }
          else 
          {
               echo "<script>alert('password and retype password not the same');window.location='../addNewUser.php'</script>";
          }      
}
else 
{
     header('Location: ../addReferee.php');
}

?>