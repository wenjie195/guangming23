<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Sharing.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];
$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $updateTitleOne = rewrite($_POST["update_title_one"]);

    $oriFileOne = rewrite($_POST["ori_file_one"]);
    $newFileOne = $_FILES['file_one']['name'];
    if($newFileOne == '')
    {
        $file = $oriFileOne;
    }
    else
    {
        $file = $oriFileOne;
        $file = $timestamp.$_FILES['file_one']['name'];
        $target_dir = "../userProfilePic/";
        $target_file = $target_dir . basename($_FILES["file_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif","pdf");
        if( in_array($imageFileType,$extensions_arr) )
        {
            move_uploaded_file($_FILES['file_one']['tmp_name'],$target_dir.$file);
        }
    }

    // $updateHost = rewrite($_POST["update_host"]);
    $updateLink = rewrite($_POST["update_link"]);
    $updateRemark = rewrite($_POST["update_remark"]);

    $updateTitleTwo = rewrite($_POST["update_title_two"]);
    $updateWhatsapp = rewrite($_POST["update_whatsapp"]);
    $updatePhone = rewrite($_POST["update_phone"]);
    $updateOffline = rewrite($_POST["update_offline"]);

    $subUid = rewrite($_POST["sub_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $subUid."<br>";
    // echo $file."<br>";

    $subIdDetails = getSharing($conn," uid = ?   ",array("uid"),array($subUid),"s");   
    
    // if(!$existingSub)
    // if(!$subId)
    // {

    if(!$subIdDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($updateTitleOne)
        {
            array_push($tableName,"title");
            array_push($tableValue,$updateTitleOne);
            $stringType .=  "s";
        }
        // if($updateHost)
        // {
        //     array_push($tableName,"host");
        //     array_push($tableValue,$updateHost);
        //     $stringType .=  "s";
        // }
        if($updateLink)
        {
            array_push($tableName,"link");
            array_push($tableValue,$updateLink);
            $stringType .=  "s";
        }
        if($updateRemark)
        {
            array_push($tableName,"remark");
            array_push($tableValue,$updateRemark);
            $stringType .=  "s";
        }
        if($file)
        {
            array_push($tableName,"file");
            array_push($tableValue,$file);
            $stringType .=  "s";
        }

        if($updateTitleTwo)
        {
            array_push($tableName,"title_two");
            array_push($tableValue,$updateTitleTwo);
            $stringType .=  "s";
        }
        if($updateWhatsapp)
        {
            array_push($tableName,"whatsapp");
            array_push($tableValue,$updateWhatsapp);
            $stringType .=  "s";
        }
        if($updatePhone)
        {
            array_push($tableName,"phone");
            array_push($tableValue,$updatePhone);
            $stringType .=  "s";
        }
        if($updateOffline)
        {
            array_push($tableName,"offline");
            array_push($tableValue,$updateOffline);
            $stringType .=  "s";
        }

        array_push($tableValue,$subUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"sharing"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "UPDATED !!";
            // header('Location: ../adminDashboard.php');
            if(isset($_SESSION['url'])) 
            {
                $url = $_SESSION['url']; 
                header("location: $url");
            }
            else 
            {
                // header("location: $url");
                header('Location: ../adminDashboard.php');
            }
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }
}
else 
{
    header('Location: ../index.php');
}
?>