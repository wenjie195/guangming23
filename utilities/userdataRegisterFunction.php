<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Registration.php';
require_once dirname(__FILE__) . '/../classes/Userdata.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"userdata",array("uid","username","phone_no","email","user_type","password","salt"),
          array($uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt),"ssssiss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function registerNewUserWithoutEmail($conn,$uid,$username,$phoneNo,$userType,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"userdata",array("uid","username","phone_no","user_type","password","salt"),
          array($uid,$username,$phoneNo,$userType,$finalPassword,$salt),"sssiss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_name']);
     $phoneNo = rewrite($_POST['register_phone']);
     // $email = rewrite($_POST['register_email']);

     $checkEmailValid = rewrite($_POST['register_email']);

     $userType = "1";

     $register_password = "123321";
     $register_password_validation = strlen($register_password);
     $register_retype_password = "123321";
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $phoneNo ."<br>";

     if ($checkEmailValid != "")
     {

          $email = rewrite($_POST['register_email']);

          $phoneRows = getUserdata($conn," WHERE phone_no = ? ",array("phone_no"),array($phoneNo),"s");
          $existingPhone = $phoneRows[0];
     
          $emailRows = getUserdata($conn," WHERE email = ? ",array("email"),array($email),"s");
          $existingEmail = $emailRows[0];
     
          if (!$existingPhone)
          {
               if (!$existingEmail)
               {
                    if(registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt))
                    {
                         $_SESSION['uid'] = $uid;
                         $_SESSION['user_type'] = 1;
                         header('Location: ../ticketView.php');
                    }
                    else
                    {
                         echo "<script>alert('Fail to pre-register.');window.location='../index.php'</script>";   
                    } 
               }
               else
               {  
                    echo "<script>alert('This email is already registered.');window.location='../index.php'</script>";   
               }  
          }
          else
          {
               echo "<script>alert('This phone number  is already registered.');window.location='../index.php'</script>"; 
          }
     }
     else
     {
          $phoneRows = getUserdata($conn," WHERE phone_no = ? ",array("phone_no"),array($phoneNo),"s");
          $existingPhone = $phoneRows[0];
          
          if (!$existingPhone)
          {
               if(registerNewUserWithoutEmail($conn,$uid,$username,$phoneNo,$userType,$finalPassword,$salt))
               {
                    $_SESSION['uid'] = $uid;
                    $_SESSION['user_type'] = 1;
                    header('Location: ../ticketView.php');
               }
               else
               {
                    echo "<script>alert('Fail to pre-register.');window.location='../index.php'</script>";   
               } 
          }
          else
          {
               echo "<script>alert('This phone number  is already registered.');window.location='../index.php'</script>"; 
          }
     }

     // $phoneRows = getUserdata($conn," WHERE phone_no = ? ",array("phone_no"),array($phoneNo),"s");
     // $existingPhone = $phoneRows[0];

     // $emailRows = getUserdata($conn," WHERE email = ? ",array("email"),array($email),"s");
     // $existingEmail = $emailRows[0];

     // if (!$existingPhone)
     // {
     //      if (!$existingEmail)
     //      {
     //           if(registerNewUser($conn,$uid,$username,$phoneNo,$email,$userType,$finalPassword,$salt))
     //           {
     //                $_SESSION['uid'] = $uid;
     //                $_SESSION['user_type'] = 1;
     //                header('Location: ../ticketView.php');
     //           }
     //           else
     //           {
     //                echo "<script>alert('Fail to pre-register.');window.location='../index.php'</script>";   
     //           } 
     //      }
     //      else
     //      {  
     //           echo "<script>alert('This email is already registered.');window.location='../index.php'</script>";   
     //      }  
     // }
     // else
     // {
     //      echo "<script>alert('This phone number  is already registered.');window.location='../index.php'</script>"; 
     // }  
}
else 
{
     header('Location: ../index.php');
}
?>