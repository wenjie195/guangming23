<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

function sentMessage($conn,$uid,$username,$message_uid,$receiveSMS)
{
     if(insertDynamicData($conn,"message",array("uid","username","message_uid","receive_message"),
     array($uid,$username,$message_uid,$receiveSMS),"ssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = md5(uniqid());

    $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $username = $userRows[0]->getUsername();

    $receiveSMS = rewrite($_POST["message_details"]);

    //   FOR DEBUGGING
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $username."<br>";
    // echo $receiveSMS."<br>";

    if(sentMessage($conn,$uid,$username,$message_uid,$receiveSMS))
    {
        header('Location: ../liveChat.php?type=1');
    }
    else
    {
        header('Location: ../liveChat.php?type=2');
    }
}
else
{
     header('Location: ../index.php');
}
?>
