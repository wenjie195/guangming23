<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/agentLogin.php" />
<meta property="og:title" content="Agent Login | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Agent Login  | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/agentLogin.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<div class="width100 gold-line"></div>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">
	<div class="width100 text-center margin-top20">
		<img src="img/gmvec-logo.jpg" class="gmvec-logo" alt="GMVEC" title="GMVEC">
	</div>
	<div class="middle-width">
		
		<h1 class="title-h1 text-center landing-title-h1 black-text">光明線上產業展<br> Guang Ming Properties E-Fair</h1>
		<h2 class="h1-title text-center">Agent Login</h2> 
		<div class="clear"></div>
		<form action="utilities/loginFunction.php" method="POST" class="margin-top30">
				<div class="per-input width100">
					<p class="input-top-text">Username</p>
					<div class="password-input-div">
						<input class="aidex-input clean password-input"  type="text" placeholder="Username" id="username" name="username" required> 
					</div> 
				</div>  
				<div class="per-input width100">
					<p class="input-top-text">Password</p>
					<div class="password-input-div">
						<input class="aidex-input clean password-input"  type="password" placeholder="Password" id="password" name="password" required>
						<img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
					</div>  
				</div>  

			<div class="clear"></div>

				<div class="width100 overflow text-center">     
					<button class="clean-button clean login-btn pink-button"   name="login">Login</button>
				</div>


		</form>
	</div>
<div class="clear"></div>
<div class="width100 overflow co-spacing">
	<div class="index-left-or">
		<p class="index-title-size"><b>Co-organizers</b></p>
		<a href="https://guangming.com.my/" target="_blank"><img src="img/guangmingdaily2.png" class="index-logo-height index-guang opacity-hover" alt="Guang Ming Daily 光明日报" title="Guang Ming Daily 光明日报"></a>
		<a href="https://www.zeon.com.my/" target="_blank"><img src="img/zeon-properties2.png" class="index-logo-height index-guang opacity-hover" alt="Zeon Properties 益安房地产集团" title="Zeon Properties 益安房地产集团"></a>
	</div>	
	<div class="index-right-or">
		<p class="index-title-size"><b>Participants</b></p>
		<a href="https://aspen.com.my/" target="_blank">
			<img src="img/1aspen.png" class="index-logo-height2 index-mah opacity-hover" alt="Aspen Group" title="Aspen Group">	
		</a>
		<a href="https://jesseltonvillas.com/" target="_blank">
			<img src="img/2berjaya.png" class="index-logo-height2 index-cod opacity-hover" alt="Berjaya Land" title="Berjaya Land">
		</a>		
		<a href="./ewein-city-of-dreams.php" target="_blank"><img src="img/3cod.png" class="index-logo-height2 index-tah opacity-hover" alt="City of Dreams 梦想之城" title="City of Dreams 梦想之城"></a>
		<a href="http://www.hunzagroup.com/" target="_blank">
			<img src="img/4hunza.png" class="index-logo-height2 index-berjaya opacity-hover" alt="Hunza Properties 汇华产业集团" title="Hunza Properties 汇华产业集团">
		</a>	
		<a href="./mahsing.php" target="_blank"><img src="img/5mahsing.png" class="index-logo-height2 index-mah opacity-hover" alt="Mah Sing Group 馬星集團" title="Mah Sing Group 馬星集團"></a>		
		<a href="http://www.tahwah.com/" target="_blank">
		<img src="img/6tahwah.png" class="index-logo-height2 index-tah opacity-hover ow-bottom3-logo" alt="Tah Wah Group 大華集團" title="Tah Wah Group 大華集團"></a>	<a href="https://www.jadigroup.com/" target="_blank">
			<img src="img/7tamanjadi.png" class="index-logo-height2 index-tah opacity-hover" alt="Taman Jadi 嘉利发展有限公司" title="Taman Jadi 嘉利发展有限公司">
		</a>	
		
		
		
		<!--
		<a href="./mahsing.php" target="_blank"><img src="img/5mahsing.png" class="index-logo-height2 index-mah opacity-hover" alt="Mah Sing Group 馬星集團" title="Mah Sing Group 馬星集團"></a>
		<a href="./ewein-city-of-dreams.php" target="_blank"><img src="img/3cod.png" class="index-logo-height2 index-mah opacity-hover" alt="City of Dreams 梦想之城" title="City of Dreams 梦想之城"></a>	
		<a href="http://www.tahwah.com/" target="_blank">
		<img src="img/6tahwah.png" class="index-logo-height2 index-tah opacity-hover" alt="Tah Wah Group 大華集團" title="Tah Wah Group 大華集團"></a>
		<a href="https://jesseltonvillas.com/" target="_blank">
			<img src="img/2berjaya.png" class="index-logo-height2 index-berjaya opacity-hover" alt="Berjaya Land" title="Berjaya Land">
		</a>
		<a href="https://www.jadigroup.com/" target="_blank">
			<img src="img/7tamanjadi.png" class="index-logo-height2 index-mah opacity-hover" alt="Taman Jadi 嘉利发展有限公司" title="Taman Jadi 嘉利发展有限公司">
		</a>
		<a href="http://www.hunzagroup.com/" target="_blank">
			<img src="img/4hunza.png" class="index-logo-height2 index-hunza opacity-hover" alt="Hunza Properties 汇华产业集团" title="Hunza Properties 汇华产业集团">
		</a>
		<a href="https://aspen.com.my/" target="_blank">
			<img src="img/1aspen.png" class="index-logo-height2 index-tah opacity-hover" alt="Aspen Group" title="Aspen Group">	
		</a>-->	
	</div>
</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>