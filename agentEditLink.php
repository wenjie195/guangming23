<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
// require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/agentEditLink.php" />
<meta property="og:title" content="Agent Edit Link | 光明線上產業展 Guang Ming Properties E-Fair" />
<title>Agent Edit Link  | 光明線上產業展 Guang Ming Properties E-Fair</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="description" content="光明線上產業展 Guang Ming Properties E-Fair - 我國7 大房屋發展商聯合榮譽參展，提供全系列房屋款型任您彈指之間挑選，豪華公寓、房屋、別墅。" />
<meta name="keywords" content="光明線上產業展,Guang Ming Properties E-Fair, Guang Ming Virtual Property Fair, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live,Zeon Properties, 益安房地产集团,Mah Sing Group, 馬星集團,City of Dreams, 梦想之城,Tah Wah Group, 大華集團,Berjaya Land,Taman Jadi, 嘉利发展有限公司,Hunza Properties, 汇华产业集团,Aspen Group, etc">
<link rel="canonical" href="https://gmvec.com/agentEditLink.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'agentHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Zoom Details</h2>
        <div class="clear"></div>

        <!-- <form method="POST" action="utilities/editLiveFunction.php"> -->
        <form method="POST" action="utilities/editGoogleMeetFunction.php">

            <?php
            if(isset($_POST['subdata_uid']))
            {
                $conn = connDB();
                $liveDetails = getSharing($conn,"WHERE uid = ? ", array("uid") ,array($_POST['subdata_uid']),"s");
            ?>
                <div class="dual-input">
                    <p class="input-top-text">Zoom Meeting Link</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $liveDetails[0]->getLink();?>" name="update_link" id="update_link" required>       
                </div>
                
                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Zoom Password</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $liveDetails[0]->getOffline();?>" name="update_code" id="update_code" required>       
                </div>

                <input type="hidden" value="<?php echo $liveDetails[0]->getUid();?>" name="sharing_uid" id="sharing_uid" required> 

            <?php
            }
            ?>
    	
            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>


<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>