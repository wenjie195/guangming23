<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$mainAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '2' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/adminAddAnnouncement.php" />
<meta property="og:title" content="Add Announcement | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Add Announcement | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/adminAddAnnouncement.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
<div class="mid-width">
    <?php
    if($mainAnnoucement)
    {   
        $totalMainAnnoucement = count($mainAnnoucement);
    }
    else
    {   $totalMainAnnoucement = 0;   }
    ?>

    <?php
    if($totalMainAnnoucement < 1)
    {   
    ?>
        <!-- <h2 class="h1-title">Add Header Announcement</h2>  -->
        <h2 class="h1-title">Add Moving Announcement</h2> 

        <div class="clear"></div>

        <form action="utilities/adminAddAnnoucementFunction.php" method="POST">
            <div class="width100">
                <p class="input-top-text">Announcement Content</p>
                <textarea class="aidex-input clean" type="text" placeholder="Announcement Content" id="content" name="content" required></textarea>        
            </div> 

            <input type="hidden" value="2" name="announcement_type" id="announcement_type" readonly> 

            <div class="clear"></div>

            <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
        </form>

        <div class="clear"></div>
    <?php
    }
    else{}
    ?>

    <h2 class="h1-title margin-top50">Add Side Announcement</h2> 
    
    <div class="clear"></div>
    
    <form action="utilities/adminAddAnnoucementFunction.php" method="POST">
        <div class="width100">
            <p class="input-top-text">Announcement Content</p>
            <textarea class="aidex-input clean" type="text" placeholder="Announcement Content" id="content" name="content" required></textarea>        
        </div> 

        <input type="hidden" value="1" name="announcement_type" id="announcement_type" readonly> 

        <div class="clear"></div>

        <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
    </form>
 </div>      
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>