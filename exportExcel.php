<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Userdata.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

include("export_data.php");
include 'selectFilecss.php';

$conn = connDB();

// $conn->close();
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/exportExcel.php" />
<meta property="og:title" content="Download Registered Data | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Download Registered Data | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/exportExcel.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

	<h1>Export Data</h1>
  <div class="outer-container">

  <?php 
  // $query = "SELECT count(*) AS total FROM `userdata`";

  $query = "SELECT count(*) AS total FROM `lucky_draw`";

  $result = mysqli_query($conn, $query);
  while ($row = mysqli_fetch_array($result))
  {
  $row['total'];
  ?>
    <h2>Total Registered User : <?php echo $row['total'] ?></h2>
  <?php
  }
  ?>

    <div class="buttons">
      <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn-hover color-10">Download Data</button>
        <a href="adminViewRegistration.php"><button type="button" class="btn-hover color-8">Cancel / Return</button></a>
      </form>
    </div>

  </div>
  
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>